/* eslint-disable func-names */
/* eslint-disable operator-linebreak */
/* eslint-disable no-console */
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Enzyme, { mount, render, shallow } from 'enzyme';
import { createSerializer } from 'enzyme-to-json';
import React from 'react';

// Set the default serializer for Jest to be the from enzyme-to-json
// This produces an easier to read (for humans) serialized format.
expect.addSnapshotSerializer(createSerializer({ mode: 'deep' }));

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() });

// Define globals to cut down on imports in test files
global.React = React;
global.shallow = shallow;
global.render = render;
global.mount = mount;

// Fail tests on any warning
/* console.error = message => {
  throw new Error(message);
}; */

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener() {},
      removeListener() {},
    };
  };
