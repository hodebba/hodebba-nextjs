import { RouterContext } from 'next/dist/next-server/lib/router-context';
import Router from 'next/router';
import PropTypes from 'prop-types';
import { useState } from 'react';

function RouterMock({ children, path }) {
  const [pathname, setPathname] = useState(path);

  const mockRouter = {
    pathname,
    prefetch: () => {},
    push: async newPathname => {
      setPathname(newPathname);
    },
  };

  Router.router = mockRouter;

  return <RouterContext.Provider value={mockRouter}>{children}</RouterContext.Provider>;
}

RouterMock.propTypes = {
  children: PropTypes.node.isRequired,
  path: PropTypes.string.isRequired,
};

export default RouterMock;
