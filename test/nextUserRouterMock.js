/* eslint-disable object-curly-newline */
// Mocks useRouter
const useRouter = jest.spyOn(require('next/router'), 'useRouter');

/**
 * nextUserRouterMock
 * Mocks the useRouter React hook from Next.js on a test-case by test-case basis
 */
export default function nextUserRouterMock({ route, pathname, query, asPath }) {
  useRouter.mockImplementationOnce(() => ({
    route,
    pathname,
    query,
    asPath,
  }));
}
