# Hodebba

This is a starter template for [Learn Next.js](https://nextjs.org/learn).

## Commands

### Runs the app in development mode

....
yarn dev
....

### Builds the app for production

....
yarn build
....

### Runs the app in production mode

....
yarn start
....

### Runs tests

....
yarn test
...

### Runs esLint

....
yarn lint
....

## Vercel

## Authors

- Martin Thénot
