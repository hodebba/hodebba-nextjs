module.exports = {
  setupFilesAfterEnv: ['<rootDir>/setupTests.js'],

  moduleNameMapper: {
    '\\.(css|less|scss|sss|styl)$': '<rootDir>/node_modules/jest-css-modules',
    '\\.svg': '<rootDir>/test/svgrMock.js',
    '\\.(css|less)$': 'identity-obj-proxy',
  },
};
