import { NextSeo } from 'next-seo';

import isAdmin from '../../HOCs/isAdmin.hoc';
import PageMenu from '../../components/PageMenu';
import CreateCelebrity from '../../containers/Admin/CreateCelebrity';
import { itemsMenu } from '../../containers/Admin/itemsMenu';

const Celebrity = () => {
  return (
    <div className="admin">
      <NextSeo noindex />
      <PageMenu items={itemsMenu} className="block" active="celebrity" />
      <CreateCelebrity />
    </div>
  );
};

export default isAdmin(Celebrity);
