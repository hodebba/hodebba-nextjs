import { NextSeo } from 'next-seo';

import isAdmin from '../../HOCs/isAdmin.hoc';
import PageMenu from '../../components/PageMenu';
import CreateDebate from '../../containers/Admin/CreateDebate';
import { itemsMenu } from '../../containers/Admin/itemsMenu';

const Debate = () => {
  return (
    <div className="admin">
      <NextSeo noindex />
      <PageMenu items={itemsMenu} className="block" active="debate" />
      <CreateDebate />
    </div>
  );
};

export default isAdmin(Debate);
