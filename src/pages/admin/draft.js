import { NextSeo } from 'next-seo';
import Proptypes from 'prop-types';

import isAdmin from '../../HOCs/isAdmin.hoc';
import PageMenu from '../../components/PageMenu';
import Draft from '../../containers/Admin/Draft';
import { itemsMenu } from '../../containers/Admin/itemsMenu';
import { getDraftDebates } from '../../services/debate.service';

const DraftList = ({ debates }) => {
  return (
    <div className="admin">
      <NextSeo noindex />
      <PageMenu items={itemsMenu} className="block" active="draft" />
      <Draft debates={debates} />
    </div>
  );
};

DraftList.propTypes = {
  debates: Proptypes.array,
};

export default isAdmin(DraftList);

export async function getStaticProps() {
  let error = '';
  let result = {};

  try {
    const response = await getDraftDebates();
    result = response;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      debates: result,
      error,
    },
    revalidate: 10,
  };
}
