import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';

import isAdmin from '../../HOCs/isAdmin.hoc';

const Admin = () => {
  const router = useRouter();
  setTimeout(() => {
    router.push('/admin/debate');
  }, 100);
  return (
    <div>
      <NextSeo noindex />
    </div>
  );
};

export default isAdmin(Admin);
