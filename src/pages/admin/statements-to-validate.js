import { NextSeo } from 'next-seo';
import Proptypes from 'prop-types';

import isAdmin from '../../HOCs/isAdmin.hoc';
import PageMenu from '../../components/PageMenu';
import { itemsMenu } from '../../containers/Admin/itemsMenu';
import NotValidatedStatements from '../../containers/Admin/NotValidatedStatements';
import { getNotValidatedStatements } from '../../services/celebrity.service';

const StatementsList = ({ statements }) => {
  return (
    <div className="admin">
      <NextSeo noindex />
      <PageMenu items={itemsMenu} className="block" active="statements" />
      <NotValidatedStatements statements={statements} />
    </div>
  );
};

StatementsList.propTypes = {
  statements: Proptypes.array,
};

export default isAdmin(StatementsList);

export async function getStaticProps() {
  let error = '';
  let result = {};

  try {
    const response = await getNotValidatedStatements();
    result = response;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      statements: result,
      error,
    },
    revalidate: 10,
  };
}
