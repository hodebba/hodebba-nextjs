import isNotBrowserRoute from '../../../../HOCs/isNotBrowserRoute';
import AddStatementMobile from '../../../../containers/Celebrities/Celebrity/Statements/AddStatementMobile';

const Compose = () => {
  return <AddStatementMobile />;
};

export default isNotBrowserRoute(Compose);
