import Celebrity from '../../../../containers/Celebrities/Celebrity';
import Statements from '../../../../containers/Celebrities/Celebrity/Statements';
import { getCelebrities, getCelebrity, getStatementsByCelebrity } from '../../../../services/celebrity.service';
import { getWikiDescription } from '../../../../services/wikipedia.service';

Statements.getLayout = page => <Celebrity activeItem="statements">{page}</Celebrity>;

export default Statements;

export async function getStaticPaths() {
  const celebrities = await getCelebrities();

  const paths = celebrities.map(celebrity => ({
    params: { id: celebrity.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let statements = {};
  let celebrity = {};
  let description = {};

  try {
    const response = await getStatementsByCelebrity(params.id);
    statements = response;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getCelebrity(params.id);
    celebrity = response;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getWikiDescription(celebrity.fullName);
    if (response.data.query.pages[Object.keys(response.data.query.pages)[0]].description) {
      const shortDescription = response.data.query.pages[Object.keys(response.data.query.pages)[0]].description;
      const longDescription = response.data.query.pages[Object.keys(response.data.query.pages)[0]].extract;
      description = { shortDescription, longDescription };
    }
  } catch (e) {
    error = e.response.data;
  }

  return {
    props: {
      statements,
      celebrity,
      description,
      error,
    },
    revalidate: 1,
  };
}
