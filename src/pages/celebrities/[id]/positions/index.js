import Celebrity from '../../../../containers/Celebrities/Celebrity';
import Positions from '../../../../containers/Celebrities/Celebrity/Positions';
import { getCelebrities, getCelebrity, getPositionsByCelebrity } from '../../../../services/celebrity.service';
import { getWikiDescription } from '../../../../services/wikipedia.service';
Positions.getLayout = page => <Celebrity activeItem="positions">{page}</Celebrity>;

export default Positions;

export async function getStaticPaths() {
  const celebrities = await getCelebrities();
  const paths = celebrities.map(celebrity => ({
    params: { id: celebrity.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let positions = {};
  let celebrity = {};
  let description = {};

  try {
    const response = await getPositionsByCelebrity(params.id);
    positions = response;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getCelebrity(params.id);
    celebrity = response;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getWikiDescription(celebrity.fullName);
    if (response.data.query.pages[Object.keys(response.data.query.pages)[0]].description) {
      const shortDescription = response.data.query.pages[Object.keys(response.data.query.pages)[0]].description;
      const longDescription = response.data.query.pages[Object.keys(response.data.query.pages)[0]].extract;
      description = { shortDescription, longDescription };
    }
  } catch (e) {
    error = e.response.data;
  }
  return {
    props: {
      positions,
      celebrity,
      description,
      error,
    },
    revalidate: 60,
  };
}
