import isNotBrowserRoute from '../../../../HOCs/isNotBrowserRoute';
import AddPositionMobile from '../../../../containers/Celebrities/Celebrity/Positions/AddPositionMobile';

const Compose = () => {
  return <AddPositionMobile />;
};

export default isNotBrowserRoute(Compose);
