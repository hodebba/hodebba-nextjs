/* eslint-disable no-await-in-loop */
import { isEmpty } from 'lodash';

import checkError from '../../../HOCs/checkError.hoc';
import CelebrityByProfession from '../../../containers/Celebrities/CelebrityByProfession';
import { getProfession, getProfessionsLight } from '../../../services/celebrity.service';
import { getMultipleWikiDescription } from '../../../services/wikipedia.service';
export default checkError(CelebrityByProfession);

// ? Need to make multiple requests to get wiki descriptions because it is limited to50 results by request
// TODO: make this beahviour cleaner
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index += 1) {
    await callback(array[index], index, array);
  }
}

export async function getStaticPaths() {
  const professions = await getProfessionsLight();

  const paths = professions.map(profession => ({
    params: { id: profession.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let result = {};
  let descriptions = {};

  try {
    const response = await getProfession(params.id);
    result = response;
  } catch (e) {
    error = e.response.data;
  }

  if (!isEmpty(result) && result.celebrities.length > 0) {
    let names = '';
    const nameList = [];
    result.celebrities.forEach((celebrity, index) => {
      names += `${celebrity.fullName}|`;
      if (index / (nameList.length + 1) >= 49) {
        nameList.push(names.substring(0, names.length - 1));
        names = '';
      }
    });
    nameList.push(names);

    await asyncForEach(nameList, async namesStr => {
      try {
        const response = await getMultipleWikiDescription(namesStr);
        descriptions = Object.assign(response, descriptions);
      } catch (e) {
        error = e.response.data;
      }
    });
  }

  return {
    props: {
      profession: result,
      descriptions,
      error,
    },
    revalidate: 120,
  };
}
