/* eslint-disable no-await-in-loop */
import checkError from '../../HOCs/checkError.hoc';
import CelebrityList from '../../containers/Celebrities/CelebrityList';
import { getProfessions } from '../../services/celebrity.service';
import { getMultipleWikiDescription } from '../../services/wikipedia.service';

export default checkError(CelebrityList);

// ? Need to make multiple requests to get wiki descriptions because it is limited to50 results by request
// TODO: make this beahviour cleaner
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index += 1) {
    await callback(array[index], index, array);
  }
}

export async function getStaticProps() {
  let error = '';
  let professions = {};
  let descriptions = [];

  try {
    const response = await getProfessions();
    professions = response;
  } catch (e) {
    error = e.response.data;
  }

  if (professions.length > 0) {
    let names = '';
    const nameList = [];
    let count = 0;
    professions.forEach(profession => {
      profession.celebrities.forEach(celebrity => {
        count += 1;
        names += `${celebrity.fullName}|`;
        if (count / (nameList.length + 1) >= 49) {
          nameList.push(names.substring(0, names.length - 1));
          names = '';
        }
      });
    });
    nameList.push(names);

    await asyncForEach(nameList, async namesStr => {
      try {
        const response = await getMultipleWikiDescription(namesStr);
        descriptions = Object.assign(response, descriptions);
      } catch (e) {
        error = e.response.data;
      }
    });
  }
  return {
    props: {
      professions,
      descriptions,
      error,
    },
    revalidate: 120,
  };
}
