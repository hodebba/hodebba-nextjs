/* eslint-disable react-hooks/exhaustive-deps */
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import { useEffect } from 'react';

const Debate = () => {
  const router = useRouter();

  useEffect(() => {
    setTimeout(() => {
      router.push('/theme/[id]', 'theme/1');
    }, 100);
  }, []);

  return (
    <div>
      <NextSeo noindex />
    </div>
  );
};

export default Debate;
