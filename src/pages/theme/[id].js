import checkError from '../../HOCs/checkError.hoc';
import Theme from '../../containers/Theme';
import { getTheme, getThemes } from '../../services/debate.service';

export default checkError(Theme);

export async function getStaticPaths() {
  const themes = await getThemes();

  const paths = themes.data.map(theme => ({
    params: { id: theme.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let result = {};

  try {
    const response = await getTheme(params.id);
    result = response.data;
  } catch (e) {
    error = e.response.data;
  }

  return {
    props: {
      debates: (result && result.debates) || [],
      currentTheme: {
        label: (result && result.label) || null,
        id: (result && result.id) || null,
      },
      error,
    },
    revalidate: 1,
  };
}
