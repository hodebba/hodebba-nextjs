import Home from '../containers/Home';
import { getDebatesForHome } from '../services/debate.service';

export default Home;

export async function getStaticProps() {
  let error = '';
  let result = [];

  try {
    const response = await getDebatesForHome();
    result = response.data;
  } catch (e) {
    error = e.response.data.message;
  }
  return {
    props: {
      debates: result,
      error,
    },
    revalidate: 120,
  };
}
