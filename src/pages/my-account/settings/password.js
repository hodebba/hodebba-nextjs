import isMobileRoute from '../../../HOCs/isMobileRoute.hoc';
import MobileMenu from '../../../components/MobileMenu';
import Settings from '../../../containers/User/Settings';

const Password = () => {
  return (
    <MobileMenu title="Mot de passe" previousPage="/my-account/settings">
      <Settings />
    </MobileMenu>
  );
};

export default isMobileRoute(Password);
