import { NextSeo } from 'next-seo';
import { Fragment } from 'react';

import isMobileRoute from '../../../HOCs/isMobileRoute.hoc';
import MobileMenu from '../../../components/MobileMenu';
import Settings from '../../../containers/User/Settings';

const DeleteAccount = () => {
  return (
    <Fragment>
      <NextSeo noindex />
      <MobileMenu title="Supprimer compte" previousPage="/my-account/settings">
        <Settings />
      </MobileMenu>
    </Fragment>
  );
};

export default isMobileRoute(DeleteAccount);
