import PropTypes from 'prop-types';

import User from '../../../containers/User';
import { getAllParticipantsByUser } from '../../../services/user.service';
import { parseCookies } from '../../../utils/parseCookies';

export default function MyDebates({ data }) {
  return <User data={data} type="my-debates" />;
}

MyDebates.propTypes = {
  data: PropTypes.array.isRequired,
};

export async function getServerSideProps({ req }) {
  const cookies = parseCookies(req);
  const { token } = cookies;

  let error = '';
  let result = {};

  if (token) {
    try {
      const response = await getAllParticipantsByUser(token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }
  }

  return {
    props: {
      data: result,
      error,
    },
  };
}
