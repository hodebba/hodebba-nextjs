import { NextSeo } from 'next-seo';
import { Fragment } from 'react';

import isMobileRoute from '../../../HOCs/isMobileRoute.hoc';
import MobileMenu from '../../../components/MobileMenu';
import Profile from '../../../containers/User/Profile';

const Location = () => {
  return (
    <Fragment>
      <NextSeo noindex />
      <MobileMenu title="Localisation" previousPage="/my-account/profile">
        <Profile />
      </MobileMenu>
    </Fragment>
  );
};

export default isMobileRoute(Location);
