import PropTypes from 'prop-types';

import User from '../../../containers/User';
import { getPostsDataByUser } from '../../../services/user.service';
import { parseCookies } from '../../../utils/parseCookies';

export default function dashboard({ data }) {
  return <User data={data} type="dashboard" />;
}

dashboard.propTypes = {
  data: PropTypes.object,
};

export async function getServerSideProps({ req }) {
  const cookies = parseCookies(req);
  const { token, user } = cookies;

  let error = '';
  let result = {};

  if (user && token) {
    try {
      const response = await getPostsDataByUser(JSON.parse(user).id, token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }
  }

  return {
    props: {
      data: result,
      error,
    },
  };
}
