import User from '../../containers/User';

export default function profile() {
  return <User type="profile" />;
}
