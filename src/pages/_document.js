/* eslint-disable react/no-danger */
/* eslint-disable object-curly-newline */
import Document, { Head, Html, Main, NextScript } from 'next/document';

import { GA_TRACKING_ID } from '../utils/gtags';
class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="fr">
        <Head>
          {/* Global Site Tag (gtag.js) - Google Analytics */}
          <script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
          <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet" />

          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
            gtag('config', 'AW-480766229');
          `,
            }}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '4b4dc221179d4718b996049021c1e3fe');
qp('track', 'ViewContent');`,
            }}
          />
          <noscript>
            <img
              height="1"
              width="1"
              alt="quora"
              style={{ display: 'none' }}
              src="https://q.quora.com/_/ad/4b4dc221179d4718b996049021c1e3fe/pixel?tag=ViewContent&noscript=1"
            />
          </noscript>

          <meta charSet="utf-8" />
          <meta name="theme-color" content="#000" />
          <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
            rel="stylesheet"
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap"
            rel="stylesheet"
          />
          <script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>

          <link rel="apple-touch-icon" sizes="152x152" href="/static/favicon/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon/favicon-16x16.png" />
          <link rel="manifest" href="/static/favicon/site.webmanifest" />
          <link rel="mask-icon" href="/static/favicon/safari-pinned-tab.svg" color="#5bbad5" />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#ffffff" />
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
