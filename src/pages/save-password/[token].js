/* eslint-disable no-unused-vars */

import SavePassword from '../../containers/User/SavePassword';
import { checkResetPasswordToken } from '../../services/user.service';

export default SavePassword;

export async function getServerSideProps({ params }) {
  let invalidToken = false;

  try {
    const response = await checkResetPasswordToken(params.token);
  } catch (e) {
    invalidToken = true;
  }
  return {
    props: {
      token: params.token,
      invalidToken,
    },
  };
}
