import Error from '../containers/Error';

const FourHundredFour = () => {
  return <Error code={404} />;
};

export default FourHundredFour;
