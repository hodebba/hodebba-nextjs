/* eslint-disable valid-typeof */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
/* eslint-disable operator-linebreak */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/prop-types */
import '../styles/toastify/main.scss';
import '../styles/app.scss';
import 'nprogress/nprogress.css';
import 'core-js/features/string/replace-all';

import Notification from '@Components/Notification';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';
import { CloudinaryContext } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import Router, { useRouter } from 'next/router';
import NProgress from 'nprogress';
import { useEffect, useRef, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { Provider, useDispatch, useSelector, useStore } from 'react-redux';
import TwitterConvTrkr from 'react-twitter-conversion-tracker';

import { getInfoFromCookies, getUser } from '../actions/user.actions';
import ConfirmAccountBanner from '../components/Banner/ConfirmAccountBanner';
import CookieConsentModal from '../components/CookieConsentModal';
import LoadingPage from '../components/LoadingPage';
import AuthModal from '../containers/Authentification/AuthModal';
import Footer from '../layout/Footer';
import Header from '../layout/Header';
import { wrapper } from '../store';
import { useWindowSize } from '../utils/general.helper';
import * as gtag from '../utils/gtags';

Router.onRouteChangeStart = () => {
  NProgress.start();
};

Router.onRouteChangeComplete = url => {
  NProgress.done();
  gtag.pageview(url);
};

Router.onRouteChangeError = () => {
  NProgress.done();
};

const noHeadersRoutes = ['debate/', 'compose', 'my-account', 'contact'];
const noBannerRoutes = ['compose', 'my-account'];

if (process.env.NEXT_PUBLIC_SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.NEXT_PUBLIC_SENTRY_DSN,
    integrations: [new Integrations.BrowserTracing()],
    tracesSampleRate: 1.0,
  });
}

export function reportWebVitals({ id, name, label, value }) {
  window.gtag('event', name, {
    event_category: label === 'web-vital' ? 'Web Vitals' : 'Next.js custom metric',
    value: Math.round(name === 'CLS' ? value * 1000 : value), // values must be integers
    event_label: id, // id unique to current page load
    non_interaction: true, // avoids affecting bounce rate.
  });
}

const MyApp = ({ Component, pageProps, err }) => {
  const store = useStore();
  const { isOpen, type, emailResetPassword } = useSelector(state => state.authentification.local.modal);
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const getLayout = Component.getLayout || (page => page);
  const router = useRouter();
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const [isMobileHeader, setIsMobileHeader] = useState(false);
  const dispatch = useDispatch();
  const app = useRef(null);
  const [isLoading, setIsLoading] = useState(true);
  const size = useWindowSize();

  const checkInnerWidth = () => {
    if (size.width < 1050) {
      setIsMobileHeader(true);
      if (size.width < 768) {
        setIsSmallScreen(true);
      } else {
        setIsSmallScreen(false);
      }
    } else {
      setIsMobileHeader(false);
    }
  };

  useEffect(() => {
    if ((app.current && !isSmallScreen) || isLoading) {
      if (isOpen || isLoading) {
        app.current.classList.add('no-scroll-desktop');
      } else {
        app.current.classList.remove('no-scroll-desktop');
      }
    }
  }, [isOpen, isLoading]);

  useEffect(() => {
    checkInnerWidth();
  }, [size]);
  useEffect(() => {
    if (!isEmpty(token) && !isEmpty(user) && !user.enabled) {
      dispatch(getUser(user.id, token));
    }
    if (isEmpty(token)) {
      dispatch(getInfoFromCookies());
    }
  }, [token]);

  useEffect(() => {
    setIsLoading(false);

    import('react-facebook-pixel')
      .then(x => x.default)
      .then(ReactPixel => {
        if (process.env.NEXT_PUBLIC_API_URL === 'https://api.hodebba.fr') {
          ReactPixel.init(process.env.NEXT_PUBLIC_FACEBOOK_PIXEL);
          ReactPixel.pageView();
          TwitterConvTrkr.init('o4xel');
          TwitterConvTrkr.pageView();
        }
      });
  }, []);

  const hideHeader =
    (noHeadersRoutes.map(element => router.pathname.includes(element)).includes(true) &&
      isSmallScreen &&
      isMobileOnly) ||
    router.pathname.includes('compose');

  const hideBanner =
    (noBannerRoutes.map(element => router.pathname.includes(element)).includes(true) && isMobileOnly) ||
    router.pathname.includes('compose');

  const appClassName = isLoading ? 'no-scroll-desktop' : '';

  const renderComponents = () => {
    return (
      <CloudinaryContext cloudName={process.env.NEXT_PUBLIC_CLOUDINARY_CLOUDNAME}>
        {isLoading && <LoadingPage />}
        {!(isMobileOnly && isOpen) && (
          <div id="app" ref={app} className={appClassName}>
            <CookieConsentModal />
            {!hideHeader && <Header isMobileHeader={isMobileHeader} />}
            {!isEmpty(user) && !user.enabled && !hideBanner && <ConfirmAccountBanner />}
            <main>
              {getLayout(<Component {...pageProps} isSmallScreen={isSmallScreen} err={err} />, isSmallScreen)}
            </main>
            {!isMobileHeader && <Footer />}
          </div>
        )}
        {isOpen && <AuthModal type={type} emailResetPassword={emailResetPassword} user={user} />}
        <Notification />
      </CloudinaryContext>
    );
  };

  return <Provider store={store}>{renderComponents()}</Provider>;
};

export default wrapper.withRedux(MyApp);
