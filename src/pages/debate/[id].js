/* eslint-disable react-hooks/exhaustive-deps */
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import { useEffect } from 'react';

const Debate = () => {
  const router = useRouter();

  useEffect(() => {
    setTimeout(() => {
      router.push('/debate/[id]/presentation', `${router.asPath}/presentation`);
    }, 100);
  }, []);

  return (
    <div>
      <NextSeo noindex />
    </div>
  );
};

export default Debate;
