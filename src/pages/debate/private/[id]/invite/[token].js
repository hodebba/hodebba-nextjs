/* eslint-disable no-unused-vars */
import JoinDebate from '../../../../../containers/Debate/JoinDebate';
import { confirmInvitation, getPrivateDebatePublicRoute } from '../../../../../services/debate.service';
import { parseCookies } from '../../../../../utils/parseCookies';

export default JoinDebate;

export async function getServerSideProps({ req, params }) {
  const cookies = parseCookies(req);
  const { token } = cookies;
  let error = null;
  let isEnabled = true;
  let result = {};

  if (token !== undefined) {
    try {
      const response = await confirmInvitation(params.token, token);
    } catch (e) {
      isEnabled = false;
      error = e.response.data.message;
    }
  } else {
    isEnabled = false;
  }

  try {
    const response = await getPrivateDebatePublicRoute(params.id);
    result = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      isEnabled,
      error,
      debate: result,
    },
  };
}
