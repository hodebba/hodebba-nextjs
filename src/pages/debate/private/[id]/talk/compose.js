import isNotBrowserRoute from '../../../../../HOCs/isNotBrowserRoute';
import AddArgumentMobile from '../../../../../containers/Debate/Talk/AddArgumentMobile';

const Compose = () => {
  return <AddArgumentMobile />;
};

export default isNotBrowserRoute(Compose);
