/* eslint-disable object-curly-newline */
import Debate from '../../../../../containers/Debate';
import Talk from '../../../../../containers/Debate/Talk';
import { getArguments, getPrivateDebate } from '../../../../../services/debate.service';
import { parseCookies } from '../../../../../utils/parseCookies';

Talk.getLayout = (page, isSmallScreen) => (
  <Debate activeItem="talk" isSmallScreen={isSmallScreen}>
    {page}
  </Debate>
);

export default Talk;

export async function getServerSideProps({ params, req }) {
  const cookies = parseCookies(req);
  const { token } = cookies;
  let argumentList = {};
  const statementList = {};

  let error = '';
  let result = {};

  if (token) {
    try {
      const response = await getPrivateDebate(params.id, token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }

    try {
      const response = await getArguments(params.id);
      argumentList = response.data;
    } catch (e) {
      error = e.response.data.message;
    }
  }

  return {
    props: {
      debate: result,
      argumentList,
      statementList,
      error,
    },
  };
}
