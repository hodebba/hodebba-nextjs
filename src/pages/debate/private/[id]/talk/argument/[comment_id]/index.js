import { Fragment } from 'react';

import Debate from '../../../../../../../containers/Debate';
import MainComment from '../../../../../../../containers/Debate/Talk/MainComment';
import { getPost, getPrivateDebate } from '../../../../../../../services/debate.service';
import { parseCookies } from '../../../../../../../utils/parseCookies';

MainComment.getLayout = (page, isSmallScreen) => {
  return (
    <Fragment>
      {isSmallScreen ? <div className="center">{page}</div> : <Debate activeItem="talk">{page}</Debate>}
    </Fragment>
  );
};

export default MainComment;

export async function getServerSideProps({ params, req }) {
  const cookies = parseCookies(req);
  const { token } = cookies;
  let mainComment = {};

  let error = '';
  let result = {};

  if (token) {
    try {
      const response = await getPrivateDebate(params.id, token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }

    try {
      const response = await getPost(params.comment_id);
      mainComment = response.data;
    } catch (e) {
      error = e.response.data.message;
    }
  }

  return {
    props: {
      debate: result,
      mainComment,
      error,
    },
  };
}
