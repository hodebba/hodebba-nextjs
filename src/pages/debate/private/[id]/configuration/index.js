import Configuration from '../../../../../containers/Debate/Configuration';
import checkError from '../../../../../HOCs/checkError.hoc';
import isFallback from '../../../../../HOCs/isFallback.hoc';
import { getPrivateDebate } from '../../../../../services/debate.service';
import { parseCookies } from '../../../../../utils/parseCookies';

export default checkError(isFallback(Configuration));

export async function getServerSideProps({ params, req }) {
  const cookies = parseCookies(req);
  const { token } = cookies;

  let error = '';
  let result = {};

  if (token) {
    try {
      const response = await getPrivateDebate(params.id, token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }
  }

  return {
    props: {
      debate: result,
      error,
    },
  };
}
