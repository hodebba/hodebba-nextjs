import PropTypes from 'prop-types';

import checkError from '../../../../../HOCs/checkError.hoc';
import isFallback from '../../../../../HOCs/isFallback.hoc';
import MobileMenu from '../../../../../components/MobileMenu';
import Configuration from '../../../../../containers/Debate/Configuration';
import { getPrivateDebate } from '../../../../../services/debate.service';
import { parseCookies } from '../../../../../utils/parseCookies';

const Invitation = ({ debate }) => {
  return (
    <MobileMenu title="" previousPage={`/debate/private/${debate.id}/configuration`}>
      <Configuration active="invitation" debate={debate} />
    </MobileMenu>
  );
};

Invitation.propTypes = {
  debate: PropTypes.object.isRequired,
};

export default checkError(isFallback(Invitation));

export async function getServerSideProps({ params, req }) {
  const cookies = parseCookies(req);
  const { token } = cookies;

  let error = '';
  let result = {};

  if (token) {
    try {
      const response = await getPrivateDebate(params.id, token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }
  }

  return {
    props: {
      debate: result,
      error,
    },
  };
}
