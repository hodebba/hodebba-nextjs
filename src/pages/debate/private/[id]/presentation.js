import Debate from '../../../../containers/Debate';
import Presentation from '../../../../containers/Debate/Presentation';
import { getPrivateDebate } from '../../../../services/debate.service';
import { parseCookies } from '../../../../utils/parseCookies';

Presentation.getLayout = page => <Debate activeItem="presentation">{page}</Debate>;

export default Presentation;

export async function getServerSideProps({ params, req }) {
  const cookies = parseCookies(req);
  const { token } = cookies;

  let error = '';
  let result = {};

  if (token) {
    try {
      const response = await getPrivateDebate(params.id, token);
      result = response.data;
    } catch (e) {
      error = e.response.data;
    }
  }

  return {
    props: {
      debate: result,
      error,
    },
  };
}
