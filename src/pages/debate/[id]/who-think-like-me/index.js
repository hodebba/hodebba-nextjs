import Debate from '../../../../containers/Debate';
import ThinkLikeMe from '../../../../containers/Debate/ThinkLikeMe';
import { getPositionsByDebate } from '../../../../services/celebrity.service';
import { getDebate, getDebates } from '../../../../services/debate.service';
import { getMultipleWikiDescription } from '../../../../services/wikipedia.service';

ThinkLikeMe.getLayout = page => <Debate activeItem="who-think-like-me">{page}</Debate>;

export default ThinkLikeMe;

export async function getStaticPaths() {
  const debates = await getDebates();

  const paths = debates.map(debate => ({
    params: { id: debate.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let result = {};
  let positions = {};
  let descriptions = [];

  try {
    const response = await getDebate(params.id);
    result = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getPositionsByDebate(params.id);
    positions = response;
  } catch (e) {
    error = e.response.data.message;
  }

  let names = '';
  if (positions && positions.pro) {
    positions.pro.forEach(element => {
      names += `${element.celebrity.fullName}|`;
    });
  }

  if (positions && positions.con) {
    positions.con.forEach(element => {
      names += `${element.celebrity.fullName}|`;
    });
  }

  if (names !== '') {
    try {
      const response = await getMultipleWikiDescription(names.substring(0, names.length - 1));
      descriptions = response;
    } catch (e) {
      error = e.response.data;
    }
  }

  return {
    props: {
      debate: result,
      positions,
      descriptions,
      error,
    },
    revalidate: 1,
  };
}
