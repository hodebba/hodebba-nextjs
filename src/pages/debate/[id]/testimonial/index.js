import Debate from '../../../../containers/Debate';
import Testimonial from '../../../../containers/Debate/Testimonial';
import { getDebate, getDebates, getTestimonials } from '../../../../services/debate.service';

Testimonial.getLayout = (page, isSmallScreen) => (
  <Debate activeItem="testimonial" isSmallScreen={isSmallScreen}>
    {page}
  </Debate>
);

export default Testimonial;

export async function getStaticPaths() {
  const debates = await getDebates();

  const paths = debates.map(debate => ({
    params: { id: debate.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let debate = {};
  let testimonials = {};

  try {
    const response = await getDebate(params.id);
    debate = response.data;
  } catch (e) {
    error = e.response.data.message || e.response.data;
  }

  try {
    const response = await getTestimonials(params.id);
    testimonials = response.data;
  } catch (e) {
    error = e.response.data.message || e.response.data;
  }

  return {
    props: {
      debate,
      testimonials,
      error,
    },
    revalidate: 1,
  };
}
