import { Fragment } from 'react';

import Debate from '../../../../../containers/Debate';
import MainComment from '../../../../../containers/Debate/Talk/MainComment';
import { getAllTestimonials, getDebate, getPost } from '../../../../../services/debate.service';

MainComment.getLayout = (page, isSmallScreen) => {
  return (
    <Fragment>
      {isSmallScreen ? <div className="center">{page}</div> : <Debate activeItem="testimonial">{page}</Debate>}
    </Fragment>
  );
};

export default MainComment;

export async function getStaticPaths() {
  const testimonials = await getAllTestimonials();

  const paths = testimonials.map(testimonial => ({
    params: { id: testimonial.debate.id.toString(), testimonial_id: testimonial.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let debate = {};
  let mainComment = {};

  try {
    const response = await getDebate(params.id);
    debate = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getPost(params.testimonial_id);
    mainComment = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      debate,
      mainComment,
      error,
    },
    revalidate: 1,
  };
}
