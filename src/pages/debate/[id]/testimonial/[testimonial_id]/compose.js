import isNotBrowserRoute from '../../../../../HOCs/isNotBrowserRoute';
import AddTestimonialCommentMobile from '../../../../../containers/Debate/Testimonial/AddTestimonialCommentMobile';

const Compose = () => {
  return <AddTestimonialCommentMobile />;
};

export default isNotBrowserRoute(Compose);
