import isNotBrowserRoute from '../../../../HOCs/isNotBrowserRoute';
import AddTestimonialMobile from '../../../../containers/Debate/Testimonial/AddTestimonialMobile';

const Compose = () => {
  return <AddTestimonialMobile />;
};

export default isNotBrowserRoute(Compose);
