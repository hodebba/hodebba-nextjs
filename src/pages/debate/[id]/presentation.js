import Debate from '../../../containers/Debate';
import Presentation from '../../../containers/Debate/Presentation';
import { getDebate, getDebates } from '../../../services/debate.service';

Presentation.getLayout = page => <Debate activeItem="presentation">{page}</Debate>;

export default Presentation;

export async function getStaticPaths() {
  const debates = await getDebates();

  const paths = debates.map(debate => ({
    params: { id: debate.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let result = {};

  try {
    const response = await getDebate(params.id);
    result = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      debate: result,
      error,
    },
    revalidate: 1,
  };
}
