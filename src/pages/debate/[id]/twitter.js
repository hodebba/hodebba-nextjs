import isEmpty from 'lodash/isEmpty';

import Debate from '../../../containers/Debate';
import Twitter from '../../../containers/Debate/Twitter';
import { getDebates, getTweets, getTweetsData } from '../../../services/debate.service';

Twitter.getLayout = page => <Debate activeItem="twitter">{page}</Debate>;

export default Twitter;

export async function getStaticPaths() {
  const debates = await getDebates();

  const paths = debates.map(debate => ({
    params: { id: debate.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let result = {};
  let tweets = [];

  try {
    const response = await getTweets(params.id);
    result = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  if (!isEmpty(result) && !isEmpty(result.tweets)) {
    let ids = '';
    result.tweets.forEach(element => {
      ids += `${element.link},`;
    });

    try {
      const response = await getTweetsData(ids.substring(0, ids.length - 1));
      tweets = response;
    } catch (e) {
      error = e.response;
    }
  }

  return {
    props: {
      debate: result,
      tweets,
      error,
    },
    revalidate: 2,
  };
}
