import { Fragment } from 'react';

import Debate from '../../../../../../containers/Debate';
import MainComment from '../../../../../../containers/Debate/Talk/MainComment';
import { getAllArguments, getDebate, getPost } from '../../../../../../services/debate.service';

MainComment.getLayout = (page, isSmallScreen) => {
  return (
    <Fragment>
      {isSmallScreen ? <div className="center">{page}</div> : <Debate activeItem="talk">{page}</Debate>}
    </Fragment>
  );
};

export default MainComment;

export async function getStaticPaths() {
  const argumentList = await getAllArguments();

  const paths = argumentList.map(argument => ({
    params: { id: argument.debate.id.toString(), comment_id: argument.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let debate = {};
  let mainComment = {};

  try {
    const response = await getDebate(params.id);
    debate = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getPost(params.comment_id);
    mainComment = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      debate,
      mainComment,
      error,
    },
    revalidate: 1,
  };
}
