import isNotBrowserRoute from '../../../../../../HOCs/isNotBrowserRoute';
import AddCommentMobile from '../../../../../../containers/Debate/Talk/AddCommentMobile';

const Compose = () => {
  return <AddCommentMobile />;
};

export default isNotBrowserRoute(Compose);
