/* eslint-disable object-curly-newline */
import Debate from '../../../../containers/Debate';
import Talk from '../../../../containers/Debate/Talk';
import { getArguments, getDebate, getDebates, getStatements } from '../../../../services/debate.service';

Talk.getLayout = (page, isSmallScreen) => (
  <Debate activeItem="talk" isSmallScreen={isSmallScreen}>
    {page}
  </Debate>
);

export default Talk;

export async function getStaticPaths() {
  const debates = await getDebates();

  const paths = debates.map(debate => ({
    params: { id: debate.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let error = '';
  let debate = {};
  let argumentList = {};
  let statementList = {};

  try {
    const response = await getDebate(params.id);
    debate = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getArguments(params.id);
    argumentList = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  try {
    const response = await getStatements(params.id);
    statementList = response.data;
  } catch (e) {
    error = e.response.data.message;
  }
  return {
    props: {
      debate,
      argumentList,
      statementList,
      error,
    },
    revalidate: 1,
  };
}
