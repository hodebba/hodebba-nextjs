/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast, ToastContainer } from 'react-toastify';

import { resendConfirmEmail } from '../../actions/authentification.actions';
import { setUserEnabled } from '../../actions/user.actions';
import InfoModal from '../../components/InfoModal';
import Home from '../../containers/Home';
import { confirmAccount } from '../../services/authentification.service';
import { getDebatesForHome } from '../../services/debate.service';

const ConfirmEmail = ({ isEnabled, error, debates }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const resendConfirmEmailResult = useSelector(state => state.authentification.data.resendConfirmEmail);
  const resendConfirmEmailError = useSelector(state => state.authentification.local.errors.resendConfirmEmail);

  const [displayErrorModal, setDisplayErrorModal] = useState(true);

  useEffect(() => {
    router.prefetch('/');
  }, []);

  useEffect(() => {
    if (isEnabled) {
      toast.success('Email confirmé avec succès!');
      dispatch(setUserEnabled());
      setTimeout(() => {
        router.push('/');
      }, 4000);
    }
  }, [isEnabled]);

  useEffect(() => {
    if (!isEnabled && user.enabled) {
      setTimeout(() => {
        router.push('/');
      }, 100);
    }
  }, [user]);

  return (
    <Fragment>
      <Home debates={debates} />
      {!isEnabled && !user.enabled && displayErrorModal && isEmpty(resendConfirmEmailError) && (
        <InfoModal onClose={() => setDisplayErrorModal(false)}>
          <h2 style={{ fontSize: '1.8rem' }}>La confirmation de votre adresse email a echoué</h2>
          <p>{error}</p>
          {!isEmpty(user) ? (
            <p>
              Si vous souhaitez que l'on vous renvoie l' email de confirmation,{' '}
              <button
                type="button"
                className="text-button"
                onClick={() => dispatch(resendConfirmEmail(user.id, token))}
              >
                cliquez-ici
              </button>
              .
            </p>
          ) : (
            <p>Connectez-vous si vous souhaitez que l'email de confirmation vous soit renvoyé.</p>
          )}
        </InfoModal>
      )}
      {!isEmpty(resendConfirmEmailResult) && displayErrorModal && (
        <InfoModal onClose={() => setDisplayErrorModal(false)}>
          <p>
            Un email de confirmation a été envoyé à votre adresse email. Merci de cliquer sur lien se trouvant dans cet
            email pour activer votre compte.
          </p>
        </InfoModal>
      )}
      {!isEmpty(resendConfirmEmailError) && displayErrorModal && (
        <InfoModal onClose={() => setDisplayErrorModal(false)}>
          <h2 style={{ fontSize: '1.8rem' }}>{resendConfirmEmailError}</h2>
        </InfoModal>
      )}

      <ToastContainer
        position="top-right"
        autoClose={4000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </Fragment>
  );
};

ConfirmEmail.propTypes = {
  isEnabled: PropTypes.bool.isRequired,
  error: PropTypes.string,
  debates: PropTypes.array,
};

export default ConfirmEmail;

export async function getServerSideProps({ params }) {
  let error = null;
  let isEnabled = true;
  let result = [];

  try {
    const response = await confirmAccount(params.token);
  } catch (e) {
    isEnabled = false;
    error = e.response.data.message;
  }

  try {
    const response = await getDebatesForHome();
    result = response.data;
  } catch (e) {
    error = e.response.data.message;
  }

  return {
    props: {
      isEnabled,
      error,
      debates: result,
    },
  };
}
