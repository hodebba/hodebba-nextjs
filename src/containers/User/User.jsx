import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';

import { itemsMenuMyAccount } from '../../components/itemsMenu';
import MobileMenu from '../../components/MobileMenu';
import PageMenu from '../../components/PageMenu';
import { useWindowSize } from '../../utils/general.helper';
import Dashboard from './Dashboard';
import { optionsProfile, optionsSettings } from './helpers';
import MyDebates from './MyDebates';
import Profile from './Profile';
import Settings from './Settings';

const User = ({ data, type }) => {
  const size = useWindowSize();
  const [width, setWidth] = useState(null);

  const renderUserComponent = () => {
    switch (type) {
      case 'dashboard':
        return <Dashboard data={data} />;
      case 'profile':
        return <Profile />;
      case 'settings':
        return <Settings />;
      case 'my-debates':
        return <MyDebates participants={data} />;
      default:
        return '';
    }
  };

  const getMobileData = () => {
    switch (type) {
      case 'dashboard':
        return { title: 'Tableau de bord', options: [] };
      case 'my-debates':
        return { title: 'Mes débats', options: [] };
      case 'profile':
        return { title: 'Profil', options: optionsProfile };
      case 'settings':
        return { title: 'Paramètres', options: optionsSettings };
      default:
    }
  };

  const renderUserMobile = () => {
    if (type === 'dashboard') {
      return (
        <MobileMenu title="Tableau de bord" options={[]}>
          <div className="space-20"></div>
          <Dashboard data={data} />
        </MobileMenu>
      );
    }
    if (type === 'my-debates') {
      return (
        <MobileMenu title="Mes débats" options={[]}>
          <div className="space-20"></div>
          <MyDebates participants={data} />
        </MobileMenu>
      );
    }
    return <MobileMenu title={getMobileData().title} options={getMobileData().options} />;
  };

  useEffect(() => {
    setWidth(size.width);
  }, [size]);

  return (
    <div>
      {width < 650 ? (
        renderUserMobile()
      ) : (
        <Fragment>
          <NextSeo noindex />
          <PageMenu items={itemsMenuMyAccount} className="block" active={type} />
          {renderUserComponent()}
        </Fragment>
      )}
    </div>
  );
};

User.propTypes = {
  data: PropTypes.any,
  type: PropTypes.string,
};

export default User;
