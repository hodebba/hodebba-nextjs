import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';

import DebatePreview from '../../../components/DebatePreview';
import styles from './my-debates.module.scss';

const MyDebates = ({ participants }) => {
  const renderDebates = () => {
    const ret = [];
    participants.forEach(participant => {
      ret.push(
        <DebatePreview key={participant.debate.id} debate={participant.debate} theme={participant.debate.theme} />,
      );
    });
    return ret;
  };

  return (
    <div className={styles.container}>
      <div className={styles['debates-container']}>{renderDebates()}</div>
      <div>{isEmpty(participants) && <p>Vous ne participez à aucun débat privée pour le moment.</p>}</div>
    </div>
  );
};

MyDebates.propTypes = {
  participants: PropTypes.array.isRequired,
};

export default MyDebates;
