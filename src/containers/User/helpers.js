import BioIcon from '../../../public/static/icons/bio.svg';
import PasswordIcon from '../../../public/static/icons/lock.svg';
import EmailIcon from '../../../public/static/icons/mail.svg';
import MapIcon from '../../../public/static/icons/map.svg';
import ProfileImageIcon from '../../../public/static/icons/profile-image.svg';
import ShareIcon from '../../../public/static/icons/share.svg';
import TrashIcon from '../../../public/static/icons/trash.svg';
import UserIcon from '../../../public/static/icons/user.svg';
import routes from '../../utils/routes';

export const optionsSettings = [
  {
    label: 'Changer mon mot de passe',
    icon: <PasswordIcon />,
    link: routes.User.pathPassword,
  },
  {
    label: 'Changer mon email',
    icon: <EmailIcon />,
    link: routes.User.pathEmail,
  },
  {
    label: 'Supprimer mon compte',
    icon: <TrashIcon />,
    link: routes.User.pathDeleteAccount,
  },
];

export const optionsProfile = [
  {
    label: 'Image de profil',
    icon: <ProfileImageIcon />,
    link: routes.User.pathImage,
  },
  {
    label: 'Pseudo',
    icon: <UserIcon />,
    link: routes.User.pathPseudo,
  },
  {
    label: 'Biographie',
    icon: <BioIcon />,
    link: routes.User.pathBio,
  },
  {
    label: 'Localisation',
    icon: <MapIcon />,
    link: routes.User.pathLocation,
  },
  {
    label: 'Réseaux sociaux',
    icon: <ShareIcon />,
    link: routes.User.pathSocialNetworks,
  },
];
