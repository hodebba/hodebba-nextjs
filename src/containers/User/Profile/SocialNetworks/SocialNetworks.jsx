/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-param-reassign */
/* eslint-disable operator-linebreak */

import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { Field, reduxForm } from 'redux-form';

import BlogIcon from '../../../../../public/static/icons/blog.svg';
import FacebookIcon from '../../../../../public/static/icons/facebook.svg';
import InstagramIcon from '../../../../../public/static/icons/instagram.svg';
import TelegramIcon from '../../../../../public/static/icons/telegram.svg';
import TwitterIcon from '../../../../../public/static/icons/twitter.svg';
import { clearUpdateUserError, sendNotification } from '../../../../actions/user.actions';
import FilterDropdown from '../../../../components/FilterDropdown';
import InfoModal from '../../../../components/InfoModal/InfoModal';
import RenderTextField from '../../../../components/RenderTextField';
import SubmitButton from '../../../../components/SubmitButton';
import { maxLength90Link, required, trim, validURL } from '../../../../utils/check-fields.helper';
import styles from './social-networks.module.scss';

const emptySocialNetworks = [
  {
    id: 0,
    name: 'Twitter',
    link: '',
    icon: TwitterIcon,
  },
  {
    id: 1,
    name: 'Blog',
    link: '',
    icon: BlogIcon,
  },
  {
    id: 2,
    name: 'Facebook',
    link: '',
    icon: FacebookIcon,
  },
  {
    id: 3,
    name: 'Instagram',
    link: '',
    icon: InstagramIcon,
  },
  {
    id: 4,
    name: 'Telegram',
    link: '',
    icon: TelegramIcon,
  },
];

const getNewSocialNetworksList = oldSocialNetworks => {
  return oldSocialNetworks
    .filter(socialnetwork => socialnetwork.link === '')
    .map(obj => obj.name)
    .map(element => {
      return { value: element, label: element };
    });
};

const SocialNetworks = ({ handleSubmit, submitting, initialize }) => {
  const socialNetworksLinks = useSelector(state => state.user.local.user.socials) || [];
  const loading = useSelector(state => state.user.local.loading.updateUser);
  const errors = useSelector(state => state.user.local.errors.updateUser);
  const field = useSelector(state => state.user.local.updatingUser.field);

  const [error, setError] = useState('');
  const [socialNetworks, setSocialNetworks] = useState(emptySocialNetworks);
  const notification = useSelector(state => state.user.local.notification);
  const dispatch = useDispatch();

  useEffect(() => {
    setSocialNetworks(
      emptySocialNetworks.map(element => {
        element.link = socialNetworksLinks[element.name] || '';
        return element;
      }),
    );
    const initiateValues = {};
    socialNetworks.map(element => (initiateValues[element.name] = element.link));
    initialize(initiateValues);
  }, [initialize]);

  const [displaySaveButton, setdisplaySaveButton] = useState(false);
  const [displayModal, setDisplayModal] = useState(false);

  // List of social newtorks already related to this profile
  const [socialNetworksList, setSocialNetworksList] = useState(socialNetworks);

  // List of other social newtorks which can be added to this profile
  const [newSocialNetworksList, setNewSocialNetworksList] = useState(getNewSocialNetworksList(socialNetworks));

  // Value of the dropdown which allow to select a social network
  const [newSocialNetwork, setNewSocialNetwork] = useState(newSocialNetworksList[0]);

  useEffect(() => {
    setNewSocialNetwork(newSocialNetworksList[0]);
  }, [newSocialNetworksList]);

  useEffect(() => {
    setNewSocialNetworksList(getNewSocialNetworksList(socialNetworksList));
  }, [socialNetworksList]);

  const handleAddLink = () => {
    const newArr = [...socialNetworksList];
    newArr.forEach(element => {
      if (element.name === newSocialNetwork.value) {
        element.link = 'https://';
      }
    });
    setSocialNetworksList(newArr);
    setDisplayModal(false);
    const initiateValues = {};
    socialNetworksList.map(element => (initiateValues[element.name] = element.link));
    initialize(initiateValues);
  };

  const handleDeleteClick = e => {
    const name = e.target.getAttribute('name');
    const newArr = [...socialNetworksList];
    newArr.map(socialnetwork => {
      if (socialnetwork.name === name) {
        socialnetwork.link = '';
      }
      return socialnetwork;
    });
    setSocialNetworksList(newArr);
    setdisplaySaveButton(true);
    const initiateValues = {};
    newArr.map(element => (initiateValues[element.name] = element.link));
    initialize(initiateValues);
  };

  useEffect(() => {
    if (notification && notification === 'social-networks') {
      toast.success('Vos réseaux sociaux ont été mise à jour avec succès !');
      dispatch(sendNotification(null));
    }
  }, [notification]);

  useEffect(() => {
    if (!isEmpty(errors)) {
      setError(errors.message);
    } else {
      setError('');
    }
  }, [errors]);

  useEffect(() => {
    dispatch(clearUpdateUserError());
  }, []);

  return (
    <div className={styles.container}>
      {!isMobileOnly && <h2>Réseaux sociaux</h2>}
      <form onSubmit={handleSubmit}>
        {socialNetworksList
          .filter(socialnetwork => socialnetwork.link !== '')
          .map(element => (
            <div className={styles.item} key={element.id}>
              <Field
                name={element.name}
                type="text"
                withIcon
                icon={element.icon}
                component={RenderTextField}
                label={element.name}
                onChange={() => setdisplaySaveButton(true)}
                validate={[required, maxLength90Link, validURL]}
                normalize={trim}
              />
              <button
                type="button"
                name={element.name}
                onClick={handleDeleteClick}
                className="text-button text-button-cancel"
              >
                Supprimer
              </button>
            </div>
          ))}

        {socialNetworksList.filter(socialnetwork => socialnetwork.link && socialnetwork.link !== '').length === 0 && (
          <p className={styles['no-social-networks']}>
            Aucun réseau social n'est associé à votre profil pour l'instant.
          </p>
        )}

        {error !== '' && field === 'social-networks' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}
        {displaySaveButton && (
          <SubmitButton
            label="Enregistrer les modifications"
            submitting={submitting}
            loading={loading && field === 'social-networks'}
          />
        )}
      </form>

      {/* Not display add button if all possible social newtorks have been added */}
      {newSocialNetworksList.length > 0 && (
        <button
          type="button"
          onClick={() => setDisplayModal(true)}
          className={`secondary-button ${styles['add-button']}`}
        >
          Ajouter un réseau social
        </button>
      )}

      {displayModal && (
        <InfoModal onClose={() => setDisplayModal(false)}>
          <div className={styles.modal}>
            <h3>Sélectionner un réseau social</h3>
            <FilterDropdown
              onChange={value => setNewSocialNetwork(value)}
              options={newSocialNetworksList}
              defaultValue={newSocialNetworksList[0]}
            />
            <SubmitButton label="Ajouter" submitting={submitting} onClick={handleAddLink} />
          </div>
        </InfoModal>
      )}
    </div>
  );
};
SocialNetworks.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  initialize: PropTypes.any,
};

export default reduxForm({
  form: 'SocialNetworks',
})(SocialNetworks);
