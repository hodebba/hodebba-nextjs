/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import cloneDeep from 'lodash/cloneDeep';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { mockStore } from '../../../../store';
import SocialNetworks from './index';

const state = cloneDeep(mockStore.getState());
state.user.local.user.socials = {};
const mockstore2 = configureMockStore([]);

const store = mockstore2(() => state);

const getMountComponent = () => {
  return mount(
    <Provider store={store}>
      <SocialNetworks />
    </Provider>,
  );
};
describe('SocialNetworks Component', () => {
  it('should render without crashing', () => {
    /* ReactDom.createPortal = jest.fn(modal => modal);
    const wrapper = getMountComponent(); */
  });
});
