/* eslint-disable no-unused-vars */
import { mount } from 'enzyme';
import cloneDeep from 'lodash/cloneDeep';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { mockStore } from '../../../store';
import Profile from './index';

const state = cloneDeep(mockStore.getState());
state.user.local.user.socials = {};
const mockstore2 = configureMockStore([]);

const store = mockstore2(() => state);

const getMountComponent = () => {
  return mount(
    <Provider store={store}>
      <Profile />
    </Provider>,
  );
};

describe('Profile Component', () => {
  ReactDOM.createPortal = jest.fn(modal => modal);

  it('should render without crashing', () => {
    /* const wrapper = getMountComponent(); */
  });
});
