/* eslint-disable no-shadow */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable prefer-destructuring */
/* eslint-disable operator-linebreak */
import { Image, Transformation } from 'cloudinary-react';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';

import PencilIcon from '../../../../public/static/icons/pencil.svg';
import cloudinaryFr from '../../../../public/static/locales/coudinary-fr.json';
import { updateUser } from '../../../actions/user.actions';
import InfoModal from '../../../components/InfoModal';
import { cloudinaryConfig } from '../../../utils/cloudinary-config';
import styles from './profile.module.scss';
import SocialNetworks from './SocialNetworks';
import UpdateProfile from './UpdateProfile';

const Profile = () => {
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const notification = useSelector(state => state.user.local.notification);
  const dispatch = useDispatch();
  const router = useRouter();
  const type = isMobileOnly && router.pathname.split('/my-account/')[1];
  const [errorUpload, setErrorUpload] = useState('');
  const [modalIsOpened, setModalIsOpened] = useState(false);
  const [myWidget, setMyWidget] = useState(null);

  useEffect(() => {
    // eslint-disable-next-line no-undef
    const widget = cloudinary.createUploadWidget(
      cloudinaryConfig(process.env.NEXT_PUBLIC_CLOUDINARY_PROFILE_PRESET, cloudinaryFr),
      (errorUpload, result) => {
        if (result.event === 'success') {
          const newUser = {
            id: user.id,
            imageLink: result.info.public_id,
          };
          dispatch(updateUser(newUser, token, 'imageLink'));
        }
        if (errorUpload) {
          setErrorUpload(
            "L'envoi de votre image a échoué. Nous n'acceptons que les formats png, jpg et jpeg, et votre fichier ne doit pas dépasser 500kB. Si votre image ne respecte pas ces critères, merci de procéder à une conversion ou utilisez une autre image",
          );
        }
      },
    );
    setMyWidget(widget);
  }, [token]);

  useEffect(() => {
    if (notification === 'imageLink') {
      setTimeout(() => {
        document.body.style.overflow = 'initial';
      }, 1000);
    }
  }, [notification]);

  const handleSubmitSocialNetworks = values => {
    if (checkUserEnabled()) {
      const newUser = {
        id: user.id,
        socials: values,
      };
      dispatch(updateUser(newUser, token, 'social-networks'));
    }
  };

  const handleSubmitProfile = ({ location, pseudo, bio }) => {
    if (checkUserEnabled()) {
      const newUser = {
        id: user.id,
        ...(pseudo === null ? null : { username: pseudo }),
        ...(location === null ? null : { location }),
        ...(bio === null ? null : { description: bio }),
      };
      dispatch(updateUser(newUser, token, 'profile'));
    }
  };

  const checkUserEnabled = () => {
    if (!user.enabled) {
      setModalIsOpened(true);
      return false;
    }
    return true;
  };

  return (
    <div className={styles.container}>
      <div>
        {!isMobileOnly && <h1>Profil utilisateur</h1>}

        <div className={styles.layout}>
          {(!isMobileOnly || (isMobileOnly && type.includes('image'))) && (
            <div className={styles.picture}>
              <div
                role="button"
                className={styles.picture__edit}
                onClick={() => {
                  if (checkUserEnabled()) {
                    myWidget.open();
                  }
                }}
              >
                <PencilIcon />
              </div>
              <Image publicId={user.imageLink} alt="votre photo de profil" secure="true">
                <Transformation crop="fill" gravity="faces" />
              </Image>
              {errorUpload !== '' && (
                <div className="error-message">
                  <div className="error-message__title">Merci de réessayer</div>
                  <p>{errorUpload}</p>
                </div>
              )}
            </div>
          )}

          <div className={styles.data}>
            {(!isMobileOnly || (isMobileOnly && !type.includes('social-networks') && !type.includes('image'))) && (
              <UpdateProfile
                onSubmit={handleSubmitProfile}
                initialValues={{ pseudo: user.username, location: user.location, bio: user.description }}
              />
            )}
            {(!isMobileOnly || (isMobileOnly && type.includes('social-networks'))) && (
              <SocialNetworks onSubmit={handleSubmitSocialNetworks} />
            )}
          </div>
        </div>
      </div>
      {modalIsOpened && (
        <InfoModal onClose={() => setModalIsOpened(false)}>
          Votre compte n'est pas activé, merci de cliquer sur le lien qui vous a été envoyé par email. Vous pourrez
          ensuite intéragir sur le site.
        </InfoModal>
      )}
    </div>
  );
};

export default Profile;
