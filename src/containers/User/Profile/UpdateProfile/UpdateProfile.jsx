/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable implicit-arrow-linebreak */

import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { Field, reduxForm } from 'redux-form';

import { clearUpdateUserError, sendNotification } from '../../../../actions/user.actions';
import RenderTextAreaField from '../../../../components/RenderTextAreaField';
import RenderTextField from '../../../../components/RenderTextField';
import SubmitButton from '../../../../components/SubmitButton';
import { maxLength50Pseudo, required, trim } from '../../../../utils/check-fields.helper';
import styles from './update-profile.module.scss';

const UpdateProfile = ({ handleSubmit, submitting }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const type = isMobileOnly && router.pathname.split('/my-account/profile/')[1];
  const loading = useSelector(state => state.user.local.loading.updateUser);
  const errors = useSelector(state => state.user.local.errors.updateUser);
  const field = useSelector(state => state.user.local.updatingUser.field);
  const notification = useSelector(state => state.user.local.notification);
  const [error, setError] = useState('');

  useEffect(() => {
    dispatch(clearUpdateUserError());
  }, []);

  useEffect(() => {
    if (!isEmpty(errors)) {
      setError(errors.message);
    } else {
      setError('');
    }
  }, [errors]);

  useEffect(() => {
    if (notification && notification !== 'social-networks') {
      if (field === 'profile') {
        toast.success('Votre profil a été mise à jour avec succès !');
      }
      dispatch(sendNotification(null));
    }
  }, [notification]);

  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      {(!isMobileOnly || (isMobileOnly && type === 'pseudo')) && (
        <Field
          name="pseudo"
          type="text"
          component={RenderTextField}
          label="Pseudo"
          validate={[required, maxLength50Pseudo]}
          normalize={trim}
        />
      )}

      {(!isMobileOnly || (isMobileOnly && type === 'location')) && (
        <Field name="location" type="text" component={RenderTextField} label="Localisation" />
      )}

      {(!isMobileOnly || (isMobileOnly && type === 'bio')) && (
        <Field name="bio" type="text-area" maxCharacters={250} component={RenderTextAreaField} label="Biographie" />
      )}

      {error !== '' && field === 'profile' && (
        <div className="error-message">
          <div className="error-message__title">Merci de réessayer</div>
          <p>{error}</p>
        </div>
      )}
      <div>
        <SubmitButton label="Enregistrer les modifications" submitting={submitting} loading={loading} />
      </div>
    </form>
  );
};

UpdateProfile.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'UpdateProfile',
})(UpdateProfile);
