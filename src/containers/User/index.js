import isConnected from '../../HOCs/isConnected.hoc';
import User from './User';

export default isConnected(User);
