/* eslint-disable object-curly-newline */
/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable implicit-arrow-linebreak */

import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { Fragment, useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { savePassword, sendNotification } from '../../../actions/user.actions';
import PasswordHelper from '../../../components/PasswordHelper';
import RenderTextField from '../../../components/RenderTextField';
import SubmitButton from '../../../components/SubmitButton';
import {
  hasALowercase,
  hasANumber,
  hasAnUppercase,
  hasASpecialSigns,
  matchPassword1,
  maxLength50Pwd,
  minLength8,
  required,
  trim,
} from '../../../utils/check-fields.helper';
import Error from '../../Error';
import styles from './save-password.module.scss';

const SavePassword = ({ handleSubmit, submitting, token, invalidToken }) => {
  const errors = useSelector(state => state.user.local.errors.savePassword);
  const loading = useSelector(state => state.user.local.loading.savePassword);
  const openModal = useSelector(state => state.authentification.local.modal.isOpen);
  const [error, setError] = useState('');
  const [displayHelpPwd, setDisplayHelpPwd] = useState(false);
  const dispatch = useDispatch(null);
  const router = useRouter();

  useEffect(() => {
    if (openModal) {
      dispatch(sendNotification('Mot de passe mise à jour avec succès!'));
      router.push('/');
    }
  }, [openModal]);

  const passwordIsOk = useCallback(value => {
    if (value && /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,50}$/g.test(value)) {
      setDisplayHelpPwd(false);
    }
  }, []);

  const submit = values => {
    const data = {
      newPassword: values.password,
      matchingPassword: values.matchingPassword,
      token,
    };
    dispatch(savePassword(data));
  };

  useEffect(() => {
    if (!isEmpty(errors)) {
      if (errors.status === 400) {
        const { codes } = errors.errors[0];
        if (codes.includes('ValidPassword')) {
          setError(
            'Le mot de passe est invalide. Il doit contenir au moins 1 lettre majuscule, 1 lettre minuscule, 1 chiffre, 1 caractère spéciale, et compter au moins 8 caractères.',
          );
        } else if (codes.includes('PasswordMatches')) {
          setError('Les mots de passe ne correspondent pas.');
        } else if (codes.includes('NotNull')) {
          setError('Tous les champs doivent être remplis');
        }
      } else {
        setError(errors.message);
      }
    }
  }, [errors]);

  const renderForm = () => {
    return (
      <form className={styles.container} onSubmit={handleSubmit(submit)}>
        <NextSeo noindex />
        {!invalidToken && (
          <Fragment>
            <h2>Enregistrer un nouveau mot de passe</h2>
            <Field
              name="password"
              type="password"
              isPassword
              checkEnabled
              component={RenderTextField}
              label="Nouveau mot de passe"
              onFocus={() => setDisplayHelpPwd(true)}
              onBlur={() => setDisplayHelpPwd(false)}
              validate={[
                required,
                hasAnUppercase,
                hasALowercase,
                hasANumber,
                hasASpecialSigns,
                minLength8,
                passwordIsOk,
                maxLength50Pwd,
              ]}
              normalize={trim}
            />
            {displayHelpPwd && <PasswordHelper />}
            <Field
              name="matchingPassword"
              checkEnabled
              type="password"
              isPassword
              component={RenderTextField}
              label="Confirmer nouveau mot de passe"
              validate={[required, matchPassword1]}
              normalize={trim}
            />
            <div>
              {error !== '' && (
                <div className="error-message">
                  <div className="error-message__title">Merci de réessayer</div>
                  <p>{error}</p>
                </div>
              )}
              <SubmitButton label="Enregistrer" submitting={submitting} loading={loading} />
            </div>
          </Fragment>
        )}
      </form>
    );
  };

  return !invalidToken ? renderForm() : <Error code={404} />;
};

SavePassword.propTypes = {
  submitting: PropTypes.bool,
  handleSubmit: PropTypes.func,
  token: PropTypes.string.isRequired,
  invalidToken: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'SavePassword',
})(SavePassword);
