/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import { mockStore } from '../../../store';
import SavePassword from './index';

const location = {
  search: {
    token: '856b811b-856f-4c2b-a2b7-b811505b9dd0',
  },
};

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <SavePassword location={location} />
    </Provider>,
  );
};
describe('SavePassword Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
});
