/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react-hooks/exhaustive-deps */
import { getDebateRootPath } from '@Utils/debate.helper';
import isUndefined from 'lodash/isUndefined';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';

import ArgumentIcon from '../../../../public/static/icons/argument.svg';
import ContributionIcon from '../../../../public/static/icons/create.svg';
import DislikeIcon from '../../../../public/static/icons/dislike.svg';
import LikeIcon from '../../../../public/static/icons/like.svg';
import Message from '../../../components/Message';
import { kFormatter } from '../../../utils/general.helper';
import styles from './dashboard.module.scss';

const Dashboard = ({ data }) => {
  const dashboard = data;
  const [userArguments, setUserArguments] = useState([]);
  const [userTestimonials, setUserTestimonials] = useState([]);
  const [maxArguments, setMaxArguments] = useState(3);
  const [maxTestimonials, setMaxTestimonials] = useState(3);
  const renderDisplayMoreButton = onClick => {
    return (
      <button type="button" className="secondary-button" onClick={onClick}>
        Afficher plus
      </button>
    );
  };

  const renderArguments = () => {
    const ret = [];
    for (let i = 0; i < Math.min(userArguments.length, maxArguments); i += 1) {
      ret.push(
        <Fragment key={userArguments[i].id}>
          <div className={styles.list__debate}>
            Débat associé:{' '}
            <Link
              href={`${getDebateRootPath(userArguments[i].debate)}/[id]/talk`}
              as={`${getDebateRootPath(userArguments[i].debate)}/${userArguments[i].debate.id}/talk`}
            >
              <a>{userArguments[i].debate.title}</a>
            </Link>
          </div>
          <Link
            href={`${getDebateRootPath(userArguments[i].debate)}/[id]/talk/argument/[comment_id]`}
            as={`${getDebateRootPath(userArguments[i].debate)}/${userArguments[i].debate.id}/talk/argument/${
              userArguments[i].id
            }`}
          >
            <a>
              <Message isArgument data={userArguments[i]} debate={userArguments[i].debate} />
            </a>
          </Link>
        </Fragment>,
      );
    }
    if (userArguments.length > maxArguments) {
      ret.push(
        <div key={-1} className={styles['display-more']}>
          {renderDisplayMoreButton(() => setMaxArguments(maxArgumentsOld => maxArgumentsOld + 3))}
        </div>,
      );
    }
    if (userArguments.length === 0) {
      ret.push(<p key={1}>Vous n'avez ajouté aucun argument pour le moment.</p>);
    }

    return ret;
  };

  const renderTestimonials = () => {
    const ret = [];
    for (let i = 0; i < Math.min(userTestimonials.length, maxTestimonials); i += 1) {
      ret.push(
        <Fragment key={userTestimonials[i].id}>
          <div className={styles.list__debate}>
            Débat associé:{' '}
            <Link
              href={`${getDebateRootPath(userTestimonials[i].debate)}/[id]/talk`}
              as={`${getDebateRootPath(userTestimonials[i].debate)}/${userTestimonials[i].debate.id}/talk`}
            >
              <a>{userTestimonials[i].debate.title}</a>
            </Link>
          </div>
          <Link
            href={`${getDebateRootPath(userTestimonials[i].debate)}/[id]/testimonial/[testimonial_id]`}
            as={`${getDebateRootPath(userTestimonials[i].debate)}/${userTestimonials[i].debate.id}/testimonial/${
              userTestimonials[i].id
            }`}
          >
            <a>
              <Message
                isArgument
                data={userTestimonials[i]}
                className="testimonial"
                debate={userTestimonials[i].debate}
              />
            </a>
          </Link>
        </Fragment>,
      );
    }
    if (userTestimonials.length > maxTestimonials) {
      ret.push(
        <div key={maxTestimonials + 1} className={styles['display-more']}>
          {renderDisplayMoreButton(() => setMaxTestimonials(maxTestimonialsOld => maxTestimonialsOld + 3))}
        </div>,
      );
    }

    if (userTestimonials.length === 0) {
      ret.push(<p key={1}>Vous n'avez ajouté aucun témoignage pour le moment.</p>);
    }

    return ret;
  };

  useEffect(() => {
    const argumentList = [];
    const testimonials = [];

    if (dashboard.posts) {
      dashboard.posts.forEach(post => {
        if (!post.postType && isUndefined(post.isLastLevel)) {
          testimonials.push(post);
        } else if (isUndefined(post.isLastLevel)) {
          argumentList.push(post);
        }
      });
    }
    setUserArguments(argumentList);
    setUserTestimonials(testimonials);
  }, [dashboard]);

  return (
    <div className={styles.container}>
      <div>
        <h1>Tableau de bord</h1>

        <div className={styles.overview}>
          <h2>Bilan</h2>
          <div className={styles.numbers}>
            <div className={styles.number}>
              <p>Contributions</p>
              <div className={styles.number__bottom}>
                {dashboard.posts
                  ? kFormatter(dashboard.posts.length + dashboard.nbStatements)
                  : kFormatter(dashboard.nbStatements || 0)}
                <span className={styles.number__contributions}>
                  <ContributionIcon />
                </span>
              </div>
            </div>
            <div className={styles.number}>
              <p>Arguments Postés</p>
              <div className={styles.number__bottom}>
                {kFormatter(userArguments.length)}
                <span className={styles.number__arguments}>
                  <ArgumentIcon />
                </span>
              </div>
            </div>
            <div className={styles.number}>
              <p>J'aime reçues</p>
              <div className={styles.number__bottom}>
                {dashboard.totalLikes ? kFormatter(dashboard.totalLikes) : 0}
                <span className={styles.number__likes}>
                  <LikeIcon />
                </span>
              </div>
            </div>
            <div className={styles.number}>
              <p>J'aime pas reçues</p>
              <div className={styles.number__bottom}>
                {dashboard.totalDislikes ? kFormatter(dashboard.totalDislikes) : 0}
                <span className={styles.number__dislikes}>
                  <DislikeIcon />
                </span>
              </div>
            </div>
          </div>
        </div>

        <div className={styles.arguments}>
          <h2>Mes arguments</h2>
          <div className={styles.list}>{renderArguments()}</div>
        </div>

        <div className={styles.testimonials}>
          <h2>Mes témoignages</h2>
          <div className={styles.list}>{renderTestimonials()}</div>
        </div>
      </div>
    </div>
  );
};

Dashboard.propTypes = {
  data: PropTypes.object,
};

export default Dashboard;
