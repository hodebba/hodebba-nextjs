/* eslint-disable no-unused-vars */
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import nextUserRouterMock from '../../../../test/nextUserRouterMock';
import { mockStore } from '../../../store';
import Settings from './index';

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <Settings />
    </Provider>,
  );
};

describe('Settings Component', () => {
  nextUserRouterMock({
    route: '/my-account/settings/email',
    pathname: '/my-account/settings/email',
    query: '',
    asPath: '',
  });
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
});
