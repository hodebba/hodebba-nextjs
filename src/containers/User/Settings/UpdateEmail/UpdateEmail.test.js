/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import { mockStore } from '../../../../store';
import UpdateEmail from './index';

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <UpdateEmail />
    </Provider>,
  );
};
describe('UpdateEmail Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
  it('should render 2 inputs: oldEmail and newEmail', () => {
    const wrapper = getMountComponent();
    const receivedNamesList = wrapper
      .find('form')
      .children()
      .map(node => node.props().name);

    const expectedNamesList = [undefined, 'oldEmail', 'newEmail', undefined];
    expect(receivedNamesList).toEqual(expect.arrayContaining(expectedNamesList));
  });
});
