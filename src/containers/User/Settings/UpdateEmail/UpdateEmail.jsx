/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable implicit-arrow-linebreak */

import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { clearUpdateEmailError } from '../../../../actions/user.actions';
import RenderTextField from '../../../../components/RenderTextField';
import SubmitButton from '../../../../components/SubmitButton';
import { email, required, trim } from '../../../../utils/check-fields.helper';

const UpdateEmail = ({ handleSubmit, submitting }) => {
  const errors = useSelector(state => state.user.local.errors.updateEmail);
  const dispatch = useDispatch(null);
  const [error, setError] = useState('');

  useEffect(() => {
    if (!isEmpty(errors)) {
      setError("L'adresse email est invalide.");
    } else {
      setError('');
    }
  }, [errors]);

  useEffect(() => {
    return () => {
      setError('');
      dispatch(clearUpdateEmailError());
    };
  }, []);

  return (
    <form className="update-email" onSubmit={handleSubmit}>
      {!isMobileOnly && <h2>Changer votre email</h2>}

      <Field
        name="oldEmail"
        type="email"
        checkEnabled
        component={RenderTextField}
        label="Email actuel"
        validate={[required, email]}
        normalize={trim}
      />

      <Field
        name="newEmail"
        type="email"
        checkEnabled
        component={RenderTextField}
        label="Nouvel email"
        validate={[required, email]}
        normalize={trim}
      />

      <div>
        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}
        <SubmitButton label="Enregistrer" submitting={submitting} />
      </div>
    </form>
  );
};

UpdateEmail.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'UpdateEmail',
})(UpdateEmail);
