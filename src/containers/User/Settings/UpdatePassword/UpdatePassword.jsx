/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable implicit-arrow-linebreak */

import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useCallback, useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { clearUpdatePasswordError } from '../../../../actions/user.actions';
import PasswordHelper from '../../../../components/PasswordHelper';
import RenderTextField from '../../../../components/RenderTextField';
import SubmitButton from '../../../../components/SubmitButton';
import {
  hasALowercase,
  hasANumber,
  hasAnUppercase,
  hasASpecialSigns,
  matchPassword2,
  maxLength50Pwd,
  minLength8,
  required,
  trim,
} from '../../../../utils/check-fields.helper';

const UpdatePassword = ({ handleSubmit, submitting }) => {
  const [displayHelpPwd, setDisplayHelpPwd] = useState(false);
  const errors = useSelector(state => state.user.local.errors.updatePassword);
  const [error, setError] = useState('');
  const dispatch = useDispatch();

  const passwordIsOk = useCallback(value => {
    if (value && /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,50}$/g.test(value)) {
      setDisplayHelpPwd(false);
    }
  }, []);

  useEffect(() => {
    if (!isEmpty(errors)) {
      if (errors.status === 400) {
        const { codes } = errors.errors[0];
        if (codes.includes('ValidPassword')) {
          setError(
            'Le mot de passe est invalide. Il doit contenir au moins 1 lettre majuscule, 1 lettre minuscule, 1 chiffre, 1 caractère spéciale, et compter au moins 8 caractères.',
          );
        } else if (codes.includes('PasswordMatches')) {
          setError('Les mots de passe ne correspondent pas.');
        } else if (codes.includes('NotNull')) {
          setError('Tous les champs doivent être remplis');
        }
      } else {
        setError(errors.message);
      }
    } else {
      setError('');
    }
  }, [errors]);

  useEffect(() => {
    return () => {
      setError('');
      dispatch(clearUpdatePasswordError());
    };
  }, []);

  return (
    <form className="update-password" onSubmit={handleSubmit}>
      {!isMobileOnly && <h2>Changer votre mot de passe</h2>}
      <Field
        name="oldPassword"
        type="password"
        isPassword
        checkEnabled
        component={RenderTextField}
        label="Ancien mot de passe"
        validate={[required]}
        normalize={trim}
      />
      <Field
        name="newPassword"
        type="password"
        isPassword
        checkEnabled
        component={RenderTextField}
        label="Nouveau mot de passe"
        onFocus={() => setDisplayHelpPwd(true)}
        onBlur={() => setDisplayHelpPwd(false)}
        validate={[
          passwordIsOk,
          required,
          hasAnUppercase,
          hasALowercase,
          hasANumber,
          hasASpecialSigns,
          minLength8,
          maxLength50Pwd,
        ]}
        normalize={trim}
      />

      {displayHelpPwd && <PasswordHelper />}

      <Field
        name="matchingPassword"
        checkEnabled
        type="password"
        component={RenderTextField}
        label="Confirmer nouveau mot de passe"
        validate={[required, matchPassword2]}
        normalize={trim}
      />

      <div>
        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}
        <SubmitButton label="Enregistrer" submitting={submitting} />
      </div>
    </form>
  );
};

UpdatePassword.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'UpdatePassword',
})(UpdatePassword);
