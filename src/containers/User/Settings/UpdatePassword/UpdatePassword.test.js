/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';

import { mockStore } from '../../../../store';
import UpdatePassword from './index';

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <UpdatePassword />
    </Provider>,
  );
};
describe('UpdatePassword Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
  it('should render 3 inputs: oldPassword,newPassword and confirmPassword', () => {
    const wrapper = getMountComponent();
    const receivedNamesList = wrapper
      .find('form')
      .children()
      .map(node => node.props().name);

    const expectedNamesList = [undefined, 'oldPassword', 'newPassword', 'matchingPassword', undefined];
    expect(receivedNamesList).toEqual(expect.arrayContaining(expectedNamesList));
  });
});
