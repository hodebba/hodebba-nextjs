/* eslint-disable react-hooks/exhaustive-deps */

import { isEmpty } from 'lodash';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import { toast, ToastContainer } from 'react-toastify';
import { reset } from 'redux-form';

import { deleteAccount, updateEmail, updatePassword } from '../../../actions/user.actions';
import InfoModal from '../../../components/InfoModal';
import styles from './settings.module.scss';
import UpdateEmail from './UpdateEmail';
import UpdatePassword from './UpdatePassword';

const Settings = () => {
  const router = useRouter();
  const type = isMobileOnly && router.pathname.split('/my-account/settings/')[1];
  const user = useSelector(state => state.user.local.user);
  const errors = useSelector(state => state.user.local.errors);
  const token = useSelector(state => state.user.local.token);
  const form = useSelector(state => state.form);
  const dispatch = useDispatch(null);
  const [modalDeleteIsOpen, setModalDeleteIsOpen] = useState(false);
  const [modalIsOpened, setModalIsOpened] = useState(false);

  useEffect(() => {
    if (form.UpdateEmail && form.UpdateEmail.submitSucceeded && errors.UpdateEmail === '' && !modalIsOpened) {
      dispatch(reset('UpdateEmail'));
      toast.success('Email mis à jour avec succès!');
    }
  }, [user.email]);

  useEffect(() => {
    if (form.UpdatePassword && form.UpdatePassword.submitSucceeded && errors.UpdateEmail === '' && !modalIsOpened) {
      dispatch(reset('UpdatePassword'));
      toast.success('Mot de passe mis à jour avec succès!');
    }
  }, [form.UpdatePassword]);

  useEffect(() => {
    if (isEmpty(token)) {
      router.push('/');
    }
  }, [token]);

  const checkUserEnabled = () => {
    if (!user.enabled) {
      setModalIsOpened(true);
      return false;
    }
    return true;
  };

  return (
    <div className={styles.container}>
      <div>
        {!isMobileOnly && <h1>Paramètres</h1>}

        <div className={styles['update-credentials']}>
          {(!isMobileOnly || (isMobileOnly && type === 'password')) && (
            <UpdatePassword
              onSubmit={values => {
                if (checkUserEnabled()) {
                  dispatch(updatePassword(values, user.id));
                }
              }}
            />
          )}
          {(!isMobileOnly || (isMobileOnly && type === 'email')) && (
            <UpdateEmail
              onSubmit={values => {
                if (checkUserEnabled()) {
                  dispatch(updateEmail(values.newEmail, user.id, token));
                }
              }}
              initialValues={{ oldEmail: user.email }}
              enableReinitialize
            />
          )}
        </div>

        {(!isMobileOnly || (isMobileOnly && type === 'delete-account')) && (
          <div className={styles['delete-account']}>
            {!isMobileOnly && <h2>Supprimer mon compte</h2>}
            <p>
              Vous vous apprêtez à lancer la procèdure de suppression de votre compte Hodebba. Votre nom utilisateur et
              votre profil public ne seront plus visibles. Vos publications seront conservés sous forme de publications
              anonymes. Certaines informations de compte peuvent encore être disponibles sur les moteurs de recherche.
              Si vous voulez simplement modifier votre pseudo, il n’est pas nécessaire de supprimer votre compte, vous
              pouvez simplement l’éditer dans{' '}
              <Link href="/my-account/profile">
                <a>Profil</a>
              </Link>
              .
            </p>
            <button type="button" className="cancel-button" onClick={() => setModalDeleteIsOpen(true)}>
              Je veux supprimer définitivement mon compte
            </button>
            {modalDeleteIsOpen && (
              <InfoModal onClose={() => setModalDeleteIsOpen(false)}>
                <div className={styles['delete-account__modal']}>
                  <p>Êtes-vous sûr de vouloir supprimer définitivement votre compte Hodebba?</p>
                  <button
                    type="button"
                    className="cancel-button"
                    onClick={() => dispatch(deleteAccount(user.id, token))}
                  >
                    Oui, je veux supprimer mon compte
                  </button>
                  <button type="button" className="secondary-button" onClick={() => setModalDeleteIsOpen(false)}>
                    Non, je souhaite conserver mon compte
                  </button>
                </div>
              </InfoModal>
            )}

            {modalIsOpened && (
              <InfoModal onClose={() => setModalIsOpened(false)}>
                Votre compte n'est pas activé, merci de cliquer sur le lien qui vous a été envoyé par email. Vous
                pourrez ensuite intéragir sur le site.
              </InfoModal>
            )}
          </div>
        )}
        <ToastContainer
          position="top-right"
          autoClose={4000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
    </div>
  );
};

export default Settings;
