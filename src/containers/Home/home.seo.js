import { NextSeo } from 'next-seo';

const HomeSeo = () => {
  return (
    <NextSeo
      title="Hodebba | Plateforme de débat en ligne"
      description="Découvrez des débats sur tous les sujets et les avis de ceux qui animent le débat public."
      openGraph={{
        url: process.env.NEXT_PUBLIC_FRONT_END_URL,
        type: 'website',
        title: 'Hodebba | Plateforme de débat en ligne',
        description: 'Découvrez des débats sur tous les sujets et les avis de ceux qui animent le débat public.',
        images: [
          {
            url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/hodebba-home_zphehh`,
            width: 1200,
            height: 630,
            alt: 'Hodebba logo',
          },
        ],
        site_name: 'Hodebba',
      }}
      twitter={{
        handle: '@handle',
        site: '@site',
        cardType: 'summary_large_image',
      }}
    />
  );
};

export default HomeSeo;
