/* eslint-disable react-hooks/exhaustive-deps */
import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { saveReturnPath, sendNotification } from '../../actions/user.actions';
import DebatePreview from '../../components/DebatePreview';
import * as gtag from '../../utils/gtags';
import DebatesCarousel from './DebatesCarousel';
import styles from './home.module.scss';
import HomeSeo from './home.seo';

const Home = ({ debates }) => {
  const notification = useSelector(state => state.user.local.notification);
  const isThemeListOpened = useSelector(state => state.user.local.isThemeListOpened);
  const [lastDebates, setLastDebates] = useState([]);
  const [themeDebates, setThemeDebates] = useState([]);

  const dispatch = useDispatch();
  const router = useRouter();

  themeDebates.sort((a, b) => b.length - a.length);

  useEffect(() => {
    const tempArray = [];
    Object.keys(debates).forEach(element => {
      if (element.includes('id=null')) {
        setLastDebates(debates[element]);
      } else {
        tempArray.push(debates[element]);
      }
    });
    setThemeDebates(tempArray);
  }, [debates]);

  useEffect(() => {
    if (notification) {
      toast.success(notification);
      dispatch(sendNotification(null));
    }
  }, [notification]);

  useEffect(() => {
    dispatch(saveReturnPath({ as: router.asPath, href: router.pathname }));
    gtag.event('conversion', { send_to: 'AW-480766229/ifWOCP_UhugBEJXSn-UB' });
  }, []);

  const renderVerticalDebates = () => {
    const verticalDebates = [];

    for (let i = 1; i < 5; i += 1) {
      verticalDebates.push(
        <DebatePreview
          key={lastDebates[i].id}
          className="vertical-debate last-debate"
          debate={lastDebates[i]}
          theme={lastDebates[i].theme}
        />,
      );
    }

    return verticalDebates;
  };

  const renderThemes = () => {
    const ret = [];
    themeDebates.forEach(element => {
      if (element.length > 0) {
        ret.push(<DebatesCarousel key={element[0].theme.id} debates={element} theme={element[0].theme} />);
      }
    });
    return ret;
  };

  if (isThemeListOpened) {
    return <div></div>;
  }

  return (
    <div className={styles.container}>
      <HomeSeo />

      <h1>Débats du moment</h1>

      <div className={styles['last-debates']}>
        {!isEmpty(lastDebates) && (
          <Fragment>
            <DebatePreview
              debate={lastDebates[0]}
              theme={lastDebates[0].theme}
              className="horizontal-debate last-debate"
            />
            {renderVerticalDebates()}
            <DebatePreview
              debate={lastDebates[5]}
              theme={lastDebates[5].theme}
              className="horizontal-debate last-debate"
            />
          </Fragment>
        )}
      </div>

      {renderThemes()}
    </div>
  );
};

Home.propTypes = {
  debates: PropTypes.object.isRequired,
};

export default Home;
