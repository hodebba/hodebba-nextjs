/* eslint-disable operator-linebreak */
import Link from 'next/link';
import PropTypes from 'prop-types';
import { withResizeDetector } from 'react-resize-detector';

import DebatePreview from '../../../components/DebatePreview';
import Carousel from '../Carousel';
import styles from './debates-carousel.module.scss';

const DebatesCarousel = ({ debates, theme, width }) => {
  const renderDebatePreview = () => {
    const tab = [];
    debates.forEach(element => {
      tab.push(<DebatePreview key={element.id} debate={element} theme={element.theme} />);
    });
    return tab;
  };

  const renderCarousel = () => {
    if (
      (debates.length < 4 && width > 850) ||
      (width < 850 && width > 450 && debates.length < 3) ||
      (width < 450 && debates.length < 2)
    ) {
      return <div className={styles.list}>{renderDebatePreview()}</div>;
    }
    return <Carousel>{renderDebatePreview()}</Carousel>;
  };

  return (
    <div className={styles.container}>
      <div className={styles.text}>
        <h2>{theme.label}</h2>
        <Link href="/theme/[id]" as={`theme/${theme.id}`}>
          <a>Voir plus</a>
        </Link>
      </div>
      {renderCarousel()}
    </div>
  );
};

DebatesCarousel.propTypes = {
  debates: PropTypes.array,
  theme: PropTypes.object,
  width: PropTypes.number,
};

export default withResizeDetector(DebatesCarousel);
