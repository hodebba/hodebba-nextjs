/* eslint-disable no-unused-vars */
import { shallow } from 'enzyme';

import DebateCarousel from './index';

const fakeDebates = [
  {
    id: 1,
    imageLink: 'mort',
    debateState: 'draft',
    title: 'Doit-on rétablir la peine de mort ?',
  },
];

const fakeTheme = {
  id: 1,
  label: 'theme',
};

describe('DebateCarousel Component', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(<DebateCarousel debates={fakeDebates} theme={fakeTheme} />);
  });
});
