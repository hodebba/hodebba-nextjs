import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

import chevronLeft from '@iconify/icons-vaadin/chevron-left';
import chevronRight from '@iconify/icons-vaadin/chevron-right';
import { Icon } from '@iconify/react';
import PropTypes from 'prop-types';
import { Fragment, useRef } from 'react';
import { isBrowser } from 'react-device-detect';
import Slider from 'react-slick';

import styles from './carousel.module.scss';

const Carousel = ({ children }) => {
  const slider = useRef(null);

  const settings = {
    centerPadding: '60px',
    className: 'debate-list',
    centerMode: true,
    dots: false,
    arrows: false,
    infinite: true,
    draggable: true,
    speed: 500,
    swipeToSlide: true,
    touchMove: true,
    swipe: true,
    initialSlide: 1,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 850,
        settings: {
          slidesToShow: 2,
          speed: 300,
        },
      },
      {
        breakpoint: 550,
        settings: {
          slidesToShow: 1,
          centerPadding: '40px',
          speed: 300,
          initialSlide: 0,
        },
      },
    ],
  };

  return (
    <div className={styles.container}>
      {isBrowser && (
        <Fragment>
          <button type="button" className={styles['left-button']} onClick={() => slider.current.slickPrev()}>
            <Icon icon={chevronLeft} color="#FFFFFF" width="25" height="25" />
          </button>
          <button type="button" className={styles['right-button']} onClick={() => slider.current.slickNext()}>
            <Icon icon={chevronRight} color="#FFFFFF" width="25" height="25" />
          </button>
        </Fragment>
      )}

      <Slider ref={slider} {...settings}>
        {children}
      </Slider>
    </div>
  );
};

export default Carousel;

Carousel.propTypes = {
  children: PropTypes.any,
};
