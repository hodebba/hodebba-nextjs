/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { getDebateRootPath } from '@Utils/debate.helper';
import { Image, Transformation } from 'cloudinary-react';
import Parser from 'html-react-parser';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect } from 'react';

import { formatDateLetters, kFormatter } from '../../../utils/general.helper';
import styles from './debate-search-preview.module.scss';

function DebateSearchPreview({ debate, id }) {
  const router = useRouter();

  useEffect(() => {
    router.prefetch(
      `${getDebateRootPath(debate)}/[id]/presentation`,
      `${getDebateRootPath(debate)}/${debate.id || id}/presentation`,
    );
  }, []);

  const getTotalVotes = voteTypes => {
    let total = 0;
    voteTypes.forEach(voteType => {
      total += voteType.count;
    });
    return total;
  };

  return (
    <div
      className={styles.container}
      onClick={() =>
        router.push(
          `${getDebateRootPath(debate)}/[id]/presentation`,
          `${getDebateRootPath(debate)}/${debate.id || id}/presentation`,
        )
      }
    >
      <div className={styles.picture}>
        <Image publicId={debate.imageLink} alt="image de fond du débat" secure="true">
          <Transformation crop="fill" gravity="faces" />
        </Image>
      </div>
      <div className={styles.info}>
        <div className={styles.info__theme}>{debate.theme.label || debate.theme}</div>
        <h2>{debate.title}</h2>
        <div className={styles.info__text}>{Parser(debate.presentation)}</div>
        <div className={styles.meta}>
          <p className={styles.meta__date}>{formatDateLetters(debate.createdDate)}</p>
          <div className={styles.meta__numbers}>
            <p>{kFormatter(debate.viewNumber)} vues</p>
            {debate.poll && <p>{kFormatter(getTotalVotes(debate.poll.pollVoteTypes))} votes</p>}
          </div>
        </div>
      </div>
    </div>
  );
}

DebateSearchPreview.propTypes = {
  debate: PropTypes.object,
  id: PropTypes.string,
};

export default DebateSearchPreview;
