/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withResizeDetector } from 'react-resize-detector';

import { clearSearch } from '../../actions/elasticSearch.actions';
import FilterDropdown from '../../components/FilterDropdown';
import { itemsMenuSearch } from '../../components/itemsMenu';
import PageMenu from '../../components/PageMenu';
import { preferencesBis, sortByNewestES, sortByOldestES } from '../../utils/general.helper';
import CelebritySearchPreview from './CelebritySearchPreview';
import DebateSearchPreview from './DebateSearchPreview';
import styles from './search.module.scss';

const Search = ({ width }) => {
  const lastResults = useSelector(state => state.search.data.lastResults);
  const lastQuery = useSelector(state => state.search.local.lastQuery);
  const returnPath = useSelector(state => state.search.local.returnPath);
  const [sortedResults, setSortedResults] = useState(lastResults.hits);
  const router = useRouter();
  const dispatch = useDispatch();
  const [filter, setFilter] = useState({ value: 'relevance' });

  const getActivePage = () => {
    if (isEmpty(router.query)) {
      return 'all';
    }
    return router.query.filter;
  };

  useEffect(() => {
    handleChangeFilter(filter);
  }, [lastResults]);

  useEffect(() => {
    return () => {
      dispatch(clearSearch());
    };
  }, []);

  const handleChangeFilter = newFilter => {
    setFilter(newFilter);
    switch (newFilter.value) {
      case 'recent':
        setSortedResults(cloneDeep(lastResults.hits).sort(sortByNewestES));
        break;
      case 'older':
        setSortedResults(cloneDeep(lastResults.hits).sort(sortByOldestES));
        break;
      case 'relevance':
        setSortedResults(cloneDeep(lastResults.hits));
        break;
      default:
    }
  };

  const renderNoResults = () => {
    return (
      <div key={-10} className={styles['no-results']}>
        <h2>Désolé, aucun résultat ne correspond à votre recherche</h2>
        <ul>
          <li>Merci de vérifier l'orthographe de votre recherche</li>
          <li>Essayer de rechercher des termes plus génériques</li>
        </ul>
      </div>
    );
  };

  const renderResults = () => {
    const ret = [];
    const activePage = getActivePage();
    if (sortedResults && !isEmpty(sortedResults) && lastResults.max_score > 0) {
      sortedResults.forEach(result => {
        if (result._index === 'debates' && (activePage === 'debates' || activePage === 'all')) {
          ret.push(<DebateSearchPreview key={`d${result._id}`} debate={result._source} id={result._id} />);
        } else if (result._index === 'celebrities' && (activePage === 'celebrities' || activePage === 'all')) {
          ret.push(<CelebritySearchPreview key={`c${result._id}`} celebrity={result._source} id={result._id} />);
        }
      });
    } else {
      ret.push(renderNoResults());
    }
    return ret;
  };

  return (
    <div className={styles.container}>
      <NextSeo noindex />
      <PageMenu
        items={itemsMenuSearch}
        className="block"
        active={getActivePage()}
        withBackButton={width > 950}
        returnPath={returnPath}
      />
      {lastResults.total && (
        <div className={styles['results-container']}>
          {width > 600 && <h1>Votre résultat de recherche pour "{lastQuery}"</h1>}

          <div className={styles.filter}>
            <FilterDropdown options={preferencesBis} defaultValue={preferencesBis[0]} onChange={handleChangeFilter} />
            <p>{lastResults.total.value} résultats</p>
          </div>

          <div className={styles.results}>{renderResults()}</div>
        </div>
      )}
    </div>
  );
};

Search.propTypes = {
  width: PropTypes.number,
};

export default withResizeDetector(Search);
