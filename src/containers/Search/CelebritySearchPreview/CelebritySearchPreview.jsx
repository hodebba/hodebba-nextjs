/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react-hooks/exhaustive-deps */
import { Image, Transformation } from 'cloudinary-react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import { getWikiDescription } from '../../../services/wikipedia.service';
import styles from './celebrity-search-preview.module.scss';

const CelebritySearchPreview = ({ celebrity, id }) => {
  const [wikiData, setWikiData] = useState({});
  const router = useRouter();
  useEffect(() => {
    router.prefetch('/celebrities/[id]/positions', `/celebrities/${parseInt(id, 10)}/positions`);
  }, []);

  const getWikiData = async () => {
    try {
      const response = await getWikiDescription(celebrity.fullName);
      setWikiData(response.data.query.pages[Object.keys(response.data.query.pages)[0]]);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getWikiData();
  }, []);

  return (
    <div
      className={styles.container}
      onClick={() => router.push('/celebrities/[id]/positions', `/celebrities/${id}/positions`)}
    >
      <div className={styles.picture}>
        <Image publicId={celebrity.imageLink} alt={`photo de ${celebrity.fullName}`} secure="true">
          <Transformation crop="fill" gravity="faces" />
        </Image>
      </div>
      <div className={styles.info}>
        <h2>{celebrity.fullName}</h2>
        <div className={styles.info__profession}>{wikiData.description || ''}</div>
        <p className={styles.info__description}>{wikiData.extract || ''}</p>
      </div>
    </div>
  );
};

CelebritySearchPreview.propTypes = {
  celebrity: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
};

export default CelebritySearchPreview;
