import LayoutArticle from '../LayoutArticle/LayoutArticle';
import styles from './article-create-post.module.scss';

const ArticleCreateDebate = () => {
  return (
    <LayoutArticle active="create-post">
      <h1>Quelles sont les recommandations principales pour publier des arguments ?</h1>
      <p>
        Hodebba souhaite que chaque débat comprenne l’ensemble des arguments qu’on retrouve, dans notre société, sur un
        sujet donné. L’objectif étant que chacun dispose des armes nécessaires pour se forger un avis.
      </p>
      <div className={styles.statement}>Chaque argument doit être le reflet d’une seule idée</div>
      <p>
        Nous souhaitons que chaque post représente un seul argument, et non une démonstration complète des raisons qui
        amène un utilisateur à avoir cette opinion. L’idée étant que chaque argument puisse être débattu de manière
        indépendante, afin de faciliter les débats et d’améliorer la lisibilité pour les lecteurs.
      </p>
      <div className={styles.statement}>Sourcer vos arguments</div>
      <p>
        <p>
          Lorsque l&#39;on cite des preuves dans le cadre d’une argumentation, il est important de pouvoir étayer ces
          preuves en fournissant sa (ou ses) source(s), afin que chacun puisse être en mesure de validé ou non la
          fiabilité de l’information.
        </p>
        <p>
          En outre, les lecteurs voudront souvent savoir plus sur les preuves citées - pour les utiliser dans
          d&#39;autres contextes, les remettre en question ou fournir des preuves supplémentaires à l&#39;appui. Pour
          faciliter tout cela, il est important de faire un usage approprié des liens vers des sources externes. Un
          champ source est donc fourni, pour cet effet, lors de la création d’un commentaire ou d’un argument.
        </p>
      </p>
      <div className={styles.statement}>Ne répétez pas des arguments déjà cités</div>
      <p>
        Le fait d&#39;avoir plusieurs arguments exprimant (sans nuances) la même idée dans une discussion, peut
        entraîner rapidement une certaine confusion, ce qui rend plus difficile pour les utilisateurs de naviguer
        efficacement dans la discussion et de comprendre rapidement les différents arguments de la communauté. Ainsi,
        nous vous encourageons à vérifier que l’argument que vous souhaitez soumettre n’a pas déjà été évoqué par un
        autre utilisateur.
      </p>
    </LayoutArticle>
  );
};

export default ArticleCreateDebate;
