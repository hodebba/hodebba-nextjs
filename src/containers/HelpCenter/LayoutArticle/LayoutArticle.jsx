import FilterDropdown from '@Components/FilterDropdown';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

import styles from './layout-article.module.scss';

const menuOptions = [
  {
    value: 'rules',
    label: "Règles d'Hodebba",
  },
  {
    value: 'create-debate',
    label: 'Comment créer un débat ?',
  },
  {
    value: 'create-post',
    label: 'Quelles sont les recommandations principales pour publier des arguments ?',
  },
];

const LayoutArticle = ({ children, active }) => {
  const router = useRouter();
  return (
    <div className={styles.container}>
      <aside>
        <ul>
          <li className={active === 'rules' && styles['menu-active']}>
            <Link href="/help-center/rules">Règles d'Hodebba</Link>
          </li>
          <li className={active === 'create-debate' && styles['menu-active']}>
            <Link href="/help-center/create-debate">Comment créer un débat ?</Link>
          </li>
          <li className={active === 'create-post' && styles['menu-active']}>
            <Link href="/help-center/create-post">
              Quelles sont les recommandations principales pour publier des arguments ?
            </Link>
          </li>
        </ul>
        <div className={styles.dropdown}>
          <FilterDropdown
            options={menuOptions}
            defaultValue={menuOptions.find(option => option.value === active)}
            isSearchable
            onChange={newArticle => router.push(`/help-center/${newArticle.value}`)}
          />
        </div>
      </aside>
      <article>{children}</article>
    </div>
  );
};

LayoutArticle.propTypes = {
  children: PropTypes.any,
  active: PropTypes.string,
};

export default LayoutArticle;
