/* eslint-disable prettier/prettier */
import LayoutArticle from '../LayoutArticle/LayoutArticle';
import styles from './article-create-debate.module.scss';

const ArticleCreateDebate = () => {
  return (
    <LayoutArticle active="create-debate">
      <h1>Comment créer un débat ?</h1>
      <p>Hodebba vous propose aussi bien de créer des débats publics, que des débats privés.</p>
      <h2>Débat public</h2>
      <p>
        Les débats publics sont accessibles à tous, et la modération est réservé aux modérateurs d’Hodebba. Ainsi, être
        le créateur d’un débat public n’implique pas de pouvoir en assurer la modération.
      </p>

      <div className={styles['list-intro']}>Créer un débat implique de remplir les champs suivants :</div>
      <ul>
        <li>
          <span className={styles.label}>Thème de débat</span> : Associer votre débat à un des thèmes proposés par
          Hodebba. Un débat ne peut être associé qu’à un seul thème.
        </li>
        <li>
          <span className={styles.label}>Tags</span> : Mots-clés associés au débat permettant de lister les thèmes
          abordés, afin de faciliter la recherche pour les utilisateurs
        </li>
        <li>
          <span className={styles.label}>Image de fond</span> : Image permettant d’illustrer le débat
        </li>
        <li>
          <span className={styles.label}>Présentation du débat</span> : Cette section est fondamentale. Elle doit
          permettre aux utilisateurs de comprendre le débat : contexte, enjeux, études sur le sujet, etc. Ce texte n’a
          pas pour objet d’évoquer les arguments mise en avant par les différents partis du débat, ni celle de son
          auteur. Nous encourageons une présentation d’une longueur comprise en 400 et 1000 mots.
        </li>
      </ul>

      <p>
        Afin de garantir sur la plateforme du contenu de qualité, chaque débat doit être validé par la rédaction de
        Hodebba avant d’être publié. Nous sommes susceptibles de le refuser s’il ne respecte pas les règles d’Hodebba ou
        que la présentation du débat est insuffisante.
      </p>

      <h2>Débat privé</h2>
      <p>
        Les débats privés sont eux uniquement accessibles sur invitation, et la modération est assuré par les
        administrateurs du débat. Vous pouvez administrer le débat par le biais de l’onglet configuration.
      </p>
    </LayoutArticle>
  );
};

export default ArticleCreateDebate;
