/* eslint-disable prettier/prettier */
import LayoutArticle from '../LayoutArticle';

const ArticleRules = () => {
  return (
    <LayoutArticle active="rules">
      <h1>Les règles d'Hodebba</h1>
      <p>
        <p>
          Hodebba souhaite que chacun puisse exprimer ses idées dans le respect. Nous pensons que la violence, les
          insultes, la haine (ou appel à haine) ou autres comportements irrespectueux détériorent grandement la qualité
          des débats.
        </p>
        <p>
          Nos règles, édictés ci-dessous, visent à ce que chacun puisse s’exprimer librement et en sécurité. Chaque
          utilisateur est en mesure de signaler des propos qui ne respecteraient pas notre charte. Nos équipes
          étudieront chaque cas individuellement, afin les confronter à nos règles établis ci-dessous.
        </p>
      </p>

      <h2>Appel à la haine</h2>
      <p>
        Vous ne pouvez pas encourager la violence contre d&#39;autres personnes, les menacer ou les harceler en raison
        de leur origine ethnique ou nationale, de leur caste, de leur orientation sexuelle, de leur sexe, de leur
        identité sexuelle, de leur appartenance religieuse, de leur âge, de leur handicap ou d&#39;une maladie grave.
      </p>

      <h2>Terrorisme / Organisations violents</h2>
      <p>
        <p>
          Vous ne pouvez pas glorifier ou promouvoir des organisations terroristes, ou tout autre organisation violente.
        </p>
        <p>Est considéré comme organisation violente, une entité répondant aux critères suivants :</p>
        <ul>
          <li>
            Ils s&#39;identifient comme groupes extrémistes dans leurs objectifs déclarés, publications ou actions
          </li>
          <li>
            Ils ont eu ou ont actuellement recours à la violence et/ou à l&#39;incitation à la violence pour faire
            avancer leur cause
          </li>
          <li>Ils ciblent des civils par leurs actes de violence et/ou l&#39;incitation à celle‑ci.</li>
        </ul>
      </p>

      <h2>Suicide et mutilations personnelles</h2>
      <p>Vous ne pouvez pas encourager ou promouvoir le suicide ou les mutilations personnelles.</p>

      <h2>Désinformation / Fake news</h2>
      <p>
        <p>
          Vous ne pouvez pas partager ou promouvoir des informations modifiés ou fabriqués dans le but de tromper. Nous
          faisons le nécessaire pour que chaque utilisateur puisse se former une opinion, en se basant sur des faits
          vérifiés.
        </p>
        <p>
          Ainsi, nous luttons activement contre le partage et la circulation de « fake news » pouvant induire en erreur
          nos utilisateurs sur un sujet donné.
        </p>
        <p>
          En outre, il est interdit de partager, par le biais du champ source, des médias modifiés à des fins de
          tromperie, en leurrant les utilisateurs quant à l&#39;authenticité de ces médias.
        </p>
      </p>

      <h2>Intégrité civique</h2>
      <p>
        Vous ne pouvez pas utiliser notre plateforme dans le but de manipuler ou d&#39;interférer dans des élections ou
        d&#39;autres processus civiques. Cela inclut la publication ou le partage de contenus susceptibles d’empêcher la
        participation ou d&#39;induire les gens en erreur sur le moment, le lieu ou la manière de participer à un
        processus civique.
      </p>

      <h2>Biens ou services commerciaux</h2>

      <p>
        Vous ne pouvez pas utiliser notre service afin de proposer la vente de biens ou services. Hodebba est une
        plateforme permettant de s’informer et d’échanger des opinions, elle ne peut en aucun cas être utilisé pour
        promouvoir une activité commerciale.
      </p>
    </LayoutArticle>
  );
};

export default ArticleRules;
