import { Image, Transformation } from 'cloudinary-react';
import Link from 'next/link';
import PropTypes from 'prop-types';

import styles from './celebrity-preview.module.scss';

const CelebrityPreview = ({ celebrity, description }) => {
  return (
    <Link href="/celebrities/[id]/positions" as={`/celebrities/${celebrity.id}/positions`}>
      <a key={celebrity.id}>
        <div className={styles.celebrity}>
          <Image publicId={celebrity.imageLink} alt={`photo de ${celebrity.fullName}`} secure="true">
            <Transformation crop="fill" gravity="faces" />
          </Image>
          <div className={styles.celebrity__name}>{celebrity.fullName}</div>
          <div className={styles.celebrity__job}>{description || <br />}</div>
        </div>
      </a>
    </Link>
  );
};

CelebrityPreview.propTypes = {
  celebrity: PropTypes.object.isRequired,
  description: PropTypes.string,
};

export default CelebrityPreview;
