/* eslint-disable operator-linebreak */
/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */
import { Image, Transformation } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { getTopics, reinitializeCreateCelebrity } from '../../../actions/celebrity.actions';
import { saveReturnPath } from '../../../actions/user.actions';
import checkError from '../../../HOCs/checkError.hoc';
import isFallback from '../../../HOCs/isFallback.hoc';
import { itemsMenuCelebrity } from '../../../components/itemsMenu';
import PageMenu from '../../../components/PageMenu';
import styles from './celebrities.module.scss';

const Celebrity = ({ activeItem, children }) => {
  const router = useRouter();
  const returnCelebrity = useSelector(state => state.celebrity.data.createCelebrity);
  const dispatch = useDispatch(null);
  const description = children.props.description || '';

  useEffect(() => {
    if (!isEmpty(returnCelebrity)) {
      dispatch(reinitializeCreateCelebrity());
      toast.success('Personnalité ajoutée avec succès !');
    }
  }, [returnCelebrity]);

  useEffect(() => {
    dispatch(getTopics());
  }, []);

  useEffect(() => {
    dispatch(saveReturnPath({ as: router.asPath, href: router.pathname }));
  }, []);

  return (
    <div className="center-block">
      <div className={styles.container}>
        <div className={styles.left}>
          <div className={styles.img}>
            <Image
              publicId={children.props.celebrity.imageLink}
              alt={`photo de ${children.props.celebrity.fullName}`}
              secure="true"
            >
              <Transformation crop="fill" gravity="faces" />
            </Image>
          </div>
        </div>
        <div className={styles.right}>
          <div>
            <h1>{children.props.celebrity.fullName}</h1>
            <div className={styles.job}>
              {description.shortDescription || children.props.celebrity.shortDescription}
            </div>
          </div>
          <p className={styles.description}>
            {description.longDescription ||
              `La biographie de ${children.props.celebrity.fullName} n'est pas disponible pour le moment.`}
          </p>
          <ul className={styles.properties}>
            {children.props.celebrity.politicalParty && (
              <li>
                <span className="bold">Parti politique: </span> {children.props.celebrity.politicalParty.label}
              </li>
            )}
            {children.props.celebrity.politicalPosition && (
              <li>
                <span className="bold">Positionnement politique: </span>{' '}
                {children.props.celebrity.politicalPosition.label}
              </li>
            )}
          </ul>
        </div>
      </div>
      <PageMenu items={itemsMenuCelebrity} className="block" active={activeItem} id={router.query.id} />
      <div className={styles.children}>{children}</div>
    </div>
  );
};

Celebrity.propTypes = {
  activeItem: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  description: PropTypes.string,
};

export default isFallback(checkError(Celebrity));
