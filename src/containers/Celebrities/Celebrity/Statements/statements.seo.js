import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';

const StatementsSeo = ({ celebrity }) => {
  return (
    <NextSeo
      title={`Toutes les déclarations de ${celebrity.fullName}`}
      description={`Toutes les déclarations de ${celebrity.fullName} sur différents sujets comme l'immigration, l'économie, la santé ou encore l'environnement.`}
      openGraph={{
        url: `${process.env.NEXT_PUBLIC_FRONT_END_URL}/celebrities/${celebrity.id}/statements`,
        title: `Toutes les déclarations de ${celebrity.fullName}`,
        type: 'website',
        description: `Toutes les déclarations de ${celebrity.fullName} sur différents sujets comme l'immigration, l'économie, la santé ou encore l'environnement.`,
        images: [
          {
            url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/${celebrity.imageLink}`,
            alt: celebrity.fullName,
          },
        ],
        site_name: 'Hodebba',
      }}
      twitter={{
        handle: '@handle',
        site: '@site',
        cardType: 'summary_large_image',
      }}
    />
  );
};

StatementsSeo.propTypes = {
  celebrity: PropTypes.object,
};

export default StatementsSeo;
