/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
import 'moment/locale/fr';

import MomentUtils from '@date-io/moment';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getTopics } from '../../../../../actions/celebrity.actions';
import { getAllDebates } from '../../../../../actions/debate.actions';
import FilterDropdown from '../../../../../components/FilterDropdown';
import { debatesArrayToOptions, topicsArrayToOptions } from '../../../../../utils/general.helper';
import styles from './additional-fields.module.scss';

moment.locale('fr');

const AdditionalFields = ({ onTopicFormChange, onRelatedDebateChange, handleDateChange, selectedDate }) => {
  const topics = useSelector(state => state.celebrity.local.topics);
  const debates = useSelector(state => state.debate.local.debates);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllDebates());
    dispatch(getTopics());
  }, []);

  return (
    <Fragment>
      <div className={styles['select-topic']}>
        <FilterDropdown
          options={topicsArrayToOptions(topics, false)}
          defaultValue={{ value: null, label: 'Sujet' }}
          isSearchable
          // onChange={values => setTopicForm(values.value)}
          onChange={onTopicFormChange}
        />
      </div>
      <div className={styles['select-debate']}>
        <FilterDropdown
          options={debatesArrayToOptions(debates, false)}
          defaultValue={{ value: null, label: 'Débat associé (optionnel)' }}
          isSearchable
          onChange={onRelatedDebateChange}
        />
        <div className="date-picker">
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <DatePicker
              autoOk
              label="Date de la déclaration"
              clearable
              value={selectedDate}
              onChange={date => handleDateChange(date)}
              format="dddd DD MMMM yyyy"
              cancelLabel="Annuler"
              clearLabel="Effacer"
            />
          </MuiPickersUtilsProvider>
        </div>
      </div>
    </Fragment>
  );
};

AdditionalFields.propTypes = {
  onTopicFormChange: PropTypes.func,
  onRelatedDebateChange: PropTypes.func,
  handleDateChange: PropTypes.func,
  selectedDate: PropTypes.object,
};

export default AdditionalFields;
