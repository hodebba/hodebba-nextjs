/* eslint-disable operator-linebreak */
/* eslint-disable import/no-cycle */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { resetAddStatement } from '../../../../actions/celebrity.actions';
import { getStatementsUserLikes } from '../../../../actions/user.actions';
import FilterDropdown from '../../../../components/FilterDropdown';
import InfoModal from '../../../../components/InfoModal';
import NotConnectedModal from '../../../../components/NotConnectedModal';
import SuccessfulAction from '../../../../components/SuccessfulAction/SuccessfulAction';
import { topicsArrayToOptions } from '../../../../utils/general.helper';
import AddButton from '../AddButton';
import AddStatementDesktop from './AddStatementDesktop/AddStatementDesktop';
import Statement from './Statement';
import styles from './statements.module.scss';
import StatementsSeo from './statements.seo';

const Statements = ({ isSmallScreen, statements, celebrity }) => {
  const [nbItems, setNbItems] = useState(3);
  const topics = useSelector(state => state.celebrity.local.topics);
  const router = useRouter();
  const [modalIsOpened, setModalIsOpened] = useState(false);
  const [modalNotConnected, setModalNotConnected] = useState({ isOpened: false, type: '' });
  const [successModalIsOpened, setSuccessModalIsOpened] = useState(false);
  const [filter, setFilter] = useState(router.query.filter || null);

  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const dispatch = useDispatch();
  const defaultFilter = topics.find(element => element.id.toString() === filter && filter.toString());
  const addStatementData = useSelector(state => state.celebrity.data.addStatement);

  const handleChangeFilter = topic => {
    setFilter(topic.value);
  };

  useEffect(() => {
    if (!isEmpty(addStatementData)) {
      dispatch(resetAddStatement());
      setModalIsOpened(false);
      setSuccessModalIsOpened(true);
    }
  }, [addStatementData]);

  useEffect(() => {
    if (!isEmpty(user)) {
      dispatch(getStatementsUserLikes(user.id));
    }
  }, []);

  const renderTopic = () => {
    const ret = [];
    Object.keys(statements)
      .filter(topic => topic === (filter ? filter.toString() : topic))
      .forEach(topicId => {
        ret.push(
          <div key={topicId} className={styles.theme}>
            <div className={styles.theme__top}>
              <div>
                <div className={styles.theme__subtitle}>Déclarations de {celebrity.fullName} sur</div>
                <h2>{statements[topicId][0].topic.label}</h2>
              </div>
            </div>
            {renderStatements(statements[topicId])}
          </div>,
        );
      });
    return ret;
  };

  const renderStatements = statements => {
    const ret = [];
    for (let i = 0; i < Math.min(statements.length, nbItems); i += 1) {
      ret.push(<Statement key={statements[i].id} statement={statements[i]} />);
    }

    if (statements.length > nbItems) {
      ret.push(
        <div key={nbItems + 1} className={styles['display-more']}>
          <button type="button" className="secondary-button" onClick={() => setNbItems(nbItems => nbItems + 3)}>
            Afficher plus
          </button>
        </div>,
      );
    }

    return ret;
  };

  return (
    <div className={styles.container}>
      <StatementsSeo celebrity={celebrity} />
      <div className={styles.filter}>
        <FilterDropdown
          options={topicsArrayToOptions(topics, true)}
          defaultValue={defaultFilter || { value: null, label: 'Tous les sujets' }}
          isSearchable
          onChange={handleChangeFilter}
        />
        <AddButton
          isSmallScreen={isSmallScreen}
          label="Ajouter une déclaration"
          onClick={() => {
            if (isEmpty(token) || !user.enabled) {
              setModalNotConnected({ isOpened: true, type: 'statement' });
            } else {
              setModalIsOpened(true);
            }
          }}
        />
      </div>
      {renderTopic()}

      {isEmpty(statements) && (
        <p className={styles['no-statements']}>
          Aucune déclaration n'a été attribuée pour le moment à {celebrity.fullName}
        </p>
      )}

      {successModalIsOpened && (
        <InfoModal onClose={() => setSuccessModalIsOpened(false)}>
          <div className={styles.success}>
            <SuccessfulAction
              title="Déclaration envoyée avec succès !"
              text="Nos équipes se chargeront de vérifier la validité de la déclaration sous 24h."
              textButton="OK"
              onClick={() => setSuccessModalIsOpened(false)}
            />
          </div>
        </InfoModal>
      )}

      {modalIsOpened && <AddStatementDesktop celebrity={celebrity} onClose={() => setModalIsOpened(false)} />}

      {modalNotConnected.isOpened && (
        <NotConnectedModal
          accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
          type={modalNotConnected.type}
          onClick={() => null}
          onClose={() => {
            setModalNotConnected({ isOpened: false, type: '' });
          }}
        />
      )}
    </div>
  );
};

Statements.propTypes = {
  isSmallScreen: PropTypes.bool,
  statements: PropTypes.object,
  celebrity: PropTypes.object,
};

export default Statements;
