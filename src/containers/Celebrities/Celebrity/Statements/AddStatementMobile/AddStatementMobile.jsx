import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stopSubmit, SubmissionError } from 'redux-form';
import isURL from 'validator/lib/isURL';

import { addStatement } from '../../../../../actions/celebrity.actions';
import MobileWriteMessage from '../../../../../components/MobileWriteMessage';
import AdditionalFields from '../AdditionalFields';

const AddStatementMobile = () => {
  const loading = useSelector(state => state.celebrity.local.loading.addStatement);
  const error = useSelector(state => state.celebrity.local.errors.addStatement);
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const dispatch = useDispatch();
  const router = useRouter();
  const [topicForm, setTopicForm] = useState(null);
  const [relatedDebate, setRelatedDebate] = useState(null);
  const [selectedDate, setDate] = useState(null);

  useEffect(() => {
    router.prefetch('/celebrities/[id]/statements', router.asPath.split('/compose')[0]);
  }, [router]);

  useEffect(() => {
    if (!isEmpty(error)) {
      stopSubmit('MobileWriteMessage', {
        _error: 'Une erreur innatendue est survenue, merci de contacter le support technique',
      });
    }
  }, [error]);

  const handleSubmit = values => {
    if (topicForm === null) {
      throw new SubmissionError({
        _error: 'Merci de sélectionner un sujet',
      });
    } else if (!values.source) {
      throw new SubmissionError({
        _error: 'La source de votre déclaration est obligatoire',
      });
    } else if (!isURL(values.source, { allow_underscores: true })) {
      throw new SubmissionError({
        _error: 'Votre lien est invalide',
      });
    } else if (!values.content) {
      throw new SubmissionError({
        _error: 'Le champ déclaration est obligatoire',
      });
    } else if (selectedDate === null) {
      throw new SubmissionError({
        _error: 'Le champ date est obligatoire',
      });
    } else {
      const body = {
        sourceLinks: [values.source],
        content: values.content,
        userId: user.id,
        topicId: topicForm,
        debateId: relatedDebate,
        celebrityId: router.asPath.split('/')[2],
        createdDate: selectedDate.toDate(),
      };
      dispatch(addStatement(body, token));
      router.push('/celebrities/[id]/statements', router.asPath.split('/compose')[0]);
    }
  };

  return (
    <MobileWriteMessage
      CTA="Publier"
      withSource
      sourceIsRequired
      placeHolderText="La déclaration..."
      onSubmit={handleSubmit}
      loading={loading}
      returnHref="/celebrities/[id]/statements"
    >
      <AdditionalFields
        onRelatedDebateChange={values => setRelatedDebate(values.value)}
        onTopicFormChange={values => setTopicForm(values.value)}
        handleDateChange={date => setDate(date)}
        selectedDate={selectedDate}
      />
    </MobileWriteMessage>
  );
};

export default AddStatementMobile;
