/* eslint-disable react-hooks/exhaustive-deps */

import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SubmissionError } from 'redux-form';

import { addStatement } from '../../../../../actions/celebrity.actions';
import { getAllDebates } from '../../../../../actions/debate.actions';
import DesktopWriteMessage from '../../../../../components/DesktopWriteMessage';
import AdditionalFields from '../AdditionalFields';
import styles from './add-statement-desktop.module.scss';

const AddStatementDesktop = ({ celebrity, onClose }) => {
  const loading = useSelector(state => state.celebrity.local.loading.addStatement);
  const error = useSelector(state => state.celebrity.local.errors.addStatement);
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const dispatch = useDispatch();
  const [topicForm, setTopicForm] = useState(null);
  const [relatedDebate, setRelatedDebate] = useState(null);
  const [selectedDate, setDate] = useState(null);

  useEffect(() => {
    dispatch(getAllDebates());
  }, []);

  const handleSubmitStatement = values => {
    if (topicForm === null) {
      throw new SubmissionError({
        _error: 'Merci de sélectionner un sujet',
      });
    } else if (selectedDate === null) {
      throw new SubmissionError({
        _error: 'Merci de sélectionner une date',
      });
    } else {
      const body = {
        sourceLinks: [values.source],
        content: values.content,
        userId: user.id,
        topicId: topicForm,
        debateId: relatedDebate,
        celebrityId: celebrity.id,
        createdDate: selectedDate.toDate(),
      };
      dispatch(addStatement(body, token));
    }
  };
  return (
    <div className={styles.add}>
      <DesktopWriteMessage
        withSummary
        withSource
        sourceRequired
        title={`Ajouter une déclaration de ${celebrity.fullName}`}
        placeHolderText="Déclaration"
        CTA="Publier"
        onClose={onClose}
        loading={loading}
        onSubmit={handleSubmitStatement}
        errorServer={error}
      >
        <AdditionalFields
          onRelatedDebateChange={values => setRelatedDebate(values.value)}
          onTopicFormChange={values => setTopicForm(values.value)}
          handleDateChange={date => setDate(date)}
          selectedDate={selectedDate}
        />
      </DesktopWriteMessage>
    </div>
  );
};

AddStatementDesktop.propTypes = {
  celebrity: PropTypes.object,
  onClose: PropTypes.func,
};

export default AddStatementDesktop;
