/* eslint-disable operator-linebreak */
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useState } from 'react';
import ClampLines from 'react-clamp-lines';
import { useSelector } from 'react-redux';

import LikeDislike from '../../../../../components/LikeDislike';
import NotConnectedModal from '../../../../../components/NotConnectedModal';
import { formatDateLetters, formatDateNumeric, useWindowSize } from '../../../../../utils/general.helper';
import { checkUserLikes } from '../../../../../utils/statement.helper';
import styles from './statement.module.scss';

const Statement = ({ statement, withoutLikes }) => {
  const userLikes = useSelector(state => state.user.data.statementUserLikes);
  const isLoading = useSelector(state => state.user.local.isLoading);
  const loadingLikes = useSelector(state => state.user.local.loading.getStatementsUserLikes);
  const user = useSelector(state => state.user.local.user);
  const initialIsLike = checkUserLikes(statement, userLikes);
  const [modal, setModal] = useState({ isOpened: false, type: '' });
  const size = useWindowSize();

  const renderDate = () => {
    if (statement.createdDate !== null) {
      if (size.width < 410) {
        return formatDateNumeric(statement.createdDate);
      }
      return formatDateLetters(statement.createdDate);
    }
    return 'Date inconnue';
  };

  return (
    <div className={styles.container}>
      <ClampLines
        text={statement.content}
        lines={4}
        ellipsis="..."
        moreText="Voir plus"
        lessText="Voir moins"
        className="see-more"
        innerElement="p"
        stopPropagation
      />
      <div className={styles.bottom}>
        <div className={`${styles.bottom__date} secondary-text`}>
          <div className={styles.line}></div>
          {renderDate()}
        </div>
        <div className={styles.bottom__likes}>
          {!loadingLikes && !withoutLikes && !isLoading && (
            <LikeDislike
              isStatement
              initialIsLike={initialIsLike}
              data={statement}
              sendModalInfo={info => setModal(info)}
            />
          )}
        </div>
        <a href={statement.sources[0].link} target="_blank" rel="noopener" type="button">
          Source
        </a>
        <div className={`${styles.bottom__author} secondary-text`}>Ajouté par {statement.user.username}</div>
      </div>

      {modal.isOpened && (
        <NotConnectedModal
          type={modal.type}
          accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
          onClick={e => {
            e.preventDefault();
            e.stopPropagation();
          }}
          onClose={e => {
            e.preventDefault();
            e.stopPropagation();
            setModal({ isOpened: false, type: '' });
          }}
        />
      )}
    </div>
  );
};

Statement.propTypes = {
  statement: PropTypes.object.isRequired,
  withoutLikes: PropTypes.bool,
};

export default Statement;
