import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment } from 'react';

import PlusIcon from '../../../../../public/static/icons/plus2.svg';
import WriteIcon from '../../../../../public/static/icons/write.svg';
import ClientOnlyPortal from '../../../../components/ClientOnlyPortal';
import styles from './add-button.module.scss';

const AddButton = ({ isSmallScreen, onClick, label }) => {
  const router = useRouter();
  return (
    <Fragment>
      {!isSmallScreen && (
        <div className={styles.add}>
          <button type="button" className="primary-button" onClick={onClick}>
            <PlusIcon /> {label}
          </button>
        </div>
      )}
      {isSmallScreen && (
        <ClientOnlyPortal selector="#app">
          <Link href={`${router.asPath}/compose`}>
            <a className={styles['add-mobile']}>
              <WriteIcon />
            </a>
          </Link>
        </ClientOnlyPortal>
      )}
    </Fragment>
  );
};

AddButton.propTypes = {
  isSmallScreen: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string,
};

export default AddButton;
