/* eslint-disable react-hooks/exhaustive-deps */
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Field, SubmissionError } from 'redux-form';

import { addPosition } from '../../../../../actions/celebrity.actions';
import { getAllDebates } from '../../../../../actions/debate.actions';
import DesktopWriteMessage from '../../../../../components/DesktopWriteMessage';
import FilterDropdown from '../../../../../components/FilterDropdown';
import RenderTextField from '../../../../../components/RenderTextField';
import { maxLength35Text, required } from '../../../../../utils/check-fields.helper';
import { debatesArrayToOptions, topicsArrayToOptions } from '../../../../../utils/general.helper';
import styles from './add-position-desktop.module.scss';

const AddPositionDesktop = ({ celebrity, onClose, defaultPosition }) => {
  const topics = useSelector(state => state.celebrity.local.topics);
  const debates = useSelector(state => state.debate.local.debates);
  const [positionForm, setPositionForm] = useState(null);
  const loading = useSelector(state => state.celebrity.local.loading.addPosition);
  const error = useSelector(state => state.celebrity.local.errors.addPosition);
  const [topicForm, setTopicForm] = useState(null);
  const [relatedDebate, setRelatedDebate] = useState(null);
  const token = useSelector(state => state.user.local.token);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllDebates());
  }, []);
  const handleSubmitStatement = values => {
    if (topicForm === null) {
      throw new SubmissionError({
        _error: 'Merci de sélectionner un sujet',
      });
    } else {
      const body = {
        question: values.question,
        summaryType: positionForm,
        summaryContent: values.summary,
        explanation: values.content || null,
        topicId: topicForm,
        debateId: relatedDebate,
        celebrityId: celebrity.id,
      };
      dispatch(addPosition(body, token));
    }
  };

  return (
    <div className={styles.add}>
      <DesktopWriteMessage
        withSummary
        contentNotRequired
        title={`Ajouter une opinion de ${celebrity.fullName}`}
        withPosition
        labels={{ pro: 'Pour', neutral: 'Neutre', con: 'Contre' }}
        placeHolderText="Opinion (optionnel)"
        CTA="Publier"
        onClose={onClose}
        defaultPosition={defaultPosition}
        loading={loading}
        getPosition={newPosition => setPositionForm(newPosition)}
        onSubmit={handleSubmitStatement}
        errorServer={error}
      >
        <div className={styles['additional-fields']}>
          <div className={styles['select-topic']}>
            <FilterDropdown
              options={topicsArrayToOptions(topics, false)}
              defaultValue={{ value: null, label: 'Sujet' }}
              isSearchable
              onChange={values => setTopicForm(values.value)}
            />
          </div>
          <div className={styles['select-debate']}>
            <FilterDropdown
              options={debatesArrayToOptions(debates, false)}
              defaultValue={{ value: null, label: 'Débat associé (optionnel)' }}
              isSearchable
              onChange={values => setRelatedDebate(values.value)}
            />
          </div>
          <Field name="question" type="text" component={RenderTextField} label="Question" validate={[required]} />
          <Field
            name="summary"
            type="text"
            component={RenderTextField}
            label="Résumé de l'opinion"
            validate={[required, maxLength35Text]}
          />
        </div>
      </DesktopWriteMessage>
    </div>
  );
};

AddPositionDesktop.propTypes = {
  celebrity: PropTypes.object,
  onClose: PropTypes.func,
  defaultPosition: PropTypes.string,
};

export default AddPositionDesktop;
