/* eslint-disable operator-linebreak */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import PlusIcon from '../../../../../public/static/icons/plus2.svg';
import { resetAddPosition } from '../../../../actions/celebrity.actions';
import FilterDropdown from '../../../../components/FilterDropdown';
import { topicsArrayToOptions } from '../../../../utils/general.helper';
import AddButton from '../AddButton';
import AddPositionDesktop from './AddPositionDesktop';
import styles from './positions.module.scss';
import PositionsSeo from './positions.seo';

const Positions = ({ isSmallScreen, positions, celebrity }) => {
  const topics = useSelector(state => state.celebrity.local.topics);
  const [contentIsVisible, setContentIsVisible] = useState({});
  const [filter, setFilter] = useState(null);
  const [modal, setModal] = useState({ isOpened: false, type: '' });
  const user = useSelector(state => state.user.local.user);
  const dispatch = useDispatch();
  const addPositionData = useSelector(state => state.celebrity.data.addPosition);
  const [positionList, setPositionList] = useState(positions);
  const router = useRouter();

  const handleChangeFilter = topic => {
    setFilter(topic.value);
  };

  useEffect(() => {
    if (!isEmpty(addPositionData)) {
      if (
        !positionList[addPositionData.topic.id] ||
        positionList[addPositionData.topic.id].filter(position => position.id === addPositionData.id).length < 1
      ) {
        const tempTab = positionList[addPositionData.topic.id] || [];
        tempTab.push(addPositionData);
        setPositionList({ ...positionList, [addPositionData.topic.id]: tempTab });
      }
      dispatch(resetAddPosition());
      setModal({ isOpened: false, type: '' });
    }
  }, [addPositionData]);
  const renderPositions = () => {
    const ret = [];
    Object.keys(positionList)
      .filter(topic => topic === (filter ? filter.toString() : topic))
      .forEach(topic => {
        ret.push(
          <div key={topic} className={styles.theme}>
            <div>
              <div className={styles.theme__subtitle}>Opinion de {celebrity.fullName} sur</div>
              <h2>{positionList[topic][0].topic.label}</h2>
            </div>
            <ul>
              {positionList[topic].map(position => (
                <li key={position.id}>
                  <div className={styles.topic}>
                    <h3>
                      {!contentIsVisible[position.id] && (
                        <PlusIcon onClick={() => setContentIsVisible({ ...contentIsVisible, [position.id]: true })} />
                      )}
                      {contentIsVisible[position.id] && (
                        <div
                          role="button"
                          onClick={() => setContentIsVisible({ ...contentIsVisible, [position.id]: false })}
                          className={styles.less}
                        ></div>
                      )}
                      {position.question}
                    </h3>
                    <div className={`${styles.theme__opinion} ${styles[position.summaryType]}`}>
                      {position.summaryContent}
                    </div>
                  </div>
                  {contentIsVisible[position.id] && (
                    <p>{position.explanation || "Aucun détail sur cet opinion n'est disponible pour le moment."}</p>
                  )}
                </li>
              ))}
            </ul>
            <Link
              href={`/celebrities/[id]/statements?filter=${positionList[topic][0].topic.id}`}
              as={`${router.asPath.split('/positions')[0]}/statements?filter=${positionList[topic][0].topic.id}`}
            >
              <a>Voir ses déclarations marquantes sur {positionList[topic][0].topic.label}</a>
            </Link>
          </div>,
        );
      });
    return ret;
  };

  return (
    <div className={styles.container}>
      <PositionsSeo celebrity={celebrity} />
      <div className={styles.filter}>
        <FilterDropdown
          options={topicsArrayToOptions(topics, true)}
          defaultValue={{ value: null, label: 'Tous les sujets' }}
          isSearchable
          onChange={handleChangeFilter}
        />
        {!isEmpty(user) && user.roles[0].name === 'ROLE_ADMIN' && (
          <AddButton
            isSmallScreen={isSmallScreen}
            onClick={() => setModal({ ...modal, isOpened: true })}
            label="Ajouter une opinion"
          />
        )}
      </div>
      {renderPositions()}

      {isEmpty(positionList) && (
        <p className={styles['no-positions']}>Aucune opinion n'a été attribuée pour le moment à {celebrity.fullName}</p>
      )}

      {modal.isOpened && (
        <AddPositionDesktop
          celebrity={celebrity}
          defaultPosition={modal.type}
          onClose={() => setModal({ ...modal, isOpened: false })}
        />
      )}
    </div>
  );
};

Positions.propTypes = {
  positions: PropTypes.object,
  celebrity: PropTypes.object,
  isSmallScreen: PropTypes.bool,
};

export default Positions;
