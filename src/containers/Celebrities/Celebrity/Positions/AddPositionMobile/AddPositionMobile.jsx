/* eslint-disable react-hooks/exhaustive-deps */
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Field, stopSubmit, SubmissionError } from 'redux-form';

import { addPosition, getTopics } from '../../../../../actions/celebrity.actions';
import { getAllDebates } from '../../../../../actions/debate.actions';
import FilterDropdown from '../../../../../components/FilterDropdown';
import MobileWriteMessage from '../../../../../components/MobileWriteMessage';
import RenderTextField from '../../../../../components/RenderTextField';
import { debatesArrayToOptions, topicsArrayToOptions } from '../../../../../utils/general.helper';
import styles from './add-position-mobile.module.scss';

const AddPositionMobile = () => {
  const loading = useSelector(state => state.celebrity.local.loading.addPosition);
  const error = useSelector(state => state.celebrity.local.errors.addPosition);
  const topics = useSelector(state => state.celebrity.local.topics);
  const debates = useSelector(state => state.debate.local.debates);
  const token = useSelector(state => state.user.local.token);
  const dispatch = useDispatch();
  const router = useRouter();
  const [topicForm, setTopicForm] = useState(null);
  const [relatedDebate, setRelatedDebate] = useState(null);
  const [position, setPosition] = useState(null);

  useEffect(() => {
    dispatch(getAllDebates());
    dispatch(getTopics());
  }, []);

  useEffect(() => {
    router.prefetch('/celebrities/[id]/positions', router.asPath.split('/compose')[0]);
  }, [router]);

  useEffect(() => {
    if (!isEmpty(error)) {
      stopSubmit('MobileWriteMessage', {
        _error: 'Une erreur innatendue est survenue, merci de contacter le support technique',
      });
    }
  }, [error]);

  const handleSubmit = values => {
    if (topicForm === null) {
      throw new SubmissionError({
        _error: 'Merci de sélectionner un sujet',
      });
    } else if (!values.question) {
      throw new SubmissionError({
        _error: 'Le champ question est obligatoire',
      });
    } else if (!values.summary) {
      throw new SubmissionError({
        _error: "Le résumé de l'opinion est obligatoire",
      });
    } else {
      const body = {
        question: values.question,
        summaryType: position,
        summaryContent: values.summary,
        explanation: values.content || null,
        topicId: topicForm,
        debateId: relatedDebate,
        celebrityId: router.asPath.split('/')[2],
      };
      dispatch(addPosition(body, token));
      router.push('/celebrities/[id]/positions', router.asPath.split('/compose')[0]);
    }
  };

  return (
    <MobileWriteMessage
      CTA="Publier"
      withPosition
      labels={{ pro: 'Pour', neutral: 'Neutre', con: 'Contre' }}
      getPosition={newPosition => setPosition(newPosition)}
      placeHolderText="Son opinion..."
      onSubmit={handleSubmit}
      loading={loading}
      returnHref="/celebrities/[id]/positions"
    >
      <div className={styles['select-topic']}>
        <FilterDropdown
          options={topicsArrayToOptions(topics, false)}
          defaultValue={{ value: null, label: 'Sujet' }}
          isSearchable
          onChange={values => setTopicForm(values.value)}
        />
        <div className={styles['select-debate']}>
          <FilterDropdown
            options={debatesArrayToOptions(debates, false)}
            defaultValue={{ value: null, label: 'Débat associé (optionnel)' }}
            isSearchable
            onChange={values => setRelatedDebate(values.value)}
          />
        </div>
        <Field name="question" type="text" component={RenderTextField} label="Question" />
        <Field name="summary" type="text" component={RenderTextField} label="Résumé de l'opinion" />
      </div>
    </MobileWriteMessage>
  );
};

export default AddPositionMobile;
