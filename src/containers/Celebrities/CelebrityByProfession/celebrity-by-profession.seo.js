import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';

const CelebrityByProfessionSeo = ({ profession }) => {
  return (
    <NextSeo
      title={`Opinions des ${profession.label.toLowerCase()}s`}
      description={`Découvrez les prises de positions et les déclarations des ${profession.label.toLowerCase()}s qui animent le débat public.`}
      openGraph={{
        url: process.env.NEXT_PUBLIC_FRONT_END_URL,
        title: `Opinions des ${profession.label.toLowerCase()}s`,
        type: 'website',
        description: `Découvrez les prises de positions et les déclarations des ${profession.label.toLowerCase()}s qui animent le débat public.`,
        images: [
          {
            url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/hodebba-celebrities_u7zumj`,
            alt: 'Hodebba logo',
          },
        ],
        site_name: 'Hodebba',
      }}
      twitter={{
        handle: '@handle',
        site: '@site',
        cardType: 'summary_large_image',
      }}
    />
  );
};

CelebrityByProfessionSeo.propTypes = {
  profession: PropTypes.object,
};

export default CelebrityByProfessionSeo;
