/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */

import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getProfessions } from '../../../actions/celebrity.actions';
import FilterDropdown from '../../../components/FilterDropdown';
import isFallback from '../../../HOCs/isFallback.hoc';
import { themeArrayToOptions } from '../../../utils/general.helper';
import CelebrityPreveiew from '../CelebrityPreview';
import styles from './celebrity-by-profession.module.scss';
import CelebrityByProfessionSeo from './celebrity-by-profession.seo';

const CelebrityByProfession = ({ profession, descriptions }) => {
  const router = useRouter();
  const professions = useSelector(state => state.celebrity.local.professions);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProfessions());
  }, []);

  useEffect(() => {
    profession.celebrities.forEach(celebrity => {
      router.prefetch('/celebrities/[id]/positions', `/celebrities/${celebrity.id}/positions`);
    });
  }, [profession]);

  const renderCelebrities = () => {
    const ret = [];
    profession.celebrities.forEach(celebrity => {
      const filterCelebrity = Object.values(descriptions).filter(
        description => description.title === celebrity.fullName && description.missing !== '',
      );
      const description = filterCelebrity.length > 0 ? filterCelebrity[0].description : null;
      ret.push(<CelebrityPreveiew key={celebrity.id} description={description} celebrity={celebrity} />);
    });
    return ret;
  };

  return (
    <div className={styles.container}>
      <CelebrityByProfessionSeo profession={profession} />
      <h1>{profession.label}</h1>

      <div className={styles.filters}>
        <FilterDropdown
          options={themeArrayToOptions(professions)}
          value={{ value: profession.id, label: profession.label }}
          isSearchable
          onChange={newCelebrityByProfession =>
            router.push('/celebrities/profession/[id]', `/celebrities/profession/${newCelebrityByProfession.value}`)
          }
        />
      </div>

      <div className={styles['celebrities-container']}>{renderCelebrities()}</div>
    </div>
  );
};

CelebrityByProfession.propTypes = {
  profession: PropTypes.object.isRequired,
  descriptions: PropTypes.object,
};

export default isFallback(CelebrityByProfession);
