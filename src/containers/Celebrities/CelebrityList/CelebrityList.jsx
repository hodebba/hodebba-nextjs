/* eslint-disable react-hooks/exhaustive-deps */
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { saveReturnPath } from '../../../actions/user.actions';
import { sortById } from '../../../utils/general.helper';
import Carousel from '../../Home/Carousel';
import CelebrityPreview from '../CelebrityPreview';
import styles from './celebrity-list.module.scss';
import CelebrityListSeo from './celebrity-list.seo';

const CelebrityList = ({ professions, descriptions }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    dispatch(saveReturnPath({ as: router.asPath, href: router.pathname }));
  }, []);

  const renderCelebrities = celebrityList => {
    const ret = [];
    celebrityList.sort(sortById);
    for (let i = 0; i < Math.min(celebrityList.length, 15); i += 1) {
      const filterCelebrity = Object.values(descriptions).filter(
        description => description.title === celebrityList[i].fullName && description.missing !== '',
      );

      const description = filterCelebrity.length > 0 ? filterCelebrity[0].description : null;
      ret.push(<CelebrityPreview key={celebrityList[i].id} description={description} celebrity={celebrityList[i]} />);
    }
    return ret;
  };

  const renderTypes = () => {
    const ret = [];
    professions.forEach(profession => {
      if (profession.celebrities.length > 3) {
        ret.push(
          <div key={profession.id} className={styles.type}>
            <div className={styles.text}>
              <h2>{profession.label}</h2>
              <Link href="/celebrities/profession/[id]" as={`/celebrities/profession/${profession.id}`}>
                <a>Voir plus</a>
              </Link>
            </div>
            <Carousel>{renderCelebrities(profession.celebrities)}</Carousel>
          </div>,
        );
      }
    });

    return ret;
  };

  return (
    <div className={styles.container}>
      <CelebrityListSeo />
      <h1>Opinions des personnalités publiques</h1>
      {renderTypes()}
    </div>
  );
};

CelebrityList.propTypes = {
  professions: PropTypes.array.isRequired,
  descriptions: PropTypes.object,
};

export default CelebrityList;
