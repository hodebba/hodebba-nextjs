import { NextSeo } from 'next-seo';

const CelebrityListSeo = () => {
  return (
    <NextSeo
      title="Opinions des personnalités publiques"
      description="Découvrez les prises de positions et les déclarations des personnalités qui animent le débat public. Politiciens, philosophes ou même artistes, qui sont ceux qui s'accordent avec vos idées ?"
      openGraph={{
        url: process.env.NEXT_PUBLIC_FRONT_END_URL,
        title: 'Opinions des personnalités publiques',
        type: 'website',
        description:
          "Découvrez les prises de positions et les déclarations des personnalités qui animent le débat public. Politiciens, philosophes ou même artistes, qui sont ceux qui s'accordent avec vos idées ?",
        images: [
          {
            url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/hodebba-celebrities_u7zumj`,
            width: 800,
            height: 600,
            alt: 'Personnalités publiques',
          },
        ],
        site_name: 'Hodebba',
      }}
      twitter={{
        handle: '@handle',
        site: '@site',
        cardType: 'summary_large_image',
      }}
    />
  );
};

export default CelebrityListSeo;
