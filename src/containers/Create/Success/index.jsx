/* eslint-disable react/no-unescaped-entities */
import Link from 'next/link';

import styles from './success.module.scss';

export const Success = () => {
  return (
    <div className={styles.container}>
      <h1>MERCI pour votre contribution !</h1>
      <p>
        Votre débat sera étudié dans les 24 heures par nos équipes, et vous serez informé s'il répond ou non aux
        règlement d'Hodebba.
      </p>
      <Link href="/">
        <button type="button" className="primary-button">
          Retour à l'accueil
        </button>
      </Link>
    </div>
  );
};

export default Success;
