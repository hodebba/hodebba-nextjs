/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import chevronLeft from '@iconify/icons-vaadin/chevron-left';
import { Icon } from '@iconify/react';
import QuestionIcon from '@Static/icons/question.svg';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import Link from 'next/link';
import { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';

import NotConnectedModal from '../../components/NotConnectedModal';
import CreateDebate from '../Admin/CreateDebate';
import styles from './create.module.scss';

const Create = () => {
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const [debateType, setDebateType] = useState({ isSelected: false, isPrivate: false });
  const [modalIsOpened, setModalIsOpened] = useState(false);

  const handleClickButton = isPrivate => {
    if (isUndefined(token) || !user.enabled) {
      setModalIsOpened(true);
    } else {
      setDebateType({ isSelected: true, isPrivate });
    }
  };

  return (
    <div className={styles.container}>
      {debateType.isSelected && (
        <button
          className={`text-button ${styles.back}`}
          onClick={() => setDebateType({ ...debateType, isSelected: false })}
          type="button"
        >
          <Icon width="18" height="18" icon={chevronLeft} />
          Retour
        </button>
      )}

      <h1>Créer un nouveau débat</h1>

      {!debateType.isSelected ? (
        <Fragment>
          <div className={styles['type-container']}>
            <div type="button" className={styles.type} onClick={() => handleClickButton(true)}>
              <h2>Privée</h2>
              <p>Seuls les utilisateurs invités peuvent participer à la discussion.</p>
              <p>Vous êtes en charge de la modération.</p>
            </div>

            <div type="button" className={styles.type} onClick={() => handleClickButton(false)}>
              <h2>Public</h2>
              <p>Tout le monde peut voir et participer au débat.</p>
              <p>Hodebba est en charge de la modération.</p>
            </div>

            {modalIsOpened && (
              <NotConnectedModal
                type="create"
                accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
                onClick={() => null}
                onClose={() => setModalIsOpened(false)}
              />
            )}
          </div>
          <div className={styles.helper}>
            <QuestionIcon />
            <p>
              Besoin d'aide pour créer un débat ? <Link href="/help-center/create-debate">Cliquez ici.</Link>
            </p>
          </div>
        </Fragment>
      ) : (
        <CreateDebate isPrivate={debateType.isPrivate} createdByCommunity />
      )}
    </div>
  );
};

export default Create;
