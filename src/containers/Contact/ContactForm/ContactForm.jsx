/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable implicit-arrow-linebreak */

import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import RenderTextAreaField from '../../../components/RenderTextAreaField';
import RenderTextField from '../../../components/RenderTextField';
import SubmitButton from '../../../components/SubmitButton';
import { email, required, trim } from '../../../utils/check-fields.helper';

const ContactForm = ({ handleSubmit, submitting }) => {
  const loading = useSelector(state => state.customer.local.loading.contactUs);
  const errors = useSelector(state => state.customer.local.errors.contactUs);
  const [error, setError] = useState('');

  useEffect(() => {
    if (!isEmpty(errors)) {
      setError(errors.message);
    } else {
      setError('');
    }
  }, [errors]);

  return (
    <form onSubmit={handleSubmit}>
      <Field name="name" type="text" component={RenderTextField} label="Nom" validate={[required]} normalize={trim} />

      <Field name="email" type="email" component={RenderTextField} label="Adresse email" validate={[required, email]} />

      <Field
        name="message"
        type="text-area"
        component={RenderTextAreaField}
        label="Votre message"
        validate={[required]}
      />

      {error !== '' && (
        <div className="error-message">
          <div className="error-message__title">Merci de réessayer</div>
          <p>{error}</p>
        </div>
      )}
      <div>
        <SubmitButton label="Envoyer" submitting={submitting} loading={loading} />
      </div>
    </form>
  );
};

ContactForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'ContactForm',
})(ContactForm);
