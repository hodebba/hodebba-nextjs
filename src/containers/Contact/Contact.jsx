/* eslint-disable react-hooks/exhaustive-deps */
import { Image, Transformation } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { reset } from 'redux-form';

import { contactUs } from '../../actions/customer.action';
import MobileMenu from '../../components/MobileMenu';
import styles from './contact.module.scss';
import ContactForm from './ContactForm';

const Contact = ({ isSmallScreen }) => {
  const dispatch = useDispatch();
  const contactUsData = useSelector(state => state.customer.data.contactUs);

  const handleSubmit = values => {
    dispatch(contactUs(values));
  };

  useEffect(() => {
    if (!isEmpty(contactUsData)) {
      toast.success(contactUsData.message);
      dispatch(reset('ContactForm'));
    }
  }, [contactUsData]);

  return (
    <Fragment>
      {isSmallScreen ? (
        <MobileMenu title="Contactez-nous">
          <div className={styles['mobile-container']}>
            <p>
              Vous souhaitez proposer une idée pour améliorer le site ? ou tout simplement demander un renseignement ?
              <br />
              <br />
              Envoyez-nous un message et nous vous répondrons au plus vite.
            </p>
            <ContactForm onSubmit={handleSubmit} />
          </div>
        </MobileMenu>
      ) : (
        <div className={styles.container}>
          <h1>Contactez-nous</h1>
          <p>
            Vous souhaitez proposer une idée pour améliorer le site ? ou tout simplement demander un renseignement ?
            <br />
            Envoyez-nous un message et nous vous répondrons au plus vite.
          </p>
          <div className={styles.content}>
            <div className={styles['img-container']}>
              <Image publicId="message_uyfqdn" alt="illustration contact" secure="true">
                <Transformation crop="fill" gravity="faces" />
              </Image>
            </div>
            <div className={styles.form}>
              <ContactForm onSubmit={handleSubmit} />
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

Contact.propTypes = {
  isSmallScreen: PropTypes.bool,
};

export default Contact;
