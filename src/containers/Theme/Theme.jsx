/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */

import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { saveReturnPath } from '../../actions/user.actions';
import DebatePreview from '../../components/DebatePreview';
import FilterDropdown from '../../components/FilterDropdown';
import isFallback from '../../HOCs/isFallback.hoc';
import { preferences, sortByNewest, sortByOldest, sortByViews, themeArrayToOptions } from '../../utils/general.helper';
import styles from './theme.module.scss';
import ThemeSeo from './theme.seo';

const Theme = ({ debates, currentTheme }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const themes = useSelector(state => state.debate.local.themes);
  const isThemeListOpened = useSelector(state => state.user.local.isThemeListOpened);
  const [sortedDebates, setSortedDebates] = useState(debates.sort(sortByViews));
  const [filter, setFilter] = useState('popularity');

  useEffect(() => {
    dispatch(saveReturnPath({ as: router.asPath, href: router.pathname }));
  }, []);

  useEffect(() => {
    handleChangeFilter(filter);
  }, [debates]);

  useEffect(() => {
    themes.forEach(theme => {
      router.prefetch('/theme/[id]', `/theme/${theme.id}`);
    });
  }, [themes]);

  const renderDebates = () => {
    const ret = [];

    sortedDebates
      .filter(debate => debate.debateState === 'published')
      .forEach(debate => {
        ret.push(<DebatePreview key={debate.id} debate={debate} theme={currentTheme} />);
      });
    return ret;
  };

  const handleChangeFilter = type => {
    setFilter(type);
    switch (type) {
      case 'popularity':
        setSortedDebates(cloneDeep(debates).sort(sortByViews));
        break;
      case 'recent':
        setSortedDebates(cloneDeep(debates).sort(sortByNewest));
        break;

      case 'older':
        setSortedDebates(cloneDeep(debates).sort(sortByOldest));
        break;
      default:
    }
  };

  if (isThemeListOpened) {
    return <div></div>;
  }

  return (
    <div className={styles.container}>
      <ThemeSeo currentTheme={currentTheme} />

      <h1>{currentTheme.label}</h1>

      <div className={styles.filters}>
        <div className={styles.filters__theme}>
          <FilterDropdown
            options={themeArrayToOptions(themes)}
            value={{ value: currentTheme.id, label: currentTheme.label }}
            isSearchable
            onChange={newTheme => router.push('/theme/[id]', `/theme/${newTheme.value}`)}
          />
        </div>
        <div className={styles.filters__preferences}>
          <FilterDropdown
            options={preferences}
            defaultValue={preferences[0]}
            onChange={type => handleChangeFilter(type.value)}
          />
        </div>
      </div>

      <div className={styles['debates-container']}>{renderDebates()}</div>
      {isEmpty(sortedDebates) && <p>Aucun débat associé à ce thème n'a été ajouté pour le moment.</p>}
    </div>
  );
};

Theme.propTypes = {
  debates: PropTypes.array.isRequired,
  currentTheme: PropTypes.object.isRequired,
};

export default isFallback(Theme);
