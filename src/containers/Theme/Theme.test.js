/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import nextUserRouterMock from '../../../test/nextUserRouterMock';
import RouterMock from '../../../test/RouterMock';
import { mockStore } from '../../store';
import Theme from './index';

const fakeDebates = [
  {
    id: 1,
    imageLink: 'profile/defaultUser_zdniza',
    debateState: 'draft',
    title: 'Titre',
    createdDate: Date.now(),
  },
];

const fakeTheme = {
  id: 1,
  label: 'theme',
};

const getMountComponent = () => {
  return mount(
    <RouterMock path="/theme/1">
      <Provider store={mockStore}>
        <Theme debates={fakeDebates} currentTheme={fakeTheme} />
      </Provider>
    </RouterMock>,
  );
};

jest.mock('cloudinary-react', () => ({
  Image: () => {
    return <div>Image</div>;
  },
}));

describe('Theme Component', () => {
  nextUserRouterMock({
    route: '/theme/id',
    pathname: '/theme/id',
    query: '',
    asPath: '/theme/1',
  });

  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
});
