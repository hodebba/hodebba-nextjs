import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';

const ThemeSeo = ({ currentTheme }) => {
  return (
    <NextSeo
      title={`${currentTheme.label} - Débats et avis des personnalités publiques`}
      description={`Découvrez tous les débats sur le sujet ${currentTheme.label}. Comprenez les enjeux des débats, ajoutez des arguments, commentez ceux des autres, participez au sondage ou encore observez les avis des personnalités publiques`}
      openGraph={{
        url: process.env.NEXT_PUBLIC_FRONT_END_URL,
        type: 'website',
        title: `${currentTheme.label} - Débats et avis des personnalités publiques`,
        description: `Découvrez tous les débats sur le sujet ${currentTheme.label}. Comprenez les enjeux des débats, ajoutez des arguments, commentez ceux des autres, participez au sondage ou encore observez les avis des personnalités publiques`,
        images: [
          {
            url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/hodebba-home_zphehh`,
            alt: 'Hodebba logo',
          },
        ],
        site_name: 'Hodebba',
      }}
      twitter={{
        handle: '@handle',
        site: '@site',
        cardType: 'summary_large_image',
      }}
    />
  );
};

ThemeSeo.propTypes = {
  currentTheme: PropTypes.object,
};

export default ThemeSeo;
