/* eslint-disable react-hooks/exhaustive-deps */

import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { deleteImageFromCloudinary } from '../../../actions/admin.actions';
import {
  createCelebrity,
  getPoliticalParties,
  getPoliticalPositions,
  getProfessions,
} from '../../../actions/celebrity.actions';
import FilterDropdown from '../../../components/FilterDropdown';
import ImageUploader from '../../../components/ImageUploader';
import SubmitButton from '../../../components/SubmitButton';
import { themeArrayToOptions } from '../../../utils/general.helper';
import CelebrityForm from './CelebrityForm';
import styles from './create-celebrity.module.scss';

// * Problem on refresh, it does not delete image in case this one has not been submitted

const CreateCelebrity = () => {
  const dispatch = useDispatch(null);
  const token = useSelector(state => state.user.local.token);
  const [error, setError] = useState('');
  const [imageUploaded, setImageUploaded] = useState({});
  const returnCelebrity = useSelector(state => state.celebrity.data.createCelebrity);
  const loading = useSelector(state => state.celebrity.local.loading.createCelebrity);
  const professions = useSelector(state => state.celebrity.local.professions);
  const politicalPositions = useSelector(state => state.celebrity.local.politicalPositions);
  const politicalParties = useSelector(state => state.celebrity.local.politicalParties);
  const [profession, setProfession] = useState(null);
  const [politicalPosition, setPoliticalPosition] = useState(null);
  const [politicalParty, setPoliticalParty] = useState(null);

  const router = useRouter();

  const handleSubmitCelebrityForm = values => {
    if (profession === null) {
      setError('Merci de sélectionner une profession.');
    } else if (isEmpty(imageUploaded)) {
      setError('Une photo de profil doit obligatoirement être sélectionnée.');
    } else {
      dispatch(
        createCelebrity(
          {
            fullName: values.name,
            imageLink: values.image.public_id,
            shortDescription: values.description,
            profession: profession.label,
            politicalPosition: (politicalPosition && politicalPosition.label) || null,
            politicalParty: (politicalParty && politicalParty.label) || null,
          },
          token,
        ),
      );
      setImageUploaded({});
    }
  };

  const clearUnusedImage = () => {
    if (!isEmpty(imageUploaded) && !loading && isEmpty(returnCelebrity)) {
      dispatch(deleteImageFromCloudinary(imageUploaded.public_id, token));
    }
  };

  useEffect(() => {
    dispatch(getProfessions());
    dispatch(getPoliticalParties());
    dispatch(getPoliticalPositions());
    return () => {
      clearUnusedImage();
    };
  }, []);

  useEffect(() => {
    if (!isEmpty(returnCelebrity)) {
      router.push('/celebrities/[id]/positions', `/celebrities/${returnCelebrity.id}/positions`);
    }
  }, [returnCelebrity]);

  const handleSendImageToForm = image => {
    clearUnusedImage();
    setImageUploaded(image);
  };
  return (
    <div className={styles.container}>
      <div>
        <CelebrityForm onSubmit={handleSubmitCelebrityForm} image={imageUploaded} />

        <div className={styles.filter}>
          <FilterDropdown
            options={themeArrayToOptions(professions)}
            isSearchable
            placeholder="Sélectionner une profession"
            onChange={newProfession => setProfession(newProfession)}
          />
        </div>

        <div className={styles.filter}>
          <FilterDropdown
            options={themeArrayToOptions(politicalPositions)}
            isSearchable
            placeholder="Sélectionner un positionnement politique (optionnel)"
            onChange={newPoliticalPosition => setPoliticalPosition(newPoliticalPosition)}
          />
        </div>

        <div className={styles.filter}>
          <FilterDropdown
            options={themeArrayToOptions(politicalParties)}
            isSearchable
            placeholder="Sélectionner un parti politique (optionnel)"
            onChange={newParty => setPoliticalParty(newParty)}
          />
        </div>

        {/* ImageUploader is outside the form to prevent its re-render when the form is changing */}
        <ImageUploader
          preset={process.env.NEXT_PUBLIC_CLOUDINARY_CELEBRITIES_PRESET}
          image={imageUploaded}
          sendErrorUpload={errorUpload => setError(errorUpload)}
          sendImageToForm={image => handleSendImageToForm(image)}
          title="Photo"
          tooltipText="Merci d'utiliser une photo libre de droits"
        />

        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}

        <SubmitButton form="celebrity-form" label="Ajouter personnalité" loading={loading} />
      </div>
    </div>
  );
};

CreateCelebrity.propTypes = {};

export default CreateCelebrity;
