/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable implicit-arrow-linebreak */

import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';

import RenderTextField from '../../../../components/RenderTextField';

const required = value => (value ? undefined : 'Champ obligatoire.');

const maxLength = max => value =>
  value && value.length > max ? `Votre pseudo ne doit pas dépasser ${max} caractères.` : undefined;
const maxLength50 = maxLength(50);

const atLeast2Letters = value =>
  value && !/.*[a-zA-Z].*[a-zA-Z].*/.test(value) ? 'Votre pseudo doit contenir au moins 2 lettres.' : undefined;

const CelebrityForm = ({ handleSubmit, change, image }) => {
  useEffect(() => {
    if (image !== '') {
      change('image', image);
    }
  }, [image]);

  return (
    <form id="celebrity-form" onSubmit={handleSubmit}>
      <Field
        name="name"
        checkEnabled
        type="text"
        component={RenderTextField}
        label="Nom complet de la personnalité"
        validate={[required, maxLength50, atLeast2Letters]}
      />

      <Field name="description" checkEnabled type="text" component={RenderTextField} label="Courte description" />
    </form>
  );
};

CelebrityForm.propTypes = {
  handleSubmit: PropTypes.func,
  change: PropTypes.func,
  image: PropTypes.object,
};

export default reduxForm({
  form: 'Celebrity',
})(CelebrityForm);
