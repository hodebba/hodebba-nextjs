/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable react-hooks/exhaustive-deps */
import isEmpty from 'lodash/isEmpty';
import isNumber from 'lodash/isNumber';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import CheckIcon from '../../../../public/static/icons/check.svg';
import CrossIcon from '../../../../public/static/icons/close.svg';
import { approveStatement, disapproveStatement } from '../../../actions/admin.actions';
import InfoModal from '../../../components/InfoModal';
import PositionButtons from '../../../components/PositionButtons';
import SubmitButton from '../../../components/SubmitButton';
import Statement from '../../Celebrities/Celebrity/Statements/Statement';
import styles from './not-validated-statements.module.scss';

const textModal = {
  error: 'Êtes-vous sûr de vouloir supprimer définitivement cette déclaration ?',
  validate: 'Êtes-vous sûr de vouloir mettre en ligne cette déclaration ?',
};

const NotValidatedStatements = ({ statements }) => {
  const [modal, setModal] = useState({ isOpened: false, type: '', id: null });
  const token = useSelector(state => state.user.local.token);
  const loadingApprove = useSelector(state => state.admin.local.loading.approveStatement);
  const errorApprove = useSelector(state => state.admin.local.errors.approveStatement);
  const errorDelete = useSelector(state => state.admin.local.errors.disapproveStatement);
  const approvedStatement = useSelector(state => state.admin.data.approvedStatement);
  const disapprovedStatement = useSelector(state => state.admin.data.disapprovedStatement);
  const [statementList, setStatementList] = useState(statements);
  const dispatch = useDispatch();
  const [positions, setPositions] = useState({});

  useEffect(() => {
    if (!isEmpty(approvedStatement)) {
      setStatementList(statementList.filter(element => element.id !== approvedStatement.id));
      setModal({ isOpened: false, type: '', id: null });
    }
  }, [approvedStatement]);

  useEffect(() => {
    if (isNumber(disapprovedStatement)) {
      setStatementList(statementList.filter(element => element.id !== disapprovedStatement));
      setModal({ isOpened: false, type: '', id: null });
    }
  }, [disapprovedStatement]);

  const handleDeleteStatement = () => {
    dispatch(disapproveStatement(modal.id, token));
  };

  const handleValidateStatement = () => {
    const body = {
      id: modal.id,
      isValidated: true,
      statementType: positions[modal.id] || null,
    };
    dispatch(approveStatement(body, token));
  };

  const renderStatement = statement => {
    return (
      <div key={statement.id} className={styles.statement}>
        {statement.debates[0] && (
          <div className={styles.statement__debate}>
            <p>
              <span>Débat associé</span>: {statement.debates[0].title}
            </p>
            <PositionButtons
              labels={{ pro: 'Pour', con: 'Contre' }}
              getPosition={newPosition =>
                setPositions(oldPositions => ({ ...oldPositions, [statement.id]: newPosition }))
              }
              defaultPosition="pro"
            />
          </div>
        )}
        <div className={styles.statement__text}>
          <Statement statement={statement} withoutLikes />
          <div className={styles.statement__buttons}>
            <button onClick={() => setModal({ isOpened: true, type: 'validate', id: statement.id })} type="button">
              <CheckIcon />
            </button>
            <button onClick={() => setModal({ isOpened: true, type: 'error', id: statement.id })} type="button">
              <CrossIcon />
            </button>
          </div>
        </div>
      </div>
    );
  };

  const renderStatements = () => {
    const ret = [];
    if (!isEmpty(statementList)) {
      statementList.forEach(statement => {
        ret.push(renderStatement(statement));
      });
    } else {
      ret.push(
        <p key={1} className={styles['no-statements']}>
          Aucune déclaration à valider.
        </p>,
      );
    }
    return ret;
  };

  const renderSubmitButton = type => {
    if (type === 'validate') {
      return <SubmitButton label="Oui, je confirme" onClick={handleValidateStatement} loading={loadingApprove} />;
    }
    return (
      <button type="submit" className="cancel-button" onClick={handleDeleteStatement}>
        Oui, je veux supprimer
      </button>
    );
  };

  return (
    <div className={styles.container}>
      {renderStatements()}

      {modal.isOpened && (
        <InfoModal onClose={() => setModal({ ...modal, isOpened: false })}>
          <p className={styles.modal__text}>{textModal[modal.type]}</p>
          <div className={styles.modal__buttons}>
            <button onClick={() => setModal({ ...modal, isOpened: false })} type="button" className="secondary-button">
              Annuler
            </button>
            {renderSubmitButton(modal.type)}
          </div>
          {(!isEmpty(errorApprove) || !isEmpty(errorDelete)) && (
            <div className="error-message">
              <div className="error-message__title">Merci de réessayer</div>
              <p>Une erreur innatendue est survenue. Merci de contacter le support technique</p>
            </div>
          )}
        </InfoModal>
      )}
    </div>
  );
};

NotValidatedStatements.propTypes = {
  statements: PropTypes.array,
};

export default NotValidatedStatements;
