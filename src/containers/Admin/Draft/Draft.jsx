import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';

import DebateSearchPreview from '../../Search/DebateSearchPreview';
import styles from './draft.module.scss';

const Draft = ({ debates }) => {
  const renderDebates = () => {
    const ret = [];
    if (!isEmpty(debates)) {
      debates.forEach(debate => {
        ret.push(<DebateSearchPreview key={debate.id} debate={debate} />);
      });
    } else {
      ret.push(
        <p key={1} className={styles['no-result']}>
          Aucun débat non publié trouvé.
        </p>,
      );
    }
    return ret;
  };

  return <div className={styles.container}>{renderDebates()}</div>;
};

Draft.propTypes = {
  debates: PropTypes.array,
};

export default Draft;
