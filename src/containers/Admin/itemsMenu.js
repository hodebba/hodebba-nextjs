import StatementIcon from '../../../public/static/icons/debate.svg';
import DebateIcon from '../../../public/static/icons/discussion.svg';
import DraftIcon from '../../../public/static/icons/draft.svg';
import PoliticianIcon from '../../../public/static/icons/politician.svg';

export const itemsMenu = [
  {
    id: 0,
    icon: <DebateIcon />,
    type: 'debate',
    href: '/admin/debate',
    label: 'Créer un débat',
  },
  {
    id: 1,
    icon: <PoliticianIcon />,
    type: 'celebrity',
    href: '/admin/celebrity',
    label: 'Ajouter une personnalité',
  },
  {
    id: 2,
    icon: <DraftIcon />,
    type: 'draft',
    href: '/admin/draft',
    label: 'Débats non publiés',
  },
  {
    id: 3,
    icon: <StatementIcon />,
    type: 'statements',
    href: '/admin/statements-to-validate',
    label: 'Déclarations à valider',
  },
];
