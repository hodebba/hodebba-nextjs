/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
/* eslint-disable implicit-arrow-linebreak */

import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';

import RenderTextField from '../../../../components/RenderTextField';
import styles from './debate-form.module.scss';

const required = value => (value ? undefined : 'Champ obligatoire.');

const maxLength = max => value =>
  value && value.length > max ? `Votre titre ne doit pas dépasser ${max} caractères.` : undefined;
const maxLength90 = maxLength(90);

const atLeast2Letters = value =>
  value && !/.*[a-zA-Z].*[a-zA-Z].*/.test(value) ? 'Votre titre doit contenir au moins 2 lettres.' : undefined;

const DebateForm = ({ handleSubmit, change, image }) => {
  useEffect(() => {
    if (image !== '') {
      change('image', image);
    }
  }, [image]);

  return (
    <form id="debate-form" className={styles.container} onSubmit={handleSubmit}>
      <Field
        name="title"
        checkEnabled
        type="text"
        component={RenderTextField}
        label="Titre du débat"
        validate={[required, maxLength90, atLeast2Letters]}
      />
    </form>
  );
};

DebateForm.propTypes = {
  handleSubmit: PropTypes.func,
  image: PropTypes.object,
  change: PropTypes.func,
};

export default reduxForm({
  form: 'Debate',
})(DebateForm);
