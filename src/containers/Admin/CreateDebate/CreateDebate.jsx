/* eslint-disable react-hooks/exhaustive-deps */

import * as gtag from '@Utils/gtags';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { WithContext as ReactTags } from 'react-tag-input';

import { deleteImageFromCloudinary } from '../../../actions/admin.actions';
import { createDebate, reinitializeCreateDebate } from '../../../actions/debate.actions';
import FilterDropdown from '../../../components/FilterDropdown';
import ImageUploader from '../../../components/ImageUploader';
import SubmitButton from '../../../components/SubmitButton';
import { compareLabel, themeArrayToOptions } from '../../../utils/general.helper';
import TextEditor from '../TextEditor';
import styles from './create-debate.module.scss';
import DebateForm from './DebateForm';

// * Problem on refresh, it does not delete image in case this one has not been submitted
const KeyCodes = {
  comma: 188,
  enter: 13,
};
const delimiters = [KeyCodes.comma, KeyCodes.enter];

const suggestions = [
  { id: '1', text: 'mango' },
  { id: '2', text: 'pineapple' },
  { id: '3', text: 'orange' },
  { id: '4', text: 'pear' },
];

const CreateDebate = ({ isPrivate, createdByCommunity }) => {
  const dispatch = useDispatch(null);
  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const themes = useSelector(state => state.debate.local.themes);
  const loading = useSelector(state => state.debate.local.loading.createDebate);
  const returnDebate = useSelector(state => state.debate.data.createDebate);
  const [error, setError] = useState('');
  const [imageUploaded, setImageUploaded] = useState({});
  const [textContent, setTextContent] = useState({});
  const [theme, setTheme] = useState('');
  const [tags, setTags] = useState([]);
  const router = useRouter();

  const checkFields = () => {
    let ret = false;
    if (isEmpty(imageUploaded)) {
      setError('Une image de fond doit obligatoirement être sélectionnée.');
    } else if (isEmpty(textContent)) {
      setError('Le champ "Comprendre le débat" est vide.');
    } else if (theme === '' && !isPrivate) {
      setError('Vous devez obligatoirement associer un thème à votre débat.');
    } else {
      setError('');
      ret = true;
    }
    return ret;
  };

  const handleSubmitDebateForm = values => {
    if (checkFields()) {
      const formatTags = [];
      tags.forEach(tag => {
        formatTags.push(tag.text);
      });
      const data = {
        title: values.title,
        imageLink: imageUploaded.public_id,
        themeId: isPrivate ? 100 : theme.value,
        isPrivate,
        createdByCommunity,
        tags: formatTags,
        presentation: textContent.level.content,
        userId: user.id,
        poll: {
          question: values.title,
          pollOptions: ['Pour', 'Neutre', 'Contre'],
        },
      };

      dispatch(createDebate(data, token));
      setImageUploaded({});
    }
  };

  const clearUnusedImage = () => {
    if (!isEmpty(imageUploaded)) {
      dispatch(deleteImageFromCloudinary(imageUploaded.public_id, token));
    }
  };

  useEffect(() => {
    return () => {
      clearUnusedImage();
    };
  }, []);

  useEffect(() => {
    if (!isEmpty(returnDebate)) {
      dispatch(reinitializeCreateDebate());
      gtag.event('debate_created', {});
      if (isPrivate) {
        router.push('/debate/private/[id]/presentation', `/debate/private/${returnDebate.id}/presentation`);
      } else {
        router.push('/create/success');
      }
    }
  }, [returnDebate]);

  const handleSendImageToForm = image => {
    clearUnusedImage();
    setImageUploaded(image);
  };

  const onDelete = i => {
    setTags(tags.filter((tag, index) => index !== i));
  };

  const onAddition = tag => {
    setTags([...tags, tag]);
  };

  let sortedThemes = [...themes];
  sortedThemes = sortedThemes.sort(compareLabel);

  return (
    <div className={styles.container}>
      <div>
        <DebateForm onSubmit={handleSubmitDebateForm} image={imageUploaded} />

        {!isPrivate && (
          <div className={styles.theme}>
            <div className={styles.theme__label}>Thème du débat</div>
            <FilterDropdown
              options={themeArrayToOptions(sortedThemes)}
              isSearchable
              placeholder="Sélectionner un thème"
              onChange={newTheme => setTheme(newTheme)}
            />
          </div>
        )}

        <div className={styles.tags}>
          <div className={styles.tags__label}>Tags (optionnel)</div>
          <ReactTags
            tags={tags}
            handleDelete={onDelete}
            handleAddition={onAddition}
            suggestions={suggestions}
            delimiters={delimiters}
            placeholder="Ajouter un nouveau tag (ex: climat)"
            allowDragDrop={false}
          />
        </div>

        {/* ImageUploader is outside the form to prevent its re-render when the form is changing */}
        <ImageUploader
          preset={process.env.NEXT_PUBLIC_CLOUDINARY_DEBATES_PRESET}
          image={imageUploaded}
          sendErrorUpload={errorUpload => setError(errorUpload)}
          sendImageToForm={image => handleSendImageToForm(image)}
          title="Image de fond"
          tooltipText="Ajoutez une image permettant d'illustrer le débat"
        />

        <TextEditor onChange={content => setTextContent(content)} />

        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}

        <SubmitButton form="debate-form" label="Créer le débat" loading={loading} />
      </div>
    </div>
  );
};

CreateDebate.defaultProps = {
  isPrivate: false,
  createdByCommunity: false,
};

CreateDebate.propTypes = {
  isPrivate: PropTypes.bool,
  createdByCommunity: PropTypes.bool,
};

export default CreateDebate;
