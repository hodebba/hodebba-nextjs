import { Editor } from '@tinymce/tinymce-react';
import PropTypes from 'prop-types';

import Tooltip from '../../../components/Tooltip';
import styles from './text-editor.module.scss';

function TextEditor({ onChange }) {
  return (
    <div className={styles.container}>
      <div className={styles.title}>
        Comprendre le débat
        <Tooltip text="Donnez tous les éléments permettant de mieux comprendre le débat: contexte, enjeux, statistiques ..." />
      </div>
      <div className={styles.reminder}>
        <span className="underline">Rappel</span>: 2 min de lecture correspond environ à 420 mots
      </div>
      <Editor
        apiKey={process.env.NEXT_PUBLIC_TEXT_EDITOR_API_KEY}
        init={{
          height: 600,
          menubar: true,
          plugins: [
            'advlist autolink lists link image',
            'charmap preview anchor help',
            'searchreplace visualblocks fullscreen ',
            'insertdatetime media paste wordcount',
          ],
          toolbar:
            'undo redo | formatselect | bold italic | alignleft aligncenter alignright image link fullscreen | bullist numlist outdent indent insertdatetime media | help',
        }}
        onChange={content => onChange(content)}
      />
    </div>
  );
}

TextEditor.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default TextEditor;
