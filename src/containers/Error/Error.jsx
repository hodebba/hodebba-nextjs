/* eslint-disable react/no-unescaped-entities */
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';

import styles from './error.module.scss';

const errorText = {
  404: {
    title: "Cette page n'existe pas",
    text:
      "Ooops, il semblerait que la page que vous recherchez n'est plus disponible. Merci de vérifier que l'URL de la page est correct.",
  },
  403: {
    title: "Impossible d'accèder à cette page",
    text: "Ooops, il semblerait que vous n'ayez pas les droits requis pour accèder à cette page.",
  },
  500: {
    title: 'Erreur inattendue du serveur',
    text:
      'Une erreur est apparue et nous travaillions actuellement pour essayer de la résoudre. Merci de réessayer plus tard.',
  },
};

const Error = ({ code }) => {
  return (
    <div className={styles.container}>
      <NextSeo noindex />
      <div className={styles.number}>{code}</div>
      <h1>{errorText[code].title}</h1>
      <p>{errorText[code].text}</p>
      <Link href="/">
        <a>
          <button type="button" className="primary-button">
            Retour à l'accueil
          </button>
        </a>
      </Link>
    </div>
  );
};

Error.propTypes = {
  code: PropTypes.number,
};

export default Error;
