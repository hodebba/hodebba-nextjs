/* eslint-disable react/no-unescaped-entities */
/* eslint-disable implicit-arrow-linebreak */

import { isEmpty } from 'lodash';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import PasswordHelper from '../../../components/PasswordHelper';
import RenderTextField from '../../../components/RenderTextField';
import SubmitButton from '../../../components/SubmitButton';
import {
  atLeast2Letters,
  email,
  hasALowercase,
  hasANumber,
  hasAnUppercase,
  hasASpecialSigns,
  matchPassword1,
  maxLength50Pseudo,
  maxLength50Pwd,
  minLength8,
  required,
  trim,
} from '../../../utils/check-fields.helper';
import * as gtag from '../../../utils/gtags';
import styles from './sign-up.module.scss';

const SignUp = ({ handleSubmit, submitting }) => {
  const [displayHelpPwd, setDisplayHelpPwd] = useState(false);
  const errors = useSelector(state => state.authentification.local.errors.signUp);
  const loading = useSelector(state => state.authentification.local.loading.signUp);
  const [error, setError] = useState('');

  const passwordIsOk = useCallback(value => {
    if (value && /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,50}$/g.test(value)) {
      setDisplayHelpPwd(false);
    }
  }, []);

  useEffect(() => {
    if (!isEmpty(errors)) {
      if (errors.status === 400) {
        const { codes } = errors.errors[0];
        if (codes.includes('ValidPassword')) {
          setError(
            'Le mot de passe est invalide. Il doit contenir au moins 1 lettre majuscule, 1 lettre minuscule, 1 chiffre, 1 caractère spéciale, et compter au moins 8 caractères.',
          );
        } else if (codes.includes('ValidEmail')) {
          setError("L'adresse email est invalide.");
        } else if (codes.includes('PasswordMatches')) {
          setError('Les mots de passe ne correspondent pas.');
        } else if (codes.includes('NotNull')) {
          setError('Tous les champs doivent être remplis');
        }
      } else {
        setError(errors.message);
      }
    }
  }, [errors]);

  useEffect(() => {
    gtag.event('start_sign_up', {
      debug_mode: true,
    });
  }, []);

  return (
    <form className={styles.container} onSubmit={handleSubmit}>
      <Field
        name="username"
        checkEnabled
        type="text"
        component={RenderTextField}
        label="Pseudo"
        validate={[required, maxLength50Pseudo, atLeast2Letters]}
        normalize={trim}
      />
      <Field
        name="email"
        type="email"
        checkEnabled
        component={RenderTextField}
        label="Email"
        validate={[required, email]}
        normalize={trim}
      />
      <Field
        name="password"
        type="password"
        isPassword
        checkEnabled
        component={RenderTextField}
        label="Mot de passe"
        normalize={trim}
        onFocus={() => setDisplayHelpPwd(true)}
        onBlur={() => setDisplayHelpPwd(false)}
        validate={[
          required,
          hasAnUppercase,
          hasALowercase,
          hasANumber,
          hasASpecialSigns,
          minLength8,
          passwordIsOk,
          maxLength50Pwd,
        ]}
      />

      {displayHelpPwd && <PasswordHelper />}

      <Field
        name="matchingPassword"
        checkEnabled
        type="password"
        isPassword
        component={RenderTextField}
        label="Confirmer mot de passe"
        validate={[
          required,
          matchPassword1,
          hasAnUppercase,
          hasALowercase,
          hasANumber,
          hasASpecialSigns,
          minLength8,
          maxLength50Pwd,
        ]}
        normalize={trim}
      />

      <p className={styles['terms-and-conditions']}>
        En vous inscrivant vous acceptez{' '}
        <Link href="/conditions-generales">
          <a className="underlined link-without-style" target="_blank">
            les conditions générales d'utilisation et la politique de confidentialité
          </a>
        </Link>
      </p>

      <div>
        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}
        <SubmitButton label="Créer votre compte" submitting={submitting} loading={loading} />
      </div>
    </form>
  );
};

SignUp.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'SignUp',
})(SignUp);
