/* eslint-disable no-unused-vars */
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import { mockStore } from '../../../store';
import SignUp from './index';

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <SignUp />
    </Provider>,
  );
};
describe('Sign Up Component', () => {
  window.gtag = jest.fn(gtag => gtag);

  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
  it('should render 2 inputs: email and password', () => {
    const wrapper = getMountComponent();
    const receivedNamesList = wrapper
      .find('form')
      .children()
      .map(node => node.props().name);

    const expectedNamesList = ['username', 'email', 'password', 'matchingPassword', undefined, undefined];
    expect(receivedNamesList).toEqual(expect.arrayContaining(expectedNamesList));
  });
});
