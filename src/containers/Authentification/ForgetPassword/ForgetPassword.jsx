/* eslint-disable implicit-arrow-linebreak */

import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import RenderTextField from '../../../components/RenderTextField';
import SubmitButton from '../../../components/SubmitButton';
import { email, required, trim } from '../../../utils/check-fields.helper';

const ForgetPassword = ({ handleSubmit, submitting }) => {
  const errors = useSelector(state => state.authentification.local.errors.forgetPassword);
  const loading = useSelector(state => state.authentification.local.loading.forgetPassword);
  const [error, setError] = useState('');

  useEffect(() => {
    if (!isEmpty(errors)) {
      setError(
        "Aucun utilisateur trouvé pour cette adresse email. Merci de vérifier l'orthographe de votre saisie, ou cliquez ci-dessous pour vous créer un compte.",
      );
    }
  }, [errors]);

  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="email"
        type="email"
        checkEnabled
        component={RenderTextField}
        label="Email"
        normalize={trim}
        validate={[required, email]}
      />

      <div>
        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}
        <SubmitButton label="Envoyer" submitting={submitting} loading={loading} />
      </div>
    </form>
  );
};

ForgetPassword.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'ForgetPassword',
})(ForgetPassword);
