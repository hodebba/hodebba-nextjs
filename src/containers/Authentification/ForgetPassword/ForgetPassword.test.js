/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import { mockStore } from '../../../store';
import ForgetPassword from './index';

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <ForgetPassword />
    </Provider>,
  );
};
describe('ForgetPassword Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
  it('should render 2 inputs: email and password', () => {
    const wrapper = getMountComponent();
    const receivedNamesList = wrapper
      .find('form')
      .children()
      .map(node => node.props().name);

    const expectedNamesList = ['email', undefined];
    expect(receivedNamesList).toEqual(expect.arrayContaining(expectedNamesList));
  });
});
