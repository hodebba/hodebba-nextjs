/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable implicit-arrow-linebreak */

import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm, reset } from 'redux-form';

import { openModalAuthentification } from '../../../actions/authentification.actions';
import RenderTextField from '../../../components/RenderTextField';
import SubmitButton from '../../../components/SubmitButton';
import { email, trim } from '../../../utils/check-fields.helper';
import styles from './sign-in.module.scss';

const SignIn = ({ submitting, handleSubmit }) => {
  const errors = useSelector(state => state.authentification.local.errors.signIn);
  const loading = useSelector(state => state.authentification.local.loading.signIn);
  const dispatch = useDispatch(null);

  useEffect(() => {
    if (!isEmpty(errors)) {
      dispatch(reset('SignIn'));
    }
  }, [errors]);

  return (
    <form method="post" className={styles.container} onSubmit={handleSubmit}>
      <Field
        name="email"
        type="email"
        component={RenderTextField}
        normalizeOnBlur={value => value && value.trim()}
        label="Email"
        validate={[email]}
        normalize={trim}
      />
      <Field
        name="password"
        isPassword
        type="password"
        component={RenderTextField}
        label="Mot de passe"
        normalize={trim}
      />
      <div className={styles.options}>
        {/* <div className="sign-in__options__remember-me">
          <Field name="remember-me" id="remember-me" type="checkbox" component="input" />
          <label htmlFor="remember-me">Se souvenir de moi</label>
        </div> */}
        <button
          type="button"
          onClick={() => dispatch(openModalAuthentification('forgetPassword'))}
          className="text-button"
        >
          mot de passe oublié ?
        </button>
      </div>

      <div>
        {!isEmpty(errors) && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>Vos identifiants de connexion sont incorrects.</p>
          </div>
        )}
        <SubmitButton label="Se connecter" submitting={submitting} loading={loading} />
      </div>
    </form>
  );
};

SignIn.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'SignIn',
})(SignIn);
