/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import { mockStore } from '../../../store';
import SignIn from './index';

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <SignIn />
    </Provider>,
  );
};
describe('Sign In Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
  it('should render 2 inputs: email and password', () => {
    const wrapper = getMountComponent();
    const receivedNamesList = wrapper
      .find('form')
      .children()
      .map(node => node.props().name);

    const expectedNamesList = ['email', 'password', undefined];
    expect(receivedNamesList).toEqual(expect.arrayContaining(expectedNamesList));
  });
});
