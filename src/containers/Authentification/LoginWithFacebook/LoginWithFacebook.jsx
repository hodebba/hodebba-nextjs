import FacebookLogin from 'react-facebook-login';
import { useDispatch } from 'react-redux';

import { continueWithFacebookGoogle } from '../../../actions/authentification.actions';

const LoginWithFacebook = () => {
  const dispatch = useDispatch();

  const responseFacebook = response => {
    dispatch(continueWithFacebookGoogle(response));
  };

  return (
    <div>
      <FacebookLogin
        appId={process.env.NEXT_PUBLIC_FACEBOOK_ID}
        textButton="Continuer avec Facebook"
        fields="name,email"
        callback={responseFacebook}
        cssClass="my-facebook-button"
        icon="fa-facebook"
      />
    </div>
  );
};

export default LoginWithFacebook;
