import GoogleLogin from 'react-google-login';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';

import { continueWithFacebookGoogle } from '../../../actions/authentification.actions';

const LoginWithGoogle = () => {
  const dispatch = useDispatch();

  const onSuccess = response => {
    const data = {
      email: response.profileObj.email,
      name: response.profileObj.name,
      accessToken: response.accessToken,
    };
    dispatch(continueWithFacebookGoogle(data));
  };

  const onFailure = () => {
    toast.error("L'identification avec Google a échoué.");
  };

  return (
    <div>
      <GoogleLogin
        clientId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}
        buttonText="Continuer avec Google"
        onSuccess={onSuccess}
        onFailure={onFailure}
        className="my-google-button"
        cookiePolicy="single_host_origin"
      />
    </div>
  );
};

export default LoginWithGoogle;
