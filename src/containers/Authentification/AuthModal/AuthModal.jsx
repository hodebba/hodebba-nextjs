/* eslint-disable operator-linebreak */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import arrowLeft from '@iconify/icons-vaadin/arrow-left';
import closeSmall from '@iconify/icons-vaadin/close-small';
import { Icon } from '@iconify/react';
import PropTypes from 'prop-types';
import { Fragment, useState } from 'react';
import { useDispatch } from 'react-redux';

import {
  closeModalAuthentification,
  forgetPassword,
  openModalAuthentification,
  signIn,
  signUp,
} from '../../../actions/authentification.actions';
import SuccessfulAction from '../../../components/SuccessfulAction';
import ForgetPassword from '../ForgetPassword';
import LoginWithFacebook from '../LoginWithFacebook';
import LoginWithGoogle from '../LoginWithGoogle';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import styles from './auth-modal.module.scss';

const AuthModal = ({ type, emailResetPassword, user }) => {
  const dispatch = useDispatch();
  const [isEmailLogin, setIsEmailLogin] = useState(false);

  const authData = {};
  switch (type) {
    case 'signUp':
      authData.button = 'Inscription avec une adresse email';
      authData.title = 'Inscription';
      authData.heading = 'Inscrivez-vous avec votre adresse email';
      authData.question = 'Déjà inscrit ?';
      authData.action = 'Se connecter';
      authData.component = <SignUp onSubmit={values => dispatch(signUp(values))} />;
      break;

    case 'signIn':
      authData.button = 'Connexion avec une adresse email';
      authData.title = 'Connexion';
      authData.heading = 'Connectez-vous avec votre adresse email';
      authData.question = 'Pas encore inscrit ?';
      authData.action = 'Créer un compte';
      authData.component = <SignIn onSubmit={values => dispatch(signIn(values))} />;
      break;

    case 'forgetPassword':
      authData.title = 'Mot de passe oublié';
      authData.heading =
        'Inscrivez votre adresse email ci-dessous, et nous vous enverrons par email un lien permettant de créer un nouveau mot de passe.';
      authData.question = 'Pas encore inscrit ?';
      authData.action = 'Créer un compte';
      authData.parentClassName = `${styles['forget-password']}`;
      authData.component = <ForgetPassword onSubmit={values => dispatch(forgetPassword(values.email))} />;
      break;
    case 'successfulSignUp':
      authData.parentClassName = `${styles['successful-email-sent']}`;
      authData.component = (
        <SuccessfulAction
          title="Votre compte a été créé avec succès"
          text={`Un email de confirmation a été envoyé à votre adresse ${user.email}. Merci de cliquer sur lien se trouvant dans cet email pour activer votre compte.`}
          textButton="Terminé"
          onClick={() => dispatch(closeModalAuthentification())}
        />
      );
      break;
    case 'successfulEmailSent':
      authData.parentClassName = `${styles['successful-email-sent']}`;
      authData.component = (
        <SuccessfulAction
          title="Email de réinitialisation de mot de passe envoyé"
          text={`Un email a été envoyé à votre adresse ${emailResetPassword}. Suivez les instructions fournies pour réinitialiser votre mot de passe.`}
          textButton="Terminé"
          onClick={() => dispatch(closeModalAuthentification())}
        />
      );
      break;

    default:
  }

  return (
    <Fragment>
      <div className={`${styles.container} ${authData.parentClassName}`}>
        <div className={type === 'forgetPassword' ? `${styles.buttons}` : `${styles['close-button-container']}`}>
          {type === 'forgetPassword' && (
            <div className={styles['back-button']} onClick={() => dispatch(openModalAuthentification('signIn'))}>
              <Icon width="22" height="22" icon={arrowLeft} className="clickable" />
            </div>
          )}

          <div className={styles['close-button']} onClick={() => dispatch(closeModalAuthentification())}>
            <Icon icon={closeSmall} height="28" width="28" />
          </div>
        </div>

        <div className={styles.layout}>
          {!isEmailLogin && (
            <Fragment>
              <LoginWithFacebook />
              <LoginWithGoogle />
              <div className={styles.separator}>
                <div className={styles.separator__line}></div>
                <div className={styles.separator__text}>OU</div>
                <div className={styles.separator__line}></div>
              </div>
              <button type="button" onClick={() => setIsEmailLogin(true)} className={styles['email-button']}>
                {authData.button}
              </button>
            </Fragment>
          )}

          {isEmailLogin && (
            <Fragment>
              <h2>{authData.title}</h2>
              <h3>{authData.heading}</h3>
              <div id={styles.error}></div>
              <div>{authData.component}</div>
            </Fragment>
          )}

          <p>
            {authData.question}
            <span
              className={styles.action}
              onClick={() => dispatch(openModalAuthentification(type === 'signUp' ? 'signIn' : 'signUp'))}
            >
              {authData.action}
            </span>
          </p>
        </div>
      </div>
      <div className="background-less-opacity"></div>
    </Fragment>
  );
};

AuthModal.propTypes = {
  type: PropTypes.string.isRequired,
  emailResetPassword: PropTypes.string,
  user: PropTypes.object,
};

export default AuthModal;
