/* eslint-disable no-unused-vars */
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import RouterMock from '../../../../test/RouterMock';
import { mockStore } from '../../../store';
import Poll from './index';

jest.mock('cloudinary-react', () => ({
  Image: () => {
    return <div>Image</div>;
  },
}));

const poll = {
  id: 57,
  question: 'Doit-on limiter la quantité de viande par habitant ?',
  pollVoteTypes: [
    {
      id: 67,
      label: 'Pour',
      count: 0,
    },
    {
      id: 68,
      label: 'Neutre',
      count: 0,
    },
    {
      id: 69,
      label: 'Contre',
      count: 0,
    },
  ],
};

const getMountComponent = () => {
  return mount(
    <RouterMock path="/debate/1/presentation">
      <Provider store={mockStore}>
        <Poll poll={poll} views={20} debate={{ id: 1, isPrivate: false }} />
      </Provider>
    </RouterMock>,
  );
};
describe('Poll Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
});
