/* eslint-disable object-curly-newline */
/* eslint-disable operator-linebreak */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
import arrowLeft from '@iconify/icons-vaadin/arrow-left';
import { Icon } from '@iconify/react';
import { isAdminParticipant, isViewerParticipant } from '@Utils/debate.helper';
import { Image, Transformation } from 'cloudinary-react';
import Cookie from 'js-cookie';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import isUndefined from 'lodash/isUndefined';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import SettingsIcon from '../../../../public/static/icons/settings.svg';
import IconVisibility from '../../../../public/static/icons/visibility.svg';
import IconVote from '../../../../public/static/icons/voting.svg';
import { openModalAuthentification } from '../../../actions/authentification.actions';
import { sendVote } from '../../../actions/debate.actions';
import { updateUserVotes } from '../../../actions/votes.actions';
import { kFormatter, sortById } from '../../../utils/general.helper';
import styles from './poll.module.scss';

const mappingVoteType = {
  pro: 0,
  neutral: 1,
  con: 2,
};

const emptyVote = { pro: 0, neutral: 0, con: 0 };
const Poll = ({ poll, image, views, debate }) => {
  const isLoggedIn = !isEmpty(useSelector(state => state.user.local.token));
  const user = useSelector(state => state.user.local.user);
  const votes = useSelector(state => state.votes.polls);
  const token = useSelector(state => state.user.local.token);
  const returnPath = useSelector(state => state.user.local.returnPath);
  const router = useRouter();
  const dispatch = useDispatch();

  const pro = useRef(null);
  const neutral = useRef(null);
  const con = useRef(null);

  const checkCookie = () => {
    if (!isUndefined(Cookie.get('polls'))) {
      return !isUndefined(JSON.parse(Cookie.get('polls'))[poll.id]);
    }
    return false;
  };

  const [hasVoted, setHasVoted] = useState(!isUndefined(votes[poll.id]));
  const [initialVote, setInitialVote] = useState(checkCookie() ? JSON.parse(Cookie.get('polls'))[poll.id] : emptyVote);

  const [isNewVote, setIsNewVote] = useState(false);
  const [userVote, setUserVote] = useState(isUndefined(votes[poll.id]) ? emptyVote : votes[poll.id]);
  const [tempVote, setTempVote] = useState(emptyVote);
  const [resultsAreVisible, setResultsAreVisible] = useState(!isLoggedIn);
  const [textButton, setTextButton] = useState('Voir résultats');
  const pollVoteTypes = poll.pollVoteTypes.sort(sortById);

  let nbVotes = 0;
  pollVoteTypes.forEach(pollVoteType => {
    nbVotes += pollVoteType.count;
  });
  const total = isNewVote ? nbVotes + 1 : nbVotes;

  useEffect(() => {
    if (
      !isUndefined(Cookie.get('polls')) &&
      !isUndefined(JSON.parse(Cookie.get('polls'))[poll.id]) &&
      isEqual(initialVote, emptyVote)
    ) {
      setInitialVote(JSON.parse(Cookie.get('polls'))[poll.id]);
    }
  }, [votes]);

  useEffect(() => {
    getResults();
  }, [poll]);

  useEffect(() => {
    if (resultsAreVisible && !hasVoted) {
      setTextButton('Voter !');
      disableButtons();
    } else if (resultsAreVisible && hasVoted) {
      setTextButton("J'ai changé d'avis");
      disableButtons();
    } else {
      setTextButton('Voir résultats');
    }
  }, [resultsAreVisible, hasVoted]);

  useEffect(() => {
    if (hasVoted) {
      toggleClassButton();
      getResults();
    }
  }, [hasVoted]);

  useEffect(() => {
    if (returnPath) {
      router.prefetch(returnPath.href, returnPath.as);
    } else {
      router.prefetch('/');
    }
    if (resultsAreVisible) {
      toggleClassButton();
      getResults();
    }
  }, []);

  useEffect(() => {
    if (!isUndefined(votes[poll.id]) && !hasVoted) {
      setUserVote(isUndefined(votes[poll.id]) ? emptyVote : votes[poll.id]);
      setHasVoted(!isUndefined(votes[poll.id]));
      if (resultsAreVisible) {
        toggleClassButton();
      }
    }
  }, [votes]);

  const disableButtons = () => {
    pro.current.disabled = true;
    neutral.current.disabled = true;
    con.current.disabled = true;
  };

  const getResults = () => {
    if (total !== 0) {
      pro.current.style.width = `${((pollVoteTypes[0].count + getCount().pro) / total) * 100}%`;
      neutral.current.style.width = `${((pollVoteTypes[1].count + getCount().neutral) / total) * 100}%`;
      con.current.style.width = `${((pollVoteTypes[2].count + getCount().con) / total) * 100}%`;
    } else {
      pro.current.style.width = '0%';
      neutral.current.style.width = '0%';
      con.current.style.width = '0%';
    }

    setResultsAreVisible(true);
  };

  const reinitializePoll = () => {
    pro.current.style.width = '100%';
    pro.current.disabled = false;
    neutral.current.style.width = '100%';
    neutral.current.disabled = false;
    con.current.style.width = '100%';
    con.current.disabled = false;
    setResultsAreVisible(false);
  };

  const toggleClassButton = () => {
    pro.current.classList.toggle(styles['vote-active']);
    neutral.current.classList.toggle(styles['vote-active']);
    con.current.classList.toggle(styles['vote-active']);
  };

  const getCount = () => {
    const count = { pro: 0, neutral: 0, con: 0 };
    if (isNewVote) {
      count.pro = userVote.pro;
      count.con = userVote.con;
      count.neutral = userVote.neutral;
    } else if (!isEqual(initialVote, userVote) && initialVote !== null) {
      count.pro = initialVote.pro ? -1 : userVote.pro;
      count.neutral = initialVote.neutral ? -1 : userVote.neutral;
      count.con = initialVote.con ? -1 : userVote.con;
    }
    return count;
  };

  const togglePoll = () => {
    toggleClassButton();
    if (resultsAreVisible) {
      reinitializePoll();

      if (hasVoted) {
        setHasVoted(false);
        setTempVote(userVote);
        setUserVote(emptyVote);
      }
    } else {
      // Reset user's vote in case he clicked on "Change my mind" but did not vote again
      if (isEqual(userVote, emptyVote) && !isEqual(tempVote, emptyVote)) {
        setUserVote(tempVote);
        setHasVoted(true);
        setTempVote(emptyVote);
        toggleClassButton();
      }
      getResults();
    }
  };

  const vote = type => {
    if (!pro.current.disabled) {
      const voteData = {
        pollId: poll.id,
        pollOptionId: pollVoteTypes[mappingVoteType[type]].id,
        userId: user.id,
      };

      if (isUndefined(votes[poll.id])) {
        setIsNewVote(true);
      }

      dispatch(sendVote(voteData, token));
      dispatch(updateUserVotes(poll.id, { ...userVote, [type]: 1 }));
      setUserVote({ ...userVote, [type]: 1 });
      setHasVoted(true);
    }
  };

  const renderUserPicture = type => {
    if (resultsAreVisible && hasVoted && userVote[type] === 1 && !isEmpty(token)) {
      return (
        <div className={styles.picture} title="mon avis">
          <Image publicId={user.imageLink} alt="votre photo" secure="true">
            <Transformation crop="fill" gravity="faces" />
          </Image>
        </div>
      );
    }
  };

  const handleGoBack = () => {
    if (returnPath) {
      router.push(returnPath.href, returnPath.as);
    } else {
      router.push('/');
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles['img-container']}>
        {debate.isPrivate && isAdminParticipant(user, debate) && (
          <div className={styles.configuration}>
            <Link href={`/debate/private/${debate.id}/configuration`}>
              <a className="neutral-button">
                <SettingsIcon />
                <span>Configuration</span>
              </a>
            </Link>
          </div>
        )}

        <Image publicId={image} alt="image de fond du débat" secure="true">
          <Transformation crop="fill" gravity="faces" />
        </Image>
        <div className={styles['background-image']}></div>
        <h1>{poll.question}</h1>
        <button type="button" onClick={handleGoBack}>
          <Icon width="25" height="25" color="white" icon={arrowLeft} className="clickable" />
        </button>
      </div>
      <div className={styles.votes}>
        <div className={styles['votes-background']}>
          <button ref={pro} type="button" className={styles.pro} onClick={() => vote('pro')}>
            <span className={styles.label}>{pollVoteTypes[0].label}</span>
          </button>
          {resultsAreVisible && (
            <span>{total !== 0 ? Math.floor(((pollVoteTypes[0].count + getCount().pro) / total) * 100) : 0}%</span>
          )}
          {renderUserPicture('pro')}
        </div>
        <div className={styles['votes-background']}>
          <button ref={neutral} type="button" className={styles.neutral} onClick={() => vote('neutral')}>
            <span className={styles.label}>{pollVoteTypes[1].label}</span>{' '}
            <span className={styles.neutral__label}>/ Partagé</span>
          </button>
          {resultsAreVisible && (
            <span>{total !== 0 ? Math.floor(((pollVoteTypes[1].count + getCount().neutral) / total) * 100) : 0}%</span>
          )}
          {renderUserPicture('neutral')}
        </div>
        <div className={styles['votes-background']}>
          <button ref={con} type="button" className={styles.con} onClick={() => vote('con')}>
            <span className={styles.label}>{pollVoteTypes[2].label}</span>
          </button>
          {resultsAreVisible && (
            <span>{total !== 0 ? Math.floor(((pollVoteTypes[2].count + getCount().con) / total) * 100) : 0}%</span>
          )}
          {renderUserPicture('con')}
        </div>
        {!resultsAreVisible && !isLoggedIn && (
          <div className={styles['hide-vote']}>
            <p>Vous devez être connecté pour participer au vote !</p>
            <div className={styles['hide-vote__buttons']}>
              <button
                type="button"
                className="primary-button"
                onClick={() => dispatch(openModalAuthentification('signUp'))}
              >
                S'inscrire
              </button>
              <button
                type="button"
                className="secondary-button"
                onClick={() => dispatch(openModalAuthentification('signIn'))}
              >
                Se connecter
              </button>
            </div>
          </div>
        )}
      </div>
      <div className={styles.bottom}>
        <div className={styles.numbers}>
          <div className={styles.number}>
            <p>{total}</p>
            <div className={styles['icon-container']}>
              <IconVote />
              votes
            </div>
          </div>
          <div className={styles.number}>
            <p>{kFormatter(views)}</p>
            <div className={styles['icon-container']}>
              <IconVisibility /> vues
            </div>
          </div>
        </div>
        {!isViewerParticipant(user, debate) && (
          <button type="button" onClick={togglePoll} className="secondary-button">
            {textButton}
          </button>
        )}
      </div>
    </div>
  );
};

Poll.propTypes = {
  poll: PropTypes.object.isRequired,
  views: PropTypes.number.isRequired,
  image: PropTypes.string,
  debate: PropTypes.object.isRequired,
};

export default Poll;
