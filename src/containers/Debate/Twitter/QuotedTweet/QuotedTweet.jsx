/* eslint-disable import/no-cycle */
import PropTypes from 'prop-types';

import Tweet from '../Tweet';

const QuotedTweet = ({ data }) => {
  return <Tweet isQuotedTweet data={data} />;
};

QuotedTweet.propTypes = {
  data: PropTypes.object.isRequired,
};

export default QuotedTweet;
