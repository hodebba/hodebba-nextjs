/* eslint-disable react/no-unescaped-entities */
import PropTypes from 'prop-types';

import Tweet from './Tweet';
import styles from './twitter.module.scss';
import TwitterSeo from './twitter.seo';

/* const tweetsList = [
  '1267541833325916161',
  '1265097720802484224',
  '1170742994062008320',
  '1170684073133649920',
  '991066023280500736',
  '1272129156495671296',
  '1267475476391624704',
  '1256678827746607104',
  '1175063062522597382',
  '1171085377874141193',
  '1268575659057713152',
  '1039552749514489856',
]; */

const Twitter = ({ debate, tweets }) => {
  return (
    <div className={styles.container}>
      <TwitterSeo debate={debate} />
      <h2>Retrouvez ci-dessous les tweets les plus populaires autour du débat</h2>
      <div className={styles.tweets}>
        {tweets.map(tweet => (
          <Tweet data={tweet} key={tweet.id} />
        ))}
        {tweets.length === 0 && <p>Aucun tweet n'a été associé à ce débat pour le moment.</p>}
      </div>
    </div>
  );
};

Twitter.propTypes = {
  debate: PropTypes.object.isRequired,
  tweets: PropTypes.array.isRequired,
};

export default Twitter;
