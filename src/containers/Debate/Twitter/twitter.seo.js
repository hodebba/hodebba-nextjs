import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { Fragment } from 'react';

const TwitterSeo = ({ debate }) => {
  const tags = [];
  debate.tags.forEach(tag => {
    tags.push(tag.label);
  });
  return (
    <Fragment>
      {debate.isPrivate ? (
        <NextSeo noindex />
      ) : (
        <NextSeo
          title={`Les tweets populaires autour du débat ${debate.title}`}
          description={`Découvrez les tweets les plus marquants sur le débat ${debate.title}`}
          openGraph={{
            url: `${process.env.NEXT_PUBLIC_FRONT_END_URL}/debate/${debate.id}/twitter`,
            type: 'article',
            article: {
              publishedTime: debate.createdDate,
              section: 'Tweets populaires',
              tags,
            },
            title: `Les tweets populaires autour du débat ${debate.title}`,
            description: `Découvrez les tweets les plus marquants sur le débat ${debate.title}`,
            images: [
              {
                url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/${debate.imageLink}`,
                alt: debate.title,
              },
            ],
            site_name: 'Hodebba',
          }}
          twitter={{
            handle: '@handle',
            site: '@site',
            cardType: 'summary_large_image',
          }}
        />
      )}
    </Fragment>
  );
};

TwitterSeo.propTypes = {
  debate: PropTypes.object,
};

export default TwitterSeo;
