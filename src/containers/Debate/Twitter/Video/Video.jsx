/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import videojs from 'video.js';

import styles from './video.module.scss';

const Video = ({ src }) => {
  const playerRef = useRef();

  useEffect(() => {
    const player = videojs(playerRef.current, { autoplay: false, controls: true, preload: 'auto' }, () => {
      player.src(src);
    });

    return () => {
      player.dispose();
    };
  }, []);

  return (
    <div className={styles.container}>
      <div data-vjs-player>
        <video ref={playerRef} className="video-js vjs-4-3" playsInline></video>
      </div>
    </div>
  );
};

Video.propTypes = {
  src: PropTypes.string.isRequired,
};

export default Video;
