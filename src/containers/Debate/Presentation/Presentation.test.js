/* eslint-disable no-unused-vars */
import { shallow } from 'enzyme';

import Presentation from './index';

describe('Presentation Component', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(
      <Presentation debate={{ presentation: 'ffdfdf', createdDate: null, user: { username: 'jean' } }} />,
    );
  });
});
