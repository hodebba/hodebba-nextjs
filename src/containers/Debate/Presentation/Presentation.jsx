import { formatDateLetters } from '@Utils/general.helper';
import Parser from 'html-react-parser';
import PropTypes from 'prop-types';

import styles from './presentation.module.scss';
import PresentationSeo from './presentation.seo';

//  TODO: check if parser used protection against XSS injection, if not use DOMPurify otherwise remove DOMPurify
const Presentation = ({ debate }) => {
  const nbChar = debate.presentation.replace(/\s/g, '').length;
  const min = Math.trunc(nbChar / 1100);
  const author = debate.user.username;

  return (
    <div className={styles.container}>
      <PresentationSeo debate={debate} />
      <div className={styles.meta}>
        <p className={styles.date}>
          Créé par <span className={styles.author}>{author === 'Admin' ? 'Hodebba' : author}</span>, le{' '}
          {formatDateLetters(debate.createdDate)}
        </p>
        <p className={styles.time}>Temps de lecture estimé: {min > 0 ? min : 1} min</p>
      </div>
      <div className={styles.content}> {Parser(debate.presentation)}</div>
    </div>
  );
};

Presentation.propTypes = {
  debate: PropTypes.object,
};

export default Presentation;
