/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable react-hooks/exhaustive-deps */
import { Image, Transformation } from 'cloudinary-react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import TrashIcon from '../../../../../public/static/icons/trash.svg';
import { deleteParticipant, promoteParticipant } from '../../../../actions/debate.actions';
import FilterDropdown from '../../../../components/FilterDropdown';
import InfoModal from '../../../../components/InfoModal';
import { roleOptions } from '../configuration.helper';
import styles from './participant.module.scss';

const Participant = ({ data, debateId }) => {
  const token = useSelector(state => state.user.local.token);
  const [modalDeleteIsOpen, setModalDeleteIsOpen] = useState(false);
  const dispatch = useDispatch(null);

  return (
    <div className={styles.container}>
      <div className={styles.info}>
        <Image publicId={data.user.imageLink} alt="photo de profil du participant" secure="true">
          <Transformation crop="fill" gravity="faces" />
        </Image>
        <div>
          <p>{data.user.username}</p>
          <FilterDropdown
            options={roleOptions}
            isDisabled={data.role === 'admin'}
            onChange={newValue => dispatch(promoteParticipant({ role: newValue.value, id: data.id, debateId }, token))}
            defaultValue={roleOptions.filter(role => role.value === data.role)[0]}
            className="no-border-dropdown"
          />
        </div>
      </div>
      {data.role !== 'admin' && <TrashIcon onClick={() => setModalDeleteIsOpen(true)} />}

      {modalDeleteIsOpen && (
        <InfoModal onClose={() => setModalDeleteIsOpen(false)}>
          <div className={styles.modal}>
            <p>Êtes-vous sûr de vouloir supprimer {data.user.username} de ce débat ?</p>
            <button
              type="button"
              className="cancel-button"
              onClick={() => dispatch(deleteParticipant(data.id, token, data))}
            >
              Oui, je veux supprimer {data.user.username}
            </button>
            <button type="button" className="secondary-button" onClick={() => setModalDeleteIsOpen(false)}>
              Non, je souhaite conserver {data.user.username}
            </button>
          </div>
        </InfoModal>
      )}
    </div>
  );
};

Participant.propTypes = {
  data: PropTypes.object.isRequired,
  debateId: PropTypes.number.isRequired,
};

export default Participant;
