import { resetNotification } from '@Actions/debate.actions';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { Field, reduxForm, reset } from 'redux-form';

import FilterDropdown from '../../../../components/FilterDropdown';
import RenderTextAreaField from '../../../../components/RenderTextAreaField';
import RenderTextField from '../../../../components/RenderTextField';
import SubmitButton from '../../../../components/SubmitButton';
import { email, required, trim } from '../../../../utils/check-fields.helper';
import { roleOptions } from '../configuration.helper';
import styles from './invite-form.module.scss';

const roleTexts = {
  viewer: 'Les lecteurs ont uniquement des droits de lecture, ils ne peuvent pas participer au débat.',
  writer: 'Les rédacteurs peuvent ajouter des arguments et des commentaires, et participer aux votes.',
  admin:
    'Les administrateurs peuvent modifier les paramètres de la discussion, changer les rôles des autres utilisateurs, inviter de nouveaux participants, et bien sûr participer au débat.',
};

const InviteForm = ({ handleSubmit, submitting, change }) => {
  const errors = useSelector(state => state.debate.local.errors.invite);
  const loading = useSelector(state => state.debate.local.loading.invite);
  const notification = useSelector(state => state.debate.local.notification);
  const dispatch = useDispatch(null);
  const [error, setError] = useState('');
  const [role, setRole] = useState(roleOptions[1]);

  useEffect(() => {
    if (!isEmpty(errors)) {
      setError('Une erreur innatendue est survenue. Si le problème persiste, merci de contacter le support technique.');
    } else {
      setError('');
    }
  }, [errors]);

  useEffect(() => {
    if (!isEmpty(notification) && notification.invitation) {
      toast.success('Votre invitation a été envoyée avec succès !');
      dispatch(reset('InviteForm'));
      dispatch(resetNotification());
    }
  }, [notification, dispatch]);

  useEffect(() => {
    return () => {
      setError('');
    };
  }, []);

  const handleChangeRole = newRole => {
    change('role', newRole.value);
    setRole(newRole);
  };

  return (
    <form className={styles.container} onSubmit={handleSubmit}>
      <h2>Inviter un participant</h2>

      <Field
        name="email"
        type="email"
        checkEnabled
        component={RenderTextField}
        label="Email"
        validate={[required, email]}
        normalize={trim}
      />

      <div className={styles.role}>
        <div className={styles.role__label}>Rôle</div>
        <div className={styles.role__content}>
          <FilterDropdown options={roleOptions} defaultValue={roleOptions[1]} onChange={handleChangeRole} />
          <p>{roleTexts[role.value]}</p>
        </div>
      </div>

      <Field name="message" type="text-area" component={RenderTextAreaField} label="Message personnalisé (optionnel)" />

      <div>
        {error !== '' && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>{error}</p>
          </div>
        )}
        <SubmitButton label="Envoyer" submitting={submitting} loading={loading} />
      </div>
    </form>
  );
};

InviteForm.propTypes = {
  handleSubmit: PropTypes.func,
  change: PropTypes.func,
  submitting: PropTypes.bool,
};

export default reduxForm({
  form: 'InviteForm',
})(InviteForm);
