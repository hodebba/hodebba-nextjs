/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
import { invite, resetDeleteParticipant, resetPromoteParticipant } from '@Actions/debate.actions';
import chevronLeft from '@iconify/icons-vaadin/chevron-left';
import { Icon } from '@iconify/react';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import MobileMenu from '../../../components/MobileMenu';
import { useWindowSize } from '../../../utils/general.helper';
import { optionsConfiguration } from './configuration.helper';
import styles from './configuration.module.scss';
import DeleteDebate from './DeleteDebate';
import InviteForm from './InviteForm';
import Participant from './Participant';

const Configuration = ({ debate, active }) => {
  const dispatch = useDispatch(null);
  const token = useSelector(state => state.user.local.token);
  const [nbItems, setNbItems] = useState(6);
  const size = useWindowSize();
  const [width, setWidth] = useState(null);
  const [participants, setParticipants] = useState(debate.participants);
  const errorPromote = useSelector(state => state.debate.local.errors.promoteParticipant);
  const errorDelete = useSelector(state => state.debate.local.errors.deleteParticipant);
  const userDelete = useSelector(state => state.debate.data.deleteParticipant);

  useEffect(() => {
    if (!isEmpty(errorPromote)) {
      toast.error(
        'Impossible de changer le rôle de ce participant. Si le problème persiste, merci de contacter le support technique.',
      );
      dispatch(resetPromoteParticipant());
    }
    if (!isEmpty(errorDelete)) {
      toast.error(
        'Impossible de supprimer ce participant. Si le problème persiste, merci de contacter le support technique.',
      );
      dispatch(resetDeleteParticipant());
    }
  }, [errorPromote, errorDelete]);

  useEffect(() => {
    if (!isEmpty(userDelete)) {
      toast.success(`${userDelete.user.username} a été retiré de ce débat.`);
      setParticipants(participants.filter(participant => participant.id !== userDelete.id));
      dispatch(resetDeleteParticipant());
    }
  }, [userDelete]);

  const handleSubmitInvitation = values => {
    const data = {
      ...values,
      debateId: debate.id,
    };
    dispatch(invite(data, token));
  };

  const renderParticipants = () => {
    const ret = [];
    for (let i = 0; i < Math.min(participants.length, nbItems); i += 1) {
      ret.push(<Participant key={participants[i].id} data={participants[i]} debateId={debate.id} />);
    }

    if (participants.length > nbItems) {
      ret.push(
        <div key={nbItems + 1} className={styles['display-more']}>
          <p>
            + {participants.length - nbItems}{' '}
            {participants.length - nbItems > 1 ? 'autres participants' : 'autre participant'}
          </p>
          <button type="button" className="secondary-button" onClick={() => setNbItems(nbItems => nbItems + 6)}>
            Afficher plus
          </button>
        </div>,
      );
    }

    return ret;
  };

  useEffect(() => {
    setWidth(size.width);
  }, [size]);

  return (
    <Fragment>
      <NextSeo noindex />
      {width < 650 && isUndefined(active) ? (
        <MobileMenu
          title="Configuration"
          options={optionsConfiguration(debate.id)}
          previousPage={`/debate/private/${debate.id}/presentation`}
        />
      ) : (
        <div className={styles.container}>
          {!isMobileOnly && (
            <div className={styles.top}>
              <Link href={`/debate/private/${debate.id}/presentation`}>
                <a className={`text-button ${styles.back}`} type="button">
                  <Icon width="18" height="18" icon={chevronLeft} />
                  Retour
                </a>
              </Link>
              <h1>Configuration</h1>
            </div>
          )}

          {(!isMobileOnly || (isMobileOnly && active === 'invitation')) && (
            <InviteForm onSubmit={handleSubmitInvitation} initialValues={{ role: 'writer' }} />
          )}

          {(!isMobileOnly || (isMobileOnly && active === 'participants')) && (
            <div>
              <h2>Liste des participants</h2>
              <div className={styles.participants}>{renderParticipants()}</div>
            </div>
          )}

          {(!isMobileOnly || (isMobileOnly && active === 'delete-debate')) && <DeleteDebate />}
        </div>
      )}
    </Fragment>
  );
};

Configuration.propTypes = {
  debate: PropTypes.object.isRequired,
  active: PropTypes.string,
};

export default Configuration;
