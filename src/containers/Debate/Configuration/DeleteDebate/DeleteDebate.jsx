import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { deleteDebate } from '../../../../actions/debate.actions';
import InfoModal from '../../../../components/InfoModal';
import styles from './delete-debate.module.scss';

const DeleteDebate = () => {
  const [modalDeleteIsOpen, setModalDeleteIsOpen] = useState(false);
  const dispatch = useDispatch();
  const error = useSelector(state => state.debate.local.errors.deleteDebate);
  const successDelete = useSelector(state => state.debate.data.deleteDebate);
  const token = useSelector(state => state.user.local.token);
  const router = useRouter();

  useEffect(() => {
    if (!isEmpty(error)) {
      toast.error(
        'Une erreur inattendue est survenue. Veuillez réessayer. Si le problème persiste, merci de contacter le service technique.',
      );
    }
  }, [error]);

  useEffect(() => {
    if (successDelete[router.query.id]) {
      toast.success('Le débat a été supprimé avec succès.');
      setModalDeleteIsOpen(false);
      setTimeout(() => {
        router.push('/');
      }, 3000);
    }
  }, [successDelete, router]);

  return (
    <div className={styles.container}>
      <h2>Supprimer le débat</h2>
      <p className={styles.space}>Vous vous apprêtez supprimer ce débat.</p>
      <p>
        Une fois supprimé, toutes les données associées à ce débat seront supprimées définitivement, et il ne sera plus
        possible de les récupérer.
      </p>
      <button type="button" className="cancel-button" onClick={() => setModalDeleteIsOpen(true)}>
        Je veux supprimer ce débat définitivement
      </button>
      {modalDeleteIsOpen && (
        <InfoModal onClose={() => setModalDeleteIsOpen(false)}>
          <div className={styles.modal}>
            <p>Êtes-vous sûr de vouloir supprimer définitivement de débat ?</p>
            <button
              type="button"
              className="cancel-button"
              onClick={() => dispatch(deleteDebate(router.query.id, token))}
            >
              Oui, je veux supprimer ce débat
            </button>
            <button type="button" className="secondary-button" onClick={() => setModalDeleteIsOpen(false)}>
              Non, je souhaite conserver ce débat pour le moment
            </button>
          </div>
        </InfoModal>
      )}
    </div>
  );
};
export default DeleteDebate;
