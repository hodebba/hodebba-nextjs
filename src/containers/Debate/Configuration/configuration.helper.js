import EmailIcon from '../../../../public/static/icons/mail.svg';
import PeopleIcon from '../../../../public/static/icons/people.svg';
import TrashIcon from '../../../../public/static/icons/trash.svg';

export const roleOptions = [
  { value: 'viewer', label: 'Lecteur' },
  { value: 'writer', label: 'Rédacteur' },
  { value: 'admin', label: 'Administrateur' },
];
export const optionsConfiguration = debateId => {
  return [
    {
      label: 'Inviter un participant',
      icon: <EmailIcon />,
      link: `/debate/private/${debateId}/configuration/invitation`,
    },
    {
      label: 'Liste des participants',
      icon: <PeopleIcon />,
      link: `/debate/private/${debateId}/configuration/participants`,
    },
    {
      label: 'Supprimer le débat',
      icon: <TrashIcon />,
      link: `/debate/private/${debateId}/configuration/delete-debate`,
    },
  ];
};
