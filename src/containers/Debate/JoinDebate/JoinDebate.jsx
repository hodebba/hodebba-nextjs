/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable operator-linebreak */
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { openModalAuthentification } from '../../../actions/authentification.actions';
import { confirmInvitation } from '../../../actions/debate.actions';
import DebatePreview from '../../../components/DebatePreview';
import styles from './join-debate.module.scss';

const JoinDebate = ({ debate, isEnabled, error }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const responseInvitation = useSelector(state => state.debate.data.confirmInvitation);
  const errorInvitation = useSelector(state => state.debate.local.errors.confirmInvitation);
  const [hasAccess, setHasAccess] = useState(false);
  const [errors, setErrors] = useState(error);

  const successConfirm = () => {
    router.push('/debate/private/[id]/presentation', `/debate/private/${debate.id}/presentation`);
    setHasAccess(true);
  };

  const checkUserAccess = () => {
    return !isEnabled && !isEmpty(user) && debate.participants.find(element => element.user.username === user.username);
  };

  useEffect(() => {
    if (!isEmpty(token) && !checkUserAccess()) {
      dispatch(confirmInvitation(router.query.token, token));
    }
  }, [token, dispatch]);

  useEffect(() => {
    if (!isEmpty(responseInvitation) && isNull(responseInvitation.error)) {
      successConfirm();
    }
    if (!isEmpty(errorInvitation) && !isEmpty(token)) {
      setErrors(responseInvitation.error.message);
    }
  }, [responseInvitation]);

  useEffect(() => {
    if (isEnabled || checkUserAccess()) {
      successConfirm();
    }
  }, [user, debate, isEnabled]);

  if (!isNull(errors) && !checkUserAccess()) {
    return (
      <div className={styles.container}>
        <div className="error-message">
          <div className="error-message__title">Une erreur est survenue</div>
          <p>{errors}</p>
        </div>
      </div>
    );
  }

  return (
    <Fragment>
      {hasAccess ? (
        <p className={styles.redirection}>Veuillez patienter, vous allez être redirigé dans quelques instants...</p>
      ) : (
        <div className={styles.container}>
          <h1 className={styles.title}>Vous avez été invité à rejoindre le débat suivant</h1>

          <div className={styles.debate}>
            <DebatePreview debate={debate} theme={debate.theme} />
          </div>

          <div className={styles.authentification}>
            <p>Vous devez être connecté pour rejoindre la discussion</p>
            <div className={styles.authentification__buttons}>
              <button
                type="button"
                className="primary-button"
                onClick={() => dispatch(openModalAuthentification('signUp'))}
              >
                S'INSCRIRE
              </button>
              <button
                type="button"
                className="secondary-button"
                onClick={() => dispatch(openModalAuthentification('signIn'))}
              >
                SE CONNECTER
              </button>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

JoinDebate.propTypes = {
  isEnabled: PropTypes.bool.isRequired,
  debate: PropTypes.object.isRequired,
  error: PropTypes.string,
};

export default JoinDebate;
