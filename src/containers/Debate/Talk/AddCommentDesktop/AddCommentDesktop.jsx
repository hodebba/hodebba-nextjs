/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */
import { Image, Transformation } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm, reset } from 'redux-form';

import NotConnectedModal from '../../../../components/NotConnectedModal';
import PositionButtons from '../../../../components/PositionButtons';
import RenderTextAreaFieldRounded from '../../../../components/RenderTextAreaFieldRounded';
import SubmitButton from '../../../../components/SubmitButton';
import styles from './add-comment.module.scss';

const AddCommentDesktop = ({ submitting, handleSubmit, getPosition, parentId, withSource, withPosition }) => {
  const [position, setPosition] = useState('pro');
  const [isFocus, setIsFocus] = useState(false);
  const commentData = useSelector(state => state.debate.data.addComment);
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const loading = useSelector(state => state.debate.local.loading.addComment);
  const dispatch = useDispatch();
  const [modal, setModal] = useState({ isOpened: false, type: '' });

  useEffect(() => {
    if (withPosition) {
      getPosition(position);
    }
  }, [position]);

  useEffect(() => {
    if (!isEmpty(commentData) && parentId === commentData.parent.id) {
      setIsFocus(false);
      dispatch(reset(`AddComment${parentId}`));
    }
  }, [commentData]);

  const handleInputFocus = () => {
    if (isEmpty(token) || !user.enabled) {
      setModal({ isOpened: true, type: 'comment' });
    } else {
      setIsFocus(true);
    }
  };

  return (
    <form method="post" className={styles.container} onSubmit={handleSubmit}>
      <div className={styles.comment}>
        <div className={styles.picture}>
          {user.imageLink && (
            <Image publicId={user.imageLink} alt="personal picture" secure="true">
              <Transformation crop="fill" gravity="faces" />
            </Image>
          )}
        </div>
        <Field
          name="content"
          type="text"
          onFocus={handleInputFocus}
          component={RenderTextAreaFieldRounded}
          label="Ajouter un commentaire..."
        />
        {modal.isOpened && (
          <NotConnectedModal
            type={modal.type}
            accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
            onClick={() => null}
            onClose={() => {
              setModal({ isOpened: false, type: '' });
            }}
          />
        )}
      </div>
      {isFocus && (
        <Fragment>
          <div className={styles.source}>
            <Field name="title" type="text" component={RenderTextAreaFieldRounded} label="Titre (optionnel)" />

            {withSource && (
              <Field name="source" type="text" component={RenderTextAreaFieldRounded} label="Source (optionnel)" />
            )}
          </div>
          <div className={styles.buttons}>
            {withPosition && (
              <div className={styles.buttons__position}>
                <PositionButtons
                  labels={{ pro: 'Approuver', neutral: 'Neutre', con: 'Réfuter' }}
                  getPosition={newPosition => setPosition(newPosition)}
                />
              </div>
            )}
            <div className={styles.buttons__submit}>
              <SubmitButton label="Publier" submitting={submitting} loading={loading} />
            </div>
          </div>
        </Fragment>
      )}
    </form>
  );
};

AddCommentDesktop.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  getPosition: PropTypes.func.isRequired,
  parentId: PropTypes.number.isRequired,
  withSource: PropTypes.bool,
  withPosition: PropTypes.bool,
};

export default reduxForm()(AddCommentDesktop);
