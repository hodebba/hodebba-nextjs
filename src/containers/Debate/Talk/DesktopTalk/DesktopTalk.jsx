/* eslint-disable object-curly-newline */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react-hooks/exhaustive-deps */
import { isViewerParticipant } from '@Utils/debate.helper';
import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import FlashIcon from '../../../../../public/static/icons/flash.svg';
import PlusIcon from '../../../../../public/static/icons/plus.svg';
import { addArgument, resetAddArgument } from '../../../../actions/debate.actions';
import DesktopWriteMessage from '../../../../components/DesktopWriteMessage';
import FilterDropdown from '../../../../components/FilterDropdown';
import Message from '../../../../components/Message';
import NotConnectedModal from '../../../../components/NotConnectedModal';
import { preferences, sortByNewest, sortByOldest } from '../../../../utils/general.helper';
import * as gtag from '../../../../utils/gtags';
import styles from './desktop-talk.module.scss';

const DesktopTalk = ({ debate, argumentList, statementList }) => {
  const router = useRouter();
  const [communityIsVisible, setCommunityIsVisible] = useState(
    router.asPath.includes('community') || !router.asPath.includes('type'),
  );
  const [modal, setModal] = useState({ isOpened: false, type: '' });
  const [modalNotConnected, setModalNotConnected] = useState({ isOpened: false, type: '' });

  const loading = useSelector(state => state.debate.local.loading.addArgument);
  const error = useSelector(state => state.debate.local.errors.addArgument);
  const argumentData = useSelector(state => state.debate.data.addArgument);
  // const loadingUserLikes = useSelector(state => state.user.local.loading.getUserLikes);
  const statementsPro = statementList.pro || [];
  const statementsCon = statementList.con || [];

  const [argumentsPro, setArgumentsPro] = useState(argumentList.pro || []);
  const [argumentsCon, setArgumentsCon] = useState(argumentList.con || []);

  const [activeCon, setActiveCon] = useState(communityIsVisible ? argumentsCon : statementsCon);
  const [activePro, setActivePro] = useState(communityIsVisible ? argumentsPro : statementsPro);

  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const communityButton = useRef(null);
  const celebritiesButton = useRef(null);
  const [position, setPosition] = useState(null);
  const [filter, setFilter] = useState('popularity');
  const dispatch = useDispatch();

  useEffect(() => {
    router.prefetch(`${router.pathname}?type=community`, `${router.asPath.split('?')[0]}?type=community`);
    router.prefetch(`${router.pathname}?type=personnalities`, `${router.asPath.split('?')[0]}?type=personnalities`);
  }, []);

  const handleChangeCommunityVisible = isCommunityVisible => {
    if (isCommunityVisible) {
      router.push(`${router.pathname}?type=community`, `${router.asPath.split('?')[0]}?type=community`);
      setCommunityIsVisible(true);
      setActivePro(argumentsPro);
      setActiveCon(argumentsCon);
    } else {
      router.push(`${router.pathname}?type=personnalities`, `${router.asPath.split('?')[0]}?type=personnalities`);
      setCommunityIsVisible(false);
      setActivePro(statementsPro);
      setActiveCon(statementsCon);
    }
    gtag.event('switch_type_debate', {
      isCommunity: isCommunityVisible,
      debug_mode: true,
    });
  };

  useEffect(() => {
    setArgumentsPro(argumentList.pro || []);
    setActivePro(communityIsVisible ? argumentList.pro || [] : statementsPro);
    setArgumentsCon(argumentList.con || []);
    setActiveCon(communityIsVisible ? argumentList.con || [] : statementsCon);
  }, [argumentList]);

  useEffect(() => {
    handleChangeFilter(filter);

    if (!debate.isPrivate) {
      toggleButton();
    }
  }, [communityIsVisible]);

  const handleSubmitArgument = data => {
    const body = {
      title: data.title || null,
      content: data.content,
      userId: user.id,
      debateId: debate.id,
      postType: position,
      sourceLinks: data.source ? [data.source] : null,
    };

    dispatch(addArgument(body, token));
  };

  useEffect(() => {
    if (!isEmpty(argumentData) && modal.isOpened) {
      toast.success('Votre argument a été ajouté avec succès !');
      setModal({ isOpened: false, type: '' });
      if (argumentData.postType === 'pro') {
        setArgumentsPro([...argumentsPro, argumentData]);
        setActivePro([...activePro, argumentData]);
      } else {
        setArgumentsCon([...argumentsCon, argumentData]);
        setActiveCon([...activeCon, argumentData]);
      }
      dispatch(resetAddArgument());
    }
  }, [argumentData]);

  const toggleButton = () => {
    if (communityIsVisible) {
      communityButton.current.classList.add('primary-button');
      communityButton.current.classList.remove('secondary-button');
      celebritiesButton.current.classList.add('secondary-button');
      celebritiesButton.current.classList.remove('primary-button');
    } else {
      celebritiesButton.current.classList.add('primary-button');
      celebritiesButton.current.classList.remove('secondary-button');
      communityButton.current.classList.add('secondary-button');
      communityButton.current.classList.remove('primary-button');
    }
  };

  const handleChangeFilter = newFilter => {
    setFilter(newFilter);
    switch (newFilter.value) {
      case 'recent':
        setActivePro(cloneDeep(activePro).sort(sortByNewest));
        setActiveCon(cloneDeep(activeCon).sort(sortByNewest));
        break;
      case 'older':
        setActivePro(cloneDeep(activePro).sort(sortByOldest));
        setActiveCon(cloneDeep(activeCon).sort(sortByOldest));
        break;
      case 'popularity':
        if (communityIsVisible) {
          setActivePro(cloneDeep(argumentList.pro || []));
          setActiveCon(cloneDeep(argumentList.con || []));
        } else {
          setActivePro(cloneDeep(statementsPro));
          setActiveCon(cloneDeep(statementsCon));
        }

        break;
      default:
    }
  };

  const renderProArguments = () => {
    const ret = [];
    if (!isEmpty(activePro)) {
      activePro.forEach(argument => {
        if (communityIsVisible) {
          ret.push(
            <Link
              key={argument.id}
              href={`${router.pathname}/argument/[comment_id]`}
              as={`${router.asPath.split('?')[0]}/argument/${argument.id}`}
            >
              <a className="link-without-style ">
                <Message isArgument isDesktop data={argument} debate={debate} />
              </a>
            </Link>,
          );
        } else {
          ret.push(<Message key={argument.id} isArgument isStatement isDesktop data={argument} debate={debate} />);
        }
      });
    } else {
      ret.push(<p key={0}>Aucun argument POUR n'a été ajouté pour le moment.</p>);
    }
    return ret;
  };

  const renderConArguments = () => {
    const ret = [];
    if (!isEmpty(activeCon)) {
      activeCon.forEach(argument => {
        if (communityIsVisible) {
          ret.push(
            <Link
              key={argument.id}
              href={`${router.pathname}/argument/[comment_id]`}
              as={`${router.asPath.split('?')[0]}/argument/${argument.id}`}
            >
              <a className="link-without-style ">
                <Message isArgument isDesktop data={argument} debate={debate} />
              </a>
            </Link>,
          );
        } else {
          ret.push(<Message isArgument key={argument.id} isStatement isDesktop data={argument} debate={debate} />);
        }
      });
    } else {
      ret.push(<p key={0}>Aucun argument CONTRE n'a été ajouté pour le moment.</p>);
    }
    return ret;
  };

  const renderHeader = type => {
    return (
      <div className={styles.header}>
        {communityIsVisible && !isViewerParticipant(user, debate) && (
          <button type="button">
            <PlusIcon
              onClick={() => {
                if (isEmpty(token) || !user.enabled) {
                  setModalNotConnected({ isOpened: true, type: 'argument' });
                } else {
                  setModal({ isOpened: true, type });
                }
              }}
            />
          </button>
        )}
        <h2>{type === 'pro' ? 'POUR' : 'CONTRE'}</h2>
      </div>
    );
  };

  return (
    <div className={styles.container}>
      <div className={styles.filters}>
        <div className={styles.preferences}>
          <FilterDropdown options={preferences} defaultValue={preferences[0]} onChange={handleChangeFilter} />
        </div>

        {!debate.isPrivate && (
          <div className={styles.switch}>
            <button
              ref={communityButton}
              type="button"
              className={`${styles.switch__community}`}
              onClick={() => handleChangeCommunityVisible(true)}
            >
              Communauté
            </button>
            <button
              type="button"
              ref={celebritiesButton}
              className={`${styles.switch__celebrities}`}
              onClick={() => handleChangeCommunityVisible(false)}
            >
              Personnalités
            </button>
          </div>
        )}
      </div>

      <div className={styles.arguments}>
        <div className={styles.pro}>
          {renderHeader('pro')}
          {renderProArguments()}
        </div>
        <div className={styles.separator}>
          <FlashIcon />
          <div className={styles.line}></div>
        </div>
        <div className={styles.con}>
          {renderHeader('con')}
          {renderConArguments()}
        </div>
      </div>

      {modal.isOpened && (
        <DesktopWriteMessage
          withTitle
          title="Ajoutez votre argument"
          withSource
          withPosition
          labels={{ pro: 'Pour', con: 'Contre' }}
          placeHolderText="Mon argument..."
          CTA="Publier"
          onClose={() => setModal({ ...modal, isOpened: false })}
          defaultPosition={modal.type}
          loading={loading}
          getPosition={newPosition => setPosition(newPosition)}
          onSubmit={handleSubmitArgument}
          errorServer={error}
        />
      )}
      {modalNotConnected.isOpened && (
        <NotConnectedModal
          type={modalNotConnected.type}
          accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
          onClick={() => null}
          onClose={() => {
            setModalNotConnected({ isOpened: false, type: '' });
          }}
        />
      )}
    </div>
  );
};

DesktopTalk.propTypes = {
  debate: PropTypes.object.isRequired,
  argumentList: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  statementList: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default DesktopTalk;
