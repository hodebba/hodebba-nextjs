import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stopSubmit, SubmissionError } from 'redux-form';
import isURL from 'validator/lib/isURL';

import { addArgument } from '../../../../actions/debate.actions';
import MobileWriteMessage from '../../../../components/MobileWriteMessage';
const AddArgumentMobile = () => {
  const [position, setPosition] = useState('pro');
  const loading = useSelector(state => state.debate.local.loading.addArgument);
  const error = useSelector(state => state.debate.local.errors.addArgument);
  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    router.prefetch(
      `${router.pathname.split('/compose')[0]}?type=community`,
      `${router.asPath.split('/compose')[0]}?type=community`,
    );
  }, [router]);

  useEffect(() => {
    if (!isEmpty(error)) {
      stopSubmit('MobileWriteMessage', {
        _error: 'Une erreur innatendue est survenue, merci de contacter le support technique',
      });
    }
  }, [error]);

  const handleSubmit = data => {
    if (data.source && !isURL(data.source, { allow_underscores: true })) {
      throw new SubmissionError({
        _error: 'La source doit être un lien valide.',
      });
    } else if (!data.content || data.content === '') {
      throw new SubmissionError({
        _error: 'Le champ argument est obligatoire.',
      });
    } else {
      const body = {
        title: data.title || null,
        content: data.content,
        userId: user.id,
        debateId: router.query.id,
        postType: position,
        sourceLinks: data.source ? [data.source] : null,
      };
      dispatch(addArgument(body, token));
      router.push(
        `${router.pathname.split('/compose')[0]}?type=community`,
        `${router.asPath.split('/compose')[0]}?type=community`,
      );
    }
  };

  return (
    <MobileWriteMessage
      CTA="Publier"
      labels={{ pro: 'Pour', con: 'Contre' }}
      withTitle
      withSource
      withPosition
      placeHolderText="Mon argument..."
      getPosition={newPosition => setPosition(newPosition)}
      onSubmit={handleSubmit}
      loading={loading}
      returnHref={`${router.pathname.split('/compose')[0]}?type=community`}
    />
  );
};

export default AddArgumentMobile;
