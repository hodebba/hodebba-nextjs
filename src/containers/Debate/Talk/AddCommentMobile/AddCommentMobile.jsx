/* eslint-disable react-hooks/exhaustive-deps */
import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stopSubmit, SubmissionError } from 'redux-form';
import isURL from 'validator/lib/isURL';

import { addComment } from '../../../../actions/debate.actions';
import MobileWriteMessage from '../../../../components/MobileWriteMessage';

const AddCommentMobile = () => {
  const [position, setPosition] = useState('pro');
  const loading = useSelector(state => state.debate.local.loading.addComment);
  const error = useSelector(state => state.debate.local.errors.addComment);
  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    router.prefetch(router.pathname.split('/compose')[0], router.asPath.split('/compose')[0]);
  }, [router]);

  useEffect(() => {
    if (!isEmpty(error)) {
      dispatch(
        stopSubmit('MobileWriteMessage', {
          _error: 'Une erreur innatendue est survenue, merci de contacter le support technique',
        }),
      );
    }
  }, [error]);

  const handleSubmit = data => {
    if (data.source && !isURL(data.source, { allow_underscores: true })) {
      throw new SubmissionError({
        _error: 'La source doit être un lien valide.',
      });
    } else if (!data.content || data.content === '') {
      throw new SubmissionError({
        _error: 'Le champ argument est obligatoire.',
      });
    } else {
      const body = {
        title: data.title || null,
        content: data.content,
        userId: user.id,
        parentId: router.query.comment_id,
        postType: position,
        sourceLinks: data.source ? [data.source] : null,
      };
      dispatch(addComment(body, token, false));
      router.push(router.pathname.split('/compose')[0], router.asPath.split('/compose')[0]);
    }
  };

  return (
    <MobileWriteMessage
      CTA="Publier"
      labels={{ pro: 'Approuver', neutral: 'Neutre', con: 'Réfuter' }}
      withSource
      withPosition
      withTitle
      placeHolderText="Mon commentaire..."
      getPosition={newPosition => setPosition(newPosition)}
      onSubmit={handleSubmit}
      loading={loading}
      returnHref={router.pathname.split('/compose')[0]}
    />
  );
};

export default AddCommentMobile;
