import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { Fragment } from 'react';

const TalkSeo = ({ debate }) => {
  const tags = [];
  debate.tags.forEach(tag => {
    tags.push(tag.label);
  });
  return (
    <Fragment>
      {debate.isPrivate ? (
        <NextSeo noindex />
      ) : (
        <NextSeo
          title={`Le débat: ${debate.title}`}
          description={`Découvrez les arguments de la communauté et des personnalités publiques sur ${debate.title}`}
          openGraph={{
            url: `${process.env.NEXT_PUBLIC_FRONT_END_URL}/debate/${debate.id}/talk`,
            title: `Le débat: ${debate.title}`,
            description: `Découvrez les arguments de la communauté et des personnalités publiques sur ${debate.title}`,
            type: 'article',
            article: {
              publishedTime: debate.createdDate,
              section: 'Le débat',
              tags,
            },
            images: [
              {
                url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/${debate.imageLink}`,
                alt: debate.title,
              },
            ],
            site_name: 'Hodebba',
          }}
          twitter={{
            handle: '@handle',
            site: '@site',
            cardType: 'summary_large_image',
          }}
        />
      )}
    </Fragment>
  );
};

TalkSeo.propTypes = {
  debate: PropTypes.object,
};

export default TalkSeo;
