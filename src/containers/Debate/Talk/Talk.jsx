/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-console */
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSWR from 'swr';

import { getStatementsUserLikes, getUserLikes } from '../../../actions/user.actions';
import isFallback from '../../../HOCs/isFallback.hoc';
import URL from '../../../services/api';
import { useWindowSize } from '../../../utils/general.helper';
import DesktopTalk from './DesktopTalk';
import MobileTalk from './MobileTalk';
import TalkSeo from './talk.seo';

const fetcher = url => axios.get(url).then(res => res.data);

const Talk = ({ debate, argumentList, statementList }) => {
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const user = useSelector(state => state.user.local.user);
  const size = useWindowSize();
  const dispatch = useDispatch();

  const { data } = useSWR(URL.post.getArguments(debate.id), fetcher, {
    initialData: argumentList,
    revalidateOnMount: true,
  });

  const checkInnerWidth = () => {
    if (size.width > 850) {
      setIsSmallScreen(false);
    } else {
      setIsSmallScreen(true);
    }
  };

  useEffect(() => {
    checkInnerWidth();
  }, [size]);

  useEffect(() => {
    checkInnerWidth();

    if (!isEmpty(user)) {
      dispatch(getUserLikes(user.id, debate.id));
      dispatch(getStatementsUserLikes(user.id));
    }
  }, []);

  return (
    <Fragment>
      <TalkSeo debate={debate} />
      {isSmallScreen ? (
        <MobileTalk debate={debate} argumentList={data} statementList={statementList} />
      ) : (
        <DesktopTalk debate={debate} argumentList={data} statementList={statementList} />
      )}
    </Fragment>
  );
};

Talk.propTypes = {
  debate: PropTypes.object.isRequired,
  argumentList: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  statementList: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default isFallback(Talk);
