/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react-hooks/exhaustive-deps */
import { getDebateRootPath, isViewerParticipant } from '@Utils/debate.helper';
import { sortByRank } from '@Utils/general.helper';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import WriteIcon from '../../../../../public/static/icons/write.svg';
import { resetAddArgument } from '../../../../actions/debate.actions';
import ClientOnlyPortal from '../../../../components/ClientOnlyPortal';
import Message from '../../../../components/Message';
import * as gtag from '../../../../utils/gtags';
import styles from './mobile-talk.module.scss';

const mergeProConArrays = (proArray, conArray) => {
  return proArray.concat(conArray);
};

const MobileTalk = ({ debate, argumentList, statementList }) => {
  const router = useRouter();
  const [communityIsVisible, setCommunityIsVisible] = useState(
    router.asPath.includes('community') || !router.asPath.includes('type'),
  );
  // const loadingUserLikes = useSelector(state => state.user.local.loading.getUserLikes);
  const argumentData = useSelector(state => state.debate.data.addArgument);
  const user = useSelector(state => state.user.local.user);
  const dispatch = useDispatch();
  const argumentsContainer = useRef(null);
  const [_arguments, setArguments] = useState(mergeProConArrays(argumentList.pro || [], argumentList.con || []));
  const [statements, setStatements] = useState(mergeProConArrays(statementList.pro || [], statementList.con || []));
  const [activeArguments, setActiveArguments] = useState(communityIsVisible ? _arguments : statements);

  useEffect(() => {
    router.prefetch(`${router.pathname}?type=community`, `${router.asPath.split('?')[0]}?type=community`);
    router.prefetch(`${router.pathname}?type=personnalities`, `${router.asPath.split('?')[0]}?type=personnalities`);
  }, []);

  useEffect(() => {
    setActiveArguments(
      communityIsVisible ? mergeProConArrays(argumentList.pro || [], argumentList.con || []) : statements,
    );
  }, [argumentList]);

  const handleChangeCommunityVisible = isCommunityVisible => {
    if (isCommunityVisible) {
      router.push(`${router.pathname}?type=community`, `${router.asPath.split('?')[0]}?type=community`);
      setActiveArguments(_arguments);
      setCommunityIsVisible(true);
    } else {
      router.push(`${router.pathname}?type=personnalities`, `${router.asPath.split('?')[0]}?type=personnalities`);
      setActiveArguments(statements);
      setCommunityIsVisible(false);
    }
    gtag.event('switch_type_debate', {
      isCommunity: isCommunityVisible,
      debug_mode: true,
    });
  };

  useEffect(() => {
    if (!isEmpty(argumentData)) {
      toast.success('Votre argument a été ajouté avec succès !');
      dispatch(resetAddArgument());
      setArguments([..._arguments, argumentData]);

      setTimeout(() => {
        window.scrollTo({
          behavior: 'smooth',
          top: argumentsContainer.current.getBoundingClientRect().bottom,
          left: 0,
        });
      }, 200);
    }
  }, [argumentData]);

  const renderArguments = () => {
    const ret = [];
    if (!isEmpty(activeArguments)) {
      activeArguments.sort(sortByRank).forEach(argument => {
        if (communityIsVisible) {
          ret.push(
            <Link
              key={argument.id}
              href={`${router.pathname}/argument/[comment_id]`}
              as={`${router.asPath.split('?')[0]}/argument/${argument.id}`}
            >
              <a className="link-without-style">
                <Message isArgument data={argument} debate={debate} />
              </a>
            </Link>,
          );
        } else {
          ret.push(<Message isArgument key={argument.id} isStatement data={argument} debate={debate} />);
        }
      });
    } else {
      ret.push(
        <p className={styles['no-arguments']} key={0}>
          Aucun argument n'a été ajouté pour le moment.
        </p>,
      );
    }
    return ret;
  };

  return (
    <div className={styles.container}>
      {!debate.isPrivate && (
        <div className={styles.switch}>
          <button
            type="button"
            className={`${styles.switch__community} ${communityIsVisible ? 'primary-button' : 'secondary-button'} `}
            onClick={() => handleChangeCommunityVisible(true)}
          >
            Communauté
          </button>
          <button
            type="button"
            className={`${styles.switch__celebrities} ${!communityIsVisible ? 'primary-button' : 'secondary-button'} `}
            onClick={() => handleChangeCommunityVisible(false)}
          >
            Personnalités
          </button>
        </div>
      )}
      <div ref={argumentsContainer} className={styles.arguments}>
        {renderArguments()}
      </div>
      {communityIsVisible && !isViewerParticipant(user, debate) && (
        <ClientOnlyPortal selector="#app">
          <Link href={`${getDebateRootPath(debate)}/[id]/talk/compose`} as={`${router.asPath.split('?')[0]}/compose`}>
            <a className={styles.add}>
              <WriteIcon />
            </a>
          </Link>
        </ClientOnlyPortal>
      )}
    </div>
  );
};

MobileTalk.propTypes = {
  debate: PropTypes.object.isRequired,
  argumentList: PropTypes.object,
  statementList: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default MobileTalk;
