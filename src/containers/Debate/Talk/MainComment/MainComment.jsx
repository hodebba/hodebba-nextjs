/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */
import arrowLeft from '@iconify/icons-vaadin/arrow-left';
import { Icon } from '@iconify/react';
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSWR from 'swr';

import { getUserLikes } from '../../../../actions/user.actions';
import Message from '../../../../components/Message';
import URL from '../../../../services/api';
import styles from './argument.module.scss';

const fetcher = url => axios.get(url).then(res => res.data);

const MainComment = ({ debate, mainComment }) => {
  const router = useRouter();
  const user = useSelector(state => state.user.local.user);
  const loadingUserLikes = useSelector(state => state.user.local.loading.getUserLikes);
  const dispatch = useDispatch();
  let returnPath = '';
  let returnHref = '';
  let isTestimonial = false;

  if (router.asPath.includes('argument')) {
    returnPath = `${router.asPath.split('/argument')[0]}?type=community`;
    returnHref = `${router.pathname.split('/argument')[0]}?type=community`;
  } else {
    returnPath = `${router.asPath.split('/testimonial')[0]}/testimonial`;
    returnHref = `${router.pathname.split('/testimonial')[0]}/testimonial`;
    isTestimonial = true;
  }

  const { data } = useSWR(URL.post.get(mainComment.id), fetcher, {
    initialData: mainComment,
    revalidateOnMount: true,
  });
  useEffect(() => {
    if (!isEmpty(user)) {
      dispatch(getUserLikes(user.id, debate.id));
    }
  }, []);

  const renderMainComment = () => {
    if (!loadingUserLikes) {
      return (
        <Message
          className="main-comment"
          maxLine={10}
          displaySource
          debate={debate}
          data={data}
          displayComments
          canDisplayComments
          isArgument
          isTestimonial={isTestimonial}
        />
      );
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.back}>
        <Link href={returnHref} as={returnPath}>
          <a>
            <Icon icon={arrowLeft} className="clickable" />
            {isTestimonial ? 'Retour aux témoignages' : 'Retour au débat'}
          </a>
        </Link>
      </div>
      <div className={styles.argument}>{renderMainComment()}</div>
    </div>
  );
};

MainComment.propTypes = {
  debate: PropTypes.object,
  mainComment: PropTypes.object,
};

export default MainComment;
