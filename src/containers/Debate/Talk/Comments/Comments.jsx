/* eslint-disable operator-linebreak */
/* eslint-disable import/no-cycle */
/* eslint-disable object-curly-newline */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { getDebateRootPath, isViewerParticipant } from '@Utils/debate.helper';
import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withResizeDetector } from 'react-resize-detector';
import { SubmissionError } from 'redux-form';
import isURL from 'validator/lib/isURL';

import CommentIcon from '../../../../../public/static/icons/comment.svg';
import { addComment, resetAddComment } from '../../../../actions/debate.actions';
import ClientOnlyPortal from '../../../../components/ClientOnlyPortal';
import Message from '../../../../components/Message';
import { sortByMostLiked, useWindowSize } from '../../../../utils/general.helper';
import AddCommentDesktop from '../AddCommentDesktop';
import styles from './comments.module.scss';

const Comments = ({ height, commentList, debate, parentId, isTestimonial }) => {
  const [comments, setComments] = useState((commentList && commentList.sort(sortByMostLiked)) || []);
  const answers = useRef(null);
  const mainLine = useRef(null);
  const mobileLine = useRef(null);

  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const loadingUserLikes = useSelector(state => state.user.local.loading.getUserLikes);
  const router = useRouter();
  const commentData = useSelector(state => state.debate.data.addComment);
  const [position, setPosition] = useState(isTestimonial ? null : 'pro');
  const dispatch = useDispatch();
  const size = useWindowSize();

  let composeHref = '';
  if (router.asPath.includes('argument')) {
    composeHref = `${getDebateRootPath(debate)}/[id]/talk/argument/[comment_id]/compose?returnPost=${parentId}`;
  } else {
    composeHref = `${getDebateRootPath(debate)}/[id]/testimonial/[testimonial_id]/compose?returnPost=${parentId}`;
  }

  useEffect(() => {
    setComments((commentList && commentList.sort(sortByMostLiked)) || []);
  }, [commentList]);

  useEffect(() => {
    if (answers.current.lastChild) {
      if (answers.current.lastChild.classList.contains('message')) {
        mainLine.current.style.height = `${height - answers.current.lastChild.offsetHeight + 57}px`;
      } else {
        mainLine.current.style.height = `${
          height - answers.current.lastChild.offsetHeight - answers.current.lastChild.previousSibling.offsetHeight + 48
        }px`;
      }
      if (comments.length > 0 && comments[0].isLastLevel) {
        mobileLine.current.style.height = `${
          height +
          mobileLine.current.parentNode.previousSibling.offsetHeight -
          answers.current.lastChild.offsetHeight -
          -25
        }px`;
        mobileLine.current.style.top = `-${mobileLine.current.parentNode.previousSibling.offsetHeight - 25}px`;
      }
    }
  }, [height]);

  const handleSubmitComment = data => {
    if (data.source && !isURL(data.source, { allow_underscores: true })) {
      throw new SubmissionError({
        source: 'La source doit être un lien valide.',
      });
    } else if (!data.content || data.content === '') {
      throw new SubmissionError({
        content: 'Le champ commentaire est obligatoire.',
      });
    } else {
      let body = {
        title: data.title || null,
        content: data.content,
        userId: user.id,
        parentId,
      };

      if (!isTestimonial) {
        body = { ...body, postType: position, sourceLinks: data.source ? [data.source] : null };
      }
      dispatch(addComment(body, token, isTestimonial));
    }
  };

  useEffect(() => {
    if (!isEmpty(commentData) && parentId === commentData.parent.id) {
      if (comments.filter(comment => comment.id === commentData.id).length < 1) {
        const newComments = cloneDeep(comments);
        newComments.unshift(commentData);
        setComments(newComments);
      }

      dispatch(resetAddComment());
    }
  }, [commentData]);

  const renderComments = () => {
    const ret = [];
    if (comments.length > 0 && !loadingUserLikes) {
      comments.forEach(comment => {
        ret.push(
          <Message
            key={comment.id}
            className={comment.isLastLevel ? 'comment-last-level' : 'comment'}
            maxLine={5}
            displaySource
            debate={debate}
            data={comment}
            canDisplayComments={!comment.isLastLevel}
            isArgument={false}
            isTestimonial={isTestimonial}
          />,
        );
      });
    }
    return ret;
  };
  return (
    <div className={styles.container}>
      <div ref={mainLine} className={styles.line}></div>

      {comments.length > 0 && comments[0].isLastLevel && <div ref={mobileLine} className={styles['mobile-line']}></div>}

      {size.width > 650 && !isViewerParticipant(user, debate) && (
        <AddCommentDesktop
          form={`AddComment${parentId}`}
          parentId={parentId}
          getPosition={newPosition => setPosition(newPosition)}
          onSubmit={handleSubmitComment}
          withSource={!isTestimonial}
          withPosition={!isTestimonial}
        />
      )}
      <div ref={answers} className={styles.answers}>
        {renderComments()}
      </div>
      {size.width <= 650 && !isViewerParticipant(user, debate) && (
        <ClientOnlyPortal selector="#app">
          <Link href={composeHref} as={`${router.asPath}/compose?returnPost=${parentId}`}>
            <a className={styles.add}>
              <CommentIcon />
            </a>
          </Link>
        </ClientOnlyPortal>
      )}
    </div>
  );
};

Comments.propTypes = {
  commentList: PropTypes.array,
  debate: PropTypes.object,
  parentId: PropTypes.number,
  height: PropTypes.number,
  isTestimonial: PropTypes.bool,
};

export default withResizeDetector(Comments);
