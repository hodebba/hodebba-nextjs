/* eslint-disable indent */
/* eslint-disable prettier/prettier */
/* eslint-disable object-curly-newline */
/* eslint-disable operator-linebreak */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
import axios from 'axios';
import debounce from 'lodash/debounce';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useRef, useState } from 'react';
import { hotjar } from 'react-hotjar';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import useSWR from 'swr';

import { getThemes, publishDebate, updateViews } from '../../actions/debate.actions';
import { sendNotification } from '../../actions/user.actions';
import { itemsMenuDebate } from '../../components/itemsMenu';
import PageMenu from '../../components/PageMenu';
import SubmitButton from '../../components/SubmitButton';
import isFallback from '../../HOCs/isFallback.hoc';
import MobileHeader from '../../layout/Header/MobileHeader';
import URL from '../../services/api';
import { incrementViews } from '../../services/debate.service';
import { requestWithToken } from '../../services/options';
import * as gtag from '../../utils/gtags';
import Error from '../Error';
import styles from './debate.module.scss';
import Poll from './Poll';

const fetcher = (url, token, debate) => {
  if (!isEmpty(token)) {
    return axios.get(url, requestWithToken(token)).then(res => res.data);
  }
  return debate;
};

const Debate = ({ children, activeItem, isSmallScreen }) => {
  const pageMenu = useRef(null);
  const router = useRouter();
  const [debate, setDebate] = useState(children.props.debate);
  const viewNumber = useSelector(state => state.debate.local.viewNumber);
  const { user, token, notification } = useSelector(state => state.user.local);
  const publishLoading = useSelector(state => state.debate.local.loading.publishDebate);
  const [userCanCreateDebate, setUserCanCreateDebate] = useState(true);
  const dispatch = useDispatch();
  const [isPublished, setIsPublished] = useState(false);

  const { data } = useSWR(
    [debate.isPrivate ? URL.debates.getPrivate(debate.id) : URL.debates.get(debate.id), token, debate],
    fetcher,
    {
      initialData: debate,
      revalidateOnMount: true,
    },
  );

  useEffect(() => {
    dispatch(getThemes());
    if (process.env.NEXT_PUBLIC_API_URL === 'https://api.hodebba.fr') {
      hotjar.initialize(2079058, 6);
    }
  }, []);

  useEffect(() => {
    let isGranted = false;
    if (!isEmpty(user)) {
      if (user.roles && user.roles[0].name === 'ROLE_ADMIN') {
        isGranted = true;
      }
    }
    setUserCanCreateDebate(isGranted);
  }, []);

  const adjustMenu = () => {
    if (pageMenu.current) {
      if (window.scrollY > 549) {
        pageMenu.current.classList.add('fix-on-top');
      } else {
        pageMenu.current.classList.remove('fix-on-top');
      }
    }
  };

  useEffect(() => {
    setDebate(children.props.debate);
  }, [children.props]);

  const debounceAdjustMenu = debounce(adjustMenu, 10);

  useEffect(() => {
    document.addEventListener('scroll', debounceAdjustMenu);
    return () => {
      document.removeEventListener('scroll', debounceAdjustMenu);
    };
  }, []);

  useEffect(() => {
    if (notification) {
      toast.success(notification);
      dispatch(sendNotification(null));
      setIsPublished(true);
    }
    if (data.id === 3) {
      gtag.event('conversion', { send_to: 'AW-480766229/M_dICPnshugBEJXSn-UB' });
    }
  }, [notification]);

  useEffect(() => {
    incrementViews(data.id);
  }, []);

  useEffect(() => {
    dispatch(updateViews(data.id, data.viewNumber));
  }, []);

  return (
    <Fragment>
      {children.props.error === '' &&
      (data.debateState === 'published' || (userCanCreateDebate && data.debateState === 'draft')) ? (
        <div className="center-block">
          <Poll poll={data.poll} image={data.imageLink} views={viewNumber[data.id] || data.viewNumber} debate={data} />
          <PageMenu
            reference={pageMenu}
            items={data.isPrivate ? itemsMenuDebate(data).slice(0, 2) : itemsMenuDebate(data)}
            className="movable"
            active={activeItem}
            id={router.query.id}
          />
          <div className={styles.children}>{children}</div>
          {userCanCreateDebate && !isPublished && data.debateState === 'draft' && (
            <div className={styles.validate}>
              <SubmitButton
                label="Mettre en ligne"
                loading={publishLoading}
                onClick={() => dispatch(publishDebate({ id: data.id, debateState: 'published' }, token))}
              />
            </div>
          )}
        </div>
      ) : (
        <Fragment>
          {isSmallScreen && <MobileHeader />}
          <Error code={children.props.error === '' ? 403 : 404} />
        </Fragment>
      )}
    </Fragment>
  );
};

Debate.propTypes = {
  children: PropTypes.any.isRequired,
  activeItem: PropTypes.string.isRequired,
  isSmallScreen: PropTypes.bool,
};

export default isFallback(Debate);
