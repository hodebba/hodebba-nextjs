import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import { Fragment } from 'react';

const ThinkLikeMeSeo = ({ debate }) => {
  const tags = [];
  debate.tags.forEach(tag => {
    tags.push(tag.label);
  });
  return (
    <Fragment>
      {debate.isPrivate ? (
        <NextSeo noindex />
      ) : (
        <NextSeo
          title={`Quelles sont les personnalités qui pensent comme vous sur ${debate.title}`}
          description={`Découvrez de quel côté se sont placés des personnalités publiques sur ${debate.title}`}
          openGraph={{
            url: `${process.env.NEXT_PUBLIC_FRONT_END_URL}/debate/${debate.id}/who-think-like-me`,
            title: `Quelles sont les personnalités qui pensent comme vous sur ${debate.title}`,
            description: `Découvrez de quel côté se sont placés des personnalités publiques sur ${debate.title}`,
            type: 'article',
            article: {
              publishedTime: debate.createdDate,
              section: 'Qui pense comme moi',
              tags,
            },
            images: [
              {
                url: `${process.env.NEXT_PUBLIC_CLOUDINARY_URL}/${debate.imageLink}`,
                alt: debate.title,
              },
            ],
            site_name: 'Hodebba',
          }}
          twitter={{
            handle: '@handle',
            site: '@site',
            cardType: 'summary_large_image',
          }}
        />
      )}
    </Fragment>
  );
};

ThinkLikeMeSeo.propTypes = {
  debate: PropTypes.object,
};

export default ThinkLikeMeSeo;
