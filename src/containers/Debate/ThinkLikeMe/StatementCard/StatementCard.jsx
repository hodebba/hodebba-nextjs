/* eslint-disable react/no-unescaped-entities */
import closeSmall from '@iconify/icons-vaadin/close-small';
import { Icon } from '@iconify/react';
import PropTypes from 'prop-types';
import ClampLines from 'react-clamp-lines';

import Card from '../Card';
import Carousel from '../Carousel';
import styles from './statement-card.module.scss';

const StatementCard = ({ onClick }) => {
  const getNumberOfLines = () => {
    if (window.innerHeight < 650) {
      return 5;
    }
    if (window.innerHeight < 700) {
      return 6;
    }
    if (window.innerHeight < 750) {
      return 7;
    }
    return 8;
  };

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.header__title}>Ils pensent comme vous !</div>
        <Icon width="28" height="28" icon={closeSmall} color="white" className="clickable" onClick={onClick} />
      </div>

      <div className={styles.text}>
        <ClampLines
          text=" Alors que de nombreux compatriotes subissent un racisme anti-blanc dont aucun expert autoproclamé ni média ne
            parle, cette provocation ne doit rester impunie. Il n'y a rien d'artistique dans ce qui est purement et
            simplement un appel à la haine et au meurtre. zfin iz viznzivn irnviez iez irznvb orzjn irzn orzn iznv orzn zo rizn izrnvizrn v"
          lines={getNumberOfLines()}
          ellipsis="..."
          moreText="Voir plus"
          lessText="Voir moins"
          className="see-more"
          innerElement="p"
          stopPropagation
        />
      </div>

      <div className={styles.carousel}>
        <Carousel>
          <div className={styles.card}>
            <Card />
          </div>
          <div className={styles.card}>
            <Card />
          </div>
          <div className={styles.card}>
            <Card />
          </div>
        </Carousel>
      </div>
    </div>
  );
};

StatementCard.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default StatementCard;
