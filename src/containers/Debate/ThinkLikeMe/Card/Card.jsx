/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { Image, Transformation } from 'cloudinary-react';
import Link from 'next/link';
import PropTypes from 'prop-types';

import styles from './card.module.scss';

const Card = ({ celebrity, description }) => {
  return (
    <Link href="/celebrities/[id]/positions" as={`/celebrities/${celebrity.id}/positions`}>
      <a className={styles.container}>
        <Image publicId={celebrity.imageLink} alt="célébrité" secure="true">
          <Transformation crop="fill" gravity="faces" />
        </Image>

        <h3>{celebrity.fullName}</h3>
        <p>{description}</p>
      </a>
    </Link>
  );
};

Card.propTypes = {
  celebrity: PropTypes.object,
  description: PropTypes.string,
};

export default Card;
