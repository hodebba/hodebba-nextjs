import PropTypes from 'prop-types';
import { useRef } from 'react';
import Slider from 'react-slick';

const Carousel = ({ children }) => {
  const slider = useRef(null);

  const settings = {
    centerPadding: '60px',
    centerMode: true,
    dots: false,
    arrows: false,
    infinite: true,
    draggable: true,
    speed: 500,
    swipeToSlide: true,
    touchMove: true,
    swipe: true,
    className: 'statements-list',
    initialSlide: 0,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <Slider ref={slider} {...settings}>
      {children}
    </Slider>
  );
};

Carousel.propTypes = {
  children: PropTypes.any,
};

export default Carousel;
