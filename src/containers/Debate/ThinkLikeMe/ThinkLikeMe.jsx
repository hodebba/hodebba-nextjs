import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useSwipeable } from 'react-swipeable';

import PositionButtons from '../../../components/PositionButtons';
import Card from './Card';
import ThinkLikeMeSeo from './think-like-me.seo';
import styles from './thinkLikeMe.module.scss';

const textMapping = {
  pro: 'Ils sont pour !',
  neutral: 'Ils sont neutres !',
  con: 'Ils sont contre !',
};

const ThinkLikeMe = ({ positions, debate, descriptions }) => {
  const [position, setPosition] = useState('pro');
  const [isSmallScreen, setIsSmallScreen] = useState(false);

  const handlers = useSwipeable({
    onSwipedLeft: () => setPosition('con'),
    onSwipedRight: () => setPosition('pro'),

    preventDefaultTouchmoveEvent: true,
    trackMouse: true,
  });

  const checkInnerWidth = () => {
    if (window.innerWidth > 850) {
      setIsSmallScreen(false);
    } else {
      setIsSmallScreen(true);
    }
  };

  useEffect(() => {
    checkInnerWidth();
    window.addEventListener('resize', checkInnerWidth);
    return () => {
      window.removeEventListener('resize', checkInnerWidth());
    };
  }, []);

  const renderPositions = () => {
    const ret = [];
    if (positions[position]) {
      positions[position].forEach(element => {
        const filterCelebrity = Object.values(descriptions).filter(
          description => description.title === element.celebrity.fullName && description.missing !== '',
        );

        const description = filterCelebrity.length > 0 ? filterCelebrity[0].description : null;
        ret.push(<Card celebrity={element.celebrity} key={element.id} description={description} />);
      });
    }

    return ret;
  };

  /* if (isEmpty(token) || isUndefined(vote)) {
    return (
      <div className={styles['not-voted']}>
        <ThinkLikeMeSeo debate={debate} />
        <p>
          Participez au vote et découvrez les personnalités publiques (politiques, journalistes, artistes...) qui
          pensent comme vous !
        </p>
      </div>
    );
  } */

  const renderTypeButtons = () => {
    return (
      <div className={` ${styles.type} ${position === 'pro' ? styles['type-pro'] : styles['type-con']}`}>
        <button
          type="button"
          onClick={() => setPosition('pro')}
          className={`${styles.type__pro} ${position === 'pro' ? styles.active : styles.inactive}`}
        >
          POUR
        </button>
        <button
          type="button"
          onClick={() => setPosition('con')}
          className={`${styles.type__con} ${position !== 'pro' ? styles.active : styles.inactive} `}
        >
          CONTRE
        </button>
      </div>
    );
  };

  return (
    <div className={styles.container} {...handlers}>
      <ThinkLikeMeSeo debate={debate} />
      {isSmallScreen ? (
        renderTypeButtons()
      ) : (
        <div className={styles.top}>
          <div className={styles.positions}>
            <PositionButtons
              labels={{ pro: 'Pour', con: 'Contre' }}
              getPosition={newPosition => setPosition(newPosition)}
            />
          </div>
          <div className={`${styles.line} ${styles[position]}`}></div>
        </div>
      )}
      <h2>{textMapping[position]}</h2>

      <div className={styles['card-list']}>{renderPositions()}</div>
    </div>
  );
};

ThinkLikeMe.propTypes = {
  positions: PropTypes.object,
  debate: PropTypes.object,
  descriptions: PropTypes.object,
};

export default ThinkLikeMe;
