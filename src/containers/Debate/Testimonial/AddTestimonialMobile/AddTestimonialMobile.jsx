import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stopSubmit, SubmissionError } from 'redux-form';

import { addTestimonial } from '../../../../actions/debate.actions';
import MobileWriteMessage from '../../../../components/MobileWriteMessage';

const AddTestimonialMobile = () => {
  const loading = useSelector(state => state.debate.local.loading.addTestimonial);
  const error = useSelector(state => state.debate.local.errors.addTestimonial);
  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    router.prefetch('/debate/[id]/testimonial', router.asPath.split('/compose')[0]);
  }, [router]);

  useEffect(() => {
    if (!isEmpty(error)) {
      stopSubmit('MobileWriteMessage', {
        _error: 'Une erreur innatendue est survenue, merci de contacter le support technique',
      });
    }
  }, [error]);

  const handleSubmit = data => {
    if (!data.content || data.content === '') {
      throw new SubmissionError({
        _error: 'Le champ argument est obligatoire.',
      });
    } else {
      const body = {
        title: data.title || null,
        content: data.content,
        userId: user.id,
        debateId: router.asPath.split('/')[2],
      };
      dispatch(addTestimonial(body, token));
      router.push('/debate/[id]/testimonial', router.asPath.split('/compose')[0]);
    }
  };

  return (
    <MobileWriteMessage
      CTA="Publier"
      withTitle
      placeHolderText="Mon témoignage..."
      onSubmit={handleSubmit}
      loading={loading}
      returnHref="/debate/[id]/testimonial"
    />
  );
};

export default AddTestimonialMobile;
