/* eslint-disable react-hooks/exhaustive-deps */
import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stopSubmit, SubmissionError } from 'redux-form';

import { addComment } from '../../../../actions/debate.actions';
import MobileWriteMessage from '../../../../components/MobileWriteMessage';

const AddCommentMobile = () => {
  const loading = useSelector(state => state.debate.local.loading.addComment);
  const error = useSelector(state => state.debate.local.errors.addComment);
  const token = useSelector(state => state.user.local.token);
  const user = useSelector(state => state.user.local.user);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    router.prefetch(
      '/debate/[id]/testimonial/[testimonial_id]',
      `/debate/${router.asPath.split('/')[2]}/testimonial/${router.query.returnPost}`,
    );
  }, [router]);

  useEffect(() => {
    if (!isEmpty(error)) {
      dispatch(
        stopSubmit('MobileWriteMessage', {
          _error: 'Une erreur innatendue est survenue, merci de contacter le support technique',
        }),
      );
    }
  }, [error]);

  const handleSubmit = data => {
    if (!data.content || data.content === '') {
      throw new SubmissionError({
        _error: 'Le champ argument est obligatoire.',
      });
    } else {
      const body = {
        title: data.title || null,
        content: data.content,
        userId: user.id,
        parentId: parseInt(router.asPath.split('/')[4], 10),
      };
      dispatch(addComment(body, token, true));
      router.push(
        '/debate/[id]/testimonial/[testimonial_id]',
        `/debate/${router.asPath.split('/')[2]}/testimonial/${router.query.returnPost}`,
      );
    }
  };

  return (
    <MobileWriteMessage
      CTA="Publier"
      withTitle
      placeHolderText="Mon commentaire..."
      onSubmit={handleSubmit}
      loading={loading}
      isTestimonial
      returnHref="/debate/[id]/testimonial/[testimonial_id]"
    />
  );
};

export default AddCommentMobile;
