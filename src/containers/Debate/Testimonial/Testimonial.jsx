/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import useSWR from 'swr';

import PlusIcon from '../../../../public/static/icons/plus2.svg';
import WriteIcon from '../../../../public/static/icons/write.svg';
import { addTestimonial, resetAddTestimonial } from '../../../actions/debate.actions';
import { getUserLikes } from '../../../actions/user.actions';
import ClientOnlyPortal from '../../../components/ClientOnlyPortal';
import DesktopWriteMessage from '../../../components/DesktopWriteMessage';
import FilterDropdown from '../../../components/FilterDropdown';
import Message from '../../../components/Message';
import NotConnectedModal from '../../../components/NotConnectedModal';
import URL from '../../../services/api';
import { preferences, sortByNewest, sortByOldest } from '../../../utils/general.helper';
import styles from './testimonial.module.scss';
import TestimonialSeo from './testimonial.seo';

const fetcher = url => axios.get(url).then(res => res.data);

const Testimonial = ({ isSmallScreen, testimonials, debate }) => {
  const router = useRouter();
  // const loadingUserLikes = useSelector(state => state.user.local.loading.getUserLikes);
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const listTestimonials = useRef(null);
  const dispatch = useDispatch();
  const [modalIsOpened, setModalIsOpened] = useState(false);
  const [modalNotConnected, setModalNotConnected] = useState({ isOpened: false, type: '' });
  const loading = useSelector(state => state.debate.local.loading.addTestimonial);
  const error = useSelector(state => state.debate.local.errors.addTestimonial);
  const testimonialData = useSelector(state => state.debate.data.addTestimonial);
  const { data } = useSWR(URL.post.getTestimonials(debate.id), fetcher, {
    initialData: testimonials,
    revalidateOnMount: true,
  });
  const [testimonialList, setTestimonialList] = useState(testimonials);

  useEffect(() => {
    if (data.length !== testimonialList.length) {
      setTestimonialList(data);
    }
  }, [data]);

  useEffect(() => {
    if (!isEmpty(user)) {
      dispatch(getUserLikes(user.id, debate.id));
    }
  }, []);

  useEffect(() => {
    if (!isEmpty(testimonialData)) {
      toast.success('Votre témoignage a été ajouté avec succès !');
      setModalIsOpened(false);

      if (testimonialList.filter(testimonial => testimonial.id === testimonialData.id).length < 1) {
        setTestimonialList([...testimonialList, testimonialData]);
      }
      dispatch(resetAddTestimonial());

      setTimeout(() => {
        window.scrollTo({
          behavior: 'smooth',
          top: listTestimonials.current.getBoundingClientRect().bottom,
          left: 0,
        });
      }, 200);
    }
  }, [testimonialData]);

  const handleSubmitTestimonial = data => {
    const body = {
      title: data.title || null,
      content: data.content,
      userId: user.id,
      debateId: debate.id,
    };

    dispatch(addTestimonial(body, token));
  };
  const handleChangeFilter = newFilter => {
    switch (newFilter.value) {
      case 'recent':
        setTestimonialList(cloneDeep(testimonialList).sort(sortByNewest));
        break;
      case 'older':
        setTestimonialList(cloneDeep(testimonialList).sort(sortByOldest));
        break;
      case 'popularity':
        setTestimonialList(data);
        break;
      default:
    }
  };

  const renderTestimonials = () => {
    const ret = [];
    if (testimonialList.length > 0) {
      testimonialList.forEach(testimonial => {
        ret.push(
          <Link
            key={testimonial.id}
            href={`${router.pathname}/[testimonial_id]`}
            as={`${router.asPath}/${testimonial.id}`}
          >
            <a className="link-without-style ">
              <Message key={testimonial.id} data={testimonial} debate={debate} className="testimonial" maxLine={7} />
            </a>
          </Link>,
        );
      });
    } else {
      ret.push(
        <p className={styles['no-arguments']} key={0}>
          Aucun témoignage n'a été ajouté pour le moment.
        </p>,
      );
    }
    return ret;
  };
  return (
    <div className={styles.container}>
      <TestimonialSeo debate={debate} />
      <div className={styles.top}>
        <FilterDropdown options={preferences} defaultValue={preferences[0]} onChange={handleChangeFilter} />
        {!isSmallScreen && (
          <button
            type="button"
            className="primary-button"
            onClick={() => {
              if (isEmpty(token) || !user.enabled) {
                setModalNotConnected({ isOpened: true, type: 'testimonial' });
              } else {
                setModalIsOpened(true);
              }
            }}
          >
            <PlusIcon /> Ajouter un témoignage
          </button>
        )}
      </div>
      <div className={styles.list} ref={listTestimonials}>
        {renderTestimonials()}
      </div>
      {isSmallScreen && (
        <ClientOnlyPortal selector="#app">
          <Link href="/debate/[id]/testimonial/compose" as={`${router.asPath}/compose`}>
            <a className={styles.add}>
              <WriteIcon />
            </a>
          </Link>
        </ClientOnlyPortal>
      )}

      {modalIsOpened && (
        <DesktopWriteMessage
          withTitle
          title="Ajoutez votre témoignage"
          placeHolderText="Mon témoignage..."
          CTA="Publier"
          onClose={() => setModalIsOpened(false)}
          loading={loading}
          onSubmit={handleSubmitTestimonial}
          error={error}
        />
      )}

      {modalNotConnected.isOpened && (
        <NotConnectedModal
          accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
          type={modalNotConnected.type}
          onClick={() => null}
          onClose={() => {
            setModalNotConnected({ isOpened: false, type: '' });
          }}
        />
      )}
    </div>
  );
};

Testimonial.propTypes = {
  isSmallScreen: PropTypes.bool,
  testimonials: PropTypes.array,
  debate: PropTypes.object,
};

export default Testimonial;
