import constants from './constants';

export function updateUserVotes(pollId, vote) {
  return {
    type: constants.updateUserVotes,
    pollId,
    vote,
  };
}

export function getUserVotes(id, token) {
  return {
    type: constants.getUserVotes.request,
    id,
    token,
  };
}
