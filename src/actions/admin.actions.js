import constants from './constants';

export function deleteImageFromCloudinary(publicId, token) {
  return {
    type: constants.deleteImageFromCloudinary.request,
    publicId,
    token,
  };
}

export function approveStatement(body, token) {
  return {
    type: constants.approveStatement.request,
    body,
    token,
  };
}

export function disapproveStatement(id, token) {
  return {
    type: constants.disapproveStatement.request,
    id,
    token,
  };
}
