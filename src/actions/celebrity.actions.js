import constants from './constants';

export function createCelebrity(data, token) {
  return {
    type: constants.createCelebrity.request,
    data,
    token,
  };
}

export function reinitializeCreateCelebrity() {
  return {
    type: constants.reinitializeCreateCelebrity,
  };
}

export function getTopics() {
  return {
    type: constants.getTopics.request,
  };
}

export function addStatement(data, token) {
  return {
    type: constants.addStatement.request,
    data,
    token,
  };
}

export function addPosition(data, token) {
  return {
    type: constants.addPosition.request,
    data,
    token,
  };
}

export function resetAddStatement() {
  return {
    type: constants.resetAddStatement,
  };
}

export function resetAddPosition() {
  return {
    type: constants.resetAddPosition,
  };
}

export function getProfessions() {
  return {
    type: constants.getProfessions.request,
  };
}

export function getPoliticalParties() {
  return {
    type: constants.getPoliticalParties.request,
  };
}

export function getPoliticalPositions() {
  return {
    type: constants.getPoliticalPositions.request,
  };
}
