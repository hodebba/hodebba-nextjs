import constants from './constants';

export function openModalAuthentification(authType) {
  return {
    type: constants.openModalAuthentification,
    authType,
  };
}

export function closeModalAuthentification() {
  return {
    type: constants.closeModalAuthentification,
  };
}

export function signUp(data) {
  return {
    type: constants.signUp.request,
    data,
  };
}

export function signIn(data) {
  return {
    type: constants.signIn.request,
    data,
  };
}

export function forgetPassword(email) {
  return {
    type: constants.forgetPassword.request,
    email,
  };
}
export function logout() {
  return {
    type: constants.logout,
  };
}

export function resendConfirmEmail(userId, token) {
  return {
    type: constants.resendConfirmEmail.request,
    userId,
    token,
  };
}

export function continueWithFacebookGoogle(data) {
  return {
    type: constants.continueWithFacebookGoogle.request,
    data,
  };
}
