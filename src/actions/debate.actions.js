import constants from './constants';

export function createDebate(data, token) {
  return {
    type: constants.createDebate.request,
    data,
    token,
  };
}

export function publishDebate(data, token) {
  return {
    type: constants.publishDebate.request,
    data,
    token,
  };
}

export function sendVote(vote, token) {
  return {
    type: constants.sendVote.request,
    vote,
    token,
  };
}

export function reinitializeCreateDebate() {
  return {
    type: constants.reinitializeCreateDebate,
  };
}

export function getThemes() {
  return {
    type: constants.getThemes.request,
  };
}

export function addArgument(data, token) {
  return {
    type: constants.addArgument.request,
    data,
    token,
  };
}

export function resetAddArgument() {
  return {
    type: constants.resetAddArgument,
  };
}

export function addComment(data, token, isTestimonial) {
  return {
    type: constants.addComment.request,
    data,
    token,
    isTestimonial,
  };
}

export function resetAddComment() {
  return {
    type: constants.resetAddComment,
  };
}
export function addTestimonial(data, token) {
  return {
    type: constants.addTestimonial.request,
    data,
    token,
  };
}
export function resetAddTestimonial() {
  return {
    type: constants.resetAddTestimonial,
  };
}

export function updateViews(id, viewNumber) {
  return {
    type: constants.updateViews,
    id,
    viewNumber,
  };
}

export function getAllDebates() {
  return {
    type: constants.getAllDebates.request,
  };
}

export function invite(data, token) {
  return {
    type: constants.invite.request,
    data,
    token,
  };
}

export function confirmInvitation(invitationToken, token) {
  return {
    type: constants.confirmInvitation.request,
    invitationToken,
    token,
  };
}

export function promoteParticipant(data, token) {
  return {
    type: constants.promoteParticipant.request,
    data,
    token,
  };
}

export function deleteParticipant(id, token, participant) {
  return {
    type: constants.deleteParticipant.request,
    id,
    token,
    participant,
  };
}

export function deleteDebate(id, token) {
  return {
    type: constants.deleteDebate.request,
    id,
    token,
  };
}
export function deletePost(id, token) {
  return {
    type: constants.deletePost.request,
    id,
    token,
  };
}

export function resetNotification() {
  return {
    type: constants.resetNotification,
  };
}

export function resetDeleteParticipant() {
  return {
    type: constants.resetDeleteParticipant,
  };
}

export function resetPromoteParticipant() {
  return {
    type: constants.resetPromoteParticipant,
  };
}
