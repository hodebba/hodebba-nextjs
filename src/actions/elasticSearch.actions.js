import constants from './constants';

export function searchText(text) {
  return {
    type: constants.searchText.request,
    text,
  };
}

export function saveSearchReturnPath(path) {
  return {
    type: constants.saveSearchReturnPath,
    path,
  };
}

export function clearSearch() {
  return {
    type: constants.clearSearch,
  };
}
