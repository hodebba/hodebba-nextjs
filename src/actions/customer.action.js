import constants from './constants';

export function contactUs(data) {
  return {
    type: constants.contactUs.request,
    data,
  };
}

export function reportPost(data) {
  return {
    type: constants.reportPost.request,
    data,
  };
}

export function resetReportPost() {
  return {
    type: constants.resetReportPost,
  };
}
