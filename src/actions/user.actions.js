import constants from './constants';

export function savePassword(data) {
  return {
    type: constants.savePassword.request,
    data,
  };
}

export function saveReturnPath(path) {
  return {
    type: constants.saveReturnPath,
    path,
  };
}

export function updateEmail(email, id, token) {
  return {
    type: constants.updateEmail.request,
    email,
    id,
    token,
  };
}

export function clearUpdateEmailError() {
  return {
    type: constants.clearUpdateEmailError,
  };
}

export function clearUpdatePasswordError() {
  return {
    type: constants.clearUpdatePasswordError,
  };
}

export function updatePassword(data, id) {
  return {
    type: constants.updatePassword.request,
    data,
    id,
  };
}

export function updateUser(user, token, field) {
  return {
    type: constants.updateUser.request,
    user,
    token,
    field,
  };
}

export function clearUpdateUserError() {
  return {
    type: constants.clearUpdateUserError,
  };
}

export function deleteAccount(id, token) {
  return {
    type: constants.deleteAccount.request,
    id,
    token,
  };
}

export function setUserEnabled() {
  return {
    type: constants.setUserEnabled,
  };
}

export function sendNotification(text) {
  return {
    type: constants.sendNotification,
    text,
  };
}

export function getUserLikes(userId, debateId) {
  return {
    type: constants.getUserLikes.request,
    userId,
    debateId,
  };
}

export function getStatementsUserLikes(userId) {
  return {
    type: constants.getStatementsUserLikes.request,
    userId,
  };
}

export function openMobileThemeList() {
  return {
    type: constants.openMobileThemeList,
  };
}

export function closeMobileThemeList() {
  return {
    type: constants.closeMobileThemeList,
  };
}

export function updateUserLikes(debateId, postId, isLike, isLikeId, likeCount, dislikeCount) {
  return {
    type: constants.updateUserLikes,
    debateId,
    postId,
    isLike,
    isLikeId,
    likeCount,
    dislikeCount,
  };
}

export function updateStatementsUserLikes(statementId, isLike, isLikeId, likeCount, dislikeCount) {
  return {
    type: constants.updateStatementsUserLikes,
    statementId,
    isLike,
    isLikeId,
    likeCount,
    dislikeCount,
  };
}

export function getPostsDataByUser(userId, token) {
  return {
    type: constants.getPostsDataByUser.request,
    userId,
    token,
  };
}

export function getUser(userId, token) {
  return {
    type: constants.getUser.request,
    userId,
    token,
  };
}

export function getInfoFromCookies() {
  return {
    type: constants.getInfoFromCookies,
  };
}
