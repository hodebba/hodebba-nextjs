import { generateActionTypes } from '../utils/redux.helper';
const authentification = 'app/AUTHENTIFICATION/';
const admin = 'app/ADMIN/';
const user = '/app/USER/';
const debate = '/app/DEBATE/';
const celebrity = '/app/CELEBRITY/';
const es = '/app/ELASTIC_SEARCH/';
const customer = '/app/CUSTOMER/';

export default {
  /* AUTHENTIFICATION */
  openModalAuthentification: `${authentification}OPEN_MODAL_AUTHENTIFICATION`,
  closeModalAuthentification: `${authentification}CLOSE_MODAL_AUTHENTIFICATION`,
  logout: `${authentification}LOGOUT`,
  signUp: generateActionTypes(authentification, 'SIGN_UP'),
  signIn: generateActionTypes(authentification, 'SIGN_IN'),
  forgetPassword: generateActionTypes(authentification, 'FORGET_PASSWORD'),
  savePassword: generateActionTypes(authentification, 'SAVE_PASSWORD'),
  resendConfirmEmail: generateActionTypes(authentification, 'RESEND_CONFIRM_EMAIL'),

  /* USER */
  saveReturnPath: `${user}SAVE_RETURN_PATH`,
  updateEmail: generateActionTypes(user, 'UPDATE_EMAIL'),
  clearUpdateEmailError: `${user}CLEAR_UPDATE_EMAIL_ERROR`,
  clearUpdatePasswordError: `${user}CLEAR_UPDATE_PASSWORD_ERROR`,
  updateUser: generateActionTypes(user, 'UPDATE_USER'),
  clearUpdateUserError: `${user}CLEAR_UPDATE_USER_ERROR`,
  updatePassword: generateActionTypes(user, 'UPDATE_PASSWORD'),
  deleteAccount: generateActionTypes(user, 'DELETE_ACCOUNT'),
  getUser: generateActionTypes(user, 'GET_USER'),
  setUserEnabled: `${user}SET_USER_ENABLED`,
  openMobileThemeList: `${user}OPEN_MOBILE_THEME_LIST`,
  closeMobileThemeList: `${user}CLOSE_MOBILE_THEME_LIST`,
  sendNotification: `${user}SEND_NOTIFICATION`,
  getUserLikes: generateActionTypes(user, 'GET_USER_LIKES'),
  getStatementsUserLikes: generateActionTypes(user, 'GET_STATEMENTS_USER_LIKES'),
  updateUserLikes: `${user}UPDATE_USER_LIKES`,
  updateStatementsUserLikes: `${user}UPDATE_STATEMENTS_USER_LIKES`,
  continueWithFacebookGoogle: generateActionTypes(user, 'CONTINUE_WITH_FACEBOOK'),
  getPostsDataByUser: generateActionTypes(debate, 'GET_POSTS_DATA_BY_USER'),
  getInfoFromCookies: `${user}GET_INFO_FROM_COOKIES`,

  /* DEBATE */
  createDebate: generateActionTypes(debate, 'CREATE_DEBATE'),
  reinitializeCreateDebate: `${debate}REINITIALIZE_CREATE_DEBATE`,
  publishDebate: generateActionTypes(debate, 'PUBLISH_DEBATE'),
  deleteImageFromCloudinary: generateActionTypes(admin, 'DELETE_IMAGE_FROM_CLOUDINARY'),
  sendVote: generateActionTypes(debate, 'SEND_VOTE'),
  updateUserVotes: `${debate}UPDATE_USER_VOTES`,
  addArgument: generateActionTypes(debate, 'ADD_ARGUMENT'),
  addComment: generateActionTypes(debate, 'ADD_COMMENT'),
  resetAddComment: `${debate}RESET_ADD_COMMENT`,
  resetAddArgument: `${debate}RESET_ADD_ARGUMENT`,
  addTestimonial: generateActionTypes(debate, 'ADD_TESTIMONIAL'),
  resetAddTestimonial: `${debate}RESET_ADD_TESTIMONIAL`,
  getThemes: generateActionTypes(debate, 'GET_THEMES'),
  updateViews: `${debate}UPDATE_VIEWS`,
  getAllDebates: generateActionTypes(debate, 'GET_ALL_DEBATES'),
  invite: generateActionTypes(debate, 'INVITE'),
  confirmInvitation: generateActionTypes(debate, 'CONFIRM_INVITATION'),
  promoteParticipant: generateActionTypes(debate, 'PROMOTE_PARTICIPANT'),
  deleteParticipant: generateActionTypes(debate, 'DELETE_PARTICIPANT'),
  deleteDebate: generateActionTypes(debate, 'DELETE_DEBATE'),
  deletePost: generateActionTypes(debate, 'DELETE_POST'),
  resetNotification: `${debate}RESET_NOTIFICATION`,
  resetDeleteParticipant: `${debate}RESET_DELETE_PARTICIPANT`,
  resetPromoteParticipant: `${debate}RESET_PROMOTE_PARTICIPANT`,

  /* VOTES */
  getUserVotes: generateActionTypes(debate, 'GET_USER_VOTES'),

  /* CELEBRITY */
  createCelebrity: generateActionTypes(celebrity, 'CREATE_CELEBRITY'),
  reinitializeCreateCelebrity: `${celebrity}REINITIALIZE_CREATE_CELEBRITY`,
  getTopics: generateActionTypes(celebrity, 'GET_TOPICS'),
  addStatement: generateActionTypes(celebrity, 'ADD_STATEMENT'),
  resetAddStatement: `${celebrity}RESET_ADD_STATEMENT`,
  addPosition: generateActionTypes(celebrity, 'ADD_POSITION'),
  resetAddPosition: `${celebrity}RESET_ADD_POSITION`,
  getProfessions: generateActionTypes(celebrity, 'GET_PROFESSIONS'),
  getPoliticalPositions: generateActionTypes(celebrity, 'GET_POLITICAL_POSITIONS'),
  getPoliticalParties: generateActionTypes(celebrity, 'GET_POLITICAL_PARTIES'),

  /* ADMIN */
  approveStatement: generateActionTypes(admin, 'APPROVE_STATEMENT'),
  disapproveStatement: generateActionTypes(admin, 'DISAPPROVE_STATEMENT'),

  /* ELASTIC SEARCH */
  searchText: generateActionTypes(es, 'SEARCH_TEXT'),
  saveSearchReturnPath: `${es}SAVE_SEARCH_RETURN_PATH`,
  clearSearch: `${es}CLEAR_SEARCH`,

  /* CUSTOMER SERVICE */
  contactUs: generateActionTypes(customer, 'CONTACT_US'),
  reportPost: generateActionTypes(customer, 'REPORT_POST'),
  resetReportPost: `${customer}RESET_REPORT_POST`,
};
