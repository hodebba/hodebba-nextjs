import AllIcon from '@Static/icons/all.svg';
import CelebritiesIcon from '@Static/icons/celebrities.svg';
import DebateIcon from '@Static/icons/debate.svg';
import HomeIcon from '@Static/icons/home.svg';
import PeopleIcon from '@Static/icons/people.svg';
import PresentationIcon from '@Static/icons/presentation.svg';
import SettingsIcon from '@Static/icons/settings.svg';
import TestimonialIcon from '@Static/icons/testimonial.svg';
import TwitterIcon from '@Static/icons/twitter2.svg';
import UserIcon from '@Static/icons/user.svg';
import { getDebateRootPath } from '@Utils/debate.helper';

export const itemsMenuMyAccount = [
  {
    id: 0,
    icon: <HomeIcon />,
    type: 'dashboard',
    href: '/my-account/dashboard',
    label: 'Tableau de bord',
  },
  {
    id: 1,
    icon: <UserIcon />,
    type: 'profile',
    href: '/my-account/profile',
    label: 'Profil',
  },
  {
    id: 2,
    icon: <SettingsIcon />,
    type: 'settings',
    href: '/my-account/settings',
    label: 'Paramètres',
  },
  {
    id: 3,
    icon: <DebateIcon />,
    type: 'my-debates',
    href: '/my-account/my-debates',
    label: 'Mes débats',
  },
];

export const itemsMenuDebate = debate => {
  return [
    {
      id: 0,
      icon: <PresentationIcon />,
      type: 'presentation',
      href: `${getDebateRootPath(debate)}/[id]/presentation`,
      label: 'Comprendre le débat',
    },
    {
      id: 1,
      icon: <DebateIcon />,
      type: 'talk',
      href: `${getDebateRootPath(debate)}/[id]/talk?type=community`,
      label: 'Le débat',
    },
    {
      id: 2,
      icon: <PeopleIcon />,
      type: 'who-think-like-me',
      href: `${getDebateRootPath(debate)}/[id]/who-think-like-me`,
      label: 'Qui pense comme moi',
    },

    {
      id: 3,
      icon: <TwitterIcon />,
      type: 'twitter',
      href: `${getDebateRootPath(debate)}/[id]/twitter`,
      label: 'Populaires sur Twitter',
    },
    {
      id: 4,
      icon: <TestimonialIcon />,
      type: 'testimonial',
      href: `${getDebateRootPath(debate)}/[id]/testimonial`,
      label: 'Témoignages',
    },
  ];
};

export const itemsMenuCelebrity = [
  {
    id: 0,
    icon: <PresentationIcon />,
    type: 'positions',
    href: '/celebrities/[id]/positions',
    label: 'Prises de positions',
  },
  {
    id: 1,
    icon: <DebateIcon />,
    type: 'statements',
    href: '/celebrities/[id]/statements',
    label: 'Déclarations',
  },
];

export const itemsMenuSearch = [
  {
    id: 0,
    icon: <AllIcon />,
    type: 'all',
    href: '/search?filter=all',
    label: 'Tout',
  },
  {
    id: 1,
    icon: <DebateIcon />,
    type: 'debates',
    href: '/search?filter=debates',
    label: 'Débats',
  },
  {
    id: 2,
    icon: <CelebritiesIcon />,
    type: 'celebrities',
    href: '/search?filter=celebrities',
    label: 'Personnalités publiques',
  },
];
