/* eslint-disable operator-linebreak */
import PropTypes from 'prop-types';
import TextareaAutosize from 'react-textarea-autosize';

import styles from './text-area-rounded.module.scss';

// eslint-disable-next-line object-curly-newline
const RenderFieldTextAreaMobile = ({ input, label, meta: { touched, error, warning } }) => (
  <div className={styles.container}>
    <TextareaAutosize {...input} placeholder={label} />
    <div>
      {touched &&
        ((error && <span className="form__field--error">{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
  </div>
);

RenderFieldTextAreaMobile.propTypes = {
  input: PropTypes.any,
  label: PropTypes.string,
  meta: PropTypes.object,
};

export default RenderFieldTextAreaMobile;
