/* eslint-disable operator-linebreak */
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

// eslint-disable-next-line object-curly-newline
const RenderFieldTextArea = ({ input, label, name, type, maxCharacters, meta: { touched, error, warning } }) => (
  <div className="text-area">
    <div className="form__group">
      <TextField
        {...input}
        label={label}
        multiline
        value={input.value}
        rows="7"
        type={type}
        name={name}
        fullWidth
        inputProps={{ maxLength: maxCharacters }}
        variant="outlined"
      />
    </div>
    <div>
      {touched &&
        ((error && <span className="form__field--error">{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
  </div>
);

RenderFieldTextArea.propTypes = {
  input: PropTypes.any,
  label: PropTypes.string,
  maxCharacters: PropTypes.number,
  type: PropTypes.string,
  name: PropTypes.string,
  meta: PropTypes.object,
};

export default RenderFieldTextArea;
