/* eslint-disable indent */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable implicit-arrow-linebreak */

import arrowLeft from '@iconify/icons-vaadin/arrow-left';
import { Icon } from '@iconify/react';
import Cookie from 'js-cookie';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Field, reduxForm } from 'redux-form';

import { trim } from '../../utils/check-fields.helper';
import NotConnectedModal from '../NotConnectedModal';
import PositionButtons from '../PositionButtons';
import RenderTextAreaFieldMobile from '../RenderTextAreaFieldMobile';
import RenderTextField from '../RenderTextField';
import SubmitButton from '../SubmitButton';
import styles from './mobile-write-message.module.scss';

const MobileWriteMessage = ({
  submitting,
  handleSubmit,
  error,
  CTA,
  labels,
  withTitle,
  withSource,
  withPosition,
  withName,
  placeHolderText,
  getPosition,
  loading,
  returnHref,
  children,
  sourceIsRequired,
}) => {
  const [position, setPosition] = useState('pro');
  const router = useRouter();
  const token = Cookie.get('token') || {};
  const user = !isUndefined(Cookie.get('user')) ? JSON.parse(Cookie.get('user')) : {};
  const [modalIsOpened, setModalIsOpened] = useState(false);
  const returnPath = router.asPath.split('/compose')[0];

  const getType = () => {
    if (router.query.returnPost) {
      return 'comment';
    }
    if (router.asPath.includes('talk')) {
      return 'argument';
    }
    if (router.asPath.includes('statements')) {
      return 'statement';
    }

    return 'testimonial';
  };

  useEffect(() => {
    if (withPosition) {
      getPosition(position);
    }
  }, [position]);

  useEffect(() => {
    if (isUndefined(token) || !user.enabled) {
      setModalIsOpened(true);
    }
  }, []);

  return (
    <form method="post" className={styles.container} onSubmit={handleSubmit}>
      <header>
        <Link href={returnHref} as={returnPath}>
          <a>
            <Icon width="25" height="25" icon={arrowLeft} color="white" className="clickable" />
          </a>
        </Link>
        <SubmitButton label={CTA} submitting={submitting} loading={loading} />
      </header>

      {error && (
        <div className="error-message">
          <div className="error-message__title">Merci de réessayer</div>
          <p>{error}</p>
        </div>
      )}

      {children}

      {withPosition && (
        <div className={styles.position}>
          <div className={styles.position__label}>Position</div>
          <PositionButtons labels={labels} getPosition={newPosition => setPosition(newPosition)} />
        </div>
      )}
      {withSource && (
        <Field
          name="source"
          type="text"
          component={RenderTextField}
          normalizeOnBlur={value => value && value.trim()}
          label={`Source ${sourceIsRequired ? '' : '(optionnel)'}`}
          normalize={trim}
        />
      )}

      {withTitle && <Field name="title" type="text" component={RenderTextField} label="Titre (optionnel)" />}

      {withName && <Field name="Name" type="text" component={RenderTextField} label="Nom de la personnalité" />}

      <div className={styles.separator}></div>
      <Field name="content" type="text" component={RenderTextAreaFieldMobile} label={placeHolderText} />

      {modalIsOpened && (
        <NotConnectedModal
          type={getType()}
          accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
          onClick={() => null}
          onClose={() => {
            router.push(returnHref, returnPath);
          }}
        />
      )}
    </form>
  );
};

MobileWriteMessage.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  CTA: PropTypes.string.isRequired,
  labels: PropTypes.object,
  withPosition: PropTypes.bool,
  withSource: PropTypes.bool,
  withTitle: PropTypes.bool,
  withName: PropTypes.bool,
  placeHolderText: PropTypes.string,
  getPosition: PropTypes.func,
  loading: PropTypes.bool,
  error: PropTypes.string,
  returnHref: PropTypes.string,
  sourceIsRequired: PropTypes.bool,
  children: PropTypes.object,
};

export default reduxForm({
  form: 'MobileWriteMessage',
})(MobileWriteMessage);
