/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable operator-linebreak */
/* eslint-disable object-curly-newline */
/* eslint-disable no-unused-vars */
import { isViewerParticipant } from '@Utils/debate.helper';
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import DislikeIcon from '../../../public/static/icons/dislike.svg';
import LikeIcon from '../../../public/static/icons/like.svg';
import { updateStatementsUserLikes, updateUserLikes } from '../../actions/user.actions';
import { addStatementLikeDislike } from '../../services/celebrity.service';
import { addLikeDislike, deleteLikeDislike } from '../../services/debate.service';
import { kFormatter, useDidUpdateEffect } from '../../utils/general.helper';
import styles from './like-dislike.module.scss';

const getCount = (number, data) => {
  if (!isNull(number) && number >= 0) {
    return number;
  }
  return data;
};

const LikeDislike = ({ data, sendModalInfo, initialIsLike, isStatement, debate }) => {
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const [isLike, setIsLike] = useState(initialIsLike);
  const [defaultIsLike, setDefaultIsLike] = useState(initialIsLike);
  const dispatch = useDispatch();

  const likesWithoutUser = defaultIsLike.status === true ? data.likes - 1 : data.likes;
  const dislikesWithoutUser = defaultIsLike.status === false ? data.dislikes - 1 : data.dislikes;

  const [like, setLike] = useState({
    status: isLike.status !== null ? isLike.status : false,
    count: getCount(initialIsLike.likeCount, isLike.status === true ? likesWithoutUser + 1 : likesWithoutUser),
  });
  const [dislike, setDislike] = useState({
    status: isLike.status !== null ? !isLike.status : false,
    count: getCount(
      initialIsLike.dislikeCount,
      isLike.status === false ? dislikesWithoutUser + 1 : dislikesWithoutUser,
    ),
  });

  const handleClickLike = e => {
    e.preventDefault();
    e.stopPropagation();

    if (isViewerParticipant(user, debate)) {
      return null;
    }

    if (isEmpty(token) || !user.enabled) {
      sendModalInfo({ isOpened: true, type: 'like' });
    } else {
      setLike({ ...like, status: !like.status, count: !like.status ? likesWithoutUser + 1 : likesWithoutUser });
    }
  };

  const handleClickDislike = e => {
    e.stopPropagation();
    e.preventDefault();

    if (isViewerParticipant(user, debate)) {
      return null;
    }

    if (isEmpty(token) || !user.enabled) {
      sendModalInfo({ isOpened: true, type: 'dislike' });
    } else {
      setDislike({
        ...dislike,
        status: !dislike.status,
        count: !dislike.status ? dislikesWithoutUser + 1 : dislikesWithoutUser,
      });
    }
  };

  useDidUpdateEffect(() => {
    if (like.status) {
      sendLikeDislike(true, like.count, dislike.status ? dislike.count - 1 : dislike.count);
      if (dislike.status) {
        setDislike({ ...dislike, status: false, count: dislikesWithoutUser });
      }
    } else if (!dislike.status) {
      removeLikeDislike();
    }
  }, [like]);

  useDidUpdateEffect(() => {
    if (dislike.status) {
      const result = sendLikeDislike(false, like.status ? like.count - 1 : like.count, dislike.count);
      if (like.status) {
        setLike({ ...like, status: false, count: likesWithoutUser });
      }
    } else if (!like.status) {
      removeLikeDislike();
    }
  }, [dislike]);

  const sendLikeDislike = async (newIsLike, likeCount, dislikeCount) => {
    try {
      let result;
      if (isStatement) {
        result = await addStatementLikeDislike(
          {
            isLike: newIsLike,
            userId: user.id,
            statementId: data.id,
          },
          token,
        );
        dispatch(updateStatementsUserLikes(data.id, newIsLike, result.data.id, likeCount, dislikeCount));
      } else {
        result = await addLikeDislike(
          {
            isLike: newIsLike,
            userId: user.id,
            postId: data.id,
          },
          token,
        );
        dispatch(updateUserLikes(data.debate.id, data.id, newIsLike, result.data.id, likeCount, dislikeCount));
      }
      setIsLike({ status: result.data.isLike, id: result.data.id });
    } catch (e) {
      console.log(e);
    }
  };

  const removeLikeDislike = () => {
    try {
      deleteLikeDislike(isLike.id, token);
      setIsLike({ status: null, id: null });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Fragment>
      <button type="button" onClick={handleClickLike} className={like.status ? styles['like-active'] : ''}>
        <LikeIcon /> {like.count > 0 && kFormatter(like.count)}
      </button>
      <button type="button" onClick={handleClickDislike} className={dislike.status ? styles['like-active'] : ''}>
        <DislikeIcon /> {dislike.count > 0 && kFormatter(dislike.count)}
      </button>
    </Fragment>
  );
};

LikeDislike.propTypes = {
  data: PropTypes.object,
  sendModalInfo: PropTypes.func,
  initialIsLike: PropTypes.object,
  isStatement: PropTypes.bool,
  debate: PropTypes.object,
};

export default LikeDislike;
