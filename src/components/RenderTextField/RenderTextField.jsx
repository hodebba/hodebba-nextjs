/* eslint-disable object-curly-newline */
/* eslint-disable operator-linebreak */
/* eslint-disable jsx-a11y/label-has-for */
import checkCircle from '@iconify/icons-vaadin/check-circle';
import exclamationCircle from '@iconify/icons-vaadin/exclamation-circle';
import { Icon } from '@iconify/react';
import { PropTypes } from 'prop-types';
import { useState } from 'react';

import IconNoVisible from '../../../public/static/icons/novisibility.svg';
import IconVisible from '../../../public/static/icons/visibility.svg';

const RenderTextField = ({
  input,
  label,
  name,
  type,
  checkEnabled,
  isPassword,
  withIcon,
  icon,
  meta: { touched, error, warning },
}) => {
  let successClassName = '';
  if (error === undefined && touched && checkEnabled) {
    successClassName = 'success';
  }

  const [isTypePassword, setIsTypePassword] = useState(true);
  const SvgIcon = withIcon ? icon : undefined;

  return (
    <div>
      <div className={error && touched ? 'form__group error' : `form__group ${successClassName}`}>
        <input
          {...input}
          className={withIcon ? 'form__field paddingLeft35' : 'form__field'}
          id={name}
          placeholder={label}
          type={isTypePassword ? type : 'text'}
        />
        <div className="form__prefix">{withIcon && <SvgIcon />}</div>
        <label className="form__label" htmlFor={name}>
          {label}
        </label>
        {isPassword && (
          <div className="form__password-visible">
            {isTypePassword && <IconVisible onClick={() => setIsTypePassword(false)} />}
            {!isTypePassword && <IconNoVisible onClick={() => setIsTypePassword(true)} />}
          </div>
        )}

        <div className="form__suffix">
          {error && touched && <Icon icon={exclamationCircle} width="18px" height="18px" color="#f16e6e" />}
          {!error && touched && checkEnabled && <Icon icon={checkCircle} width="18px" height="18px" color="#79e1a3" />}
        </div>
      </div>
      <div>
        {touched &&
          ((error && <span className="form__field--error">{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  );
};

RenderTextField.propTypes = {
  input: PropTypes.any,
  label: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  checkEnabled: PropTypes.bool,
  isPassword: PropTypes.bool,
  meta: PropTypes.object,
  withIcon: PropTypes.bool,
  icon: PropTypes.any,
};

export default RenderTextField;
