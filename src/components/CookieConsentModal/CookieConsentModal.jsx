import Link from 'next/link';
import CookieConsent from 'react-cookie-consent';

const CookieConsentModal = () => {
  return (
    <CookieConsent
      location="bottom"
      buttonText="Accepter"
      cookieName="cookie-rgpd"
      style={{ background: '#1f1f4b', alignItems: 'center', padding: '0 3%' }}
      buttonWrapperClasses="cookie-consent-wrapper-button"
      buttonStyle={{
        background: '#4c92e3',
        color: 'white',
        fontSize: '16px',
        padding: '10px 30px',
        borderRadius: '3px',
        width: '100%',
      }}
      expires={365}
    >
      Nous utilisons des cookies afin de vous proposez la meilleure expérience possible. En continuant à utiliser ce
      site, vous acceptez cette politique. Pour en savoir plus sur la gestion des cookies, vous pouvez consulter nos{' '}
      <Link href="/conditions-generales">conditions générales d'utilisation </Link>.
    </CookieConsent>
  );
};

export default CookieConsentModal;
