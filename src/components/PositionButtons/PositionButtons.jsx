/* eslint-disable react-hooks/exhaustive-deps */
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import styles from './position-buttons.module.scss';

const PositionButtons = ({ labels, getPosition, defaultPosition }) => {
  const [position, setPosition] = useState(defaultPosition || 'pro');

  useEffect(() => {
    getPosition(position);
  }, [position]);

  return (
    <div className={styles.container}>
      <button
        onClick={() => setPosition('pro')}
        className={`${styles.pro} ${position === 'pro' && styles.active}`}
        type="button"
      >
        {labels.pro}
      </button>
      {labels.neutral && (
        <button
          onClick={() => setPosition('neutral')}
          className={`${styles.neutral} ${position === 'neutral' && styles.active}`}
          type="button"
        >
          {labels.neutral}
        </button>
      )}
      <button
        onClick={() => setPosition('con')}
        className={`${styles.con} ${position === 'con' && styles.active}`}
        type="button"
      >
        {labels.con}
      </button>
    </div>
  );
};

PositionButtons.propTypes = {
  labels: PropTypes.object.isRequired,
  getPosition: PropTypes.func.isRequired,
  defaultPosition: PropTypes.string,
};

export default PositionButtons;
