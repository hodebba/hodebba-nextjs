/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable implicit-arrow-linebreak */

import closeSmall from '@iconify/icons-vaadin/close-small';
import { Icon } from '@iconify/react';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Field, reduxForm } from 'redux-form';

import { required, validURL } from '../../utils/check-fields.helper';
import ClientOnlyPortal from '../ClientOnlyPortal';
import PositionButtons from '../PositionButtons';
import RenderTextAreaField from '../RenderTextAreaField';
import RenderTextField from '../RenderTextField';
import SubmitButton from '../SubmitButton';
import styles from './desktop-write-message.module.scss';

const DesktopWriteMessage = ({
  submitting,
  handleSubmit,
  CTA,
  labels,
  withTitle,
  contentNotRequired,
  sourceRequired,
  title,
  withSource,
  withPosition,
  withName,
  placeHolderText,
  onClose,
  defaultPosition,
  getPosition,
  children,
  loading,
  errorServer,
  error,
}) => {
  const [position, setPosition] = useState(defaultPosition);
  const validateContent = contentNotRequired ? [] : [required];
  const validateSource = sourceRequired ? [required, validURL] : [validURL];

  useEffect(() => {
    if (withPosition) {
      getPosition(position);
    }
  }, [position]);

  return (
    <ClientOnlyPortal selector="#app">
      <form method="post" className={styles.container} onSubmit={handleSubmit}>
        <div className={styles['close-button']}>
          <Icon icon={closeSmall} width="25" height="25" color="white" onClick={onClose} />
        </div>
        <div className={styles.title}>{title}</div>
        {children}

        {withPosition && (
          <div className={styles.position}>
            <div className={styles.position__label}>Position</div>
            <PositionButtons
              defaultPosition={defaultPosition}
              labels={labels}
              getPosition={newPosition => setPosition(newPosition)}
            />
          </div>
        )}

        {withSource && (
          <Field
            name="source"
            type="text"
            component={RenderTextField}
            label={`Source ${sourceRequired ? '' : '(optionnel)'}`}
            validate={validateSource}
          />
        )}

        {withTitle && <Field name="title" type="text" component={RenderTextField} label="Titre (optionnel)" />}

        {withName && <Field name="Name" type="text" component={RenderTextField} label="Nom de la personnalité" />}

        <Field
          name="content"
          type="text"
          component={RenderTextAreaField}
          label={placeHolderText}
          validate={validateContent}
        />

        {(!isEmpty(errorServer) || error) && (
          <div className="error-message">
            <div className="error-message__title">Merci de réessayer</div>
            <p>
              {error !== '' ? error : 'Une erreur innatendue est survenue. Merci de contacter le support technique'}
            </p>
          </div>
        )}

        <SubmitButton label={CTA} submitting={submitting} loading={loading} />
      </form>
      <div className="background-less-opacity"></div>
    </ClientOnlyPortal>
  );
};

DesktopWriteMessage.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.string,
  errorServer: PropTypes.object,
  CTA: PropTypes.string.isRequired,
  labels: PropTypes.object,
  withPosition: PropTypes.bool,
  withSource: PropTypes.bool,
  sourceRequired: PropTypes.bool,
  withTitle: PropTypes.bool,
  contentNotRequired: PropTypes.bool,
  title: PropTypes.string,
  withName: PropTypes.bool,
  placeHolderText: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  defaultPosition: PropTypes.string,
  getPosition: PropTypes.func,
  children: PropTypes.object,
};

export default reduxForm({
  form: 'DesktopWriteMessage',
})(DesktopWriteMessage);
