/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import nextUserRouterMock from '../../../test/nextUserRouterMock';
import RouterMock from '../../../test/RouterMock';
import { optionsProfile } from '../../containers/User/helpers';
import { mockStore } from '../../store';
import MobileMenu from './index';

const data = {
  title: 'test',
  options: optionsProfile,
};

const getMountComponent = () => {
  return mount(
    <Provider store={mockStore}>
      <RouterMock path="/my-account/settings">
        <MobileMenu title={data.title} options={data.options} />
      </RouterMock>
    </Provider>,
  );
};

describe('MobileMenu Component', () => {
  it('should render without crashing', () => {
    const wrapper = getMountComponent();
  });
});
