/* eslint-disable prefer-destructuring */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import arrowLeft from '@iconify/icons-vaadin/arrow-left';
import chevronRightSmall from '@iconify/icons-vaadin/chevron-right-small';
import { Icon } from '@iconify/react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';

import styles from './mobile-menu.module.scss';

// eslint-disable-next-line object-curly-newline
const MobileMenu = ({ title, options, children, previousPage }) => {
  const returnPath = useSelector(state => state.user.local.returnPath);
  const router = useRouter();
  const currentPath = router.pathname;

  const getPreviousPath = useCallback(() => {
    const lastPage = previousPage || '/';
    if (currentPath && currentPath.split('/').length === 3 && returnPath !== null) {
      return returnPath;
    }
    return { href: lastPage, as: lastPage };
  }, [previousPage, currentPath, returnPath]);

  useEffect(() => {
    if (options) {
      options.map(element => router.prefetch(element.link));
    }
    router.prefetch(getPreviousPath());
  }, [getPreviousPath, options, router]);

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div
          role="button"
          className={styles['header__back-button']}
          onClick={() => router.push(getPreviousPath().href, getPreviousPath().as)}
        >
          <Icon width="25" height="25" icon={arrowLeft} className="clickable" />
        </div>
        <h1>{title}</h1>
      </div>

      {options && (
        <nav className={styles.options}>
          <ul>
            {options.map(element => (
              <li key={element.label} className={styles.option}>
                <div role="button" onClick={() => router.push({ pathname: element.link })}>
                  <span className={styles.option__item}>
                    {element.icon} {element.label}
                  </span>
                  <Icon width="18" height="18" icon={chevronRightSmall} />
                </div>
              </li>
            ))}
          </ul>
        </nav>
      )}

      {children}
    </div>
  );
};

MobileMenu.propTypes = {
  title: PropTypes.string.isRequired,
  options: PropTypes.array,
  children: PropTypes.any,
  previousPage: PropTypes.string,
};
export default MobileMenu;
