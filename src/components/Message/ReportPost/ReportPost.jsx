/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { reportPost, resetReportPost } from '../../../actions/customer.action';
import SubmitButton from '../../SubmitButton';
import styles from './report-post.module.scss';

const reasons = {
  a:
    'Propos incitant à la haine envers une catégorie protégée sur la base d’une race, d’une religion, d’un sexe, d’une orientation sexuelle ou d’un handicap.',
  b: 'Propos choquants, vulgaires, ou irrespectueux.',
  c: 'Propos innappropriés ou spam.',
  d: 'L’utilisateur menace d’user de la violence ou de blesser quelqu’un.',
  e: 'L’utilisateur propage volontairement des fausses informations dans le but d’influencer une élection.',
};

const ReportPost = ({ post, onSuccess }) => {
  const [reasonSelected, setReasonSelected] = useState(false);
  const [reason, setReason] = useState('');
  const loading = useSelector(state => state.customer.local.loading.reportPost);
  const user = useSelector(state => state.user.local.user);
  const reportPostData = useSelector(state => state.customer.data.reportPost);
  const list = useRef(null);
  const dispatch = useDispatch();

  const preventDefault = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  useEffect(() => {
    if (!isEmpty(reportPostData)) {
      toast.success(reportPostData.message);
      onSuccess();
      dispatch(resetReportPost());
    }
  }, [reportPostData]);

  const handleSubmit = e => {
    preventDefault(e);
    const data = {
      postId: post.id,
      postContent: post.content,
      reason,
      userEmail: user.email || null,
    };
    dispatch(reportPost(data));
  };

  const handleClickReason = e => {
    preventDefault(e);
    const oldReason = list.current.getElementsByClassName(styles.active)[0];
    if (oldReason) {
      oldReason.classList.remove(styles.active);
    }
    setReason(reasons[e.target.id]);
    e.target.classList.add(styles.active);
    setReasonSelected(true);
  };

  const renderReasons = () => {
    const ret = [];
    Object.keys(reasons).forEach(key => {
      ret.push(
        <li id={key} key={key} onClick={handleClickReason}>
          {reasons[key]}
        </li>,
      );
    });
    return ret;
  };

  return (
    <div className={styles.container}>
      <div className={styles.title}>Signaler un incident</div>
      <p>Aidez-nous à comprendre, pourquoi ce post pose problème ?</p>
      <ul ref={list}>{renderReasons()}</ul>

      <SubmitButton label="Envoyer" disabled={!reasonSelected} loading={loading} onClick={handleSubmit} />
    </div>
  );
};

ReportPost.propTypes = {
  post: PropTypes.object,
  onSuccess: PropTypes.func,
};

export default ReportPost;
