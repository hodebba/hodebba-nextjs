/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable import/no-cycle */
/* eslint-disable operator-linebreak */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
import { deletePost } from '@Actions/debate.actions';
import commentsO from '@iconify/icons-vaadin/comments-o';
import { Icon } from '@iconify/react';
import { getDebateRootPath, isAdminParticipant } from '@Utils/debate.helper';
import { Image, Transformation } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import isNumber from 'lodash/isNumber';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useRef, useState } from 'react';
import ClampLines from 'react-clamp-lines';
import { useDispatch, useSelector } from 'react-redux';
import { withResizeDetector } from 'react-resize-detector';
import { toast } from 'react-toastify';

import LinkIcon from '../../../public/static/icons/external-link.svg';
import MoreIcon from '../../../public/static/icons/more.svg';
import Comments from '../../containers/Debate/Talk/Comments';
import { checkUserLikes, getLabel } from '../../utils/comment.helper';
import { kFormatter, useOutsideAlerter, useWindowSize } from '../../utils/general.helper';
import { checkUserLikes as checkStatementUserLikes } from '../../utils/statement.helper';
import InfoModal from '../InfoModal';
import LikeDislike from '../LikeDislike';
import NotConnectedModal from '../NotConnectedModal';
import styles from './message.module.scss';
import ReportPost from './ReportPost/ReportPost';

const Message = ({
  isDesktop,
  className,
  displaySource,
  maxLine,
  data,
  isArgument,
  debate,
  displayComments,
  canDisplayComments,
  isTestimonial,
  isStatement,
}) => {
  const [isMoreOpened, setIsMoreOpened] = useState(false);
  const { user, token } = useSelector(state => state.user.local);
  const [modal, setModal] = useState({ isOpened: false, type: '' });
  const more = useRef(null);
  const messageRef = useRef(null);
  useOutsideAlerter(more, () => setIsMoreOpened(false));
  const [showComments, setShowComments] = useState(displayComments);
  const typeLabel = getLabel(data.postType || data.statementType, isArgument);
  const router = useRouter();
  const statementUserLikes = useSelector(state => state.user.data.statementUserLikes);
  const isLoading = useSelector(state => state.user.local.isLoading);
  const commentUserLikes = useSelector(state => state.user.data.userLikes);
  const postsDeleted = useSelector(state => state.debate.data.deletePost);
  const dispatch = useDispatch();
  const size = useWindowSize();

  const [reportPostIsOpened, setReportPostIsOpened] = useState(false);
  const [modalDeleteIsOpened, setmodalDeleteIsOpened] = useState(false);

  let initialIsLike;
  if (isStatement) {
    initialIsLike = checkStatementUserLikes(data, statementUserLikes);
  } else {
    initialIsLike = checkUserLikes(data, commentUserLikes[debate.id]);
  }

  useEffect(() => {
    if (size.width <= 650) {
      if (router.asPath.includes('argument')) {
        router.prefetch(
          `${getDebateRootPath(debate)}/[id]/talk/argument/[comment_id]/compose`,
          `${getDebateRootPath(debate)}/${router.query.id}/talk/argument/${data.id}/compose?returnPost=${
            router.query.comment_id
          }`,
        );
      } else {
        router.prefetch(
          `${getDebateRootPath(debate)}/[id]/testimonial/[testimonial_id]/compose`,
          `${getDebateRootPath(debate)}/${router.query.id}/testimonial/${data.id}/compose?returnPost=${
            router.query.testimonial_id
          }`,
        );
      }
    }
  }, []);

  useEffect(() => {
    if (postsDeleted[data.id]) {
      toast.success('Post supprimé avec succès !');
      setmodalDeleteIsOpened(false);

      if (isArgument) {
        router.push(
          `${getDebateRootPath(debate)}/id/talk?type=community`,
          `${getDebateRootPath(debate)}/${router.query.id}/talk?type=community`,
          { shallow: true },
        );
      }
      messageRef.current.style.display = 'none';
    }
  }, [postsDeleted]);

  const preventPropagation = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  const handleClickComments = e => {
    if (canDisplayComments) {
      preventPropagation(e);
      if (size.width <= 650) {
        if (router.asPath.includes('argument')) {
          router.push(
            `${getDebateRootPath(debate)}/talk/argument/[comment_id]/compose?returnPost=${router.query.comment_id}`,
            `${getDebateRootPath(debate)}/${router.query.id}/talk/argument/${data.id}/compose?returnPost=${
              router.query.comment_id
            }`,
          );
        } else {
          router.push(
            `${getDebateRootPath(debate)}/testimonial/[testimonial_id]/compose?returnPost=${router.query.comment_id}`,
            `${getDebateRootPath(debate)}/${router.query.id}/testimonial/${data.id}/compose?returnPost=${
              router.query.testimonial_id
            }`,
          );
        }
      } else {
        setShowComments(true);
      }
    }
  };

  return (
    <Fragment>
      <div
        className={`${styles.container} message ${styles[className]}`}
        role="button"
        ref={messageRef}
        onClick={() => setShowComments(true)}
      >
        <div
          onClick={() =>
            isStatement && router.push('/celebrities/[id]/positions', `/celebrities/${data.celebrity.id}/positions`)
          }
          className={`${styles.picture} ${styles[data.postType || data.statementType]}`}
        >
          <Image
            publicId={isStatement ? data.celebrity.imageLink : data.user.imageLink}
            alt="photo de profil de l'auteur"
            secure="true"
          >
            <Transformation crop="fill" gravity="faces" />
          </Image>
        </div>
        <div className={styles.right}>
          <div className={`${styles.top} ${styles[data.postType || data.statementType]}`}>
            <div className={styles.top__text}>
              <p className={styles.top__pseudo}>
                {isStatement ? (
                  <Link href="/celebrities/[id]/positions" as={`/celebrities/${data.celebrity.id}/positions`}>
                    <a>{data.celebrity.fullName}</a>
                  </Link>
                ) : (
                  data.user.username
                )}
              </p>{' '}
              {typeLabel && '-'}
              <span className={styles.top__type}>{typeLabel}</span>
            </div>

            {(!debate.isPrivate || isAdminParticipant(user, debate)) && (
              <button
                onClick={e => {
                  preventPropagation(e);
                  setIsMoreOpened(true);
                }}
                type="button"
              >
                <MoreIcon />
              </button>
            )}

            {isMoreOpened && (
              <div ref={more} className={styles['more-menu']}>
                {!debate.isPrivate && (
                  <button
                    type="button"
                    onClick={e => {
                      preventPropagation(e);
                      setReportPostIsOpened(true);
                    }}
                  >
                    Signaler
                  </button>
                )}
                {isAdminParticipant(user, debate) && (
                  <button
                    type="button"
                    onClick={e => {
                      preventPropagation(e);
                      setmodalDeleteIsOpened(true);
                    }}
                  >
                    Supprimer
                  </button>
                )}
              </div>
            )}
            {isMoreOpened && <div className="transparent-background"></div>}
          </div>
          <div className={styles.argument}>
            {data.title && <div className={styles.argument__title}>{data.title}</div>}

            <ClampLines
              text={data.content}
              lines={maxLine}
              ellipsis="..."
              moreText="Voir plus"
              lessText="Voir moins"
              className="see-more"
              innerElement="p"
              stopPropagation
            />
          </div>

          {displaySource && data.sources && data.sources.length > 0 && (
            <div className={styles.source}>
              <LinkIcon />
              <a href={data.sources[0].link} target="_blank" rel="noopener">
                {data.sources[0].link}
              </a>
            </div>
          )}

          <div className={styles.bottom}>
            <div className={styles.bottom__interactions}>
              {!data.isLastLevel && !isStatement && (
                <button type="button" onClick={handleClickComments}>
                  <Icon icon={commentsO} width={23} />
                  {!isNumber(data.comments) &&
                    data.comments &&
                    data.comments.length !== undefined &&
                    data.comments.length !== 0 &&
                    kFormatter(data.comments.length)}
                  {isNumber(data.comments) && data.comments > 0 && kFormatter(data.comments)}
                </button>
              )}
              {!isLoading && (
                <LikeDislike
                  data={data}
                  debate={debate}
                  isStatement={isStatement}
                  initialIsLike={initialIsLike}
                  sendModalInfo={info => setModal(info)}
                />
              )}
            </div>
            {!displaySource && typeLabel && data.sources[0] && (
              <button
                type="button"
                className="text-button"
                onClick={e => {
                  preventPropagation(e);
                  window.open(data.sources[0].link, '_blank');
                }}
              >
                Source
              </button>
            )}
          </div>
        </div>

        {isDesktop && (
          <div className={`${styles.hanging} ${styles[data.postType || data.statementType]}`}>
            <div className={styles.hanging__circle}></div>
            <div className={styles.hanging__line}></div>
          </div>
        )}

        {(className === 'comment' || className === 'comment-last-level') && (
          <div className={styles['hanging-comment']}>
            <div></div>
          </div>
        )}
      </div>
      {canDisplayComments && showComments && (
        <Comments commentList={data.comments} parentId={data.id} debate={debate} isTestimonial={isTestimonial} />
      )}

      {modal.isOpened && (
        <NotConnectedModal
          type={modal.type}
          accountNotConfirmed={!isEmpty(user) ? !user.enabled : undefined}
          onClick={e => {
            preventPropagation(e);
          }}
          onClose={e => {
            preventPropagation(e);
            setModal({ isOpened: false, type: '' });
          }}
        />
      )}

      {reportPostIsOpened && (
        <InfoModal
          onClose={e => {
            preventPropagation(e);
            setReportPostIsOpened(false);
          }}
        >
          <ReportPost post={data} onSuccess={() => setReportPostIsOpened(false)} />
        </InfoModal>
      )}

      {modalDeleteIsOpened && (
        <InfoModal
          onClose={e => {
            preventPropagation(e);
            setmodalDeleteIsOpened(false);
          }}
        >
          <div className={styles['delete-modal']}>
            <p>Êtes-vous sûr de vouloir supprimer définitivement ce post ?</p>
            <button
              type="button"
              className="cancel-button"
              onClick={e => {
                preventPropagation(e);
                dispatch(deletePost(data.id, token));
              }}
            >
              Oui, je veux supprimer ce post
            </button>
            <button
              type="button"
              className="secondary-button"
              onClick={e => {
                preventPropagation(e);
                setmodalDeleteIsOpened(false);
              }}
            >
              Annuler
            </button>
          </div>
        </InfoModal>
      )}
    </Fragment>
  );
};

Message.defaultProps = {
  maxLine: 7,
};

Message.propTypes = {
  isDesktop: PropTypes.bool,
  className: PropTypes.string,
  displaySource: PropTypes.bool,
  maxLine: PropTypes.number,
  data: PropTypes.object.isRequired,
  debate: PropTypes.object,
  displayComments: PropTypes.bool,
  canDisplayComments: PropTypes.bool,
  isArgument: PropTypes.bool,
  isTestimonial: PropTypes.bool,
  isStatement: PropTypes.bool,
};

export default withResizeDetector(Message);
