/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import closeSmall from '@iconify/icons-vaadin/close-small';
import { Icon } from '@iconify/react';
import PropTypes from 'prop-types';
import { useEffect } from 'react';

import ClientOnlyPortal from '../ClientOnlyPortal';
import styles from './info-modal.module.scss';

const InfoModal = ({ onClose, children }) => {
  useEffect(() => {
    const app = document.getElementById('app');
    const top = -window.scrollY;
    app.style.top = `${top}px`;
    app.classList.add('no-scroll-desktop');

    return () => {
      app.classList.remove('no-scroll-desktop');
      app.style.top = '';
      window.scrollTo(0, parseInt(top || '0', 10) * -1);
    };
  }, []);

  return (
    <ClientOnlyPortal selector="#__next">
      <div className={styles.container}>
        <div className={styles['close-button-container']}>
          <button type="button" className={styles['close-button']} onClick={onClose}>
            <Icon icon={closeSmall} height="28" width="28" />
          </button>
        </div>
        {children}
      </div>
      <div className="background-less-opacity"></div>
    </ClientOnlyPortal>
  );
};

InfoModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  children: PropTypes.any,
};
export default InfoModal;
