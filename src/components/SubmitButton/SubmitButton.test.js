/* eslint-disable no-unused-vars */
import { shallow } from 'enzyme';

import SubmitButton from './index';

describe('SubmitButton Component', () => {
  it('should render without crashing ', () => {
    const wrapper = shallow(<SubmitButton label="test" />);
  });

  it('should not display svg but the label', () => {
    const wrapper = shallow(<SubmitButton label="test" loading={false} />);
    expect(wrapper.find('svg').exists()).toEqual(false);
    expect(wrapper.find('span').exists()).toEqual(true);
  });

  it('should display svg but not the label', () => {
    const wrapper = shallow(<SubmitButton label="test" loading />);
    expect(wrapper.find('svg').exists()).toEqual(true);
    expect(wrapper.find('span').exists()).toEqual(false);
  });
});
