/* eslint-disable object-curly-newline */
import PropTypes from 'prop-types';
import React from 'react';

import styles from './submit-button.module.scss';

const SubmitButton = React.memo(({ label, submitting, form, loading, onClick, disabled }) => {
  return (
    <button form={form} type="submit" className={styles.container} disabled={submitting || disabled} onClick={onClick}>
      {loading ? (
        <svg viewBox="0 0 16 16">
          <circle strokeOpacity=".1" cx="8" cy="8" r="6"></circle>
          <circle className="load" cx="8" cy="8" r="6"></circle>
        </svg>
      ) : (
        <span>{label}</span>
      )}
    </button>
  );
});

SubmitButton.propTypes = {
  label: PropTypes.string.isRequired,
  submitting: PropTypes.bool,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  onClick: PropTypes.func,
  form: PropTypes.string,
};

export default SubmitButton;
