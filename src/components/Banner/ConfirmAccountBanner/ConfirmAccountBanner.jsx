/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import isEmpty from 'lodash/isEmpty';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { resendConfirmEmail } from '../../../actions/authentification.actions';
import InfoModal from '../../InfoModal';
import Banner from '../Banner';

const ConfirmAccountBanner = () => {
  const user = useSelector(state => state.user.local.user);
  const token = useSelector(state => state.user.local.token);
  const dispatch = useDispatch();
  const resendConfirmEmailResult = useSelector(state => state.authentification.data.resendConfirmEmail);
  const resendConfirmEmailError = useSelector(state => state.authentification.local.errors.resendConfirmEmail);

  const [displayErrorModal, setDisplayErrorModal] = useState(true);

  useEffect(() => {
    if (isEmpty(resendConfirmEmailResult)) {
      setDisplayErrorModal(true);
    }
  }, [resendConfirmEmailResult]);

  return (
    <Banner>
      <p>Votre compte n'est pas activé, merci de cliquer sur le lien qui vous a été envoyé par email.</p>
      <p>
        Vous n'avez rien reçu ?{' '}
        <span role="button" onClick={() => dispatch(resendConfirmEmail(user.id, token))} className="underlined">
          renvoyer cet email
        </span>
        .
      </p>
      {!isEmpty(resendConfirmEmailResult) && displayErrorModal && (
        <InfoModal onClose={() => setDisplayErrorModal(false)}>
          <p>
            Un email de confirmation a été envoyé à votre adresse email. Merci de cliquer sur lien se trouvant dans cet
            email pour activer votre compte.
          </p>
        </InfoModal>
      )}
      {!isEmpty(resendConfirmEmailError) && displayErrorModal && (
        <InfoModal onClose={() => setDisplayErrorModal(false)}>
          <h2 style={{ fontSize: '1.8rem' }}>{resendConfirmEmailError}</h2>
        </InfoModal>
      )}
    </Banner>
  );
};

export default ConfirmAccountBanner;
