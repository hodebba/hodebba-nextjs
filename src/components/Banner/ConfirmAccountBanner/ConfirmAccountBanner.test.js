/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import { mockStore } from '../../../store';
import ConfirmAccountBanner from '../index';

describe('ConfirmAccountBanner Component', () => {
  it('should render without crashing', () => {
    const wrapper = mount(
      <Provider store={mockStore}>
        <ConfirmAccountBanner />
      </Provider>,
    );
  });
});
