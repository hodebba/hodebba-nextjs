import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';

import styles from './banner.module.scss';

const Banner = ({ children }) => {
  return (
    <Fade>
      <div className={styles.container}>{children}</div>
    </Fade>
  );
};

Banner.propTypes = {
  children: PropTypes.any.isRequired,
};

export default Banner;
