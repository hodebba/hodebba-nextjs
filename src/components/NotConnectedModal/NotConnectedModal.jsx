/* eslint-disable react/no-unescaped-entities */
/* eslint-disable object-curly-newline */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import CommentIcon from '../../../public/static/icons/comment.svg';
import ArgumentIcon from '../../../public/static/icons/create.svg';
import StatementIcon from '../../../public/static/icons/debate.svg';
import DislikeIcon from '../../../public/static/icons/dislike.svg';
import LikeIcon from '../../../public/static/icons/like.svg';
import TestimonialIcon from '../../../public/static/icons/testimonial.svg';
import { openModalAuthentification } from '../../actions/authentification.actions';
import InfoModal from '../InfoModal';
import styles from './not-connected-modal.module.scss';

const data = {
  like: {
    title: 'Aimez un commentaire pour montrer votre approbation.',
    p: "Connectez-vous pour ajouter une mention j'aime.",
    icon: <LikeIcon />,
  },
  dislike: {
    title: 'Montrez votre désapprobation.',
    p: "Connectez-vous pour ajouter une mention je n'aime pas.",
    icon: <DislikeIcon />,
  },
  comment: {
    title: 'Donnez votre avis et enrichissez le débat.',
    p: 'Connectez-vous pour ajouter un commentaire.',
    icon: <CommentIcon />,
  },
  argument: {
    title: 'Donnez votre avis et enrichissez le débat.',
    p: 'Connectez-vous pour ajouter un argument.',
    icon: <ArgumentIcon />,
  },
  create: {
    title: 'Lancez un nouveau débat.',
    p: 'Connectez-vous pour créer un débat.',
    icon: <StatementIcon />,
  },
  testimonial: {
    title: 'Ajoutez un témoignage pour partager votre expérience personnelle sur le sujet.',
    p: 'Connectez-vous pour ajouter un témoignage.',
    icon: <TestimonialIcon />,
  },
  statement: {
    title: "Ajoutez la déclaration d'une personnalité public afin de faciliter la connaissance de ces opinions",
    p: 'Connectez-vous pour ajouter une déclaration.',
    icon: <StatementIcon />,
  },
};

const NotConnectedModal = ({ onClick, onClose, type, accountNotConfirmed }) => {
  const dispatch = useDispatch();

  return (
    <div onClick={e => onClick(e)} className={styles.parent}>
      <InfoModal onClose={e => onClose(e)}>
        <div className={styles.container}>
          {data[type].icon}
          <div className={styles.text}>
            <div className={styles.text__title}>{data[type].title}</div>
            <p>
              {accountNotConfirmed
                ? "Votre compte n'est pas activé, merci de cliquer sur le lien qui vous a été envoyé par email. Vous pourrez ensuite intéragir sur le site."
                : data[type].p}
            </p>
          </div>
          {!accountNotConfirmed && (
            <div className={styles.buttons}>
              <button
                type="button"
                className="primary-button"
                onClick={e => {
                  onClose(e);
                  dispatch(openModalAuthentification('signUp'));
                }}
              >
                S'inscrire
              </button>
              <button
                type="button"
                className="secondary-button"
                onClick={e => {
                  onClose(e);
                  dispatch(openModalAuthentification('signIn'));
                }}
              >
                Se connecter
              </button>
            </div>
          )}
        </div>
      </InfoModal>
    </div>
  );
};

NotConnectedModal.propTypes = {
  onClick: PropTypes.func,
  onClose: PropTypes.func,
  type: PropTypes.string,
  accountNotConfirmed: PropTypes.bool,
};

export default NotConnectedModal;
