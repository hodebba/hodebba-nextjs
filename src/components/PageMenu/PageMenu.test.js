/* eslint-disable no-unused-vars */
import { shallow } from 'enzyme';

import DebateIcon from '../../../public/static/icons/discussion.svg';
import PoliticianIcon from '../../../public/static/icons/politician.svg';
import PageMenu from './index';

const itemsMenu = [
  {
    id: 0,
    icon: <DebateIcon />,
    type: 'debate',
    href: '/admin/debate',
    label: 'Créer un débat',
  },
  {
    id: 1,
    icon: <PoliticianIcon />,
    type: 'celebrity',
    href: '/admin/celebrity',
    label: 'Ajouter une personnalité',
  },
];

const active = 'debate';

describe('PageMenu Component', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(<PageMenu active={active} className="fix" items={itemsMenu} />);
  });
});
