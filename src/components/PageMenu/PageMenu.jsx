/* eslint-disable array-callback-return */
/* eslint-disable object-curly-newline */
import chevronLeft from '@iconify/icons-vaadin/chevron-left';
import { Icon } from '@iconify/react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';

import styles from './page-menu.module.scss';

const PageMenu = ({ items, reference, active, className, id, withBackButton, returnPath }) => {
  const activeItem = useRef(null);
  const router = useRouter();

  useEffect(() => {
    if (activeItem.current) {
      activeItem.current.scrollIntoView({ behavior: 'instant', block: 'end' });
    }
  }, []);

  const handleClickBack = () => {
    if (!returnPath) {
      router.push('/');
    } else {
      router.back();
    }
  };

  const renderItems = () => {
    const elements = [];
    items.map(item => {
      let as = item.href;
      if (id) {
        as = as.replace('[id]', id);
      }
      elements.push(
        <li
          key={item.id.toString()}
          ref={active === item.type ? activeItem : null}
          className={active === item.type ? styles.active : ''}
        >
          <Link href={item.href} as={as}>
            <a>
              {item.icon}
              {item.label}
            </a>
          </Link>
        </li>,
      );
    });
    return elements;
  };

  return (
    <nav ref={reference} className={styles[className]}>
      {withBackButton && (
        <div className={styles['back-container']}>
          <button className={`text-button ${styles.back}`} type="button" onClick={handleClickBack}>
            <Icon width="18" height="18" icon={chevronLeft} />
            {!returnPath ? "Retour à l'accueil" : 'Retour'}
          </button>
        </div>
      )}
      <ul className={styles['items-list']}>{renderItems()}</ul>
    </nav>
  );
};

PageMenu.propTypes = {
  items: PropTypes.array.isRequired,
  reference: PropTypes.any,
  active: PropTypes.string,
  className: PropTypes.string.isRequired,
  id: PropTypes.string,
  withBackButton: PropTypes.bool,
  returnPath: PropTypes.string,
};

export default PageMenu;
