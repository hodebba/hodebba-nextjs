/* eslint-disable object-curly-newline */
import PropTypes from 'prop-types';
import Select from 'react-select';

const FilterDropdown = ({
  options,
  onChange,
  isSearchable,
  defaultValue,
  placeholder,
  value,
  className,
  isDisabled,
}) => {
  return (
    <div className={`filter-dropdown-container ${className}`}>
      <Select
        instanceId={options.length > 0 ? options.length : Math.floor(Math.random() * -10)}
        options={options}
        isSearchable={isSearchable}
        defaultValue={defaultValue}
        value={value}
        className="filter-dropdown"
        classNamePrefix="filter-dropdown"
        placeholder={placeholder}
        noOptionsMessage={() => 'Aucun résultat trouvé'}
        onChange={onChange}
        isDisabled={isDisabled}
      />
    </div>
  );
};

FilterDropdown.defaultProps = {
  isSearchable: false,
};

FilterDropdown.propTypes = {
  options: PropTypes.array.isRequired,
  isSearchable: PropTypes.bool,
  defaultValue: PropTypes.object,
  value: PropTypes.object,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
  isDisabled: PropTypes.bool,
};

export default FilterDropdown;
