/* eslint-disable object-curly-newline */

import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import cloudinaryFr from '../../../public/static/locales/coudinary-fr.json';
import { cloudinaryConfig } from '../../utils/cloudinary-config';
import Tooltip from '../Tooltip';
import styles from './image-uploader.module.scss';

const ImageUploader = ({ image, preset, sendImageToForm, sendErrorUpload, title, tooltipText }) => {
  const [imageHasBeenUploaded, setImageHasBeenUploaded] = useState(false);
  const [myWidget, setMyWidget] = useState(null);

  useEffect(() => {
    setMyWidget(
      // eslint-disable-next-line no-undef
      cloudinary.createUploadWidget(cloudinaryConfig(preset, cloudinaryFr), (errorUpload, result) => {
        if (result.event === 'success') {
          const info = {
            public_id: result.info.public_id,
            filename: result.info.original_filename,
            extension: result.info.format,
          };
          sendImageToForm(info);
          setImageHasBeenUploaded(true);
        }
        if (errorUpload) {
          sendErrorUpload(
            "L'envoi de votre image a échoué. Nous n'acceptons que les formats png, jpg et jpeg, et votre fichier ne doit pas dépasser 500kB. Si votre image ne respecte pas ces critères, merci de procéder à une conversion ou utilisez une autre image",
          );
        }
      }),
    );
  }, [preset, sendImageToForm, sendErrorUpload]);

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        {title}
        <Tooltip text={tooltipText} />
      </div>
      <button type="button" id={styles.upload_widget} onClick={() => myWidget.open()} className="primary-button">
        {!imageHasBeenUploaded ? 'Ajouter une image' : "Changer d'image"}
      </button>
      {imageHasBeenUploaded && !isEmpty(image) && (
        <div className={styles.filename}>
          {image.filename}.{image.extension}
        </div>
      )}
    </div>
  );
};

ImageUploader.propTypes = {
  image: PropTypes.object,
  preset: PropTypes.string.isRequired,
  sendImageToForm: PropTypes.func.isRequired,
  sendErrorUpload: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  tooltipText: PropTypes.string.isRequired,
};

export default ImageUploader;
