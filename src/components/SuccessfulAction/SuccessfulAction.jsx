/* eslint-disable object-curly-newline */
import PropTypes from 'prop-types';

import SuccessIcon from '../../../public/static/icons/success.svg';
import styles from './successful-action.module.scss';

const SuccessfulAction = ({ title, text, textButton, onClick }) => {
  return (
    <div className={styles.container}>
      <SuccessIcon />
      <div>
        <h3>{title}</h3>
        <p>{text}</p>
      </div>
      <button type="button" className="secondary-button" onClick={onClick}>
        {textButton}
      </button>
    </div>
  );
};

SuccessfulAction.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string,
  textButton: PropTypes.string,
  onClick: PropTypes.func,
};

export default SuccessfulAction;
