/* eslint-disable no-unused-vars */
import { shallow } from 'enzyme';

import DebatePreview from './index';

const fakeData = {
  id: 1,
  imageLink: 'mort',
  debateState: 'draft',
  title: 'Doit-on rétablir la peine de mort ?',
};

const fakeTheme = {
  label: 'Société',
  id: '2',
};
describe('DebatePreview Component', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(<DebatePreview debate={fakeData} theme={fakeTheme} />);
  });
});
