import { getDebateRootPath } from '@Utils/debate.helper';
import { Image, Transformation } from 'cloudinary-react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { Fragment } from 'react';

import styles from './debate-preview.module.scss';

const DebatePreview = ({ debate, theme, className }) => {
  let classNameContainer = `${styles.container}`;
  if (className) {
    const [class1, class2] = className.split(' ');
    classNameContainer += ` ${styles[class1]} ${styles[class2]}`;
  }

  if (debate.debateState === 'draft') {
    classNameContainer += ` ${styles.draft}`;
  }

  const renderContent = () => {
    return (
      <Fragment>
        <Image publicId={debate.imageLink} alt="image de fond du débat" secure="true">
          <Transformation crop="fill" gravity="faces" />
        </Image>
        <div className={styles['linear-gradient']}>
          <p>{theme.label}</p>
          <h3>{debate.title}</h3>
        </div>
      </Fragment>
    );
  };

  return (
    <div className={classNameContainer}>
      {debate.debateState === 'draft' ? (
        <Fragment>
          {renderContent()}
          <div className={styles.draft__soon}>Bientôt disponible</div>
        </Fragment>
      ) : (
        <Link
          href={`${getDebateRootPath(debate)}/[id]/presentation`}
          as={`${getDebateRootPath(debate)}/${debate.id}/presentation`}
        >
          <a>{renderContent()}</a>
        </Link>
      )}
    </div>
  );
};

DebatePreview.defaultProps = {
  className: '',
};

DebatePreview.propTypes = {
  className: PropTypes.string,
  debate: PropTypes.object.isRequired,
  theme: PropTypes.object,
};

export default DebatePreview;
