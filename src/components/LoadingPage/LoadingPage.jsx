const LoadingPage = () => {
  return (
    <div
      style={{
        backgroundColor: '#141534',
        position: 'absolute',
        textAlign: 'center',
        top: 0,
        left: 0,
        height: 'calc(100vh + 80px)',
        width: '100vw',
        zIndex: 1000,
      }}
    >
      <div
        style={{
          position: 'absolute',
          top: 'calc(50% - 50px)',
          left: '50%',
          color: '#4c92e3',
          fontSize: '20px',
          transform: 'translate(-50%, -50%)',
          letterSpacing: '3px',
          fontFamily: 'Open Sans',
          textTransform: 'uppercase',
          fontWeight: 900,
        }}
      >
        Chargement...
      </div>
    </div>
  );
};

export default LoadingPage;
