import TooltipMU from '@material-ui/core/Tooltip';
import Proptypes from 'prop-types';

import InfoIcon from '../../../public/static/icons/info.svg';
import styles from './tooltip.module.scss';

const Tooltip = ({ text }) => {
  return (
    <TooltipMU title={text} arrow placement="top-start">
      <button type="button" className={styles.button}>
        <InfoIcon />
      </button>
    </TooltipMU>
  );
};

Tooltip.propTypes = {
  text: Proptypes.string.isRequired,
};

export default Tooltip;
