import styles from './password-helper.module.scss';

const PasswordHelper = () => {
  return (
    <div className={styles.container}>
      Pour des raisons de sécurité, le mot de passe doit contenir:
      <ul>
        <li>Au moins 8 caractères</li>
        <li>Au moins 1 majuscule et 1 minuscule</li>
        <li>Au moins 1 chiffre</li>
        <li>Au moins 1 caractère spéciale (#$^+=!*()@%&)</li>
      </ul>
    </div>
  );
};

export default PasswordHelper;
