import { ToastContainer } from 'react-toastify';

import ClientOnlyPortal from '../ClientOnlyPortal';

const Notification = () => {
  return (
    <ClientOnlyPortal selector="#app">
      <ToastContainer
        position="top-right"
        autoClose={4000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </ClientOnlyPortal>
  );
};

export default Notification;
