/* eslint-disable object-curly-newline */
import { call, delay, put, takeLatest } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/elasticSearch.service';

export function* searchText(type) {
  try {
    yield delay(300);
    const result = yield call(api.searchText, type.text);
    yield put({
      type: constants.searchText.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.searchText.failure, errors: e.response.data });
  }
}

export function* searchTextWatcher() {
  yield takeLatest(constants.searchText.request, searchText);
}
