// eslint-disable-next-line object-curly-newline
import { call, cancel, fork, put, take, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/authentification.service';

export function* signUp(type) {
  try {
    const result = yield call(api.signUp, type.data);
    const token = result.headers.authorization;

    yield put({
      type: constants.signUp.success,
      data: result.data,
      token,
    });
  } catch (e) {
    yield put({ type: constants.signUp.failure, errors: e.response.data });
  }
}

export function* forgetPassword(type) {
  try {
    const result = yield call(api.forgetPassword, type.email);
    yield put({
      type: constants.forgetPassword.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.forgetPassword.failure, errors: e.response.data });
  }
}

export function* signIn(data) {
  try {
    const result = yield call(api.signIn, data);
    const token = result.headers.authorization;
    const user = result.data;
    yield put({
      type: constants.signIn.success,
      data: user,
      token,
    });
    yield put({
      type: constants.getUserVotes.request,
      id: user.id,
      token,
    });
  } catch (e) {
    yield put({ type: constants.signIn.failure, errors: e });
  }
}

export function* resendConfirmEmail(type) {
  try {
    const result = yield call(api.resendConfirmEmail, type.userId, type.token);
    yield put({
      type: constants.resendConfirmEmail.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.resendConfirmEmail.failure, errors: e.response.data.message });
  }
}

export function* continueWithFacebookGoogle(type) {
  try {
    const result = yield call(api.continueWithFacebookGoogle, type.data);
    const token = result.headers.authorization;
    yield put({
      type: constants.continueWithFacebookGoogle.success,
      data: result.data,
      token,
    });
    yield put({
      type: constants.getUserVotes.request,
      id: result.data.id,
      token,
    });
  } catch (e) {
    yield put({ type: constants.continueWithFacebookGoogle.failure, errors: e.response });
  }
}

export function* signUpWatcher() {
  yield takeEvery(constants.signUp.request, signUp);
}

export function* forgetPasswordWatcher() {
  yield takeEvery(constants.forgetPassword.request, forgetPassword);
}

export function* resendConfirmEmailWatcher() {
  yield takeEvery(constants.resendConfirmEmail.request, resendConfirmEmail);
}

export function* signInWatcher() {
  /* Login Flow */
  while (true) {
    const { data } = yield take(constants.signIn.request);
    // fork return a Task object
    const task = yield fork(signIn, data);
    const action = yield take([constants.logout, constants.signIn.failure]);
    if (action.type === constants.logout) {
      yield cancel(task);
    }
  }
}
export function* continueWithFacebookGoogleWatcher() {
  yield takeEvery(constants.continueWithFacebookGoogle.request, continueWithFacebookGoogle);
}
