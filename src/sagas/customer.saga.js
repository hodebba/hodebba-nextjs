// eslint-disable-next-line object-curly-newline
import { call, put, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/customer.service';

export function* contactUs(type) {
  try {
    const result = yield call(api.contactUs, type.data);
    yield put({
      type: constants.contactUs.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.contactUs.failure, errors: e });
  }
}

export function* reportPost(type) {
  try {
    const result = yield call(api.reportPost, type.data);
    yield put({
      type: constants.reportPost.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.reportPost.failure, errors: e });
  }
}

export function* contactUsWatcher() {
  yield takeEvery(constants.contactUs.request, contactUs);
}

export function* reportPostWatcher() {
  yield takeEvery(constants.reportPost.request, reportPost);
}
