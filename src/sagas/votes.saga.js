import { call, put, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/votes.service';

export function* getUserVotes(type) {
  try {
    const result = yield call(api.getUserVotes, type.id, type.token);
    yield put({
      type: constants.getUserVotes.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.getUserVotes.failure, errors: e.response.data });
  }
}

export function* getUserVotesWatcher() {
  yield takeEvery(constants.getUserVotes.request, getUserVotes);
}
