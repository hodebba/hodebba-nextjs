/* eslint-disable object-curly-newline */
import { all } from 'redux-saga/effects';

import { approveStatementWatcher, deleteImageFromCloudinaryWatcher, disapproveStatementWatcher } from './admin.saga';
import {
  continueWithFacebookGoogleWatcher,
  forgetPasswordWatcher,
  resendConfirmEmailWatcher,
  signInWatcher,
  signUpWatcher,
} from './authentification.saga';
import {
  addPositionWatcher,
  addStatementWatcher,
  createCelebrityWatcher,
  getPoliticalPartiesWatcher,
  getPoliticalPositionsWatcher,
  getProfessionsWatcher,
  getTopicsWatcher,
} from './celebrity.saga';
import { contactUsWatcher, reportPostWatcher } from './customer.saga';
import {
  addArgumentWatcher,
  addCommentWatcher,
  addTestimonialWatcher,
  confirmInvitationWatcher,
  createDebateWatcher,
  deleteDebateWatcher,
  deleteParticipantWatcher,
  deletePostWatcher,
  getAllDebatesWatcher,
  getThemesWatcher,
  inviteWatcher,
  promoteParticipantWatcher,
  publishDebateWatcher,
  sendVoteWatcher,
} from './debate.saga';
import { searchTextWatcher } from './search.saga';
import {
  deleteAccountWatcher,
  getPostsDataByUserWatcher,
  getStatementsUserLikesWatcher,
  getUserLikesWatcher,
  getUserWatcher,
  savePasswordWatcher,
  updateEmailWatcher,
  updatePasswordWatcher,
  updateUserWatcher,
} from './user.saga';
import { getUserVotesWatcher } from './votes.saga';

export default function* rootSaga() {
  yield all([
    signUpWatcher(),
    signInWatcher(),
    forgetPasswordWatcher(),
    deleteImageFromCloudinaryWatcher(),
    savePasswordWatcher(),
    updateEmailWatcher(),
    updateUserWatcher(),
    deleteAccountWatcher(),
    updatePasswordWatcher(),
    resendConfirmEmailWatcher(),
    getThemesWatcher(),
    createDebateWatcher(),
    publishDebateWatcher(),
    sendVoteWatcher(),
    addArgumentWatcher(),
    getUserLikesWatcher(),
    addCommentWatcher(),
    addTestimonialWatcher(),
    getUserVotesWatcher(),
    createCelebrityWatcher(),
    getTopicsWatcher(),
    getStatementsUserLikesWatcher(),
    addPositionWatcher(),
    addStatementWatcher(),
    getAllDebatesWatcher(),
    approveStatementWatcher(),
    disapproveStatementWatcher(),
    getProfessionsWatcher(),
    getPoliticalPartiesWatcher(),
    getPoliticalPositionsWatcher(),
    searchTextWatcher(),
    getPostsDataByUserWatcher(),
    reportPostWatcher(),
    contactUsWatcher(),
    getUserWatcher(),
    continueWithFacebookGoogleWatcher(),
    inviteWatcher(),
    promoteParticipantWatcher(),
    confirmInvitationWatcher(),
    deleteParticipantWatcher(),
    deleteDebateWatcher(),
    deletePostWatcher(),
  ]);
}
