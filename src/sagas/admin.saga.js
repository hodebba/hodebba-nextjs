// eslint-disable-next-line object-curly-newline
import { call, put, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/admin.service';

export function* deleteImageFromCloudinary(type) {
  try {
    const result = yield call(api.deleteImageFromCloudinary, type.publicId, type.token);
    yield put({
      type: constants.deleteImageFromCloudinary.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.deleteImageFromCloudinary.failure, errors: e });
  }
}

export function* approveStatement(type) {
  try {
    const result = yield call(api.approveStatement, type.body, type.token);
    yield put({
      type: constants.approveStatement.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.approveStatement.failure, errors: e });
  }
}

export function* disapproveStatement(type) {
  try {
    const result = yield call(api.disapproveStatement, type.id, type.token);
    yield put({
      type: constants.disapproveStatement.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.disapproveStatement.failure, errors: e });
  }
}

export function* deleteImageFromCloudinaryWatcher() {
  yield takeEvery(constants.deleteImageFromCloudinary.request, deleteImageFromCloudinary);
}

export function* approveStatementWatcher() {
  yield takeEvery(constants.approveStatement.request, approveStatement);
}

export function* disapproveStatementWatcher() {
  yield takeEvery(constants.disapproveStatement.request, disapproveStatement);
}
