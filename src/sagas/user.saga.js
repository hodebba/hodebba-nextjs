// eslint-disable-next-line object-curly-newline
import { call, put, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/user.service';

export function* savePassword(type) {
  try {
    const result = yield call(api.savePassword, type.data);
    yield put({
      type: constants.savePassword.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.savePassword.failure, errors: e.response.data });
  }
}

export function* updateEmail(type) {
  try {
    const result = yield call(api.updateEmail, type.email, type.id, type.token);
    yield put({
      type: constants.updateEmail.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.updateEmail.failure, errors: e.response.data });
  }
}

export function* updatePassword(type) {
  try {
    const result = yield call(api.updatePassword, type.data, type.id);
    yield put({
      type: constants.updatePassword.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.updatePassword.failure, errors: e.response.data });
  }
}

export function* updateUser(type) {
  try {
    const result = yield call(api.updateUser, type.user, type.token);
    yield put({
      type: constants.updateUser.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.updateUser.failure, errors: e.response.data });
  }
}

export function* deleteAccount(type) {
  try {
    const result = yield call(api.deleteAccount, type.id, type.token);
    yield put({
      type: constants.deleteAccount.success,
      data: result.data,
    });
    yield put({
      type: constants.logout,
    });
  } catch (e) {
    yield put({ type: constants.deleteAccount.failure, errors: e.response.data });
  }
}

export function* getUserLikes(type) {
  try {
    const result = yield call(api.getUserLikes, type.userId, type.debateId);
    yield put({
      type: constants.getUserLikes.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.getUserLikes.failure, errors: e.response.data });
  }
}

export function* getStatementsUserLikes(type) {
  try {
    const result = yield call(api.getStatementsUserLikes, type.userId);
    yield put({
      type: constants.getStatementsUserLikes.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.getStatementsUserLikes.failure, errors: e.response.data });
  }
}

export function* getPostsDataByUser(type) {
  try {
    const result = yield call(api.getPostsDataByUser, type.userId, type.token);
    yield put({
      type: constants.getPostsDataByUser.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.getPostsDataByUser.failure, errors: e.response.data });
  }
}

export function* getUser(type) {
  try {
    const result = yield call(api.getUser, type.userId, type.token);
    yield put({
      type: constants.getUser.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.getUser.failure, errors: e.response.data });
  }
}

export function* savePasswordWatcher() {
  yield takeEvery(constants.savePassword.request, savePassword);
}

export function* updateEmailWatcher() {
  yield takeEvery(constants.updateEmail.request, updateEmail);
}

export function* updatePasswordWatcher() {
  yield takeEvery(constants.updatePassword.request, updatePassword);
}

export function* updateUserWatcher() {
  yield takeEvery(constants.updateUser.request, updateUser);
}

export function* deleteAccountWatcher() {
  yield takeEvery(constants.deleteAccount.request, deleteAccount);
}

export function* getUserLikesWatcher() {
  yield takeEvery(constants.getUserLikes.request, getUserLikes);
}

export function* getStatementsUserLikesWatcher() {
  yield takeEvery(constants.getStatementsUserLikes.request, getStatementsUserLikes);
}

export function* getPostsDataByUserWatcher() {
  yield takeEvery(constants.getPostsDataByUser.request, getPostsDataByUser);
}

export function* getUserWatcher() {
  yield takeEvery(constants.getUser.request, getUser);
}
