import { call, put, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/celebrity.service';

export function* createCelebrity(type) {
  try {
    const result = yield call(api.createCelebrity, type.data, type.token);
    yield put({
      type: constants.createCelebrity.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.createCelebrity.failure, errors: e.response.data });
  }
}

export function* getTopics() {
  try {
    const result = yield call(api.getTopics);
    yield put({
      type: constants.getTopics.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.getTopics.failure, errors: e.response.data });
  }
}

export function* addStatement(type) {
  try {
    const result = yield call(api.addStatement, type.data, type.token);
    yield put({
      type: constants.addStatement.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.addStatement.failure, errors: e.response.data });
  }
}

export function* addPosition(type) {
  try {
    const result = yield call(api.addPosition, type.data, type.token);
    yield put({
      type: constants.addPosition.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.addPosition.failure, errors: e.response.data });
  }
}

export function* getProfessions() {
  try {
    const result = yield call(api.getProfessionsLight);
    yield put({
      type: constants.getProfessions.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.getProfessions.failure, errors: e.response.data });
  }
}

export function* getPoliticalParties() {
  try {
    const result = yield call(api.getPoliticalParties);
    yield put({
      type: constants.getPoliticalParties.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.getPoliticalParties.failure, errors: e.response.data });
  }
}
export function* getPoliticalPositions() {
  try {
    const result = yield call(api.getPoliticalPositions);
    yield put({
      type: constants.getPoliticalPositions.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.getPoliticalPositions.failure, errors: e.response.data });
  }
}

export function* createCelebrityWatcher() {
  yield takeEvery(constants.createCelebrity.request, createCelebrity);
}

export function* getTopicsWatcher() {
  yield takeEvery(constants.getTopics.request, getTopics);
}

export function* addStatementWatcher() {
  yield takeEvery(constants.addStatement.request, addStatement);
}

export function* addPositionWatcher() {
  yield takeEvery(constants.addPosition.request, addPosition);
}

export function* getProfessionsWatcher() {
  yield takeEvery(constants.getProfessions.request, getProfessions);
}

export function* getPoliticalPartiesWatcher() {
  yield takeEvery(constants.getPoliticalParties.request, getPoliticalParties);
}

export function* getPoliticalPositionsWatcher() {
  yield takeEvery(constants.getPoliticalPositions.request, getPoliticalPositions);
}
