/* eslint-disable no-unused-vars */
// eslint-disable-next-line object-curly-newline
import { call, put, takeEvery } from 'redux-saga/effects';

import constants from '../actions/constants';
import * as api from '../services/debate.service';

export function* createDebate(type) {
  try {
    const result = yield call(api.createDebate, type.data, type.token);
    yield put({
      type: constants.createDebate.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.createDebate.failure, errors: e.response.data });
  }
}

export function* publishDebate(type) {
  try {
    const result = yield call(api.publishDebate, type.data, type.token);
    yield put({
      type: constants.publishDebate.success,
      data: result,
    });
    yield put({
      type: constants.sendNotification,
      text: 'Ce débat est désormais en ligne !',
    });
  } catch (e) {
    yield put({ type: constants.publishDebate.failure, errors: e.response.data });
  }
}

export function* sendVote(type) {
  try {
    const result = yield call(api.sendVote, type.vote, type.token);
    yield put({
      type: constants.sendVote.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.sendVote.failure, errors: e.response.data });
  }
}

export function* getThemes() {
  try {
    const result = yield call(api.getThemes);
    yield put({
      type: constants.getThemes.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.getThemes.failure, errors: e.response.data });
  }
}

export function* addArgument(type) {
  try {
    const result = yield call(api.addArgument, type.data, type.token);
    yield put({
      type: constants.addArgument.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.addArgument.failure, errors: e.response.data });
  }
}

export function* addTestimonial(type) {
  try {
    const result = yield call(api.addTestimonial, type.data, type.token);
    yield put({
      type: constants.addTestimonial.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.addTestimonial.failure, errors: e.response.data });
  }
}

export function* addComment(type) {
  try {
    const result = yield call(api.addComment, type.data, type.token, type.isTestimonial);
    yield put({
      type: constants.addComment.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.addComment.failure, errors: e.response.data });
  }
}

export function* getAllDebates() {
  try {
    const result = yield call(api.getDebates);
    yield put({
      type: constants.getAllDebates.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.getAllDebates.failure, errors: e.response.data });
  }
}

export function* invite(type) {
  try {
    const result = yield call(api.invite, type.data, type.token);
    yield put({
      type: constants.invite.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.invite.failure, errors: e.response.data });
  }
}

export function* confirmInvitation(type) {
  try {
    const result = yield call(api.confirmInvitation, type.invitationToken, type.token);
    yield put({
      type: constants.confirmInvitation.success,
      data: result.data,
    });
  } catch (e) {
    yield put({ type: constants.confirmInvitation.failure, errors: e.response.data });
  }
}

export function* promoteParticipant(type) {
  try {
    const result = yield call(api.promoteParticipant, type.data, type.token);
    yield put({
      type: constants.promoteParticipant.success,
      data: result,
    });
  } catch (e) {
    yield put({ type: constants.promoteParticipant.failure, errors: e.response.data });
  }
}

export function* deleteParticipant(type) {
  try {
    const result = yield call(api.deleteParticipant, type.id, type.token);
    yield put({
      type: constants.deleteParticipant.success,
      data: type.participant,
    });
  } catch (e) {
    yield put({ type: constants.deleteParticipant.failure, errors: e.response.data });
  }
}

export function* deleteDebate(type) {
  try {
    const result = yield call(api.deleteDebate, type.id, type.token);
    yield put({
      type: constants.deleteDebate.success,
      id: type.id,
    });
  } catch (e) {
    yield put({ type: constants.deleteDebate.failure, errors: e.response.data });
  }
}

export function* deletePost(type) {
  try {
    const result = yield call(api.deletePost, type.id, type.token);
    yield put({
      type: constants.deletePost.success,
      id: type.id,
    });
  } catch (e) {
    yield put({ type: constants.deletePost.failure, errors: e.response.data });
  }
}

export function* getThemesWatcher() {
  yield takeEvery(constants.getThemes.request, getThemes);
}

export function* createDebateWatcher() {
  yield takeEvery(constants.createDebate.request, createDebate);
}

export function* publishDebateWatcher() {
  yield takeEvery(constants.publishDebate.request, publishDebate);
}

export function* sendVoteWatcher() {
  yield takeEvery(constants.sendVote.request, sendVote);
}

export function* addArgumentWatcher() {
  yield takeEvery(constants.addArgument.request, addArgument);
}

export function* addTestimonialWatcher() {
  yield takeEvery(constants.addTestimonial.request, addTestimonial);
}

export function* addCommentWatcher() {
  yield takeEvery(constants.addComment.request, addComment);
}

export function* getAllDebatesWatcher() {
  yield takeEvery(constants.getAllDebates.request, getAllDebates);
}

export function* inviteWatcher() {
  yield takeEvery(constants.invite.request, invite);
}

export function* confirmInvitationWatcher() {
  yield takeEvery(constants.confirmInvitation.request, confirmInvitation);
}

export function* promoteParticipantWatcher() {
  yield takeEvery(constants.promoteParticipant.request, promoteParticipant);
}

export function* deleteParticipantWatcher() {
  yield takeEvery(constants.deleteParticipant.request, deleteParticipant);
}

export function* deleteDebateWatcher() {
  yield takeEvery(constants.deleteDebate.request, deleteDebate);
}

export function* deletePostWatcher() {
  yield takeEvery(constants.deletePost.request, deletePost);
}
