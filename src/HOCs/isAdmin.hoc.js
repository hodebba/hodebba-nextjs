/* eslint-disable react-hooks/exhaustive-deps */
import Cookie from 'js-cookie';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

const isAdmin = Component => props => {
  const roles = useSelector(state => state.user.local.user.roles);
  const router = useRouter();

  useEffect(() => {
    if (!Cookie.get('user') && !(roles && roles[0].name === 'ROLE_ADMIN')) {
      setTimeout(() => {
        router.push('/');
      }, 5000);
    }
  }, [roles]);

  if (roles && roles[0].name === 'ROLE_ADMIN') {
    return <Component {...props} />;
  }

  return (
    <div
      style={{
        marginTop: '50px',
        padding: '3%',
        width: '100%',
        textAlign: 'center',
      }}
    >
      <NextSeo noindex />
      <p style={{ fontSize: '2rem' }}>Vous ne disposez pas des droits nécessaire pour cette page.</p>
      <p style={{ fontSize: '2rem', marginTop: '15px' }}>Vous allez être redirigé dans quelques secondes ...</p>
    </div>
  );
};

export default isAdmin;
