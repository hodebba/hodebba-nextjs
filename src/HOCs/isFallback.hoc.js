import LoadingPage from '@Components/LoadingPage';
import { useRouter } from 'next/router';

const isFallback = Component => props => {
  const router = useRouter();

  if (router.isFallback) {
    return <LoadingPage />;
  }
  return <Component {...props} />;
};

export default isFallback;
