/* eslint-disable react-hooks/exhaustive-deps */
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { isMobileOnly } from 'react-device-detect';

const isMobileRoute = Component => props => {
  const router = useRouter();

  useEffect(() => {
    if (!isMobileOnly) {
      setTimeout(() => {
        const desktopPath = router.asPath.split('/');
        router.push(`/${desktopPath[1]}/${desktopPath[2]}`);
      }, 100);
    }
  }, []);

  if (isMobileOnly) {
    return <Component {...props} />;
  }

  return <div></div>;
};

export default isMobileRoute;
