/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import Error from '@Containers/Error';
import isUndefined from 'lodash/isUndefined';

const checkError = Component => props => {
  let error = '';
  if (!isUndefined(props.error)) {
    error = props.error;
  } else if (!isUndefined(props.children) && !isUndefined(props.children.props.error)) {
    error = props.children.props.error;
  } else {
    error = 'unknown error';
  }
  if (error !== '') {
    return <Error code={404} />;
  }
  return <Component {...props} />;
};

export default checkError;
