/* eslint-disable react-hooks/exhaustive-deps */
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { isMobile } from 'react-device-detect';

const isNotBrowserRoute = Component => props => {
  const router = useRouter();

  useEffect(() => {
    if (!isMobile) {
      setTimeout(() => {
        const desktopPath = router.asPath.split('/compose');
        router.push(desktopPath[0]);
      }, 100);
    }
  }, []);

  if (isMobile) {
    return <Component {...props} />;
  }

  return <div></div>;
};

export default isNotBrowserRoute;
