/* eslint-disable react-hooks/exhaustive-deps */
import { openModalAuthentification } from '@Actions/authentification.actions';
import Cookie from 'js-cookie';
import { isUndefined } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const isConnected = Component => props => {
  const isLoggedIn = !isUndefined(Cookie.get('token'));
  const token = useSelector(state => state.user.local.user);
  const dispatch = useDispatch(null);
  const router = useRouter();

  useEffect(() => {
    if (!isLoggedIn) {
      dispatch(openModalAuthentification('signUp'));
      router.push('/');
    }
  }, [token]);

  return <Component {...props} />;
};

export default isConnected;
