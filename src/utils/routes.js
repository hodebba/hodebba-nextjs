export default {
  Home: {
    path: '/',
  },
  Theme: {
    path: '/theme/:id',
  },
  Admin: {
    path: '/admin',
    pathDebate: '/admin/debate',
    pathCelebrity: '/admin/celebrity',
  },
  User: {
    path: '/my-account',
    pathDashboard: '/my-account/dashboard',
    pathProfile: '/my-account/profile',
    pathSettings: '/my-account/settings',
    pathImage: '/my-account/profile/image',
    pathPseudo: '/my-account/profile/pseudo',
    pathLocation: '/my-account/profile/location',
    pathBio: '/my-account/profile/bio',
    pathSocialNetworks: '/my-account/profile/social-networks',
    pathPassword: '/my-account/settings/password',
    pathEmail: '/my-account/settings/email',
    pathDeleteAccount: '/my-account/settings/delete-account',
  },
  FourHundredFour: {
    path: '/*',
  },
  SavePassword: {
    path: '/save-password',
  },
};
