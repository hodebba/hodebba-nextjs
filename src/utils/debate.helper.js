export const getDebateRootPath = debate => {
  return debate.isPrivate ? '/debate/private' : '/debate';
};

export const isAdminParticipant = (user, debate) => {
  const participant = debate.participants.find(element => element.user.username === user.username);
  if (participant) {
    return participant.role === 'admin';
  }
  return false;
};

export const isViewerParticipant = (user, debate) => {
  const participant = debate.participants.find(element => element.user.username === user.username);
  if (participant) {
    return participant.role === 'viewer';
  }
  return false;
};
