/* eslint-disable implicit-arrow-linebreak */
import isURL from 'validator/lib/isURL';

export const required = value => (value ? undefined : 'Champ obligatoire.');

const maxLength = (max, label) => value =>
  value && value.length > max ? `Votre ${label} ne doit pas dépasser ${max} caractères.` : undefined;

export const maxLength50Pseudo = maxLength(50, 'pseudo');
export const maxLength50Pwd = maxLength(50, 'mot de passe');
export const maxLength90Link = maxLength(90, 'lien');
export const maxLength35Text = maxLength(35, 'texte');

const minLength = min => value =>
  value && value.length < min ? `Votre mot de passe doit avoir au moins ${min} caractères.` : undefined;
export const minLength8 = minLength(8);

export const atLeast2Letters = value =>
  value && !/.*[a-zA-Z].*[a-zA-Z].*/.test(value) ? 'Votre pseudo doit contenir au moins 2 lettres.' : undefined;

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Adresse email invalide.' : undefined;
export const hasAnUppercase = value =>
  value && !/^(?=.*[A-Z]).*$/g.test(value) ? 'Le mot de passe doit avoir au moins une majuscule' : undefined;
export const hasALowercase = value =>
  value && !/^(?=.*[a-z]).*$/g.test(value) ? 'Le mot de passe doit avoir au moins une minuscule' : undefined;

export const hasANumber = value =>
  value && !/^(?=.*\d).*$/g.test(value) ? 'Le mot de passe doit avoir au moins un chiffre' : undefined;

export const hasASpecialSigns = value =>
  value && !/^(?=.*[#$^+=!*()@%&]).*$/g.test(value)
    ? 'Le mot de passe doit avoir au moins un caractère spécial'
    : undefined;

const matchPassword = name => value =>
  value !== document.getElementsByName(name)[0].value ? 'Les 2 mots de passes ne correspondent pas.' : undefined;

export const matchPassword1 = matchPassword('password');
export const matchPassword2 = matchPassword('newPassword');

export const validURL = value =>
  value === undefined || isURL(value, { allow_underscores: true }) ? undefined : 'URL invalide.';

export const trim = value => value && value.trim();
