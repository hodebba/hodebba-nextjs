export const getVote = poll => {
  const typeTab = [];
  const vote = { pro: 0, neutral: 0, con: 0 };
  poll.poll.pollVoteTypes.forEach(type => {
    typeTab.push(type.id);
  });
  typeTab.sort();
  const index = typeTab.indexOf(poll.pollVoteType.id);
  if (index === 0) {
    return { ...vote, pro: 1 };
  }
  if (index === 1) {
    return { ...vote, neutral: 1 };
  }
  return { ...vote, con: 1 };
};
