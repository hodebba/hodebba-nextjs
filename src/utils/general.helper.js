/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from 'react';

// Detect click outside an element
export const useOutsideAlerter = (ref, callback) => {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        callback();
      }
    }

    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ref]);
};

export const preferences = [
  { value: 'popularity', label: 'Les plus populaires' },
  { value: 'recent', label: 'Les plus récents' },
  { value: 'older', label: 'les plus anciens' },
];

export const preferencesBis = [
  { value: 'relevance', label: 'Par pertinence' },
  { value: 'recent', label: 'Les plus récents' },
  { value: 'older', label: 'les plus anciens' },
];

export const arrayToOptions = list => {
  const options = [];
  list.forEach(element => {
    options.push({ value: element, label: element });
  });
  return options;
};

export const themeArrayToOptions = list => {
  const options = [];
  list.forEach(element => {
    // DO NOT ADD THEME "MY DEBATES"
    if (element.id !== 100) {
      options.push({ value: element.id, label: element.label });
    }
  });
  return options;
};

export const topicsArrayToOptions = (list, withAll) => {
  const options = [];
  list.forEach(element => {
    options.push({ value: element.id, label: element.label });
  });
  if (withAll) {
    options.unshift({ value: null, label: 'Tous les sujets' });
  }

  return options;
};

export const debatesArrayToOptions = (list, withAll) => {
  const options = [];
  list.forEach(element => {
    options.push({ value: element.id, label: element.title });
  });
  if (withAll) {
    options.unshift({ value: null, label: 'Tous les débats' });
  }

  return options;
};

export const compareLabel = (a, b) => {
  const labelA = a.label.replace('É', 'E').toUpperCase();
  const labelB = b.label.replace('É', 'E').toUpperCase();
  let comparison = 0;
  if (labelA > labelB) {
    comparison = 1;
  } else if (labelA < labelB) {
    comparison = -1;
  }
  return comparison;
};

export const compareTopic = (a, b) => {
  const labelA = a.label
    .replace('é', 'e')
    .toUpperCase()
    .split(/['\s]+/)[1];
  const labelB = b.label
    .replace('é', 'e')
    .toUpperCase()
    .split(/['\s]+/)[1];

  let comparison = 0;
  if (labelA > labelB) {
    comparison = 1;
  } else if (labelA < labelB) {
    comparison = -1;
  }
  return comparison;
};

export const sortById = (a, b) => {
  const idA = a.id;
  const idB = b.id;

  let comparison = 0;
  if (idA > idB) {
    comparison = 1;
  } else if (idA < idB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByViews = (a, b) => {
  const viewsA = a.viewNumber;
  const viewsB = b.viewNumber;

  let comparison = 0;
  if (viewsA < viewsB) {
    comparison = 1;
  } else if (viewsA > viewsB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByNewest = (a, b) => {
  const dateA = new Date(a.createdDate).getTime();
  const dateB = new Date(b.createdDate).getTime();

  let comparison = 0;
  if (dateA < dateB) {
    comparison = 1;
  } else if (dateA > dateB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByNewestES = (a, b) => {
  const dateA = new Date(a._source.createdDate).getTime();
  const dateB = new Date(b._source.createdDate).getTime();

  let comparison = 0;
  if (dateA < dateB) {
    comparison = 1;
  } else if (dateA > dateB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByOldest = (a, b) => {
  const dateA = new Date(a.createdDate).getTime();
  const dateB = new Date(b.createdDate).getTime();

  let comparison = 0;
  if (dateA > dateB) {
    comparison = 1;
  } else if (dateA < dateB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByOldestES = (a, b) => {
  const dateA = new Date(a._source.createdDate).getTime();
  const dateB = new Date(b._source.createdDate).getTime();

  let comparison = 0;
  if (dateA > dateB) {
    comparison = 1;
  } else if (dateA < dateB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByMostLiked = (a, b) => {
  const likesA = a.likes;
  const likesB = b.likes;

  let comparison = 0;
  if (likesA < likesB) {
    comparison = 1;
  } else if (likesA > likesB) {
    comparison = -1;
  }
  return comparison;
};

export const sortByRank = (a, b) => {
  const rankA = a.rank;
  const rankB = b.rank;

  let comparison = 0;
  if (rankB < rankA) {
    comparison = 1;
  } else if (rankB > rankA) {
    comparison = -1;
  }
  return comparison;
};

export const kFormatter = num => {
  return Math.abs(num) > 999
    ? `${Math.sign(num) * (Math.abs(num) / 1000).toFixed(1)}k`
    : Math.sign(num) * Math.abs(num);
};
export const useDidUpdateEffect = (fn, inputs) => {
  const didMountRef = useRef(false);

  useEffect(() => {
    if (didMountRef.current) {
      fn();
    } else {
      didMountRef.current = true;
    }
  }, inputs);
};

export const formatDateLetters = date => {
  return new Date(date).toLocaleString('fr-FR', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
};

export const formatDateNumeric = date => {
  return new Date(date).toLocaleString('fr-FR', {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  });
};
const getInnerWidth = () => {
  if (window.screen.width) {
    return Math.min(window.screen.width, window.innerWidth);
  }
  return window.innerWidth;
};

export function useWindowSize() {
  const isSSR = typeof window === 'undefined';
  const [windowSize, setWindowSize] = useState({
    width: isSSR ? 1200 : getInnerWidth(),
    height: isSSR ? 800 : window.innerHeight,
  });

  function changeWindowSize() {
    setWindowSize({
      width: getInnerWidth(),
      height: window.innerHeight,
    });
  }

  useEffect(() => {
    window.addEventListener('resize', changeWindowSize);

    return () => {
      window.removeEventListener('resize', changeWindowSize);
    };
  }, []);

  return windowSize;
}
