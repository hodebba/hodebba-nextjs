export const checkUserLikes = (statement, userLikes) => {
  let ret = {
    id: null,
    status: null,
    likeCount: null,
    dislikeCount: null,
  };
  if (userLikes) {
    userLikes.forEach(userLike => {
      if (userLike.statement.id === statement.id) {
        ret = {
          id: userLike.id,
          status: userLike.isLike,
          likeCount: userLike.likeCount,
          dislikeCount: userLike.dislikeCount,
        };
      }
    });
  }
  return ret;
};
