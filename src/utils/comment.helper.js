export const checkUserLikes = (comment, userLikes) => {
  let ret = {
    id: null,
    status: null,
    likeCount: null,
    dislikeCount: null,
  };
  if (userLikes) {
    userLikes.forEach(userLike => {
      if (userLike.post.id === comment.id) {
        ret = {
          id: userLike.id,
          status: userLike.isLike,
          likeCount: userLike.likeCount,
          dislikeCount: userLike.dislikeCount,
        };
      }
    });
  }
  return ret;
};

export const getLabel = (postType, isArgument) => {
  switch (postType) {
    case 'pro':
      return isArgument ? 'Pour' : 'Approuve';
    case 'con':
      return isArgument ? 'Contre' : 'Réfute';
    case 'neutral':
      return 'Neutre';
    default:
  }
};
