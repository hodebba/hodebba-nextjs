export const cloudinaryConfig = (preset, text) => {
  return {
    cloudName: process.env.NEXT_PUBLIC_CLOUDINARY_CLOUDNAME,
    api_key: process.env.NEXT_PUBLIC_CLOUDINARY_API_KEY,
    upload_preset: preset,
    multiple: false,
    singleUploadAutoClose: false,
    sources: ['local', 'url'],
    clientAllowedFormats: ['png', 'jpg', 'jpeg'],
    showPoweredBy: false,
    maxImageFileSize: 500000,
    language: 'fr',
    text,
    maxFiles: 1,
    cropping: true,
  };
};
