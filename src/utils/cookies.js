import Cookie from 'js-cookie';

export const removeAllUserCookies = () => {
  Cookie.remove('token');
  Cookie.remove('user');
  Cookie.remove('userLikes');
  Cookie.remove('statementUserLikes');
};
