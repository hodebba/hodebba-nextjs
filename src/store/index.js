/* eslint-disable no-unused-vars */
import { createWrapper } from 'next-redux-wrapper';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { rootReducer } from '../reducers';
import rootSaga from '../sagas';

let composeEnhancers = compose;

if (process.env.NODE_ENV !== 'production' && typeof window === 'object') {
  /* eslint-disable no-underscore-dangle */
  if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});
}

const makeConfiguredStore = reducer => {
  const sagaMiddleware = createSagaMiddleware();
  const enhancers = [applyMiddleware(sagaMiddleware)];
  const store = createStore(reducer, composeEnhancers(...enhancers));
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};

export const mockStore = makeConfiguredStore(rootReducer);

export const makeStore = (initialState, options) => {
  // 1: Create the middleware
  const sagaMiddleware = createSagaMiddleware();

  // Before we returned the created store without assigning it to a variable:
  // return createStore(reducer, initialState);
  const enhancers = [applyMiddleware(sagaMiddleware)];

  // 2: Add an extra parameter for applying middleware:
  const store = createStore(rootReducer, initialState, composeEnhancers(...enhancers));

  // 3: Run your sagas:
  sagaMiddleware.run(rootSaga);

  // 4: now return the store:
  return store;
};

// export an assembled wrapper
export const wrapper = createWrapper(makeStore, { debug: true });
