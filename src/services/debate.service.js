import axios from 'axios';

import URL from './api';
import { requestWithToken } from './options';

export const getTheme = async id => {
  const result = await axios.get(`${URL.themes.get}/${id}`);
  return result;
};

export const getThemes = async () => {
  const result = await axios.get(URL.themes.get);
  return result;
};

export const getDebate = async id => {
  const result = await axios.get(URL.debates.get(id));
  return result;
};

export const getPrivateDebate = async (id, token) => {
  const result = await axios.get(URL.debates.getPrivate(id), requestWithToken(token));
  return result;
};

export const getPrivateDebatePublicRoute = async id => {
  const result = await axios.get(URL.debates.getPrivatePublicRoute(id));
  return result;
};

export const getDebates = async () => {
  const result = await axios.get(URL.debates.getAll);
  return result.data;
};

export const getPrivateDebates = async () => {
  const result = await axios.get(URL.debates.getAllPrivate);
  return result.data;
};

export const getDraftDebates = async () => {
  const result = await axios.get(URL.debates.getDraftDebates);
  return result.data;
};

export const getAllArguments = async () => {
  const result = await axios.get(URL.post.getAllArguments);
  return result.data;
};

export const getAllTestimonials = async () => {
  const result = await axios.get(URL.post.getAllTestimonials);
  return result.data;
};

export const getDebatesForHome = async () => {
  const result = await axios.get(URL.debates.getForHome);
  return result;
};

export const createDebate = async (data, token) => {
  const result = await axios.post(URL.debates.create, data, requestWithToken(token));
  return result.data;
};

export const publishDebate = async (data, token) => {
  const result = await axios.put(URL.debates.put, data, requestWithToken(token));
  return result.data;
};

export const sendVote = async (vote, token) => {
  const result = await axios.post(URL.debates.postVote, vote, requestWithToken(token));
  return result.data;
};

export const incrementViews = async debateId => {
  const result = await axios.post(URL.debates.views, { debateId });
  return result.data;
};

export const addArgument = async (data, token) => {
  const result = await axios.post(URL.post.createArgument, data, requestWithToken(token));
  return result;
};

export const addComment = async (data, token, isTestimonial) => {
  let result;
  if (isTestimonial) {
    result = await axios.post(URL.post.createComment, data, requestWithToken(token));
  } else {
    result = await axios.post(URL.post.createArgumentComment, data, requestWithToken(token));
  }
  return result;
};

export const getArguments = async id => {
  const result = await axios.get(URL.post.getArguments(id));
  return result;
};

export const getPost = async id => {
  const result = await axios.get(URL.post.get(id));
  return result;
};

export const deletePost = async (id, token) => {
  const result = await axios.delete(URL.post.delete(id), requestWithToken(token));
  return result;
};

export const addLikeDislike = async (data, token) => {
  const result = await axios.post(URL.like.post, data, requestWithToken(token));
  return result;
};

export const deleteLikeDislike = async (id, token) => {
  const result = await axios.delete(`${URL.like.delete}/${id}`, requestWithToken(token));
  return result;
};

export const addTestimonial = async (data, token) => {
  const result = await axios.post(URL.post.createTestimonial, data, requestWithToken(token));
  return result;
};

export const getTestimonials = async id => {
  const result = await axios.get(URL.post.getTestimonials(id));
  return result;
};

export const getTweets = async id => {
  const result = await axios.get(URL.tweets.get(id));
  return result;
};

export const getTweetsData = async ids => {
  const result = await axios.get(
    URL.tweets.getData(ids),
    requestWithToken(`Bearer ${process.env.NEXT_PUBLIC_TWITTER_TOKEN}`),
  );
  return result.data;
};

export const getStatements = async id => {
  const result = await axios.get(URL.statements.getByDebate(id));
  return result;
};

export const invite = async (data, token) => {
  const result = await axios.post(URL.debates.invite, data, requestWithToken(token));
  return result;
};

export const promoteParticipant = async (data, token) => {
  const result = await axios.put(URL.participants.promote, data, requestWithToken(token));
  return result;
};

export const deleteParticipant = async (id, token) => {
  const result = await axios.delete(URL.participants.delete(id), requestWithToken(token));
  return result;
};

export const confirmInvitation = async (invitationToken, token) => {
  const result = await axios.get(URL.debates.confirmInvitation(invitationToken), requestWithToken(token));
  return result;
};

export const deleteDebate = async (id, token) => {
  const result = await axios.delete(URL.debates.delete(id), requestWithToken(token));
  return result;
};
