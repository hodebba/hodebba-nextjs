/* eslint-disable implicit-arrow-linebreak */
const users = {
  post: `${process.env.NEXT_PUBLIC_API_URL}/users`,
  resetPassword: `${process.env.NEXT_PUBLIC_API_URL}/users/password/reset`,
  checkResetPasswordToken: `${process.env.NEXT_PUBLIC_API_URL}/users/password/valid`,
  savePassword: `${process.env.NEXT_PUBLIC_API_URL}/users/password/save`,
  updateEmail: `${process.env.NEXT_PUBLIC_API_URL}/users/email`,
  updatePassword: `${process.env.NEXT_PUBLIC_API_URL}/users/password/`,
  updateUser: `${process.env.NEXT_PUBLIC_API_URL}/users`,
  delete: `${process.env.NEXT_PUBLIC_API_URL}/users/`,
  confirmAccount: `${process.env.NEXT_PUBLIC_API_URL}/users/registration/confirm`,
  resendConfirmEmail: `${process.env.NEXT_PUBLIC_API_URL}/users/registration/resend-token`,
  get: id => `${process.env.NEXT_PUBLIC_API_URL}/users/${id}`,
  continueWithFacebookGoogle: `${process.env.NEXT_PUBLIC_API_URL}/users/socials`,
};

const debates = {
  create: `${process.env.NEXT_PUBLIC_API_URL}/debates`,
  getAll: `${process.env.NEXT_PUBLIC_API_URL}/debates`,
  get: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}`,
  delete: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}`,
  getAllPrivate: `${process.env.NEXT_PUBLIC_API_URL}/debates/private`,
  getPrivate: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}/private`,
  getPrivatePublicRoute: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}/private-public`,
  put: `${process.env.NEXT_PUBLIC_API_URL}/debates`,
  getForHome: `${process.env.NEXT_PUBLIC_API_URL}/debates/home`,
  getDraftDebates: `${process.env.NEXT_PUBLIC_API_URL}/debates/debate-state/draft`,
  postVote: `${process.env.NEXT_PUBLIC_API_URL}/debates/poll`,
  getUserVotes: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/user/${id}/poll`,
  views: `${process.env.NEXT_PUBLIC_API_URL}/debates/views`,
  invite: `${process.env.NEXT_PUBLIC_API_URL}/debates/invite`,
  confirmInvitation: token => `${process.env.NEXT_PUBLIC_API_URL}/debates/invite/${token}`,
};

const participants = {
  promote: `${process.env.NEXT_PUBLIC_API_URL}/participants`,
  delete: id => `${process.env.NEXT_PUBLIC_API_URL}/participants/${id}`,
  getAllByUser: `${process.env.NEXT_PUBLIC_API_URL}/participants/users`,
};

const post = {
  createArgument: `${process.env.NEXT_PUBLIC_API_URL}/posts/arguments`,
  createTestimonial: `${process.env.NEXT_PUBLIC_API_URL}/posts/testimonials`,
  createComment: `${process.env.NEXT_PUBLIC_API_URL}/posts/comments`,
  createArgumentComment: `${process.env.NEXT_PUBLIC_API_URL}/posts/argument-comments`,
  getArguments: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}/arguments`,
  getAllArguments: `${process.env.NEXT_PUBLIC_API_URL}/posts/arguments`,
  getAllTestimonials: `${process.env.NEXT_PUBLIC_API_URL}/posts/testimonials`,
  getTestimonials: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}/testimonials`,
  get: id => `${process.env.NEXT_PUBLIC_API_URL}/posts/${id}`,
  getDataByUser: id => `${process.env.NEXT_PUBLIC_API_URL}/posts/users/${id}`,
  delete: id => `${process.env.NEXT_PUBLIC_API_URL}/posts/${id}`,
};

const like = {
  post: `${process.env.NEXT_PUBLIC_API_URL}/posts/likes`,
  postStatement: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/statements/likes`,
  delete: `${process.env.NEXT_PUBLIC_API_URL}/likes`,
  getUserLikes: `${process.env.NEXT_PUBLIC_API_URL}/posts/likes`,
  getStatementsUserLikes: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/statements/likes`,
};

const celebrity = {
  create: `${process.env.NEXT_PUBLIC_API_URL}/celebrities`,
  getAll: `${process.env.NEXT_PUBLIC_API_URL}/celebrities`,
  get: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/${id}`,
};

const positions = {
  getPositionsByCelebrity: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/${id}/positions`,
  getPositionsByDebate: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/debates/${id}/positions`,
  post: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/positions`,
};

const statements = {
  getStatementsByCelebrity: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/${id}/statements`,
  getNotValidatedStatements: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/statements`,
  update: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/statements`,
  delete: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/statements/${id}`,
  post: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/statements`,
  getByDebate: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/debates/${id}/statements`,
};

const topic = {
  getAll: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/topics`,
};

const tweets = {
  get: id => `${process.env.NEXT_PUBLIC_API_URL}/debates/${id}/tweets`,
  getData: ids =>
    `https://api.twitter.com/1.1/statuses/lookup.json?id=${ids}&include_entities=true&tweet_mode=extended`,
};

const themes = {
  get: `${process.env.NEXT_PUBLIC_API_URL}/debates/themes`,
};

const professions = {
  getAll: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/professions`,
  getAllLight: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/professions/light`,
  get: id => `${process.env.NEXT_PUBLIC_API_URL}/celebrities/professions/${id}`,
};

const politicalPositions = {
  getAll: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/political-positions`,
};

const politicalParties = {
  getAll: `${process.env.NEXT_PUBLIC_API_URL}/celebrities/political-parties`,
};

const login = {
  post: `${process.env.NEXT_PUBLIC_API_URL}/login`,
};

const cloudinary = {
  delete: `${process.env.NEXT_PUBLIC_API_URL}/cloudinary`,
};

const wikipedia = {
  getDescription: name =>
    `https://fr.wikipedia.org/w/api.php?format=json&origin=*&action=query&prop=extracts%7Cdescription&exintro&explaintext&redirects=1&exsentences=2&redirects=1&titles=${name}`,
  getShortDescriptions: names =>
    `https://fr.wikipedia.org/w/api.php?action=query&format=json&prop=description&redirects=1&titles=${names}`,
};

const es = {
  searchAll: `${process.env.NEXT_PUBLIC_API_URL}/search/debates,celebrities/_search`,
  // searchAll: 'http://localhost:9200/debates,celebrities/_search',
};

const customer = {
  contactUs: `${process.env.NEXT_PUBLIC_API_URL}/customers/contact-us`,
  reportPost: `${process.env.NEXT_PUBLIC_API_URL}/customers/report-post`,
};

export default {
  users,
  login,
  debates,
  post,
  like,
  tweets,
  themes,
  celebrity,
  positions,
  statements,
  professions,
  politicalPositions,
  politicalParties,
  topic,
  wikipedia,
  es,
  customer,
  cloudinary,
  participants,
};
