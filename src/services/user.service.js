import axios from 'axios';

import URL from './api';
import { requestWithToken } from './options';

export const checkResetPasswordToken = async token => {
  const result = await axios.get(`${URL.users.checkResetPasswordToken}/${token}`);
  return result;
};

export const savePassword = async data => {
  const result = await axios.post(URL.users.savePassword, data);
  return result;
};

export const updateEmail = async (email, id, token) => {
  const result = await axios.put(URL.users.updateEmail, { userId: id, email }, requestWithToken(token));
  return result;
};

export const updateUser = async (user, token) => {
  const result = await axios.put(URL.users.updateUser, user, requestWithToken(token));
  return result;
};

export const updatePassword = async (data, id) => {
  const result = await axios.put(
    URL.users.updatePassword,
    {
      userId: id,
      oldPassword: data.oldPassword,
      newPassword: data.newPassword,
      matchingPassword: data.matchingPassword,
    },
    requestWithToken,
  );
  return result;
};

export const deleteAccount = async (id, token) => {
  const result = await axios.delete(URL.users.delete + id, requestWithToken(token));
  return result;
};

export const getUserLikes = async (userId, debateId) => {
  const result = await axios.get(`${URL.like.getUserLikes}?userId=${userId}&debateId=${debateId}`);
  return result;
};

export const getStatementsUserLikes = async userId => {
  const result = await axios.get(`${URL.like.getStatementsUserLikes}?userId=${userId}`);
  return result;
};

export const getPostsDataByUser = async (userId, token) => {
  const result = await axios.get(URL.post.getDataByUser(userId), requestWithToken(token));
  return result;
};

export const getUser = async (userId, token) => {
  const result = await axios.get(URL.users.get(userId), requestWithToken(token));
  return result;
};

export const getAllParticipantsByUser = async token => {
  const result = await axios.get(URL.participants.getAllByUser, requestWithToken(token));
  return result;
};
