import axios from 'axios';

import URL from './api';

export const contactUs = async data => {
  const result = await axios.post(URL.customer.contactUs, data);
  return result.data;
};

export const reportPost = async data => {
  const result = await axios.post(URL.customer.reportPost, data);
  return result.data;
};
