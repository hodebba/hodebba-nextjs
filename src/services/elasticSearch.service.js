import axios from 'axios';

import URL from './api';

export const searchText = async text => {
  const queryText = `${text.trim().replaceAll(' ', ' AND ')}~1`;
  const body = {
    query: {
      bool: {
        must: [
          {
            query_string: {
              query: queryText,
              fields: [
                'fullName^3.0',
                'title^3.0',
                'tags^2.0',
                'theme^1.8',
                'politicalParty^1.6',
                'politicalPosition^1.6',
                // 'presentation^1.5',
                'profession^1.3',
                // 'statements^1.2',
                // 'arguments^1.0',
              ],
            },
          },
        ],
        must_not: {
          term: {
            debateState: 'draft',
          },
        },
      },
    },
    from: 0,
    size: 40,
  };
  const result = await axios.post(URL.es.searchAll, body, {
    auth: {
      username: 'defaultuser',
      password: 'vM8JLu8sq',
    },
  });
  return result;
};
