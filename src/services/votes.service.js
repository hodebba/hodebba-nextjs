import axios from 'axios';

import URL from './api';
import { requestWithToken } from './options';

export const getUserVotes = async (id, token) => {
  const result = await axios.get(URL.debates.getUserVotes(id), requestWithToken(token));
  return result;
};
