import axios from 'axios';

import URL from './api';
import { requestWithToken } from './options';

export const deleteImageFromCloudinary = async (publicId, token) => {
  const result = await axios.delete(`${URL.cloudinary.delete}/${publicId}`, requestWithToken(token));
  return result.data;
};

export const disapproveStatement = async (id, token) => {
  const result = await axios.delete(URL.statements.delete(id), requestWithToken(token));
  return result;
};

export const approveStatement = async (body, token) => {
  const result = await axios.put(URL.statements.update, body, requestWithToken(token));
  return result;
};
