export const requestWithToken = token => {
  return {
    headers: {
      Authorization: token,
    },
  };
};
