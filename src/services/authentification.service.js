import axios from 'axios';

import URL from './api';
import { requestWithToken } from './options';

export const signUp = async data => {
  const result = await axios.post(URL.users.post, {
    email: data.email,
    username: data.username,
    password: data.password,
    matchingPassword: data.matchingPassword,
  });
  return result;
};

export const continueWithFacebookGoogle = async data => {
  const result = await axios.post(URL.users.continueWithFacebookGoogle, {
    email: data.email,
    username: data.name,
    accessToken: data.accessToken,
  });
  return result;
};

export const signIn = async data => {
  const result = await axios.post(URL.login.post, data, { 'Access-Control-Request-Headers': 'Authorization' });
  return result;
};

export const forgetPassword = async email => {
  const result = await axios.post(`${URL.users.resetPassword}/${email}`, {});
  return result.data;
};

export const confirmAccount = async token => {
  const result = await axios.get(`${URL.users.confirmAccount}/${token}`);
  return result.data;
};

export const resendConfirmEmail = async (userId, token) => {
  const result = await axios.get(`${URL.users.resendConfirmEmail}/${userId}`, requestWithToken(token));
  return result.data;
};
