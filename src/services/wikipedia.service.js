import axios from 'axios';

import URL from './api';

export const getWikiDescription = async name => {
  const result = await axios.get(URL.wikipedia.getDescription(encodeURI(name)), {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  });
  return result;
};

export const getMultipleWikiDescription = async names => {
  const result = await axios.get(URL.wikipedia.getShortDescriptions(encodeURI(names)), {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  });
  return result.data.query.pages;
};
