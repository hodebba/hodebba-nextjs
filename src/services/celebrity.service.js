import axios from 'axios';

import URL from './api';
import { requestWithToken } from './options';

export const createCelebrity = async (data, token) => {
  const result = await axios.post(URL.celebrity.create, data, requestWithToken(token));
  return result.data;
};

export const getPositionsByCelebrity = async id => {
  const result = await axios.get(URL.positions.getPositionsByCelebrity(id));
  return result.data;
};

export const getStatementsByCelebrity = async id => {
  const result = await axios.get(URL.statements.getStatementsByCelebrity(id));
  return result.data;
};

export const getCelebrities = async () => {
  const result = await axios.get(URL.celebrity.getAll);
  return result.data;
};

export const getCelebrity = async id => {
  const result = await axios.get(URL.celebrity.get(id));
  return result.data;
};

export const getTopics = async () => {
  const result = await axios.get(URL.topic.getAll);
  return result.data;
};

export const addStatementLikeDislike = async (data, token) => {
  const result = await axios.post(URL.like.postStatement, data, requestWithToken(token));
  return result;
};

export const addStatement = async (data, token) => {
  const result = await axios.post(URL.statements.post, data, requestWithToken(token));
  return result;
};

export const addPosition = async (data, token) => {
  const result = await axios.post(URL.positions.post, data, requestWithToken(token));
  return result;
};

export const getPositionsByDebate = async id => {
  const result = await axios.get(URL.positions.getPositionsByDebate(id));
  return result.data;
};

export const getNotValidatedStatements = async () => {
  const result = await axios.get(URL.statements.getNotValidatedStatements);
  return result.data;
};

export const getProfessions = async () => {
  const result = await axios.get(URL.professions.getAll);
  return result.data;
};

export const getProfessionsLight = async () => {
  const result = await axios.get(URL.professions.getAllLight);
  return result.data;
};

export const getProfession = async id => {
  const result = await axios.get(URL.professions.get(id));
  return result.data;
};

export const getPoliticalPositions = async () => {
  const result = await axios.get(URL.politicalPositions.getAll);
  return result.data;
};

export const getPoliticalParties = async () => {
  const result = await axios.get(URL.politicalParties.getAll);
  return result.data;
};
