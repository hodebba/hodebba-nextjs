/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */

import arrowLeft from '@iconify/icons-vaadin/arrow-left';
import chevronRightSmall from '@iconify/icons-vaadin/chevron-right-small';
import closeSmall from '@iconify/icons-vaadin/close-small';
import facebookSquare from '@iconify/icons-vaadin/facebook-square';
import searchIcon from '@iconify/icons-vaadin/search';
import twitterSquare from '@iconify/icons-vaadin/twitter-square';
import { Icon } from '@iconify/react';
import { Image, Transformation } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import CreateIcon from '../../../../public/static/icons/create.svg';
import LogoutIcon from '../../../../public/static/icons/logout.svg';
import { logout, openModalAuthentification } from '../../../actions/authentification.actions';
import { saveSearchReturnPath, searchText } from '../../../actions/elasticSearch.actions';
import { closeMobileThemeList, openMobileThemeList, saveReturnPath } from '../../../actions/user.actions';
import { itemsMenuMyAccount } from '../../../components/itemsMenu';
import { useOutsideAlerter } from '../../../utils/general.helper';
import styles from './mobile-header.module.scss';

const MobileHeader = ({ themes }) => {
  const [inputIsFocus, setInputIsFocus] = useState(false);
  const [navIsOpened, setNavIsOpened] = useState(false);
  const [themeListIsOpened, setThemeListIsOpened] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const isLoggedIn = !isEmpty(useSelector(state => state.user.local.token));
  const user = useSelector(state => state.user.local.user);
  const roles = useSelector(state => state.user.local.user.roles);
  const searchIsOpened = useSelector(state => state.search.local.searchIsOpened);
  const returnPath = useSelector(state => state.search.local.returnPath);

  const main = document.getElementsByTagName('main')[0];

  const input = useRef(null);
  const inputContainer = useRef(null);
  const dispatch = useDispatch();
  const router = useRouter();
  const currentPath = router.pathname;

  useOutsideAlerter(inputContainer, () => {
    if (!router.pathname.includes('search')) {
      setInputIsFocus(false);
    }
  });

  useEffect(() => {
    router.prefetch('/my-account');
    router.prefetch('/my-account/settings');
    router.prefetch('/my-account/profile');
    router.prefetch('/admin/debate');

    if (router.pathname.split('/')[1] === 'search') {
      setInputIsFocus(true);
    }
  }, [router]);

  useEffect(() => {
    if (!searchIsOpened) {
      setSearchValue('');
      setInputIsFocus(false);
    }
  }, [searchIsOpened]);

  const handleClearInput = () => {
    if (input.current) {
      input.current.value = '';
      input.current.focus();
    }

    setSearchValue('');
  };

  const handleInputChange = e => {
    setSearchValue(e.target.value);
    if (router.pathname.split('/')[1] !== 'search') {
      dispatch(saveSearchReturnPath(router.pathname));
      router.push('/search');
    }
    if (e.target.value !== '') {
      dispatch(searchText(e.target.value));
    }
  };

  useEffect(() => {
    if (input && inputIsFocus) {
      input.current.focus();
    }
  }, [input, inputIsFocus]);

  useEffect(() => {
    if (main) {
      if (navIsOpened) {
        main.classList.add('no-scroll-main-mobile');
      } else {
        main.classList.remove('no-scroll-main-mobile');
      }
    }
  }, [navIsOpened]);

  const handleClickUserItem = path => {
    setNavIsOpened(false);
    router.push(path);
    dispatch(saveReturnPath({ href: currentPath, as: router.asPath }));
  };

  useEffect(() => {
    if (themeListIsOpened) {
      dispatch(openMobileThemeList());
    } else {
      dispatch(closeMobileThemeList());
    }
  }, [themeListIsOpened]);

  const closeSearch = () => {
    setInputIsFocus(false);
    if (router.pathname.includes('search')) {
      if (!returnPath) {
        router.push('/');
      } else {
        router.back();
      }
    }
  };

  return (
    <div className={styles.container}>
      {/* MOBILE HEADER ON TOP: LOGO AND SEARCH  */}
      <div className={styles.menu}>
        <div role="button" className={styles.burger} onClick={() => setNavIsOpened(true)}>
          <div className="line1"></div>
          <div className="line2"></div>
          <div className="line3"></div>
        </div>
        <div className={styles.brand}>
          <Link href="/">
            <a>
              Hodebba<span className={styles.beta}> beta</span>
            </a>
          </Link>
        </div>
      </div>
      <div className={styles['search-button']} role="button" onClick={() => setInputIsFocus(true)}>
        <Icon width="20" height="20" icon={searchIcon} />
      </div>
      {inputIsFocus && (
        <div ref={inputContainer} className={styles['search-focus']}>
          <Icon width="25" height="25" icon={arrowLeft} onClick={closeSearch} className="clickable" />
          <input
            type="text"
            onChange={handleInputChange}
            ref={input}
            placeholder="Rechercher un débat ou une personnalité..."
          />
          {searchValue !== '' && (
            <Icon width="25" height="25" icon={closeSmall} onClick={handleClearInput} className="clickable" />
          )}
        </div>
      )}
      <nav className={navIsOpened ? `${styles['nav-mobile']} ${styles.active}` : styles['nav-mobile']}>
        <div
          role="button"
          className={`${styles['nav-mobile__close-button']} clickable`}
          onClick={() => setNavIsOpened(false)}
        >
          <Icon width="25" height="25" icon={closeSmall} />
        </div>

        {/* MOBILE MENU ON LEFT: THEMES, USER ... */}
        {isLoggedIn ? (
          <Fragment>
            <div className={styles.user}>
              <Link href="/my-account/dashboard">
                <a onClick={() => setNavIsOpened(false)}>
                  <div className={styles.user__picture}>
                    <Image publicId={user.imageLink} alt="votre photo" secure="true">
                      <Transformation crop="fill" gravity="faces" />
                    </Image>
                  </div>
                </a>
              </Link>
              <Link href="/my-account/dashboard">
                <a onClick={() => setNavIsOpened(false)}>
                  <div className={styles.user__username}>{user.username}</div>
                </a>
              </Link>
            </div>

            <div className={styles['user-menu']}>
              <div className={styles['user-menu__title']}>Mon compte</div>
              <ul>
                {itemsMenuMyAccount.map(item => (
                  <li type="button" onClick={() => handleClickUserItem(item.href)} key={item.id}>
                    <div className={styles['user-menu__label']}>
                      {item.icon}
                      {item.label}
                    </div>
                    <Icon
                      width="18"
                      className={styles['user-menu__label__chevron-right']}
                      height="18"
                      icon={chevronRightSmall}
                    />
                  </li>
                ))}
                {roles[0].name === 'ROLE_ADMIN' && (
                  <li type="button" onClick={() => handleClickUserItem('/admin/debate')}>
                    <div className={styles['user-menu__label']}>
                      <CreateIcon />
                      Créer
                    </div>
                    <Icon
                      width="18"
                      className={styles['user-menu__label__chevron-right']}
                      height="18"
                      icon={chevronRightSmall}
                    />
                  </li>
                )}
                <li
                  className={`clickable ${styles['user-menu__logout']}`}
                  onClick={() => {
                    dispatch(logout());
                  }}
                  type="button"
                >
                  <LogoutIcon /> Déconnexion
                </li>
              </ul>
            </div>
          </Fragment>
        ) : (
          <div className={styles['nav-mobile__buttons']}>
            <button
              type="button"
              className="primary-button"
              onClick={() => dispatch(openModalAuthentification('signUp'))}
            >
              S'inscrire
            </button>
            <button
              type="button"
              className="secondary-button"
              onClick={() => dispatch(openModalAuthentification('signIn'))}
            >
              Se connecter
            </button>
          </div>
        )}

        <div className={styles['nav-mobile__separator']}></div>

        <div className={styles['nav-mobile__categories']}>
          <div className={styles['nav-mobile__title']}>Thèmes</div>
          <ul>
            {themes.slice(0, 6).map(element => (
              <li key={element.id}>
                <Link href="/theme/[id]" as={`/theme/${element.id}`}>
                  <a className="link-without-style" onClick={() => setNavIsOpened(false)}>
                    {element.label} <Icon width="18" height="18" icon={chevronRightSmall} />
                  </a>
                </Link>
              </li>
            ))}
          </ul>
          <button type="button" className="text-button" onClick={() => setThemeListIsOpened(true)}>
            Plus de thèmes
          </button>
        </div>

        <div className={styles['nav-mobile__separator']}></div>

        <div className={styles['nav-mobile__celebrities']}>
          <Link href="/celebrities">
            <a onClick={() => setNavIsOpened(false)}>
              Personnalités publiques
              <Icon width="18" height="18" icon={chevronRightSmall} />
            </a>
          </Link>
        </div>

        <div className={styles['nav-mobile__separator']}></div>

        {(!isLoggedIn || roles[0].name !== 'ROLE_ADMIN') && (
          <div className={styles['nav-mobile__celebrities']}>
            <Link href="/create">
              <a onClick={() => setNavIsOpened(false)}>
                Créer un débat
                <Icon width="18" height="18" icon={chevronRightSmall} />
              </a>
            </Link>
          </div>
        )}

        <div className={styles['nav-mobile__separator']}></div>

        <div className={styles['nav-mobile__about-us']}>
          <div className={styles['nav-mobile__title']}>Plus de Hodebba</div>
          <Link href="/contact">
            <a className="link-without-style" onClick={() => setNavIsOpened(false)}>
              Contactez-nous
            </a>
          </Link>
          <Link href="/help-center">
            <a className="link-without-style" onClick={() => setNavIsOpened(false)}>
              Centre d'assistance
            </a>
          </Link>
          <Link href="/conditions-generales">
            <a className="link-without-style" onClick={() => setNavIsOpened(false)}>
              Conditions générales d'utilisation
            </a>
          </Link>
          <div className={styles['nav-mobile__social-networks']}>
            <a href="https://www.facebook.com/hodebba" target="_blank" rel="noopener">
              <Icon icon={facebookSquare} width="30" height="30" color="#4267B2" />
            </a>
            <a href="https://twitter.com/hodebba" target="_blank" rel="noopener">
              <Icon icon={twitterSquare} width="30" height="30" color="#1DA1F2" />
            </a>
          </div>
        </div>
      </nav>
      {navIsOpened && (
        <div
          type="button"
          className={styles['nav-mobile__hide-background']}
          onClick={() => setNavIsOpened(false)}
        ></div>
      )}

      {themeListIsOpened && (
        <Fragment>
          <div className={styles['theme-navigation']}>
            <div className={styles['theme-navigation__container']}>
              <div className={styles['theme-navigation__title']}>Tous les thèmes</div>
              <ul>
                {themes.map(element => (
                  <li key={element.id}>
                    <Link href="/theme/[id]" as={`/theme/${element.id}`}>
                      <a
                        className="link-without-style"
                        onClick={() => {
                          setNavIsOpened(false);
                          setThemeListIsOpened(false);
                        }}
                      >
                        {element.label}
                      </a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div
            role="button"
            className={`${styles['theme-navigation__close-button']} clickable`}
            onClick={() => setThemeListIsOpened(false)}
          >
            <Icon width="25" height="25" color="#1f1f4b" icon={closeSmall} />
          </div>
        </Fragment>
      )}
    </div>
  );
};

MobileHeader.propTypes = {
  themes: PropTypes.array.isRequired,
};

export default MobileHeader;
