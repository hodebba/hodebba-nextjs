/* eslint-disable no-unused-vars */
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import RouterMock from '../../../../test/RouterMock';
import { mockStore } from '../../../store';
import MobileHeader from '../index';

describe('Mobile Header Component', () => {
  it('should render without crashing', () => {
    const wrapper = mount(
      <RouterMock path="/">
        <Provider store={mockStore}>
          <MobileHeader />
        </Provider>
      </RouterMock>,
    );
  });
  it('should match snapshot', () => {
    const wrapper = mount(
      <RouterMock path="/">
        <Provider store={mockStore}>
          <MobileHeader />
        </Provider>
      </RouterMock>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
