/* eslint-disable react-hooks/exhaustive-deps */
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getThemes } from '../../actions/debate.actions';
import { compareLabel } from '../../utils/general.helper';
import DesktopHeader from './DesktopHeader';
import MobileHeader from './MobileHeader';

const Header = ({ isMobileHeader }) => {
  const dispatch = useDispatch();
  const themes = useSelector(state => state.debate.local.themes);

  let sortedThemes = [...themes.filter(theme => theme.id !== 100)];
  sortedThemes = sortedThemes.sort(compareLabel);

  useEffect(() => {
    dispatch(getThemes());
  }, []);

  return (
    <header>{isMobileHeader ? <MobileHeader themes={sortedThemes} /> : <DesktopHeader themes={sortedThemes} />}</header>
  );
};

Header.propTypes = {
  isMobileHeader: PropTypes.bool.isRequired,
};

export default Header;
