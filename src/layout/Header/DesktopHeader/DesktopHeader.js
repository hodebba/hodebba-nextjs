/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/no-unescaped-entities */

import angleDown from '@iconify/icons-vaadin/angle-down';
import gridSmall from '@iconify/icons-vaadin/grid-small';
import searchIcon from '@iconify/icons-vaadin/search';
import { Icon } from '@iconify/react';
import { Image, Transformation } from 'cloudinary-react';
import isEmpty from 'lodash/isEmpty';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import CelebritiesIcon from '../../../../public/static/icons/celebrities.svg';
import CreateIcon from '../../../../public/static/icons/create.svg';
import LogoutIcon from '../../../../public/static/icons/logout.svg';
import { logout, openModalAuthentification } from '../../../actions/authentification.actions';
import { saveSearchReturnPath, searchText } from '../../../actions/elasticSearch.actions';
import { itemsMenuMyAccount } from '../../../components/itemsMenu';
import { useOutsideAlerter } from '../../../utils/general.helper';
import styles from './desktop-header.module.scss';

const DesktopHeader = ({ themes }) => {
  const dispatch = useDispatch();
  const [themeMenuIsDisplayed, setThemeMenuIsDisplayed] = useState(false);
  const [menuIsOpened, setMenuIsOpened] = useState(false);
  const isLoggedIn = !isEmpty(useSelector(state => state.user.local.token));
  const roles = useSelector(state => state.user.local.user.roles);
  const user = useSelector(state => state.user.local.user);
  const searchIsOpened = useSelector(state => state.search.local.searchIsOpened);
  const router = useRouter();
  const input = useRef();
  const [inputIsFocus, setInputIsFocus] = useState(false);

  const categoriesMenu = useRef(null);
  const userMenu = useRef(null);

  useOutsideAlerter(categoriesMenu, () => setThemeMenuIsDisplayed(false));
  useOutsideAlerter(userMenu, () => setMenuIsOpened(false));

  useEffect(() => {
    if (!searchIsOpened) {
      setInputIsFocus(false);
    }
  }, [searchIsOpened]);

  useEffect(() => {
    router.prefetch('/search');

    if (router.pathname.split('/')[1] === 'search') {
      setInputIsFocus(true);
    } else {
      setInputIsFocus(false);
    }
  }, [router]);

  useEffect(() => {
    if (inputIsFocus) {
      input.current.focus();
    }
  }, [inputIsFocus]);

  const handleInputChange = e => {
    if (e.target.value !== '') {
      dispatch(searchText(e.target.value));
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.menu}>
        <div className={styles.brand}>
          <Link href="/">
            <a>
              Hodebba<span className={styles.beta}> beta</span>
            </a>
          </Link>
        </div>

        {!inputIsFocus && (
          <Fragment>
            <div className={styles.separator}></div>

            <div className={styles.categories}>
              <div role="button" onClick={() => setThemeMenuIsDisplayed(true)} className={styles.categories__directory}>
                <Icon icon={gridSmall} width="20" height="20" />
                <p>Thèmes</p>
              </div>

              {themeMenuIsDisplayed && (
                <div className={styles.categories__menu} ref={categoriesMenu}>
                  <ul>
                    {themes.map(element => (
                      <li key={element.id}>
                        <Link href="/theme/[id]" as={`/theme/${element.id}`}>
                          <a onClick={() => setThemeMenuIsDisplayed(false)}>{element.label}</a>
                        </Link>
                      </li>
                    ))}
                  </ul>
                </div>
              )}

              <Link href="/celebrities">
                <a className={`${styles.categories__directory} ${styles.categories__link}`}>
                  <CelebritiesIcon width={29} height={29} />
                  <p>Personnalités publiques</p>
                </a>
              </Link>

              <Link href="/create">
                <a className={`${styles.categories__directory} ${styles.categories__link}`}>
                  <CreateIcon width={25} height={25} />
                  <p>Créer un débat</p>
                </a>
              </Link>
            </div>
          </Fragment>
        )}
      </div>

      {inputIsFocus && (
        <div className={styles.search}>
          <input
            type="text"
            ref={input}
            onChange={handleInputChange}
            placeholder="Rechercher un débat ou une personnalité..."
          />

          <Icon width="21" height="21" icon={searchIcon} role="button" />
        </div>
      )}

      <div className={styles.user_and_search}>
        {!inputIsFocus && (
          <Icon
            width="21"
            height="21"
            icon={searchIcon}
            className={styles.search_icon}
            role="button"
            onClick={() => {
              setInputIsFocus(true);
              dispatch(saveSearchReturnPath(router.pathname));
              router.push('/search');
            }}
          />
        )}

        {isLoggedIn ? (
          <Fragment>
            <div className={styles.user}>
              <Link href="/my-account/dashboard">
                <a>
                  <div className={styles.user__picture}>
                    <Image publicId={user.imageLink} alt="votre photo" secure="true">
                      <Transformation crop="fill" gravity="faces" />
                    </Image>
                  </div>
                </a>
              </Link>
              <Link href="/my-account/dashboard">
                <a>
                  <div className={styles.user__username}>{user.username}</div>
                </a>
              </Link>
              <Icon
                className="clickable"
                icon={angleDown}
                width="20"
                height="20"
                onClick={() => setMenuIsOpened(true)}
              />
            </div>
            {menuIsOpened && (
              <div ref={userMenu} className={styles['user-menu']}>
                <ul>
                  {itemsMenuMyAccount.map(item => (
                    <li type="button" key={item.id} onClick={() => setMenuIsOpened(false)}>
                      <Link href={item.href}>
                        <a>
                          {item.icon}
                          {item.label}
                        </a>
                      </Link>
                    </li>
                  ))}
                  {roles[0].name === 'ROLE_ADMIN' && (
                    <li type="button" onClick={() => setMenuIsOpened(false)}>
                      <Link href="/admin/debate">
                        <a>
                          <CreateIcon />
                          Créer
                        </a>
                      </Link>
                    </li>
                  )}

                  <li
                    className={`clickable ${styles['user-menu__logout']}`}
                    onClick={() => {
                      dispatch(logout());
                      setMenuIsOpened(false);
                    }}
                    type="button"
                  >
                    <LogoutIcon /> Déconnexion
                  </li>
                </ul>
              </div>
            )}
          </Fragment>
        ) : (
          <div className={styles.buttons}>
            <button
              type="button"
              className="primary-button"
              onClick={() => dispatch(openModalAuthentification('signUp'))}
            >
              S'inscrire
            </button>
            <button
              type="button"
              className="secondary-button"
              onClick={() => dispatch(openModalAuthentification('signIn'))}
            >
              Se connecter
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

DesktopHeader.propTypes = {
  themes: PropTypes.array.isRequired,
};

export default DesktopHeader;
