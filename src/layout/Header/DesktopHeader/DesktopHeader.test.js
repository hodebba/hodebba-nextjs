/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';

import RouterMock from '../../../../test/RouterMock';
import { mockStore } from '../../../store';
import DesktopHeader from '../index';

describe('Desktop Header Component', () => {
  it('should render without crashing', () => {
    const wrapper = mount(
      <RouterMock path="/">
        <Provider store={mockStore}>
          <DesktopHeader />
        </Provider>
      </RouterMock>,
    );
  });
  it('should match snapshot', () => {
    const wrapper = mount(
      <RouterMock path="/">
        <Provider store={mockStore}>
          <DesktopHeader />
        </Provider>
      </RouterMock>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
