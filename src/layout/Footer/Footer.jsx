import facebookSquare from '@iconify/icons-vaadin/facebook-square';
import twitterSquare from '@iconify/icons-vaadin/twitter-square';
import { Icon } from '@iconify/react';
import Link from 'next/link';
import { useRef } from 'react';

import styles from './footer.module.scss';

const Footer = () => {
  const footer = useRef(null);

  return (
    <footer ref={footer}>
      <div className={styles.container}>
        <div className={styles.contacts}>
          <div>
            <p>Suivez Hodebba sur </p>
            <a className="link-without-style" href="https://www.facebook.com/hodebba" target="_blank" rel="noopener">
              <Icon width="22" height="22" icon={facebookSquare} color="#4267B2" />
              Facebook
            </a>
            <a className="link-without-style" href="https://twitter.com/hodebba" target="_blank" rel="noopener">
              <Icon width="22" height="22" icon={twitterSquare} color="#1DA1F2" />
              Twitter
            </a>
          </div>
          <div className={styles.links}>
            <Link href="/contact">
              <a className="link-without-style">Contactez-nous</a>
            </Link>
            <Link href="/help-center">
              <a className="link-without-style">Centre d'assistance</a>
            </Link>
            <Link href="/conditions-generales">
              <a className="link-without-style">CGU</a>
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
