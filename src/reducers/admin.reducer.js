/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';

import constants from '../actions/constants';
export const initialState = {
  data: {
    approvedStatement: {},
    disapprovedStatement: {},
  },
  local: {
    errors: {
      deleteImageFromCloudinary: {},
      approveStatement: {},
      disapproveStatement: {},
    },
    loading: {
      deleteImageFromCloudinary: false,
      approveStatement: false,
      disapproveStatement: false,
    },
  },
};

const adminReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* DELETE CLOUDINARY IMAGE REQUEST */
      case constants.deleteImageFromCloudinary.request:
        draft.local.loading.deleteImageFromCloudinary = true;
        break;
      case constants.deleteImageFromCloudinary.success:
        draft.local.loading.deleteImageFromCloudinary = false;
        draft.local.errors.deleteImageFromCloudinary = {};
        break;
      case constants.deleteImageFromCloudinary.failure:
        draft.local.loading.deleteImageFromCloudinary = false;
        draft.local.errors.deleteImageFromCloudinary = action.errors;
        break;

      /* APPROVE STATEMENT */
      case constants.approveStatement.request:
        draft.local.loading.approveStatement = true;
        draft.local.errors.approveStatement = {};
        draft.data.approvedStatement = {};
        break;
      case constants.approveStatement.success:
        draft.local.loading.approveStatement = false;
        draft.local.errors.approveStatement = {};
        draft.data.approvedStatement = action.data;
        break;
      case constants.approveStatement.failure:
        draft.local.loading.approveStatement = false;
        draft.local.errors.approveStatement = action.errors;
        break;
      /* DISAPPROVE STATEMENT */
      case constants.disapproveStatement.request:
        draft.local.loading.disapproveStatement = true;
        draft.local.errors.approveStatement = {};
        draft.data.disapprovedStatement = {};
        break;
      case constants.disapproveStatement.success:
        draft.local.loading.disapproveStatement = false;
        draft.local.errors.disapproveStatement = {};
        draft.data.disapprovedStatement = action.data;
        break;
      case constants.disapproveStatement.failure:
        draft.local.loading.disapproveStatement = false;
        draft.local.errors.disapproveStatement = action.errors;
        break;
      default:
    }
  });
};
export default adminReducer;
