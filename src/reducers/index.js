import { HYDRATE } from 'next-redux-wrapper';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import adminReducer from './admin.reducer';
import authentificationReducer from './authentification.reducer';
import celebrityReducer from './celebrity.reducer';
import customerReducer from './customer.reducer';
import debateReducer from './debate.reducer';
import searchReducer from './search.reducer';
import userReducer from './user.reducer';
import votesReducer from './votes.reducer';

const combinedReducer = combineReducers({
  authentification: authentificationReducer,
  admin: adminReducer,
  user: userReducer,
  form: formReducer,
  debate: debateReducer,
  votes: votesReducer,
  celebrity: celebrityReducer,
  search: searchReducer,
  customer: customerReducer,
});

export const rootReducer = (state, action) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
    if (state.count) nextState.count = state.count; // preserve count value on client side navigation
    return nextState;
  }
  return combinedReducer(state, action);
};
