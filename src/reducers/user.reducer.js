/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import constants from '@Actions/constants';
import { removeAllUserCookies } from '@Utils/cookies';
import * as gtag from '@Utils/gtags';
import produce from 'immer';
import Cookie from 'js-cookie';

export const initialState = {
  data: {
    userLikes: {},
    statementUserLikes: [],
    dashboard: {},
  },
  local: {
    isLoading: true,
    returnPath: null,
    user: {},
    updatingUser: {
      field: null,
    },
    token: {},
    isThemeListOpened: false,
    notification: null,
    errors: {
      savePassword: {},
      updateEmail: {},
      updatePassword: {},
      updateUser: {},
      deleteAccount: {},
      getUserLikes: {},
      getStatementsUserLikes: {},
      getPostsDataByUser: {},
      getUser: {},
    },
    loading: {
      savePassword: false,
      updateEmail: false,
      updatePassword: false,
      updateUser: false,
      deleteAccount: false,
      getUserLikes: false,
      getStatementsUserLikes: false,
      getPostsDataByUser: false,
      getUser: false,
    },
  },
};

const userReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* SAVE NEW PASSWORD */
      case constants.savePassword.request:
        draft.local.loading.savePassword = true;
        break;
      case constants.savePassword.success:
        draft.local.loading.savePassword = false;
        draft.local.errors.savePassword = {};
        draft.local.validToken = false;
        break;
      case constants.savePassword.failure:
        draft.local.loading.savePassword = false;
        draft.local.errors.savePassword = action.errors;
        break;

      /* SAVE RETURN PATH ON MOBILE */
      case constants.saveReturnPath:
        draft.local.returnPath = action.path;
        break;

      /* SAVE USER ON SIGN IN SUCCESS */
      case constants.signIn.success:
        draft.local.user = action.data;
        draft.local.token = action.token;
        Cookie.set('user', action.data);
        Cookie.set('token', action.token);
        gtag.event('login', {
          method: 'email address',
          debug_mode: true,
        });
        break;

      /* SAVE USER ON CONTINUE WITH FACEBOOK SUCCESS */
      case constants.continueWithFacebookGoogle.success:
        draft.local.user = action.data;
        draft.local.token = action.token;
        Cookie.set('user', action.data);
        Cookie.set('token', action.token);
        gtag.event('login', {
          method: 'facebook',
          debug_mode: true,
        });
        break;

      /* SAVE USER ON SIGN UP SUCCESS */
      case constants.signUp.success:
        draft.local.user = action.data;
        draft.local.token = action.token;
        Cookie.set('user', action.data);
        Cookie.set('token', action.token);
        gtag.event('sign_up', {
          method: 'email address',
          debug_mode: true,
        });
        break;

      /* LOGOUT */
      case constants.logout:
        draft.local.user = {};
        draft.local.token = {};
        draft.data.userLikes = {};
        draft.data.statementUserLikes = [];
        removeAllUserCookies();
        break;

      /* UPDATE EMAIL */
      case constants.updateEmail.request:
        draft.local.loading.updateEmail = true;
        break;
      case constants.updateEmail.success:
        draft.local.loading.updateEmail = false;
        draft.local.errors.updateEmail = {};
        draft.local.user = action.data;
        break;
      case constants.updateEmail.failure:
        draft.local.loading.updateEmail = false;
        draft.local.errors.updateEmail = action.errors;
        break;
      case constants.clearUpdateEmailError:
        draft.local.errors.updateEmail = {};
        break;

      /* UPDATE PASSWORD */
      case constants.updatePassword.request:
        draft.local.loading.updatePassword = true;
        break;
      case constants.updatePassword.success:
        draft.local.loading.updatePassword = false;
        draft.local.errors.updatePassword = {};
        break;
      case constants.updatePassword.failure:
        draft.local.loading.updatePassword = false;
        draft.local.errors.updatePassword = action.errors;
        break;
      case constants.clearUpdatePasswordError:
        draft.local.errors.updatePassword = {};
        break;

      /* DELETE ACCOUNT */
      case constants.deleteAccount.request:
        draft.local.loading.deleteAccount = true;
        break;
      case constants.deleteAccount.success:
        draft.local.loading.deleteAccount = false;
        draft.local.errors.deleteAccount = {};
        draft.local.user = {};
        draft.local.token = {};
        draft.data.userLikes = {};
        draft.data.statementUserLikes = [];
        removeAllUserCookies();
        break;
      case constants.deleteAccount.failure:
        draft.local.loading.deleteAccount = false;
        draft.local.errors.deleteAccount = action.errors;
        break;

      /* UPDATE AN USER */
      case constants.updateUser.request:
        draft.local.loading.updateUser = true;
        draft.local.updatingUser.field = action.field;
        break;
      case constants.updateUser.success:
        draft.local.loading.updateUser = false;
        draft.local.errors.updateUser = {};
        draft.local.user = action.data;
        draft.local.notification = draft.local.updatingUser.field;
        break;
      case constants.updateUser.failure:
        draft.local.loading.updateUser = false;
        draft.local.errors.updateUser = action.errors;
        break;

      case constants.clearUpdateUserError:
        draft.local.errors.updateUser = {};
        break;

      /* ENABLE USER */
      case constants.setUserEnabled:
        draft.local.user.enabled = true;
        break;

      /* USER NOTIFICATION */
      case constants.sendNotification:
        draft.local.notification = action.text;
        break;

      /* GET USER LIKES */
      case constants.getUserLikes.request:
        draft.local.loading.getUserLikes = true;
        draft.local.temp = action.debateId;
        break;
      case constants.getUserLikes.success:
        draft.local.loading.getUserLikes = false;
        draft.local.errors.getUserLikes = {};
        draft.data.userLikes[draft.local.temp] = action.data;
        Cookie.set('userLikes', draft.data.userLikes);
        delete draft.local.temp;
        break;
      case constants.getUserLikes.failure:
        draft.local.loading.getUserLikes = false;
        draft.local.errors.getUserLikes = action.errors;
        delete draft.local.temp;
        break;

      /* GET STATEMENT USER LIKES */
      case constants.getStatementsUserLikes.request:
        draft.local.loading.getStatementsUserLikes = true;
        break;
      case constants.getStatementsUserLikes.success:
        draft.local.loading.getStatementsUserLikes = false;
        draft.local.errors.getStatementsUserLikes = {};
        draft.data.statementUserLikes = action.data;
        Cookie.set('statementUserLikes', draft.data.statementUserLikes);
        break;
      case constants.getStatementsUserLikes.failure:
        draft.local.loading.getStatementsUserLikes = false;
        draft.local.errors.getStatementsUserLikes = action.errors;
        break;

      /* OPEN / CLOSE THEME LIST */
      case constants.openMobileThemeList:
        draft.local.isThemeListOpened = true;
        break;

      case constants.closeMobileThemeList:
        draft.local.isThemeListOpened = false;
        break;

      /* UPDATE LIKES */
      case constants.updateUserLikes:
        const newData = {
          id: action.isLikeId,
          isLike: action.isLike,
          post: {
            id: action.postId,
          },
          likeCount: action.likeCount,
          dislikeCount: action.dislikeCount,
        };
        const index = draft.data.userLikes[action.debateId].findIndex(element => element.post.id === action.postId);
        if (index === -1) {
          draft.data.userLikes[action.debateId].push(newData);
        } else {
          draft.data.userLikes[action.debateId][index] = newData;
        }
        Cookie.set('userLikes', draft.data.userLikes);
        break;

      case constants.updateStatementsUserLikes:
        const newData2 = {
          id: action.isLikeId,
          isLike: action.isLike,
          statement: {
            id: action.statementId,
          },
          likeCount: action.likeCount,
          dislikeCount: action.dislikeCount,
        };
        const index2 = draft.data.statementUserLikes.findIndex(element => element.statement.id === action.statementId);
        if (index2 === -1) {
          draft.data.statementUserLikes.push(newData2);
        } else {
          draft.data.statementUserLikes[index2] = newData2;
        }
        Cookie.set('statementUserLikes', draft.data.statementUserLikes);
        break;

      /* GET POSTS DATA BY USER */
      case constants.getPostsDataByUser.request:
        draft.local.loading.getPostsDataByUser = true;
        break;
      case constants.getPostsDataByUser.failure:
        draft.local.loading.getPostsDataByUser = false;
        draft.local.errors.getPostsDataByUser = action.errors;
        break;
      case constants.getPostsDataByUser.success:
        draft.local.errors.getPostsDataByUser = {};
        draft.data.dashboard = action.data;
        draft.local.loading.getPostsDataByUser = false;
        break;

      /* GET USER */
      case constants.getUser.request:
        draft.local.loading.getUser = true;
        break;
      case constants.getUser.failure:
        draft.local.loading.getUser = false;
        draft.local.errors.getUser = action.errors;
        break;
      case constants.getUser.success:
        draft.local.errors.getUser = {};
        draft.local.loading.getUser = false;

        if (action.data.enabled) {
          Cookie.set('user', action.data);
          draft.local.user.enabled = true;
        }
        break;

      /* GET INFO FROM COOKIES */
      case constants.getInfoFromCookies:
        if (Cookie.get('token')) {
          draft.local.token = Cookie.get('token');
        }
        if (Cookie.get('userLikes')) {
          draft.data.userLikes = JSON.parse(Cookie.get('userLikes'));
        }
        if (Cookie.get('statementUserLikes')) {
          draft.data.statementUserLikes = JSON.parse(Cookie.get('statementUserLikes'));
        }
        if (Cookie.get('user')) {
          draft.local.user = JSON.parse(Cookie.get('user'));
        }
        draft.local.isLoading = false;
        break;

      default:
    }
  });
};
export default userReducer;
