/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';

import constants from '../actions/constants';
export const initialState = {
  data: {
    contactUs: {},
    reportPost: {},
  },
  local: {
    errors: {
      reportPost: {},
      contactUs: {},
    },
    loading: {
      reportPost: false,
      contactUs: false,
    },
  },
};

const customerReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* CONTACT US */
      case constants.contactUs.request:
        draft.local.loading.contactUs = true;
        draft.local.errors.contactUs = {};
        draft.data.contactUs = {};
        break;
      case constants.contactUs.success:
        draft.local.loading.contactUs = false;
        draft.local.errors.contactUs = {};
        draft.data.contactUs = action.data;
        break;
      case constants.contactUs.failure:
        draft.local.loading.contactUs = false;
        draft.local.errors.contactUs = action.errors;
        break;

      /* REPORT POST */
      case constants.reportPost.request:
        draft.local.loading.reportPost = true;
        draft.local.errors.reportPost = {};
        draft.data.reportPost = {};
        break;
      case constants.reportPost.success:
        draft.local.loading.reportPost = false;
        draft.local.errors.reportPost = {};
        draft.data.reportPost = action.data;
        break;
      case constants.reportPost.failure:
        draft.local.loading.reportPost = false;
        draft.local.errors.reportPost = action.errors;
        break;
      case constants.resetReportPost:
        draft.data.reportPost = {};
        break;
      default:
    }
  });
};
export default customerReducer;
