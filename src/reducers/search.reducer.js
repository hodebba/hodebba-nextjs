/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';

import constants from '../actions/constants';

export const initialState = {
  data: {
    lastResults: '',
  },
  local: {
    lastQuery: '',
    returnPath: null,
    searchIsOpened: false,
    loading: {
      searchText: false,
    },
    errors: {
      searchText: {},
    },
  },
};

const searchReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* SEARCH TEXT */
      case constants.searchText.request:
        draft.local.loading.searchText = true;
        draft.local.lastQuery = action.text;
        draft.local.searchIsOpened = true;
        break;
      case constants.searchText.failure:
        draft.local.loading.searchText = false;
        draft.local.errors.searchText = action.errors;
        break;
      case constants.searchText.success:
        draft.local.errors.searchText = {};
        draft.local.loading.searchText = false;
        draft.data.lastResults = action.data.hits;

        break;

      /* SAVE SEARCH RETURN PATH */
      case constants.saveSearchReturnPath:
        draft.local.returnPath = action.path;
        break;

      /* CLEAR SEARCH */
      case constants.clearSearch:
        draft.data.lastResults = '';
        draft.local.lastQuery = '';
        draft.local.searchIsOpened = false;
        break;
      default:
    }
  });
};
export default searchReducer;
