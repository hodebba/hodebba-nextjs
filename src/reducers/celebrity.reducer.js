/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';

import constants from '../actions/constants';
import { compareLabel, compareTopic } from '../utils/general.helper';
import * as gtag from '../utils/gtags';

export const initialState = {
  data: {
    createCelebrity: {},
    addStatement: {},
    addPosition: {},
  },
  local: {
    topics: [],
    professions: [],
    politicalPositions: [],
    politicalParties: [],
    errors: {
      createCelebrity: {},
      getTopics: {},
      addStatement: {},
      addPosition: {},
      getProfessions: {},
      getPoliticalPositions: {},
      getPoliticalParties: {},
    },
    loading: {
      createCelebrity: false,
      getTopics: false,
      addStatement: false,
      addPosition: false,
      getProfessions: false,
      getPoliticalParties: false,
      getPoliticalPositions: false,
    },
  },
};

const celebrityReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* CREATE CELEBRITY */
      case constants.createCelebrity.request:
        draft.local.loading.createCelebrity = true;
        draft.data.createCelebrity = {};
        break;
      case constants.createCelebrity.success:
        draft.local.loading.createCelebrity = false;
        draft.local.errors.createCelebrity = {};
        draft.data.createCelebrity = action.data;
        break;
      case constants.createCelebrity.failure:
        draft.local.loading.createCelebrity = false;
        draft.local.errors.createCelebrity = action.errors;
        break;
      case constants.reinitializeCreateCelebrity:
        draft.data.createCelebrity = {};
        break;

      /* GET TOPICS */
      case constants.getTopics.request:
        draft.local.loading.getTopics = true;
        break;
      case constants.getTopics.success:
        draft.local.loading.getTopics = false;
        draft.local.errors.getTopics = {};
        draft.local.topics = action.data.sort(compareTopic);
        break;
      case constants.getTopics.failure:
        draft.local.loading.getTopics = false;
        draft.local.errors.getTopics = action.errors;
        break;

      /* ADD STATEMENT */
      case constants.addStatement.request:
        draft.local.loading.addStatement = true;
        break;
      case constants.addStatement.success:
        draft.local.loading.addStatement = false;
        draft.local.errors.addStatement = {};
        draft.data.addStatement = action.data;
        gtag.event('new_statement', {
          topic: action.data.topic.label,
          celebrity: action.data.celebrity.fullName,
          debug_mode: true,
        });

        break;
      case constants.addStatement.failure:
        draft.local.loading.addStatement = false;
        draft.local.errors.addStatement = action.errors;
        break;
      case constants.resetAddStatement:
        draft.data.addStatement = {};
        break;

      /* ADD POSITION */
      case constants.addPosition.request:
        draft.local.loading.addPosition = true;
        break;
      case constants.addPosition.success:
        draft.local.loading.addPosition = false;
        draft.local.errors.addPosition = {};
        draft.data.addPosition = action.data;
        gtag.event('new_position', {
          topic: action.data.topic.label,
          celebrity: action.data.celebrity.fullName,
          debug_mode: true,
        });

        break;
      case constants.addPosition.failure:
        draft.local.loading.addPosition = false;
        draft.local.errors.addPosition = action.errors;
        break;
      case constants.resetAddPosition:
        draft.data.addPosition = {};
        break;

      /* GET PROFESSIONS */
      case constants.getProfessions.request:
        draft.local.loading.getProfessions = true;
        break;
      case constants.getProfessions.success:
        draft.local.loading.getProfessions = false;
        draft.local.errors.getProfessions = {};
        draft.local.professions = action.data.sort(compareLabel);
        break;
      case constants.getProfessions.failure:
        draft.local.loading.getProfessions = false;
        draft.local.errors.getProfessions = action.errors;
        break;

      /* GET POLITICAL PROFESSIONS */
      case constants.getPoliticalPositions.request:
        draft.local.loading.getPoliticalPositions = true;
        break;
      case constants.getPoliticalPositions.success:
        draft.local.loading.getPoliticalPositions = false;
        draft.local.errors.getPoliticalPositions = {};
        draft.local.politicalPositions = action.data;
        break;
      case constants.getPoliticalPositions.failure:
        draft.local.loading.getPoliticalPositions = false;
        draft.local.errors.getPoliticalPositions = action.errors;
        break;

      /* GET POLITICAL PARTIES */
      case constants.getPoliticalParties.request:
        draft.local.loading.getPoliticalParties = true;
        break;
      case constants.getPoliticalParties.success:
        draft.local.loading.getPoliticalParties = false;
        draft.local.errors.getPoliticalParties = {};
        draft.local.politicalParties = action.data;
        break;
      case constants.getPoliticalParties.failure:
        draft.local.loading.getPoliticalParties = false;
        draft.local.errors.getPoliticalParties = action.errors;
        break;
      default:
    }
  });
};
export default celebrityReducer;
