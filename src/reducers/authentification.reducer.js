/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';
import { HYDRATE } from 'next-redux-wrapper';

import constants from '../actions/constants';

export const initialState = {
  data: {
    resendConfirmEmail: {},
  },
  local: {
    errors: {
      signUp: {},
      signIn: {},
      forgetPassword: {},
      resendConfirmEmail: {},
      continueWithFacebookGoogle: {},
    },
    loading: {
      signUp: false,
      signIn: false,
      forgetPassword: false,
      resendConfirmEmail: false,
      continueWithFacebookGoogle: false,
    },

    modal: {
      isOpen: false,
      type: '',
      emailResetPassword: '',
    },
  },
};

const authentificationReducer = (state = initialState, action) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
    if (state.count) nextState.count = state.count; // preserve count value on client side navigation
    return nextState;
  }

  return produce(state, draft => {
    switch (action.type) {
      /* AUTHENTIFICATION MODAL HANDLING */
      case constants.openModalAuthentification:
        draft.local.modal.isOpen = true;
        draft.local.modal.type = action.authType;
        break;

      case constants.closeModalAuthentification:
        draft.local.modal.isOpen = false;
        draft.local.modal.type = '';
        draft.local.errors.signUp = {};
        draft.local.errors.signIn = {};
        break;

      /* SIGN UP REQUEST */
      case constants.signUp.request:
        draft.local.loading.signUp = true;
        break;
      case constants.signUp.success:
        draft.data.signUp = { email: action.data.email, username: action.data.username };
        draft.local.loading.signUp = false;
        draft.local.errors.signUp = {};
        draft.local.modal.type = 'successfulSignUp';
        break;
      case constants.signUp.failure:
        draft.local.loading.signUp = false;
        draft.local.errors.signUp = action.errors;
        break;

      /* SIGN IN REQUEST */
      case constants.signIn.request:
        draft.local.loading.signIn = true;
        break;
      case constants.signIn.success:
        draft.local.loading.signIn = false;
        draft.local.errors.signIn = {};
        draft.local.modal.isOpen = false;
        draft.local.modal.type = '';
        break;
      case constants.signIn.failure:
        draft.local.loading.signIn = false;
        draft.local.errors.signIn = action.errors;
        break;

      /* CONTINUE WITH FACEBBOK REQUEST */
      case constants.continueWithFacebookGoogle.request:
        draft.local.loading.continueWithFacebookGoogle = true;
        break;
      case constants.continueWithFacebookGoogle.success:
        draft.local.loading.continueWithFacebookGoogle = false;
        draft.local.errors.continueWithFacebookGoogle = {};
        draft.local.modal.isOpen = false;
        draft.local.modal.type = '';
        break;
      case constants.continueWithFacebookGoogle.failure:
        draft.local.loading.continueWithFacebookGoogle = false;
        draft.local.errors.continueWithFacebookGoogle = action.errors;
        break;

      /* FORGET PASSWORD REQUEST */
      case constants.forgetPassword.request:
        draft.local.loading.forgetPassword = true;
        draft.local.modal.emailResetPassword = action.email;
        break;
      case constants.forgetPassword.success:
        draft.local.loading.forgetPassword = false;
        draft.local.errors.forgetPassword = {};
        draft.local.modal.type = 'successfulEmailSent';
        break;
      case constants.forgetPassword.failure:
        draft.local.loading.forgetPassword = false;
        draft.local.errors.forgetPassword = action.errors;
        break;

      /* REDIRECTION TOLOGIN AFTER SUCCESSFULLY RESET PASSWORD */
      case constants.savePassword.success:
        draft.local.modal.isOpen = true;
        draft.local.modal.type = 'signIn';
        break;

      /* RESEND EMAIL IN ORDER TO CONFIRM USER ACCOUNT */
      case constants.resendConfirmEmail.request:
        draft.local.loading.resendConfirmEmail = true;
        draft.local.errors.resendConfirmEmail = {};
        draft.data.resendConfirmEmail = {};
        break;
      case constants.resendConfirmEmail.success:
        draft.local.loading.resendConfirmEmail = false;
        draft.local.errors.resendConfirmEmail = {};
        draft.data.resendConfirmEmail = action.data;
        break;
      case constants.resendConfirmEmail.failure:
        draft.local.loading.resendConfirmEmail = false;
        draft.local.errors.resendConfirmEmail = action.errors;
        break;

      default:
    }
  });
};
export default authentificationReducer;
