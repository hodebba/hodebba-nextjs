/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';
import Cookie from 'js-cookie';

import constants from '../actions/constants';
import { getVote } from '../utils/votes.helper';

export const initialState = {
  polls: {},
  local: {
    loading: {
      getUserVotes: false,
    },
    errors: {
      getUserVotes: {},
    },
  },
};

const votesReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* UPDATE USER VOTES */
      case constants.updateUserVotes:
        draft.polls[action.pollId] = action.vote;
        Cookie.set('polls', draft.polls);
        break;

      /* DELETE USER VOTES ON LOG OUT */
      case constants.logout:
        draft.polls = {};
        Cookie.remove('polls');
        break;

      /* Get User votes */
      case constants.getUserVotes.request:
        draft.local.loading.getUserVotes = true;
        break;
      case constants.getUserVotes.failure:
        draft.local.loading.getUserVotes = false;
        draft.local.errors.getUserVotes = action.errors;
        break;
      case constants.getUserVotes.success:
        draft.local.errors.getUserVotes = {};
        draft.local.loading.getUserVotes = false;
        action.data.forEach(poll => {
          draft.polls[poll.poll.id] = getVote(poll);
        });
        Cookie.set('polls', draft.polls);
        break;

      /* GET INFO FROM COOKIES */
      case constants.getInfoFromCookies:
        if (Cookie.get('polls')) {
          draft.polls = JSON.parse(Cookie.get('polls'));
        }

        break;

      default:
    }
  });
};
export default votesReducer;
