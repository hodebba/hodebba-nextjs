/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import produce from 'immer';

import constants from '../actions/constants';
import * as gtag from '../utils/gtags';

export const initialState = {
  data: {
    createDebate: {},
    addArgument: {},
    addComment: {},
    addTestimonial: {},
    confirmInvitation: {},
    deleteParticipant: {},
    deleteDebate: {},
    deletePost: {},
  },
  local: {
    themes: [],
    debates: [],
    viewNumber: {},
    notification: {},
    errors: {
      publishDebate: {},
      createDebate: {},
      addArgument: {},
      addComment: {},
      getThemes: {},
      sendVote: {},
      addTestimonial: {},
      getAllDebates: {},
      invite: {},
      promoteParticipant: {},
      deleteParticipant: {},
      confirmInvitation: {},
      deleteDebate: {},
      deletePost: {},
    },
    loading: {
      publishDebate: false,
      createDebate: false,
      getThemes: false,
      sendVote: false,
      addArgument: false,
      addComment: false,
      addTestimonial: false,
      getAllDebates: false,
      invite: false,
      promoteParticipant: false,
      deleteParticipant: false,
      confirmInvitation: false,
      deleteDebate: false,
      deletePost: false,
    },
  },
};

const debateReducer = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      /* CREATE DEBATE (DRAFT) */
      case constants.createDebate.request:
        draft.local.loading.createDebate = true;
        draft.data.createDebate = {};
        break;
      case constants.createDebate.failure:
        draft.local.loading.createDebate = false;
        draft.local.errors.createDebate = action.errors;
        break;
      case constants.createDebate.success:
        draft.local.errors.createDebate = {};
        draft.local.loading.createDebate = false;
        draft.data.createDebate = action.data;
        break;
      case constants.reinitializeCreateDebate:
        draft.data.createDebate = {};
        break;

      /* PUBLISH DEBATE  */
      case constants.publishDebate.request:
        draft.local.loading.publishDebate = true;
        draft.data.publishDebate = {};
        break;
      case constants.publishDebate.failure:
        draft.local.loading.publishDebate = false;
        draft.local.errors.publishDebate = action.errors;
        break;
      case constants.publishDebate.success:
        draft.local.errors.publishDebate = {};
        draft.local.loading.publishDebate = false;
        break;

      /* Update Views */
      case constants.updateViews:
        if (action.viewNumber > draft.local.viewNumber[action.id] || !draft.local.viewNumber[action.id]) {
          draft.local.viewNumber[action.id] = action.viewNumber;
        }
        break;

      /* GET THEMES */
      case constants.getThemes.request:
        draft.local.loading.getThemes = true;
        break;
      case constants.getThemes.failure:
        draft.local.loading.getThemes = false;
        draft.local.errors.getThemes = action.errors;
        break;
      case constants.getThemes.success:
        draft.local.themes = action.data;
        draft.local.errors.getThemes = {};
        draft.local.loading.getThemes = false;
        break;

      /* SEND VOTE */
      case constants.sendVote.request:
        draft.local.loading.sendVote = true;
        break;
      case constants.sendVote.failure:
        draft.local.loading.sendVote = false;
        draft.local.errors.sendVote = action.errors;
        break;
      case constants.sendVote.success:
        draft.local.errors.sendVote = {};
        draft.local.loading.sendVote = false;
        break;

      /* ADD ARGUMENT */
      case constants.addArgument.request:
        draft.local.loading.addArgument = true;
        draft.data.addArgument = {};
        break;
      case constants.addArgument.failure:
        draft.local.loading.addArgument = false;
        draft.local.errors.addArgument = action.errors;
        break;
      case constants.addArgument.success:
        draft.local.errors.addArgument = {};
        draft.data.addArgument = action.data;
        draft.local.loading.addArgument = false;
        gtag.event('new_argument', {
          debate: action.data.debate.id,
          debug_mode: true,
        });
        break;
      case constants.resetAddArgument:
        draft.data.addArgument = {};
        break;

      /* ADD TESTIMONIAL */
      case constants.addTestimonial.request:
        draft.local.loading.addTestimonial = true;
        draft.data.addTestimonial = {};
        break;
      case constants.addTestimonial.failure:
        draft.local.loading.addTestimonial = false;
        draft.local.errors.addTestimonial = action.errors;
        break;
      case constants.addTestimonial.success:
        draft.local.errors.addTestimonial = {};
        draft.data.addTestimonial = action.data;
        draft.local.loading.addTestimonial = false;
        gtag.event('new_testimonial', {
          debate: action.data.debate.id,
          debug_mode: true,
        });

        break;
      case constants.resetAddTestimonial:
        draft.data.addTestimonial = {};
        break;

      /* ADD COMMENT */
      case constants.addComment.request:
        draft.local.loading.addComment = true;
        draft.data.addComment = {};
        break;
      case constants.addComment.failure:
        draft.local.loading.addComment = false;
        draft.local.errors.addComment = action.errors;
        break;
      case constants.addComment.success:
        draft.local.errors.addComment = {};
        draft.data.addComment = action.data;
        draft.local.loading.addComment = false;
        gtag.event('new_comment', {
          debate: action.data.debate.id,
          debug_mode: true,
        });

        break;
      case constants.resetAddComment:
        draft.data.addComment = {};
        break;

      /* GET ALL DEBATES */
      case constants.getAllDebates.request:
        draft.local.loading.getAllDebates = true;
        break;
      case constants.getAllDebates.failure:
        draft.local.loading.getAllDebates = false;
        draft.local.errors.getAllDebates = action.errors;
        break;
      case constants.getAllDebates.success:
        draft.local.errors.getAllDebates = {};
        draft.local.debates = action.data;
        draft.local.loading.getAllDebates = false;
        break;

      /* INVITE NEW PARTICIPANT */
      case constants.invite.request:
        draft.local.loading.invite = true;
        draft.local.notification.invitation = false;
        break;
      case constants.invite.failure:
        draft.local.loading.invite = false;
        draft.local.errors.invite = action.errors;
        break;
      case constants.invite.success:
        draft.local.errors.invite = {};
        draft.local.loading.invite = false;
        draft.local.notification.invitation = true;
        break;

      case constants.resetNotification:
        draft.local.notification = {};
        break;

      /* CONFIRM INVITATION */
      case constants.confirmInvitation.request:
        draft.local.loading.confirmInvitation = true;
        draft.data.confirmInvitation = {};
        break;
      case constants.confirmInvitation.failure:
        draft.local.loading.confirmInvitation = false;
        draft.local.errors.confirmInvitation = action.errors;
        break;
      case constants.confirmInvitation.success:
        draft.local.errors.confirmInvitation = {};
        draft.local.loading.confirmInvitation = false;
        draft.data.confirmInvitation = action.data;
        break;

      /* PROMOTE PARTICIPANT */
      case constants.promoteParticipant.request:
        draft.local.loading.promoteParticipant = true;
        draft.local.errors.promoteParticipant = {};
        break;
      case constants.promoteParticipant.failure:
        draft.local.loading.promoteParticipant = false;
        draft.local.errors.promoteParticipant = action.errors;
        break;
      case constants.promoteParticipant.success:
        draft.local.errors.promoteParticipant = {};
        draft.local.loading.promoteParticipant = false;
        break;
      case constants.resetPromoteParticipant:
        draft.local.errors.promoteParticipant = {};
        break;

      /* DELETE PARTICIPANT */
      case constants.deleteParticipant.request:
        draft.local.loading.deleteParticipant = true;
        draft.local.errors.deleteParticipant = {};
        draft.data.deleteParticipant = {};
        break;
      case constants.deleteParticipant.failure:
        draft.local.loading.deleteParticipant = false;
        draft.local.errors.deleteParticipant = action.errors;
        break;
      case constants.deleteParticipant.success:
        draft.local.errors.deleteParticipant = {};
        draft.local.loading.deleteParticipant = false;
        draft.data.deleteParticipant = action.data;
        break;

      case constants.resetDeleteParticipant:
        draft.data.deleteParticipant = {};
        draft.local.errors.deleteParticipant = {};
        break;

      /* DELETE DEBATE */
      case constants.deleteDebate.request:
        draft.local.loading.deleteDebate = true;
        draft.local.errors.deleteDebate = {};
        break;
      case constants.deleteDebate.failure:
        draft.local.loading.deleteDebate = false;
        draft.local.errors.deleteDebate = action.errors;
        break;
      case constants.deleteDebate.success:
        draft.local.errors.deleteDebate = {};
        draft.local.loading.deleteDebate = false;
        draft.data.deleteDebate[action.id] = true;
        break;

      /* DELETE POST */
      case constants.deletePost.request:
        draft.local.loading.deletePost = true;
        draft.local.errors.deletePost = {};
        draft.data.deletePost = {};
        break;
      case constants.deletePost.failure:
        draft.local.loading.deletePost = false;
        draft.local.errors.deletePost = action.errors;
        break;
      case constants.deletePost.success:
        draft.local.errors.deletePost = {};
        draft.local.loading.deletePost = false;
        draft.data.deletePost[action.id] = true;
        break;
      default:
    }
  });
};
export default debateReducer;
